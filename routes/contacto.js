// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de contacto
var contacto_controller = require('../controllers/contacto');

// GET /:id
router.get('/:id', contacto_controller.contacto_details);
// GET /
router.get('/', contacto_controller.contacto_all);
// POST /
router.post('/', contacto_controller.contacto_create);
// PUT /:id
router.put('/:id', contacto_controller.contacto_update);
// DELETE /:id
router.delete('/:id', contacto_controller.contacto_delete);
// PUT tipocontacto 
router.put('/tipocontacto/:id',contacto_controller.Contacto_add_tipocontacto);

//export router
module.exports = router;