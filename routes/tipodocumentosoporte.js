// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de tipodocumentosoporte
var tipodocumentosoporte_controller = require('../controllers/tipodocumentosoporte');

// GET /:id
router.get('/:id', tipodocumentosoporte_controller.tipodocumentosoporte_details);
// GET /
router.get('/', tipodocumentosoporte_controller.tipodocumentosoporte_all);
// POST /
router.post('/', tipodocumentosoporte_controller.tipodocumentosoporte_create);
// PUT /:id
router.put('/:id', tipodocumentosoporte_controller.tipodocumentosoporte_update);
// DELETE /:id
router.delete('/:id', tipodocumentosoporte_controller.tipodocumentosoporte_delete);

//export router
module.exports = router;