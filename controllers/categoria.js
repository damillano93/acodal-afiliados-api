//importar modelo
var Categoria = require('../models/categoria');

//funcion create
exports.categoria_create = function (req, res, next) {
    var categoria = new Categoria(
        req.body
    );

    categoria.save(function (err, categoria) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: categoria._id})
    })
};
//funcion read by id
exports.categoria_details = function (req, res, next) {
    Categoria.findById(req.params.id, function (err, categoria) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(categoria);
    })
};
//funcion read all
exports.categoria_all = function (req, res, next) {
    Categoria.find(req.params.id, function (err, categoria) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(categoria);
    })
};
//funcion update
exports.categoria_update = function (req, res, next) {
    Categoria.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, categoria) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", categoria: categoria });
    });
};

//funcion delete
exports.categoria_delete = function (req, res, next) {
    Categoria.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};