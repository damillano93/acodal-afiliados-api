//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Aspirantejuridico
var AspirantejuridicoSchema = new Schema({
fecha: {type:String, required:true}, 
razonsocial: {type:String, required:true}, 
CIUU: {type:String, required:true}, 
Nit: {type:String, required:true}, 
direccion: {type:String, required:true}, 
telefono: {type:String, required:true}, 
email: {type:String, required:true}, 
ciudad:[ {type:Schema.Types.ObjectId, ref: 'Ciudad' , autopopulate: true }], 
servicios: {type:String, required:true}, 
nombregerente: {type:String, required:true}, 
emailgerente: {type:String, required:true}, 
celulargerente: {type:Number, required:true}, 
paginaweb: {type:String, required:true}, 
seccional:[ {type:Schema.Types.ObjectId, ref: 'Seccional' , autopopulate: true }], 
tipoempresa:[ {type:Schema.Types.ObjectId, ref: 'Tipoempresa' , autopopulate: true }], 
subafinidad:[ {type:Schema.Types.ObjectId, ref: 'Subafinidad' , autopopulate: true }],
abeasdata: {type:String, required:true}, 
lavadoactivos: {type:String, required:true}, 

});

AspirantejuridicoSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Aspirantejuridico', AspirantejuridicoSchema);