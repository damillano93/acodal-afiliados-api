//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Factura
var FacturaSchema = new Schema({
    afiliado: [ {type:Schema.Types.ObjectId, ref: 'Afiliado' , autopopulate: true }], 
    numero: { type: Number, required: true },
    v_pagado: { type: Number, required: true },
    fecha_pago: { type: String, required: true },
    rc: { type: Number, required: true },
    fecha:  { type: Date, default: Date.now },
    documento: [{ type: String, required: true }], 
    estado:  { type: String, required: true }, 
},

    {
        timestamps: true
    }


);

FacturaSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Factura', FacturaSchema);

