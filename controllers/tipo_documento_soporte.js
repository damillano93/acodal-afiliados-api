//importar modelo
var Tipo_documento_soporte = require('../models/tipo_documento_soporte');

//funcion create
exports.tipo_documento_soporte_create = function (req, res, next) {
    var tipo_documento_soporte = new Tipo_documento_soporte(
        req.body
    );

    tipo_documento_soporte.save(function (err, tipo_documento_soporte) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: tipo_documento_soporte._id})
    })
};
//funcion read by id
exports.tipo_documento_soporte_details = function (req, res, next) {
    Tipo_documento_soporte.findById(req.params.id, function (err, tipo_documento_soporte) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(tipo_documento_soporte);
    })
};
//funcion read all
exports.tipo_documento_soporte_all = function (req, res, next) {
    Tipo_documento_soporte.find(req.params.id, function (err, tipo_documento_soporte) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(tipo_documento_soporte);
    })
};
//funcion update
exports.tipo_documento_soporte_update = function (req, res, next) {
    Tipo_documento_soporte.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, tipo_documento_soporte) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", tipo_documento_soporte: tipo_documento_soporte });
    });
};

//funcion delete
exports.tipo_documento_soporte_delete = function (req, res, next) {
    Tipo_documento_soporte.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};