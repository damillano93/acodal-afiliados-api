// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de documento
var documento_controller = require('../controllers/documento');

// GET /:id
router.get('/:id', documento_controller.documento_details);
// GET /
router.get('/', documento_controller.documento_all);
// POST /
router.post('/', documento_controller.documento_create);
// PUT /:id
router.put('/:id', documento_controller.documento_update);
// DELETE /:id
router.delete('/:id', documento_controller.documento_delete);
// PUT tipodocumentosoporte 
router.put('/tipodocumentosoporte/:id',documento_controller.Documento_add_tipodocumentosoporte);

//export router
module.exports = router;