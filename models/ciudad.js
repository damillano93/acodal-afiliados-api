//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Ciudad
var CiudadSchema = new Schema({
    nombre: { type: String, required: true },
    descripcion: { type: String, required: true },
    abreviacion: { type: String, required: true },
   

},

    {
        timestamps: true
    }


);

CiudadSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Ciudad', CiudadSchema);