// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de aspirantejuridico
var aspirantejuridico_controller = require('../controllers/aspirantejuridico');

// GET /:id
router.get('/:id', aspirantejuridico_controller.aspirantejuridico_details);
// GET /
router.get('/', aspirantejuridico_controller.aspirantejuridico_all);
// POST /
router.post('/', aspirantejuridico_controller.aspirantejuridico_create);
// PUT /:id
router.put('/:id', aspirantejuridico_controller.aspirantejuridico_update);
// DELETE /:id
router.delete('/:id', aspirantejuridico_controller.aspirantejuridico_delete);
// PUT tipoempresa 
router.put('/tipoempresa/:id',aspirantejuridico_controller.Aspirantejuridico_add_tipoempresa);

//export router
module.exports = router;