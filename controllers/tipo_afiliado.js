//importar modelo
var Tipo_afiliado = require('../models/tipo_afiliado');

//funcion create
exports.tipo_afiliado_create = function (req, res, next) {
    var tipo_afiliado = new Tipo_afiliado(
        req.body
    );

    tipo_afiliado.save(function (err, tipo_afiliado) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: tipo_afiliado._id})
    })
};
//funcion read by id
exports.tipo_afiliado_details = function (req, res, next) {
    Tipo_afiliado.findById(req.params.id, function (err, tipo_afiliado) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(tipo_afiliado);
    })
};
//funcion read all
exports.tipo_afiliado_all = function (req, res, next) {
    Tipo_afiliado.find(req.params.id, function (err, tipo_afiliado) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(tipo_afiliado);
    })
};
//funcion update
exports.tipo_afiliado_update = function (req, res, next) {
    Tipo_afiliado.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, tipo_afiliado) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", tipo_afiliado: tipo_afiliado });
    });
};

//funcion delete
exports.tipo_afiliado_delete = function (req, res, next) {
    Tipo_afiliado.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};