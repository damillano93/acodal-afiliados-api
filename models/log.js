//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Log
var LogSchema = new Schema({
    user: [{ type: Schema.Types.ObjectId, ref: 'User', autopopulate: true , required: true}],
    accion: { type: String, required: true },
    fecha: { type: String, required: true },

},

    {
        timestamps: true
    }


);

LogSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Log', LogSchema);