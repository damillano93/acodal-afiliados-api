//importar modelo
var Aspirantenatural = require('../models/aspirantenatural');

//funcion create
exports.aspirantenatural_create = function (req, res, next) {
    var aspirantenatural = new Aspirantenatural(
        req.body
    );

    aspirantenatural.save(function (err, aspirantenatural) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: aspirantenatural._id})
    })
};
//funcion read by id
exports.aspirantenatural_details = function (req, res, next) {
    Aspirantenatural.findById(req.params.id, function (err, aspirantenatural) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(aspirantenatural);
    })
};
//funcion read all
exports.aspirantenatural_all = function (req, res, next) {
    Aspirantenatural.find(req.params.id, function (err, aspirantenatural) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(aspirantenatural);
    })
};
//funcion update
exports.aspirantenatural_update = function (req, res, next) {
    Aspirantenatural.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, aspirantenatural) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", aspirantenatural: aspirantenatural });
    });
};
//funcion add tipodocumento
exports.Aspirantenatural_add_tipodocumento = function (req, res, next) {
Aspirantenatural.findByIdAndUpdate(req.params.id, { $push: { tipodocumento: req.body.tipodocumento } }, { new: true }, function (err, Aspirantenatural) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Aspirantenatural: Aspirantenatural });
});
};
//funcion add ciudad
exports.Aspirantenatural_add_ciudad = function (req, res, next) {
Aspirantenatural.findByIdAndUpdate(req.params.id, { $push: { ciudad: req.body.ciudad } }, { new: true }, function (err, Aspirantenatural) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Aspirantenatural: Aspirantenatural });
});
};
//funcion add seccional
exports.Aspirantenatural_add_seccional = function (req, res, next) {
Aspirantenatural.findByIdAndUpdate(req.params.id, { $push: { seccional: req.body.seccional } }, { new: true }, function (err, Aspirantenatural) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Aspirantenatural: Aspirantenatural });
});
};

//funcion delete
exports.aspirantenatural_delete = function (req, res, next) {
    Aspirantenatural.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};