// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de estudio
var estudio_controller = require('../controllers/estudio');

// GET /:id
router.get('/:id', estudio_controller.estudio_details);
// GET /
router.get('/', estudio_controller.estudio_all);
// POST /
router.post('/', estudio_controller.estudio_create);
// PUT /:id
router.put('/:id', estudio_controller.estudio_update);
// DELETE /:id
router.delete('/:id', estudio_controller.estudio_delete);

//export router
module.exports = router;