// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de tipodocumento
var tipodocumento_controller = require('../controllers/tipodocumento');

// GET /:id
router.get('/:id', tipodocumento_controller.tipodocumento_details);
// GET /
router.get('/', tipodocumento_controller.tipodocumento_all);
// POST /
router.post('/', tipodocumento_controller.tipodocumento_create);
// PUT /:id
router.put('/:id', tipodocumento_controller.tipodocumento_update);
// DELETE /:id
router.delete('/:id', tipodocumento_controller.tipodocumento_delete);

//export router
module.exports = router;