// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de categoria
var categoria_controller = require('../controllers/categoria');

// GET /:id
router.get('/:id', categoria_controller.categoria_details);
// GET /
router.get('/', categoria_controller.categoria_all);
// POST /
router.post('/', categoria_controller.categoria_create);
// PUT /:id
router.put('/:id', categoria_controller.categoria_update);
// DELETE /:id
router.delete('/:id', categoria_controller.categoria_delete);

//export router
module.exports = router;