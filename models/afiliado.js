//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Afiliado
var AfiliadoSchema = new Schema({
    tipoafiliado: [{ type: Schema.Types.ObjectId, ref: 'Tipoafiliado', autopopulate: true }],
    categoria: [{ type: Schema.Types.ObjectId, ref: 'Categoria', autopopulate: true }],
    codigocliente: { type: String, required: true },
    nombres: { type: String, required: true },
    apellidos: { type: String, required: false },
    clasificacion: [{ type: Schema.Types.ObjectId, ref: 'Clasificacion', autopopulate: true }],
    tipodocumento: [{ type: Schema.Types.ObjectId, ref: 'Tipodocumento', autopopulate: true }],
    documento: { type: String, required: true },
    fechanacimiento: { type: String, required: false },
    ciiu: { type: String, required: false },
    seccional: [{ type: Schema.Types.ObjectId, ref: 'Seccional', autopopulate: true }],
    subafinidad: [{ type: Schema.Types.ObjectId, ref: 'Subafinidad', autopopulate: true }],
    pais: {type:String,required:false},
    ciudad: [{ type: Schema.Types.ObjectId, ref: 'Ciudad', autopopulate: true }],
    direccion: { type: String, required: true },
    telefono: { type: String, required: true },
    email: { type: String, required: true },
    sitioweb: { type: String, required: false },
    informaciondirectorio: { type: String, required: true },
    estudio: [{ type: Schema.Types.ObjectId, ref: 'Estudio', autopopulate: true , required: false}],
    docsoporte: [{ type: Schema.Types.ObjectId, ref: 'Documento', autopopulate: true , required: false}],
    user: [{ type: Schema.Types.ObjectId, ref: 'User', autopopulate: true , required: true}],
    representantelegal: {type: String, required: false},
    beneficiarios: [{ type: Object, required: false}],
    estado: {type: String, default: 'Activo', required:false},
    fechavinculacion: { type: String, required: false },
},

    {
        timestamps: true
    }
);

AfiliadoSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Afiliado', AfiliadoSchema);