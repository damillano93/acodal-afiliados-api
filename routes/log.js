// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de log
var log_controller = require('../controllers/log');

// GET /:id
router.get('/:id', log_controller.log_details);
// GET /
router.get('/', log_controller.log_all);
// POST /
router.post('/', log_controller.log_create);
// PUT /:id
router.put('/:id', log_controller.log_update);
// DELETE /:id
router.delete('/:id', log_controller.log_delete);

//export router
module.exports = router;