//importar modelo
var Tipoempresa = require('../models/tipoempresa');

//funcion create
exports.tipoempresa_create = function (req, res, next) {
    var tipoempresa = new Tipoempresa(
        req.body
    );

    tipoempresa.save(function (err, tipoempresa) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: tipoempresa._id})
    })
};
//funcion read by id
exports.tipoempresa_details = function (req, res, next) {
    Tipoempresa.findById(req.params.id, function (err, tipoempresa) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(tipoempresa);
    })
};
//funcion read all
exports.tipoempresa_all = function (req, res, next) {
    Tipoempresa.find(req.params.id, function (err, tipoempresa) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(tipoempresa);
    })
};
//funcion update
exports.tipoempresa_update = function (req, res, next) {
    Tipoempresa.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, tipoempresa) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", tipoempresa: tipoempresa });
    });
};

//funcion delete
exports.tipoempresa_delete = function (req, res, next) {
    Tipoempresa.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};