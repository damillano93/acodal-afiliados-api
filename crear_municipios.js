const axios = require('axios')
const fs = require('fs')

const timeout =  5000
const headers = {}


municipios = [
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Medellín'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Abejorral'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Abriaquí'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Alejandría'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Amagá'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Amalfi'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Andes'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Angelópolis'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Angostura'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Anorí'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Chimá'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Anza'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Apartadó'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Arboletes'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Argelia'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Armenia'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Barbosa'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Bello'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Betania'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Betulia'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Ciudad Bolívar'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Briceño'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Buriticá'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Cáceres'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Caicedo'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Caldas'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Campamento'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Cañasgordas'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Caracolí'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Caramanta'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Carepa'
  },
  {
    DEPARTAMENTO: 'Sucre',
    MUNICIPIO: 'Sampués'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Carolina'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Caucasia'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Chigorodó'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Cisneros'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Cocorná'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Concepción'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Concordia'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Copacabana'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Dabeiba'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Don Matías'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Ebéjico'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'El Bagre'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Entrerrios'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Envigado'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Fredonia'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Giraldo'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Girardota'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Gómez Plata'
  },
  {
    DEPARTAMENTO: 'Casanare',
    MUNICIPIO: 'Nunchía'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Guadalupe'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Guarne'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Guatapé'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Heliconia'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Hispania'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Itagui'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Ituango'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Pamplona'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Jericó'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'La Ceja'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'La Estrella'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'La Pintada'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'La Unión'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Liborina'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Maceo'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Marinilla'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Montebello'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Murindó'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Mutatá'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Nariño'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Necoclí'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Nechí'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Olaya'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Peñol'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Peque'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Pueblorrico'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Puerto Berrío'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Puerto Nare'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Puerto Triunfo'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Remedios'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Retiro'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Rionegro'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Sabanalarga'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Sabaneta'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Salgar'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Albán'
  },
  {
    DEPARTAMENTO: 'Vaupés',
    MUNICIPIO: 'Yavaraté'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'San Francisco'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'San Jerónimo'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'Montelíbano'
  },
  {
    DEPARTAMENTO: 'Putumayo',
    MUNICIPIO: 'Puerto Asís'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'San Luis'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'San Pedro'
  },
  {
    DEPARTAMENTO: 'Sucre',
    MUNICIPIO: 'Corozal'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'San Rafael'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'San Roque'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'San Vicente'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Santa Bárbara'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Buesaco'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Santo Domingo'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'El Santuario'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Segovia'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Sopetrán'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Támesis'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Tarazá'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Tarso'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Titiribí'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Toledo'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Turbo'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Uramita'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Urrao'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Valdivia'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Valparaíso'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Vegachí'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Venecia'
  },
  {
    DEPARTAMENTO: 'Casanare',
    MUNICIPIO: 'Maní'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Yalí'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Yarumal'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Yolombó'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Yondó'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Zaragoza'
  },
  {
    DEPARTAMENTO: 'Atlántico',
    MUNICIPIO: 'Barranquilla'
  },
  {
    DEPARTAMENTO: 'Atlántico',
    MUNICIPIO: 'Baranoa'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'El Peñón'
  },
  {
    DEPARTAMENTO: 'Atlántico',
    MUNICIPIO: 'Candelaria'
  },
  {
    DEPARTAMENTO: 'Atlántico',
    MUNICIPIO: 'Galapa'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Tuluá'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Casabianca'
  },
  {
    DEPARTAMENTO: 'Atlántico',
    MUNICIPIO: 'Luruaco'
  },
  {
    DEPARTAMENTO: 'Atlántico',
    MUNICIPIO: 'Malambo'
  },
  {
    DEPARTAMENTO: 'Atlántico',
    MUNICIPIO: 'Manatí'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Anolaima'
  },
  {
    DEPARTAMENTO: 'Atlántico',
    MUNICIPIO: 'Piojó'
  },
  {
    DEPARTAMENTO: 'Atlántico',
    MUNICIPIO: 'Polonuevo'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Chía'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'San Andrés de Tumaco'
  },
  {
    DEPARTAMENTO: 'Atlántico',
    MUNICIPIO: 'Sabanagrande'
  },
  {
    DEPARTAMENTO: 'Atlántico',
    MUNICIPIO: 'Sabanalarga'
  },
  {
    DEPARTAMENTO: 'Atlántico',
    MUNICIPIO: 'Santa Lucía'
  },
  {
    DEPARTAMENTO: 'Atlántico',
    MUNICIPIO: 'Santo Tomás'
  },
  {
    DEPARTAMENTO: 'Atlántico',
    MUNICIPIO: 'Soledad'
  },
  {
    DEPARTAMENTO: 'Atlántico',
    MUNICIPIO: 'Suan'
  },
  {
    DEPARTAMENTO: 'Atlántico',
    MUNICIPIO: 'Tubará'
  },
  {
    DEPARTAMENTO: 'Atlántico',
    MUNICIPIO: 'Usiacurí'
  },
  {
    DEPARTAMENTO: 'Caquetá',
    MUNICIPIO: 'Milán'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Capitanejo'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Achí'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Anzoátegui'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Arenal'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Arjona'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Arroyohondo'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Florida'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Calamar'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Cantagallo'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Cicuco'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Córdoba'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Clemencia'
  },
  {
    DEPARTAMENTO: 'Atlántico',
    MUNICIPIO: 'Repelón'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'El Guamo'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Frontino'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Magangué'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Mahates'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Margarita'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Montecristo'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Mompós'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Morales'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Norosí'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Pinillos'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Regidor'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Río Viejo'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'San Estanislao'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'San Fernando'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'El Peñón'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Pamplonita'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'San Juan Nepomuceno'
  },
  {
    DEPARTAMENTO: 'Amazonas',
    MUNICIPIO: 'Miriti Paraná'
  },
  {
    DEPARTAMENTO: 'Casanare',
    MUNICIPIO: 'Támara'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Santa Catalina'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Santa Rosa'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Tibasosa'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Simití'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Soplaviento'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Talaigua Nuevo'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Tiquisio'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Turbaco'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Turbaná'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Villanueva'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Páez'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Tunja'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Almeida'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Aquitania'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Arcabuco'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Ibagué'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Berbeo'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Betéitiva'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Boavita'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Boyacá'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Briceño'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Buena Vista'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Busbanzá'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Caldas'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Campohermoso'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Cerinza'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Chinavita'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Chiquinquirá'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Chiscas'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Chita'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Chitaraque'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Chivatá'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Cómbita'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Coper'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Corrales'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Covarachía'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Cubará'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Cucaita'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Cuítiva'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Chíquiza'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Chivor'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Duitama'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'El Cocuy'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'El Espino'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Firavitoba'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Floresta'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Gachantivá'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Gameza'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Garagoa'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Guacamayas'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Guateque'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Guayatá'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Güicán'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Iza'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Jenesano'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Jericó'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Labranzagrande'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'La Capilla'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'La Victoria'
  },
  {
    DEPARTAMENTO: 'Atlántico',
    MUNICIPIO: 'Puerto Colombia'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Belén'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Macanal'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Maripí'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Miraflores'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Mongua'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Monguí'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Moniquirá'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Muzo'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Nobsa'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Nuevo Colón'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Oicatá'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Otanche'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Pachavita'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Páez'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Paipa'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Pajarito'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Panqueba'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Pauna'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Paya'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Sopó'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Pesca'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Pisba'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Puerto Boyacá'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Quípama'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Ramiriquí'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Ráquira'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Rondón'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Saboyá'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Sáchica'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Samacá'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'San Eduardo'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Carmen del Darien'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Gama'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'San Mateo'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Sasaima'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Chachagüí'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Santana'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Santa María'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Cúcuta'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Santa Sofía'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Sativanorte'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Sativasur'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Siachoque'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Soatá'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Socotá'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Socha'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Sogamoso'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Somondoco'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Sora'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Sotaquirá'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Soracá'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Susacón'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Sutamarchán'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Sutatenza'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Tasco'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Tenza'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Tibaná'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Tinjacá'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Tipacoque'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Toca'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Cartagena'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Tópaga'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Tota'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Turmequé'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Granada'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Tutazá'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Umbita'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Ventaquemada'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Viracachá'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Zetaquira'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'Manizales'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'Aguadas'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'Anserma'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'Aranzazu'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'Belalcázar'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'Chinchiná'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'Filadelfia'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'La Dorada'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'La Merced'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'Manzanares'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'Marmato'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'Marulanda'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'Neira'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'Norcasia'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'Pácora'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'Palestina'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'Pensilvania'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'Riosucio'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'Risaralda'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'Salamina'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'Samaná'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'San José'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'Supía'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'Victoria'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'Villamaría'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'Viterbo'
  },
  {
    DEPARTAMENTO: 'Caquetá',
    MUNICIPIO: 'Florencia'
  },
  {
    DEPARTAMENTO: 'Caquetá',
    MUNICIPIO: 'Albania'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'Santa Bárbara de Pinto'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'María la Baja'
  },
  {
    DEPARTAMENTO: 'Caquetá',
    MUNICIPIO: 'Curillo'
  },
  {
    DEPARTAMENTO: 'Caquetá',
    MUNICIPIO: 'El Doncello'
  },
  {
    DEPARTAMENTO: 'Caquetá',
    MUNICIPIO: 'El Paujil'
  },
  {
    DEPARTAMENTO: 'Caquetá',
    MUNICIPIO: 'Morelia'
  },
  {
    DEPARTAMENTO: 'Caquetá',
    MUNICIPIO: 'Puerto Rico'
  },
  {
    DEPARTAMENTO: 'Caquetá',
    MUNICIPIO: 'La Montañita'
  },
  {
    DEPARTAMENTO: 'Caquetá',
    MUNICIPIO: 'San Vicente del Caguán'
  },
  {
    DEPARTAMENTO: 'Caquetá',
    MUNICIPIO: 'Solano'
  },
  {
    DEPARTAMENTO: 'Caquetá',
    MUNICIPIO: 'Solita'
  },
  {
    DEPARTAMENTO: 'Caquetá',
    MUNICIPIO: 'Valparaíso'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Popayán'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Almaguer'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Argelia'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Balboa'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Bolívar'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Buenos Aires'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Cajibío'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Caldono'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Caloto'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Corinto'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'El Tambo'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Florencia'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Guachené'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Guapi'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Inzá'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Jambaló'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'La Sierra'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'La Vega'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'López'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Mercaderes'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Miranda'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Morales'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Padilla'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Patía'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Piamonte'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Piendamó'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Puerto Tejada'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Puracé'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Rosas'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'El Peñón'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Jardín'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Santa Rosa'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Silvia'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Sotara'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Suárez'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Sucre'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Timbío'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Timbiquí'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Toribio'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Totoró'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Villa Rica'
  },
  {
    DEPARTAMENTO: 'Cesar',
    MUNICIPIO: 'Valledupar'
  },
  {
    DEPARTAMENTO: 'Cesar',
    MUNICIPIO: 'Aguachica'
  },
  {
    DEPARTAMENTO: 'Cesar',
    MUNICIPIO: 'Agustín Codazzi'
  },
  {
    DEPARTAMENTO: 'Cesar',
    MUNICIPIO: 'Astrea'
  },
  {
    DEPARTAMENTO: 'Cesar',
    MUNICIPIO: 'Becerril'
  },
  {
    DEPARTAMENTO: 'Cesar',
    MUNICIPIO: 'Bosconia'
  },
  {
    DEPARTAMENTO: 'Cesar',
    MUNICIPIO: 'Chimichagua'
  },
  {
    DEPARTAMENTO: 'Cesar',
    MUNICIPIO: 'Chiriguaná'
  },
  {
    DEPARTAMENTO: 'Cesar',
    MUNICIPIO: 'Curumaní'
  },
  {
    DEPARTAMENTO: 'Cesar',
    MUNICIPIO: 'El Copey'
  },
  {
    DEPARTAMENTO: 'Cesar',
    MUNICIPIO: 'El Paso'
  },
  {
    DEPARTAMENTO: 'Cesar',
    MUNICIPIO: 'Gamarra'
  },
  {
    DEPARTAMENTO: 'Cesar',
    MUNICIPIO: 'González'
  },
  {
    DEPARTAMENTO: 'Cesar',
    MUNICIPIO: 'La Gloria'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Jamundí'
  },
  {
    DEPARTAMENTO: 'Cesar',
    MUNICIPIO: 'Manaure'
  },
  {
    DEPARTAMENTO: 'Cesar',
    MUNICIPIO: 'Pailitas'
  },
  {
    DEPARTAMENTO: 'Cesar',
    MUNICIPIO: 'Pelaya'
  },
  {
    DEPARTAMENTO: 'Cesar',
    MUNICIPIO: 'Pueblo Bello'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Tadó'
  },
  {
    DEPARTAMENTO: 'Cesar',
    MUNICIPIO: 'La Paz'
  },
  {
    DEPARTAMENTO: 'Cesar',
    MUNICIPIO: 'San Alberto'
  },
  {
    DEPARTAMENTO: 'Cesar',
    MUNICIPIO: 'San Diego'
  },
  {
    DEPARTAMENTO: 'Cesar',
    MUNICIPIO: 'San Martín'
  },
  {
    DEPARTAMENTO: 'Cesar',
    MUNICIPIO: 'Tamalameque'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'Montería'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'Ayapel'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'Buenavista'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'Canalete'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'Cereté'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'Chimá'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'Chinú'
  },
  {
    DEPARTAMENTO: 'Casanare',
    MUNICIPIO: 'Orocué'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'Cotorra'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Líbano'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'Lorica'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'Los Córdobas'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'Momil'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'Moñitos'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'Planeta Rica'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'Pueblo Nuevo'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'Puerto Escondido'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Yacopí'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'Purísima'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'Sahagún'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'San Andrés Sotavento'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'San Antero'
  },
  {
    DEPARTAMENTO: 'Quindío',
    MUNICIPIO: 'Calarcá'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Sonsón'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'El Carmen'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'San Pelayo'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'Tierralta'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'Tuchín'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'Valencia'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Lérida'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Anapoima'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Arbeláez'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Beltrán'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Bituima'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Bojacá'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Cabrera'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Cachipay'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Cajicá'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Caparrapí'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Caqueza'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'La Apartada'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Chaguaní'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Chipaque'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Choachí'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Chocontá'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Cogua'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Cota'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Cucunubá'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'El Colegio'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'El Rosal'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Fomeque'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Fosca'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Funza'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Fúquene'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Gachala'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Gachancipá'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Gachetá'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'San Cristóbal'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Girardot'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Granada'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Guachetá'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Guaduas'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Guasca'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Guataquí'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Guatavita'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Fusagasugá'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Guayabetal'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Gutiérrez'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Jerusalén'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Junín'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'La Calera'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'La Mesa'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'La Palma'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'La Peña'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'La Vega'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Lenguazaque'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Macheta'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Madrid'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Manta'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Medina'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Mosquera'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Nariño'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Nemocón'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Nilo'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Nimaima'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Nocaima'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Venecia'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Pacho'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Paime'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Pandi'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Paratebueno'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Pasca'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Puerto Salgar'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Pulí'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Quebradanegra'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Quetame'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Quipile'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Apulo'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Ricaurte'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Zambrano'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'San Bernardo'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'San Cayetano'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'San Francisco'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'La Uvita'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Zipaquirá'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Sesquilé'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Sibaté'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Silvania'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Simijaca'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Soacha'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Subachoque'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Suesca'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Supatá'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Susa'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Sutatausa'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Tabio'
  },
  {
    DEPARTAMENTO: 'Quindío',
    MUNICIPIO: 'Génova'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Tausa'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Tena'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Tenjo'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Tibacuy'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Tibirita'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Tocaima'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Tocancipá'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Topaipí'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Ubalá'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Ubaque'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Suárez'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Une'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Útica'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'Castilla la Nueva'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Vianí'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Villagómez'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Villapinzón'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Villeta'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Viotá'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Zipacón'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Quibdó'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Acandí'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Alto Baudo'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Atrato'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Bagadó'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Bahía Solano'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Bajo Baudó'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Belén'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Bojaya'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Unión Panamericana'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'Pueblo Viejo'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Cértegui'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Condoto'
  },
  {
    DEPARTAMENTO: 'Putumayo',
    MUNICIPIO: 'Villagarzón'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Facatativá'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Juradó'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Lloró'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Medio Atrato'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Medio Baudó'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Medio San Juan'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Nóvita'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Nuquí'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Río Iro'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Río Quito'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Riosucio'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'Puerto Libertador'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Sipí'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Unguía'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Neiva'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Acevedo'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Agrado'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Aipe'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Algeciras'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Altamira'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Baraya'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Campoalegre'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Colombia'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Elías'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Garzón'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Gigante'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Guadalupe'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Hobo'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Iquira'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Isnos'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'La Argentina'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'La Plata'
  },
  {
    DEPARTAMENTO: 'Caldas',
    MUNICIPIO: 'Marquetalia'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Nátaga'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Oporapa'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Paicol'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Palermo'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Palestina'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Pital'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Pitalito'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Rivera'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Saladoblanco'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Arboleda'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Santa María'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Suaza'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Tarqui'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Tesalia'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Tello'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Teruel'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Timaná'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Villavieja'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'Yaguará'
  },
  {
    DEPARTAMENTO: 'La Guajira',
    MUNICIPIO: 'Riohacha'
  },
  {
    DEPARTAMENTO: 'La Guajira',
    MUNICIPIO: 'Albania'
  },
  {
    DEPARTAMENTO: 'La Guajira',
    MUNICIPIO: 'Barrancas'
  },
  {
    DEPARTAMENTO: 'La Guajira',
    MUNICIPIO: 'Dibula'
  },
  {
    DEPARTAMENTO: 'La Guajira',
    MUNICIPIO: 'Distracción'
  },
  {
    DEPARTAMENTO: 'La Guajira',
    MUNICIPIO: 'El Molino'
  },
  {
    DEPARTAMENTO: 'La Guajira',
    MUNICIPIO: 'Fonseca'
  },
  {
    DEPARTAMENTO: 'La Guajira',
    MUNICIPIO: 'Hatonuevo'
  },
  {
    DEPARTAMENTO: 'La Guajira',
    MUNICIPIO: 'Maicao'
  },
  {
    DEPARTAMENTO: 'La Guajira',
    MUNICIPIO: 'Manaure'
  },
  {
    DEPARTAMENTO: 'La Guajira',
    MUNICIPIO: 'Uribia'
  },
  {
    DEPARTAMENTO: 'La Guajira',
    MUNICIPIO: 'Urumita'
  },
  {
    DEPARTAMENTO: 'La Guajira',
    MUNICIPIO: 'Villanueva'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'Santa Marta'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'Algarrobo'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'Aracataca'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'Ariguaní'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'Cerro San Antonio'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'Chivolo'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'Concordia'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'El Banco'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'El Piñon'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'El Retén'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'Fundación'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'Guamal'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'Nueva Granada'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'Pedraza'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'Pivijay'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'Plato'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'Remolino'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'Salamina'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'San Zenón'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'Santa Ana'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'Sitionuevo'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'Tenerife'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'Zapayán'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'Zona Bananera'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'Villavicencio'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'Acacias'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'Cabuyaro'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'Cubarral'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'Cumaral'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'El Calvario'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'El Castillo'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'El Dorado'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Buenaventura'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'Granada'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'Guamal'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'Mapiripán'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'Mesetas'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'La Macarena'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'Uribe'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'Lejanías'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'Puerto Concordia'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'Puerto Gaitán'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'Puerto López'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'Puerto Lleras'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'Puerto Rico'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'Restrepo'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'Ciénaga'
  },
  {
    DEPARTAMENTO: 'Atlántico',
    MUNICIPIO: 'Ponedera'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'San Juanito'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'San Martín'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'Vista Hermosa'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Pasto'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Albán'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Aldana'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Ancuyá'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Tununguá'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Barbacoas'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Motavita'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'San Bernardo del Viento'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Colón'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Consaca'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Contadero'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Córdoba'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Cuaspud'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Cumbal'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Cumbitara'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'El Charco'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'El Peñol'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'El Rosario'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Istmina'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'El Tambo'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Funes'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Guachucal'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Guaitarilla'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Gualmatán'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Iles'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Imués'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Ipiales'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'La Cruz'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'La Florida'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'La Llanada'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'La Tola'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'La Unión'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Leiva'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Linares'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Los Andes'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Magüí'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Mallama'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Mosquera'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Nariño'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Olaya Herrera'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Ospina'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Francisco Pizarro'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Policarpa'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Potosí'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Providencia'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Puerres'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Pupiales'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Ricaurte'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Roberto Payán'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Samaniego'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Sandoná'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'San Bernardo'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'San Lorenzo'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'San Pablo'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Belmira'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Ciénega'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Santa Bárbara'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Sapuyes'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Taminango'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Tangua'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Santacruz'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Túquerres'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'Yacuanquer'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Puerto Wilches'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Puerto Parra'
  },
  {
    DEPARTAMENTO: 'Quindío',
    MUNICIPIO: 'Armenia'
  },
  {
    DEPARTAMENTO: 'Quindío',
    MUNICIPIO: 'Buenavista'
  },
  {
    DEPARTAMENTO: 'Quindío',
    MUNICIPIO: 'Circasia'
  },
  {
    DEPARTAMENTO: 'Quindío',
    MUNICIPIO: 'Córdoba'
  },
  {
    DEPARTAMENTO: 'Quindío',
    MUNICIPIO: 'Filandia'
  },
  {
    DEPARTAMENTO: 'Quindío',
    MUNICIPIO: 'La Tebaida'
  },
  {
    DEPARTAMENTO: 'Quindío',
    MUNICIPIO: 'Montenegro'
  },
  {
    DEPARTAMENTO: 'Quindío',
    MUNICIPIO: 'Pijao'
  },
  {
    DEPARTAMENTO: 'Quindío',
    MUNICIPIO: 'Quimbaya'
  },
  {
    DEPARTAMENTO: 'Quindío',
    MUNICIPIO: 'Salento'
  },
  {
    DEPARTAMENTO: 'Risaralda',
    MUNICIPIO: 'Pereira'
  },
  {
    DEPARTAMENTO: 'Risaralda',
    MUNICIPIO: 'Apía'
  },
  {
    DEPARTAMENTO: 'Risaralda',
    MUNICIPIO: 'Balboa'
  },
  {
    DEPARTAMENTO: 'Risaralda',
    MUNICIPIO: 'Dosquebradas'
  },
  {
    DEPARTAMENTO: 'Risaralda',
    MUNICIPIO: 'Guática'
  },
  {
    DEPARTAMENTO: 'Risaralda',
    MUNICIPIO: 'La Celia'
  },
  {
    DEPARTAMENTO: 'Risaralda',
    MUNICIPIO: 'La Virginia'
  },
  {
    DEPARTAMENTO: 'Risaralda',
    MUNICIPIO: 'Marsella'
  },
  {
    DEPARTAMENTO: 'Risaralda',
    MUNICIPIO: 'Mistrató'
  },
  {
    DEPARTAMENTO: 'Risaralda',
    MUNICIPIO: 'Pueblo Rico'
  },
  {
    DEPARTAMENTO: 'Risaralda',
    MUNICIPIO: 'Quinchía'
  },
  {
    DEPARTAMENTO: 'Risaralda',
    MUNICIPIO: 'Santuario'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Bucaramanga'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Aguada'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Albania'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Aratoca'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Barbosa'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Barichara'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Barrancabermeja'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Betulia'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Bolívar'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Cabrera'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'California'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Carcasí'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Cepitá'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Cerrito'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Charalá'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Charta'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Chipatá'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Cimitarra'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Concepción'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Confines'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Contratación'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Coromoro'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Curití'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'El Guacamayo'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'El Playón'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Encino'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Enciso'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Florián'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Floridablanca'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Galán'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Gambita'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Girón'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Guaca'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Guadalupe'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Guapotá'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Guavatá'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Güepsa'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Jesús María'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Jordán'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'La Belleza'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Landázuri'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'La Paz'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Lebríja'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Los Santos'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Macaravita'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Málaga'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Matanza'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Mogotes'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Molagavita'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Ocamonte'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Oiba'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Onzaga'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Palmar'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Páramo'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Piedecuesta'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Pinchote'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Puente Nacional'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Rionegro'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'San Andrés'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'San Gil'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'San Joaquín'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'San Miguel'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Santa Bárbara'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Simacota'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Socorro'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Suaita'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Sucre'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Suratá'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Tona'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Vélez'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Vetas'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Villanueva'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Zapatoca'
  },
  {
    DEPARTAMENTO: 'Sucre',
    MUNICIPIO: 'Sincelejo'
  },
  {
    DEPARTAMENTO: 'Sucre',
    MUNICIPIO: 'Buenavista'
  },
  {
    DEPARTAMENTO: 'Sucre',
    MUNICIPIO: 'Caimito'
  },
  {
    DEPARTAMENTO: 'Sucre',
    MUNICIPIO: 'Coloso'
  },
  {
    DEPARTAMENTO: 'Sucre',
    MUNICIPIO: 'Coveñas'
  },
  {
    DEPARTAMENTO: 'Sucre',
    MUNICIPIO: 'Chalán'
  },
  {
    DEPARTAMENTO: 'Sucre',
    MUNICIPIO: 'El Roble'
  },
  {
    DEPARTAMENTO: 'Sucre',
    MUNICIPIO: 'Galeras'
  },
  {
    DEPARTAMENTO: 'Sucre',
    MUNICIPIO: 'Guaranda'
  },
  {
    DEPARTAMENTO: 'Sucre',
    MUNICIPIO: 'La Unión'
  },
  {
    DEPARTAMENTO: 'Sucre',
    MUNICIPIO: 'Los Palmitos'
  },
  {
    DEPARTAMENTO: 'Sucre',
    MUNICIPIO: 'Majagual'
  },
  {
    DEPARTAMENTO: 'Sucre',
    MUNICIPIO: 'Morroa'
  },
  {
    DEPARTAMENTO: 'Sucre',
    MUNICIPIO: 'Ovejas'
  },
  {
    DEPARTAMENTO: 'Sucre',
    MUNICIPIO: 'Palmito'
  },
  {
    DEPARTAMENTO: 'Sucre',
    MUNICIPIO: 'San Benito Abad'
  },
  {
    DEPARTAMENTO: 'Sucre',
    MUNICIPIO: 'San Marcos'
  },
  {
    DEPARTAMENTO: 'Sucre',
    MUNICIPIO: 'San Onofre'
  },
  {
    DEPARTAMENTO: 'Sucre',
    MUNICIPIO: 'San Pedro'
  },
  {
    DEPARTAMENTO: 'Sucre',
    MUNICIPIO: 'Sucre'
  },
  {
    DEPARTAMENTO: 'Sucre',
    MUNICIPIO: 'Tolú Viejo'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Alpujarra'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Alvarado'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Ambalema'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Armero'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Ataco'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Cajamarca'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Chaparral'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Coello'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Coyaima'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Cunday'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Dolores'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Espinal'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Falan'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Flandes'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Fresno'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Guamo'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Herveo'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Honda'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Icononzo'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Mariquita'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Melgar'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Murillo'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Natagaima'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Ortega'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Palocabildo'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Piedras'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Planadas'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Prado'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Purificación'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Rio Blanco'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Roncesvalles'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Rovira'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Saldaña'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Santa Isabel'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Venadillo'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Villahermosa'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Villarrica'
  },
  {
    DEPARTAMENTO: 'Arauca',
    MUNICIPIO: 'Arauquita'
  },
  {
    DEPARTAMENTO: 'Arauca',
    MUNICIPIO: 'Cravo Norte'
  },
  {
    DEPARTAMENTO: 'Arauca',
    MUNICIPIO: 'Fortul'
  },
  {
    DEPARTAMENTO: 'Arauca',
    MUNICIPIO: 'Puerto Rondón'
  },
  {
    DEPARTAMENTO: 'Arauca',
    MUNICIPIO: 'Saravena'
  },
  {
    DEPARTAMENTO: 'Arauca',
    MUNICIPIO: 'Tame'
  },
  {
    DEPARTAMENTO: 'Arauca',
    MUNICIPIO: 'Arauca'
  },
  {
    DEPARTAMENTO: 'Casanare',
    MUNICIPIO: 'Yopal'
  },
  {
    DEPARTAMENTO: 'Casanare',
    MUNICIPIO: 'Aguazul'
  },
  {
    DEPARTAMENTO: 'Casanare',
    MUNICIPIO: 'Chámeza'
  },
  {
    DEPARTAMENTO: 'Casanare',
    MUNICIPIO: 'Hato Corozal'
  },
  {
    DEPARTAMENTO: 'Casanare',
    MUNICIPIO: 'La Salina'
  },
  {
    DEPARTAMENTO: 'Casanare',
    MUNICIPIO: 'Monterrey'
  },
  {
    DEPARTAMENTO: 'Casanare',
    MUNICIPIO: 'Pore'
  },
  {
    DEPARTAMENTO: 'Casanare',
    MUNICIPIO: 'Recetor'
  },
  {
    DEPARTAMENTO: 'Casanare',
    MUNICIPIO: 'Sabanalarga'
  },
  {
    DEPARTAMENTO: 'Casanare',
    MUNICIPIO: 'Sácama'
  },
  {
    DEPARTAMENTO: 'Casanare',
    MUNICIPIO: 'Tauramena'
  },
  {
    DEPARTAMENTO: 'Casanare',
    MUNICIPIO: 'Trinidad'
  },
  {
    DEPARTAMENTO: 'Casanare',
    MUNICIPIO: 'Villanueva'
  },
  {
    DEPARTAMENTO: 'Putumayo',
    MUNICIPIO: 'Mocoa'
  },
  {
    DEPARTAMENTO: 'Putumayo',
    MUNICIPIO: 'Colón'
  },
  {
    DEPARTAMENTO: 'Putumayo',
    MUNICIPIO: 'Orito'
  },
  {
    DEPARTAMENTO: 'Putumayo',
    MUNICIPIO: 'Puerto Caicedo'
  },
  {
    DEPARTAMENTO: 'Putumayo',
    MUNICIPIO: 'Puerto Guzmán'
  },
  {
    DEPARTAMENTO: 'Putumayo',
    MUNICIPIO: 'Leguízamo'
  },
  {
    DEPARTAMENTO: 'Putumayo',
    MUNICIPIO: 'Sibundoy'
  },
  {
    DEPARTAMENTO: 'Putumayo',
    MUNICIPIO: 'San Francisco'
  },
  {
    DEPARTAMENTO: 'Putumayo',
    MUNICIPIO: 'San Miguel'
  },
  {
    DEPARTAMENTO: 'Putumayo',
    MUNICIPIO: 'Santiago'
  },
  {
    DEPARTAMENTO: 'Amazonas',
    MUNICIPIO: 'Leticia'
  },
  {
    DEPARTAMENTO: 'Amazonas',
    MUNICIPIO: 'El Encanto'
  },
  {
    DEPARTAMENTO: 'Amazonas',
    MUNICIPIO: 'La Chorrera'
  },
  {
    DEPARTAMENTO: 'Amazonas',
    MUNICIPIO: 'La Pedrera'
  },
  {
    DEPARTAMENTO: 'Amazonas',
    MUNICIPIO: 'La Victoria'
  },
  {
    DEPARTAMENTO: 'Amazonas',
    MUNICIPIO: 'Puerto Arica'
  },
  {
    DEPARTAMENTO: 'Amazonas',
    MUNICIPIO: 'Puerto Nariño'
  },
  {
    DEPARTAMENTO: 'Amazonas',
    MUNICIPIO: 'Puerto Santander'
  },
  {
    DEPARTAMENTO: 'Amazonas',
    MUNICIPIO: 'Tarapacá'
  },
  {
    DEPARTAMENTO: 'Guainía',
    MUNICIPIO: 'Inírida'
  },
  {
    DEPARTAMENTO: 'Guainía',
    MUNICIPIO: 'Barranco Minas'
  },
  {
    DEPARTAMENTO: 'Guainía',
    MUNICIPIO: 'Mapiripana'
  },
  {
    DEPARTAMENTO: 'Guainía',
    MUNICIPIO: 'San Felipe'
  },
  {
    DEPARTAMENTO: 'Guainía',
    MUNICIPIO: 'Puerto Colombia'
  },
  {
    DEPARTAMENTO: 'Guainía',
    MUNICIPIO: 'La Guadalupe'
  },
  {
    DEPARTAMENTO: 'Guainía',
    MUNICIPIO: 'Cacahual'
  },
  {
    DEPARTAMENTO: 'Guainía',
    MUNICIPIO: 'Pana Pana'
  },
  {
    DEPARTAMENTO: 'Guainía',
    MUNICIPIO: 'Morichal'
  },
  {
    DEPARTAMENTO: 'Vaupés',
    MUNICIPIO: 'Mitú'
  },
  {
    DEPARTAMENTO: 'Vaupés',
    MUNICIPIO: 'Caruru'
  },
  {
    DEPARTAMENTO: 'Vaupés',
    MUNICIPIO: 'Pacoa'
  },
  {
    DEPARTAMENTO: 'Vaupés',
    MUNICIPIO: 'Taraira'
  },
  {
    DEPARTAMENTO: 'Vaupés',
    MUNICIPIO: 'Papunaua'
  },
  {
    DEPARTAMENTO: 'Vichada',
    MUNICIPIO: 'Puerto Carreño'
  },
  {
    DEPARTAMENTO: 'Vichada',
    MUNICIPIO: 'La Primavera'
  },
  {
    DEPARTAMENTO: 'Vichada',
    MUNICIPIO: 'Santa Rosalía'
  },
  {
    DEPARTAMENTO: 'Vichada',
    MUNICIPIO: 'Cumaribo'
  },
  {
    DEPARTAMENTO: 'Caquetá',
    MUNICIPIO: 'San José del Fragua'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'Barranca de Upía'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Palmas del Socorro'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'San Juan de Río Seco'
  },
  {
    DEPARTAMENTO: 'Atlántico',
    MUNICIPIO: 'Juan de Acosta'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'Fuente de Oro'
  },
  {
    DEPARTAMENTO: 'Casanare',
    MUNICIPIO: 'San Luis de Gaceno'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'El Litoral del San Juan'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Villa de San Diego de Ubate'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Barranco de Loba'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Togüí'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Santa Rosa del Sur'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'El Cantón del San Pablo'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Villa de Leyva'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'San Sebastián de Buenavista'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Paz de Río'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Hatillo de Loba'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'Sabanas de San Angel'
  },
  {
    DEPARTAMENTO: 'Guaviare',
    MUNICIPIO: 'Calamar'
  },
  {
    DEPARTAMENTO: 'Cesar',
    MUNICIPIO: 'Río de Oro'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'San Pedro de Uraba'
  },
  {
    DEPARTAMENTO: 'Guaviare',
    MUNICIPIO: 'San José del Guaviare'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Santa Rosa de Viterbo'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'Santander de Quilichao'
  },
  {
    DEPARTAMENTO: 'Guaviare',
    MUNICIPIO: 'Miraflores'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Santafé de Antioquia'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'San Carlos de Guaroa'
  },
  {
    DEPARTAMENTO: 'Atlántico',
    MUNICIPIO: 'Palmar de Varela'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Santa Rosa de Osos'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'San Andrés de Cuerquía'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Valle de San Juan'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'San Vicente de Chucurí'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'San José de Miranda'
  },
  {
    DEPARTAMENTO: 'Archipiélago de San Andrés, Providencia y Santa Catalina',
    MUNICIPIO: 'Providencia'
  },
  {
    DEPARTAMENTO: 'Risaralda',
    MUNICIPIO: 'Santa Rosa de Cabal'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Guayabal de Siquima'
  },
  {
    DEPARTAMENTO: 'Caquetá',
    MUNICIPIO: 'Belén de Los Andaquies'
  },
  {
    DEPARTAMENTO: 'Casanare',
    MUNICIPIO: 'Paz de Ariporo'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Santa Helena del Opón'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'San Pablo de Borbur'
  },
  {
    DEPARTAMENTO: 'La Guajira',
    MUNICIPIO: 'La Jagua del Pilar'
  },
  {
    DEPARTAMENTO: 'Cesar',
    MUNICIPIO: 'La Jagua de Ibirico'
  },
  {
    DEPARTAMENTO: 'Sucre',
    MUNICIPIO: 'San Luis de Sincé'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'San Luis de Gaceno'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'El Carmen de Bolívar'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'El Carmen de Atrato'
  },
  {
    DEPARTAMENTO: 'Sucre',
    MUNICIPIO: 'San Juan de Betulia'
  },
  {
    DEPARTAMENTO: 'Magdalena',
    MUNICIPIO: 'Pijiño del Carmen'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'Vigía del Fuerte'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'San Martín de Loba'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'Altos del Rosario'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'Carmen de Apicala'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'San Antonio del Tequendama'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Sabana de Torres'
  },
  {
    DEPARTAMENTO: 'Guaviare',
    MUNICIPIO: 'El Retorno'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'San José de Uré'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'San Pedro de Cartago'
  },
  {
    DEPARTAMENTO: 'Atlántico',
    MUNICIPIO: 'Campo de La Cruz'
  },
  {
    DEPARTAMENTO: 'Meta',
    MUNICIPIO: 'San Juan de Arama'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'San José de La Montaña'
  },
  {
    DEPARTAMENTO: 'Caquetá',
    MUNICIPIO: 'Cartagena del Chairá'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'San José del Palmar'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Agua de Dios'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'San Jacinto del Cauca'
  },
  {
    DEPARTAMENTO: 'Huila',
    MUNICIPIO: 'San Agustín'
  },
  {
    DEPARTAMENTO: 'Nariño',
    MUNICIPIO: 'El Tablón de Gómez'
  },
  {
    DEPARTAMENTO: 'Archipiélago de San Andrés, Providencia y Santa Catalina',
    MUNICIPIO: 'San Andrés'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'San José de Pare'
  },
  {
    DEPARTAMENTO: 'Putumayo',
    MUNICIPIO: 'Valle de Guamez'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'San Pablo de Borbur'
  },
  {
    DEPARTAMENTO: 'Sucre',
    MUNICIPIO: 'Santiago de Tolú'
  },
  {
    DEPARTAMENTO: 'Bogotá D.C.',
    MUNICIPIO: 'Bogotá D.C.'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Carmen de Carupa'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'Ciénaga de Oro'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'San Juan de Urabá'
  },
  {
    DEPARTAMENTO: 'La Guajira',
    MUNICIPIO: 'San Juan del Cesar'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'El Carmen de Chucurí'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'El Carmen de Viboral'
  },
  {
    DEPARTAMENTO: 'Risaralda',
    MUNICIPIO: 'Belén de Umbría'
  },
  {
    DEPARTAMENTO: 'Chocó',
    MUNICIPIO: 'Belén de Bajira'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Valle de San José'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'San Luis'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'San Miguel de Sema'
  },
  {
    DEPARTAMENTO: 'Tolima',
    MUNICIPIO: 'San Antonio'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'San Benito'
  },
  {
    DEPARTAMENTO: 'Cundinamarca',
    MUNICIPIO: 'Vergara'
  },
  {
    DEPARTAMENTO: 'Córdoba',
    MUNICIPIO: 'San Carlos'
  },
  {
    DEPARTAMENTO: 'Amazonas',
    MUNICIPIO: 'Puerto Alegría'
  },
  {
    DEPARTAMENTO: 'Santander',
    MUNICIPIO: 'Hato'
  },
  {
    DEPARTAMENTO: 'Bolívar',
    MUNICIPIO: 'San Jacinto'
  },
  {
    DEPARTAMENTO: 'Cauca',
    MUNICIPIO: 'San Sebastián'
  },
  {
    DEPARTAMENTO: 'Antioquia',
    MUNICIPIO: 'San Carlos'
  },
  {
    DEPARTAMENTO: 'Boyacá',
    MUNICIPIO: 'Tuta'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Silos'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Cácota'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'El Dovio'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Toledo'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Roldanillo'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Mutiscua'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Argelia'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'El Zulia'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Salazar'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Sevilla'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Zarzal'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Cucutilla'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'El Cerrito'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Cartago'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Caicedonia'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Puerto Santander'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Gramalote'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'El Cairo'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'El Tarra'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'La Unión'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Restrepo'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Teorama'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Dagua'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Arboledas'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Guacarí'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Lourdes'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Ansermanuevo'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Bochalema'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Bugalagrande'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Convención'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Hacarí'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'La Victoria'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Herrán'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Ginebra'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Yumbo'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Obando'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Tibú'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'San Cayetano'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'San Calixto'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Bolívar'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'La Playa'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Cali'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'San Pedro'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Guadalajara de Buga'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Chinácota'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Ragonvalia'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'La Esperanza'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Villa del Rosario'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Chitagá'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Calima'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Sardinata'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Andalucía'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Pradera'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Abrego'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Los Patios'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Ocaña'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Bucarasica'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Yotoco'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Palmira'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Riofrío'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Santiago'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Alcalá'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Versalles'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Labateca'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Cachirá'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Villa Caro'
  },
  {
    DEPARTAMENTO: 'Norte de Santander',
    MUNICIPIO: 'Durania'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'El Águila'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Toro'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Candelaria'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'La Cumbre'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Ulloa'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Trujillo'
  },
  {
    DEPARTAMENTO: 'Valle del Cauca',
    MUNICIPIO: 'Vijes'
  }
];
async function uploadcity(ciudad){
   
    const body =  { nombre:ciudad.MUNICIPIO, descripcion:ciudad.MUNICIPIO, abreviacion:ciudad.MUNICIPIO }
    url = 'http://localhost:3000'

    const id = await axios.post(`${url}/ciudad`, body)
    .then(( {data} ) =>{ 
       return data.id
    })
    .catch(error => {
     console.log(`error en crear municipio,`,  error)
     return null
    })
    return id
}
async function all(){
let todos = []
for(i=0;i<municipios.length;i++){
    const id = await uploadcity(municipios[i])
    console.log('el id= ',id)
    municipios[i].id = id
    todos.push(
      municipios[i]
    )
}


fs.appendFile('municipios.json', JSON.stringify(todos), function (err) {
  if (err) throw err;
  console.log('Saved!');
});
}

all()