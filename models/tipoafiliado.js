//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Tipoafiliado
var TipoafiliadoSchema = new Schema({
nombre: {type:String, required:true}, 
descripcion: {type:String, required:true}, 
estado: {type:String, required:true}, 
abreviacion: {type:String, required:true}, 

});

TipoafiliadoSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Tipoafiliado', TipoafiliadoSchema);