//importar modelo
var Tipo_contacto = require('../models/tipo_contacto');

//funcion create
exports.tipo_contacto_create = function (req, res, next) {
    var tipo_contacto = new Tipo_contacto(
        req.body
    );

    tipo_contacto.save(function (err, tipo_contacto) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: tipo_contacto._id})
    })
};
//funcion read by id
exports.tipo_contacto_details = function (req, res, next) {
    Tipo_contacto.findById(req.params.id, function (err, tipo_contacto) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(tipo_contacto);
    })
};
//funcion read all
exports.tipo_contacto_all = function (req, res, next) {
    Tipo_contacto.find(req.params.id, function (err, tipo_contacto) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(tipo_contacto);
    })
};
//funcion update
exports.tipo_contacto_update = function (req, res, next) {
    Tipo_contacto.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, tipo_contacto) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", tipo_contacto: tipo_contacto });
    });
};

//funcion delete
exports.tipo_contacto_delete = function (req, res, next) {
    Tipo_contacto.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};