//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Aspirantenatural
var AspirantenaturalSchema = new Schema({
fecha: {type:String, required:true}, 
nombres: {type:String, required:true}, 
apellidos: {type:String, required:true}, 
tipodocumento:[ {type:Schema.Types.ObjectId, ref: 'Tipodocumento' , autopopulate: true }], 
documento: {type:String, required:true}, 
fechanacimiento: {type:String, required:true}, 
ciudad:[ {type:Schema.Types.ObjectId, ref: 'Ciudad' , autopopulate: true }], 
direccion: {type:String, required:true}, 
telefono: {type:String, required:true}, 
celular: {type:Number, required:true}, 
email: {type:String, required:true}, 
profesion: {type:String, required:true}, 
anogrado: {type:Number, required:true}, 
matriculaprofesional: {type:String, required:true}, 
universidad: {type:String, required:true}, 
seccional:[ {type:Schema.Types.ObjectId, ref: 'Seccional' , autopopulate: true }], 
subafinidad:[ {type:Schema.Types.ObjectId, ref: 'Subafinidad' , autopopulate: true }],
abeasdata: {type:String, required:true}, 
lavadoactivos: {type:String, required:true}, 

});

AspirantenaturalSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Aspirantenatural', AspirantenaturalSchema);