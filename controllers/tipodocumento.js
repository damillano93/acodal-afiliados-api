//importar modelo
var Tipodocumento = require('../models/tipodocumento');

//funcion create
exports.tipodocumento_create = function (req, res, next) {
    var tipodocumento = new Tipodocumento(
        req.body
    );

    tipodocumento.save(function (err, tipodocumento) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: tipodocumento._id})
    })
};
//funcion read by id
exports.tipodocumento_details = function (req, res, next) {
    Tipodocumento.findById(req.params.id, function (err, tipodocumento) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(tipodocumento);
    })
};
//funcion read all
exports.tipodocumento_all = function (req, res, next) {
    Tipodocumento.find(req.params.id, function (err, tipodocumento) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(tipodocumento);
    })
};
//funcion update
exports.tipodocumento_update = function (req, res, next) {
    Tipodocumento.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, tipodocumento) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", tipodocumento: tipodocumento });
    });
};

//funcion delete
exports.tipodocumento_delete = function (req, res, next) {
    Tipodocumento.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};