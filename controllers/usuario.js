//importar modelo
const Usuario = require('../models/user');
const {MAIL_USER, MAIL_PASSWORD, URL_SERVER} = process.env;
const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: MAIL_USER,
      pass: MAIL_PASSWORD
    }
  });

//funcion create
exports.usuario_create = function (req, res, next) {
    var usuario = new Usuario(
        req.body
    );

    usuario.save(function (err, usuario) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: usuario._id})
    })
};
//funcion read by id
exports.usuario_details = function (req, res, next) {
    Usuario.findById(req.params.id, function (err, usuario) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(usuario);
    })
};
exports.usuario_findbymail = function (req, res, next) {
    Usuario.find({username:req.params.id}, function (err, usuario) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        sendEmail(usuario[0].username,usuario[0]._id)

        res.send(usuario[0]);
    })
};
//funcion read all
exports.usuario_all = function (req, res, next) {
    Usuario.find(req.params.id, function (err, usuario) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(usuario);
    })
};
//funcion update
exports.usuario_update = function (req, res, next) {
    Usuario.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, usuario) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", usuario: usuario });
    });
};
//funcion update password
exports.usuario_update_password = function (req, res, next) {
    Usuario.findById(req.params.id)
    .then((u) => {
        u.setPassword(req.body.password,(err, u) => {
            if (err) return next(err);
            u.save();
            res.status(200).json({ status:'OK', message: 'password change successful' });
        });
    
    })   
};
//funcion delete
exports.usuario_delete = function (req, res, next) {
    Usuario.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};

function sendEmail(mail,url){
    url = `${URL_SERVER}/restore/${url}`

    const mailOptions = {
      from: MAIL_USER,
      to: mail,
      subject: 'Recuperación contraseña Acodal',
      html: `<h1>Recuperacion de contraseña</h1><p>En el siguiente enlace puede recuperar su contraseña</p><a href="${url}">click aqui</a>`
    };
    
    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
      }
    });
    return 'ok'
    }

