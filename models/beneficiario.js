//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Beneficiario
var BeneficiarioSchema = new Schema({
    nombres: { type: String, required: true },
    apellidos: { type: String, required: true },
    cargo: { type: String, required: true },
    correo: { type: String, required: true },

},

    {
        timestamps: true
    }


);

BeneficiarioSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Beneficiario', BeneficiarioSchema);