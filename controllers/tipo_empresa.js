//importar modelo
var Tipo_empresa = require('../models/tipo_empresa');

//funcion create
exports.tipo_empresa_create = function (req, res, next) {
    var tipo_empresa = new Tipo_empresa(
        req.body
    );

    tipo_empresa.save(function (err, tipo_empresa) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: tipo_empresa._id})
    })
};
//funcion read by id
exports.tipo_empresa_details = function (req, res, next) {
    Tipo_empresa.findById(req.params.id, function (err, tipo_empresa) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(tipo_empresa);
    })
};
//funcion read all
exports.tipo_empresa_all = function (req, res, next) {
    Tipo_empresa.find(req.params.id, function (err, tipo_empresa) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(tipo_empresa);
    })
};
//funcion update
exports.tipo_empresa_update = function (req, res, next) {
    Tipo_empresa.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, tipo_empresa) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", tipo_empresa: tipo_empresa });
    });
};

//funcion delete
exports.tipo_empresa_delete = function (req, res, next) {
    Tipo_empresa.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};