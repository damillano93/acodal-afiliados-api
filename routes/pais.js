// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de pais
var pais_controller = require('../controllers/pais');

// GET /:id
router.get('/:id', pais_controller.pais_details);
// GET /
router.get('/', pais_controller.pais_all);
// POST /
router.post('/', pais_controller.pais_create);
// PUT /:id
router.put('/:id', pais_controller.pais_update);
// DELETE /:id
router.delete('/:id', pais_controller.pais_delete);

//export router
module.exports = router;