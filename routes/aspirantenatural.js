// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de aspirantenatural
var aspirantenatural_controller = require('../controllers/aspirantenatural');

// GET /:id
router.get('/:id', aspirantenatural_controller.aspirantenatural_details);
// GET /
router.get('/', aspirantenatural_controller.aspirantenatural_all);
// POST /
router.post('/', aspirantenatural_controller.aspirantenatural_create);
// PUT /:id
router.put('/:id', aspirantenatural_controller.aspirantenatural_update);
// DELETE /:id
router.delete('/:id', aspirantenatural_controller.aspirantenatural_delete);
// PUT seccional 
router.put('/seccional/:id',aspirantenatural_controller.Aspirantenatural_add_seccional);

//export router
module.exports = router;