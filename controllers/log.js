//importar modelo
var Log = require('../models/log');

//funcion create
exports.log_create = function (req, res, next) {
    var log = new Log(
        req.body
    );

    log.save(function (err, log) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: log._id})
    })
};
//funcion read by id
exports.log_details = function (req, res, next) {
    Log.findById(req.params.id, function (err, log) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(log);
    })
};
//funcion read all
exports.log_all = function (req, res, next) {
    Log.find(req.params.id, function (err, log) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(log);
    })
};
//funcion update
exports.log_update = function (req, res, next) {
    Log.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, log) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", log: log });
    });
};

//funcion delete
exports.log_delete = function (req, res, next) {
    Log.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};