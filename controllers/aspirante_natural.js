//importar modelo
var Aspirante_natural = require('../models/aspirante_natural');

//funcion create
exports.aspirante_natural_create = function (req, res, next) {
    var aspirante_natural = new Aspirante_natural(
        req.body
    );

    aspirante_natural.save(function (err, aspirante_natural) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: aspirante_natural._id})
    })
};
//funcion read by id
exports.aspirante_natural_details = function (req, res, next) {
    Aspirante_natural.findById(req.params.id, function (err, aspirante_natural) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(aspirante_natural);
    })
};
//funcion read all
exports.aspirante_natural_all = function (req, res, next) {
    Aspirante_natural.find(req.params.id, function (err, aspirante_natural) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(aspirante_natural);
    })
};
//funcion update
exports.aspirante_natural_update = function (req, res, next) {
    Aspirante_natural.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, aspirante_natural) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", aspirante_natural: aspirante_natural });
    });
};
//funcion add tipo_documento
exports.Aspirante_natural_add_tipo_documento = function (req, res, next) {
Aspirante_natural.findByIdAndUpdate(req.params.id, { $push: { tipo_documento: req.body.tipo_documento } }, { new: true }, function (err, Aspirante_natural) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Aspirante_natural: Aspirante_natural });
});
};
//funcion add ciudad
exports.Aspirante_natural_add_ciudad = function (req, res, next) {
Aspirante_natural.findByIdAndUpdate(req.params.id, { $push: { ciudad: req.body.ciudad } }, { new: true }, function (err, Aspirante_natural) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Aspirante_natural: Aspirante_natural });
});
};
//funcion add seccional
exports.Aspirante_natural_add_seccional = function (req, res, next) {
Aspirante_natural.findByIdAndUpdate(req.params.id, { $push: { seccional: req.body.seccional } }, { new: true }, function (err, Aspirante_natural) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Aspirante_natural: Aspirante_natural });
});
};

//funcion delete
exports.aspirante_natural_delete = function (req, res, next) {
    Aspirante_natural.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};