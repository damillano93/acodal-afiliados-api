// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();
var Verify = require('./verify');

// importar controlador de afiliado
var afiliado_controller = require('../controllers/afiliado');

// GET /:id
router.get('/:id', Verify.verifyOrdinaryUser, afiliado_controller.afiliado_details);
// GET seccional/:id
router.get('/seccional/:id', afiliado_controller.afiliado_find_seccional);
// GET /:idUser
router.get('/user/:id', Verify.verifyOrdinaryUser, afiliado_controller.afiliado_find_by_user);
// GET /
router.get('/',  Verify.verifyOrdinaryUser, afiliado_controller.afiliado_all);
// POST /
router.post('/', afiliado_controller.afiliado_create);
// PUT /:id
router.put('/:id', Verify.verifyOrdinaryUser, afiliado_controller.afiliado_update);
// DELETE /:id
router.delete('/:id', Verify.verifyOrdinaryUser, afiliado_controller.afiliado_delete);
// PUT grupocorreos 
router.put('/grupocorreos/:id', afiliado_controller.Afiliado_add_grupocorreos);
// PATCH data 
router.patch('/data', afiliado_controller.afiliado_patch);
router.post( '/data', afiliado_controller.afiliado_find_by_doc);

//export router
module.exports = router;