//importar modelo
var Grupoafinidad = require('../models/grupoafinidad');

//funcion create
exports.grupoafinidad_create = function (req, res, next) {
    var grupoafinidad = new Grupoafinidad(
        req.body
    );

    grupoafinidad.save(function (err, grupoafinidad) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: grupoafinidad._id})
    })
};
//funcion read by id
exports.grupoafinidad_details = function (req, res, next) {
    Grupoafinidad.findById(req.params.id, function (err, grupoafinidad) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(grupoafinidad);
    })
};
//funcion read all
exports.grupoafinidad_all = function (req, res, next) {
    Grupoafinidad.find(req.params.id, function (err, grupoafinidad) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(grupoafinidad);
    })
};
//funcion update
exports.grupoafinidad_update = function (req, res, next) {
    Grupoafinidad.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, grupoafinidad) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", grupoafinidad: grupoafinidad });
    });
};

//funcion delete
exports.grupoafinidad_delete = function (req, res, next) {
    Grupoafinidad.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};