//importar modelo
var Afiliado = require('../models/afiliado');

//funcion create
exports.afiliado_create = function (req, res, next) {
    var afiliado = new Afiliado(
        req.body
    );

    afiliado.save(function (err, afiliado) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: afiliado._id})
    })
};
//funcion read by id
exports.afiliado_details = function (req, res, next) {
    Afiliado.findById(req.params.id, function (err, afiliado) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(afiliado);
    })
};
//funcion find by seccional
exports.afiliado_find_seccional = function (req, res, next) {
    Afiliado.find({seccional: req.params.id}, function (err, afiliado) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(afiliado);
    })
};
//funcion read all
exports.afiliado_all = function (req, res, next) {
    Afiliado.find(req.params.id, function (err, afiliado) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(afiliado);
    })
};
//funcion read all
exports.afiliado_find_by_user = function (req, res, next) {
    Afiliado.find({user:req.params.id}, function (err, afiliado) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(afiliado);
    })
};
//funcion update
exports.afiliado_update = function (req, res, next) {
    Afiliado.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, afiliado) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", afiliado: afiliado });
    });
};
exports.afiliado_patch = function (req, res, next) {

    const conditions = { pais: '' }
   
    const update = { pais:'Colombia' }
   
    Afiliado.updateMany(conditions,update,function(err,result){
       if(err){
        res.send({status: "Err", error:err})
        return next(err);
       }else{
         console.log(result);
         res.send({status: "updated", afiliado: result });
       }
     });
 
};

exports.afiliado_find_by_doc = function (req, res, next) {
    Afiliado.find({nombres:req.body.nombres, ciudad:'5dd45baba37661000885033e' }, function (err, afiliado) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
         
        res.send(afiliado[0]|| null);
    })
};


//funcion add tipoafiliado
exports.Afiliado_add_tipoafiliado = function (req, res, next) {
Afiliado.findByIdAndUpdate(req.params.id, { $push: { tipoafiliado: req.body.tipoafiliado } }, { new: true }, function (err, Afiliado) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Afiliado: Afiliado });
});
};
//funcion add categoria
exports.Afiliado_add_categoria = function (req, res, next) {
Afiliado.findByIdAndUpdate(req.params.id, { $push: { categoria: req.body.categoria } }, { new: true }, function (err, Afiliado) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Afiliado: Afiliado });
});
};
//funcion add seccional
exports.Afiliado_add_seccional = function (req, res, next) {
Afiliado.findByIdAndUpdate(req.params.id, { $push: { seccional: req.body.seccional } }, { new: true }, function (err, Afiliado) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Afiliado: Afiliado });
});
};
//funcion add ciudad
exports.Afiliado_add_ciudad = function (req, res, next) {
Afiliado.findByIdAndUpdate(req.params.id, { $push: { ciudad: req.body.ciudad } }, { new: true }, function (err, Afiliado) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Afiliado: Afiliado });
});
};
//funcion add contacto
exports.Afiliado_add_contacto = function (req, res, next) {
Afiliado.findByIdAndUpdate(req.params.id, { $push: { contacto: req.body.contacto } }, { new: true }, function (err, Afiliado) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Afiliado: Afiliado });
});
};
//funcion add grupocorreos
exports.Afiliado_add_grupocorreos = function (req, res, next) {
Afiliado.findByIdAndUpdate(req.params.id, { $push: { grupocorreos: req.body.grupocorreos } }, { new: true }, function (err, Afiliado) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Afiliado: Afiliado });
});
};
//funcion add documento
exports.Afiliado_add_documento = function (req, res, next) {
Afiliado.findByIdAndUpdate(req.params.id, { $push: { documento: req.body.documento } }, { new: true }, function (err, Afiliado) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Afiliado: Afiliado });
});
};

//funcion delete
exports.afiliado_delete = function (req, res, next) {
    Afiliado.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};

