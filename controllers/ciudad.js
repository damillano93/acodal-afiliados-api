//importar modelo
var Ciudad = require('../models/ciudad');

//funcion create
exports.ciudad_create = function (req, res, next) {
    var ciudad = new Ciudad(
        req.body
    );

    ciudad.save(function (err, ciudad) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: ciudad._id})
    })
};
//funcion read by id
exports.ciudad_details = function (req, res, next) {
    Ciudad.findById(req.params.id, function (err, ciudad) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(ciudad);
    })
};
//funcion read all
exports.ciudad_all = function (req, res, next) {
    Ciudad.find(req.params.id, function (err, ciudad) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(ciudad);
    })
};
//funcion update
exports.ciudad_update = function (req, res, next) {
    Ciudad.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, ciudad) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", ciudad: ciudad });
    });
};

//funcion delete
exports.ciudad_delete = function (req, res, next) {
    Ciudad.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};