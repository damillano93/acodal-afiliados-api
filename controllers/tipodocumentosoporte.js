//importar modelo
var Tipodocumentosoporte = require('../models/tipodocumentosoporte');

//funcion create
exports.tipodocumentosoporte_create = function (req, res, next) {
    var tipodocumentosoporte = new Tipodocumentosoporte(
        req.body
    );

    tipodocumentosoporte.save(function (err, tipodocumentosoporte) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: tipodocumentosoporte._id})
    })
};
//funcion read by id
exports.tipodocumentosoporte_details = function (req, res, next) {
    Tipodocumentosoporte.findById(req.params.id, function (err, tipodocumentosoporte) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(tipodocumentosoporte);
    })
};
//funcion read all
exports.tipodocumentosoporte_all = function (req, res, next) {
    Tipodocumentosoporte.find(req.params.id, function (err, tipodocumentosoporte) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(tipodocumentosoporte);
    })
};
//funcion update
exports.tipodocumentosoporte_update = function (req, res, next) {
    Tipodocumentosoporte.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, tipodocumentosoporte) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", tipodocumentosoporte: tipodocumentosoporte });
    });
};

//funcion delete
exports.tipodocumentosoporte_delete = function (req, res, next) {
    Tipodocumentosoporte.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};