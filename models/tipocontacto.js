//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Tipocontacto
var TipocontactoSchema = new Schema({
nombre: {type:String, required:true}, 
descripcion: {type:String, required:true}, 

});

TipocontactoSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Tipocontacto', TipocontactoSchema);