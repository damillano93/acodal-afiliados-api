//importar modelo
var Aspirantejuridico = require('../models/aspirantejuridico');

//funcion create
exports.aspirantejuridico_create = function (req, res, next) {
    var aspirantejuridico = new Aspirantejuridico(
        req.body
    );

    aspirantejuridico.save(function (err, aspirantejuridico) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: aspirantejuridico._id})
    })
};
//funcion read by id
exports.aspirantejuridico_details = function (req, res, next) {
    Aspirantejuridico.findById(req.params.id, function (err, aspirantejuridico) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(aspirantejuridico);
    })
};
//funcion read all
exports.aspirantejuridico_all = function (req, res, next) {
    Aspirantejuridico.find(req.params.id, function (err, aspirantejuridico) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(aspirantejuridico);
    })
};
//funcion update
exports.aspirantejuridico_update = function (req, res, next) {
    Aspirantejuridico.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, aspirantejuridico) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", aspirantejuridico: aspirantejuridico });
    });
};
//funcion add ciudad
exports.Aspirantejuridico_add_ciudad = function (req, res, next) {
Aspirantejuridico.findByIdAndUpdate(req.params.id, { $push: { ciudad: req.body.ciudad } }, { new: true }, function (err, Aspirantejuridico) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Aspirantejuridico: Aspirantejuridico });
});
};
//funcion add seccional
exports.Aspirantejuridico_add_seccional = function (req, res, next) {
Aspirantejuridico.findByIdAndUpdate(req.params.id, { $push: { seccional: req.body.seccional } }, { new: true }, function (err, Aspirantejuridico) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Aspirantejuridico: Aspirantejuridico });
});
};
//funcion add tipoempresa
exports.Aspirantejuridico_add_tipoempresa = function (req, res, next) {
Aspirantejuridico.findByIdAndUpdate(req.params.id, { $push: { tipoempresa: req.body.tipoempresa } }, { new: true }, function (err, Aspirantejuridico) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Aspirantejuridico: Aspirantejuridico });
});
};

//funcion delete
exports.aspirantejuridico_delete = function (req, res, next) {
    Aspirantejuridico.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};