//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Tipoempresa
var TipoempresaSchema = new Schema({
nombre: {type:String, required:true}, 
descripcion: {type:String, required:true}, 
estado: {type:String, required:true}, 
abreviacion: {type:String, required:true}, 

});

TipoempresaSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Tipoempresa', TipoempresaSchema);