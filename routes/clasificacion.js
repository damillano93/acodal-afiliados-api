// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de clasificacion
var clasificacion_controller = require('../controllers/clasificacion');

// GET /:id
router.get('/:id', clasificacion_controller.clasificacion_details);
// GET /
router.get('/', clasificacion_controller.clasificacion_all);
// POST /
router.post('/', clasificacion_controller.clasificacion_create);
// PUT /:id
router.put('/:id', clasificacion_controller.clasificacion_update);
// DELETE /:id
router.delete('/:id', clasificacion_controller.clasificacion_delete);

//export router
module.exports = router;