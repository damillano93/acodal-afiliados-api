//importar modelo
var Grupocorreos = require('../models/grupocorreos');

//funcion create
exports.grupocorreos_create = function (req, res, next) {
    var grupocorreos = new Grupocorreos(
        req.body
    );

    grupocorreos.save(function (err, grupocorreos) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: grupocorreos._id})
    })
};
//funcion read by id
exports.grupocorreos_details = function (req, res, next) {
    Grupocorreos.findById(req.params.id, function (err, grupocorreos) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(grupocorreos);
    })
};
//funcion read all
exports.grupocorreos_all = function (req, res, next) {
    Grupocorreos.find(req.params.id, function (err, grupocorreos) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(grupocorreos);
    })
};
//funcion update
exports.grupocorreos_update = function (req, res, next) {
    Grupocorreos.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, grupocorreos) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", grupocorreos: grupocorreos });
    });
};

//funcion delete
exports.grupocorreos_delete = function (req, res, next) {
    Grupocorreos.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};