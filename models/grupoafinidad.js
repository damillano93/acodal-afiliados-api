//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Grupoafinidad
var GrupoafinidadSchema = new Schema({
nombre: {type:String, required:true}, 
descripcion: {type:String, required:true}, 
abreviacion: {type:String, required:true}, 
subafinidad:[ {type:Schema.Types.ObjectId, ref: 'Subafinidad' , autopopulate: true }], 
});

GrupoafinidadSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Grupoafinidad', GrupoafinidadSchema);