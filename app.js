// importar biblioteca express
var express = require('express');
// importar biblioteca body-parser
var bodyParser = require('body-parser');
// importar biblioteca morgan
var logger = require('morgan')

var multer = require('multer');

//importar variables de entorno 
const { MONGO_URL, PORT } = process.env 

// importar router seccional
var seccional = require('./routes/seccional');
// importar router categoria
var categoria = require('./routes/categoria');
// importar router categoria
var factura = require('./routes/factura');
// importar router tipoafiliado
var tipoafiliado = require('./routes/tipoafiliado');
// importar router tipoempresa
var tipoempresa = require('./routes/tipoempresa');
// importar router aspirantejuridico
var aspirantejuridico = require('./routes/aspirantejuridico');
// importar router tipocontacto
var tipocontacto = require('./routes/tipocontacto');
// importar router contacto
var contacto = require('./routes/contacto');
// importar router ciudad
var ciudad = require('./routes/ciudad');
// importar router pais
var pais = require('./routes/pais');
// importar router log
var log = require('./routes/log');
// importar router estudio
var estudio = require('./routes/estudio');
// importar router clasificacion
var clasificacion = require('./routes/clasificacion');
// importar router grupoafinidad
var grupoafinidad = require('./routes/grupoafinidad');
// importar router subafinidad
var subafinidad = require('./routes/subafinidad');
// importar router grupocorreos
var grupocorreos = require('./routes/grupocorreos');
// importar router tipodocumento
var tipodocumento = require('./routes/tipodocumento');
// importar router tipodocumentosoporte
var tipodocumentosoporte = require('./routes/tipodocumentosoporte');
// importar router documento
var documento = require('./routes/documento');
// importar router aspirantenatural
var aspirantenatural = require('./routes/aspirantenatural');
// importar router afiliado
var afiliado = require('./routes/afiliado');

var users = require('./routes/users');

var passport = require('passport');
var authenticate = require('./authenticate');
const fileUpload = require('express-fileupload');
var upload = require('./routes/upload');
var cors = require('cors');
// importar router de status
var status = require('./routes/status'); 
// uso biblioteca express
var app = express();
//visualizacion respuestas en consola
app.use(logger('dev'))
//agrega CORS para tener accesso desde otras aplicaciones
app.use(cors());
// uso biblioteca mongoose
var mongoose = require('mongoose');


uploader = multer({
	dest: 'uploads/'
});



// conexion DB 
const dev_db_url = MONGO_URL

mongoose.connect(dev_db_url);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
// uso de respuestas y llamados en JSON
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
var config = require('./config');
// passport config
app.use(passport.initialize());
app.use(passport.session());

// default options
//app.use(fileUpload());

//creacion ruta /seccional
app.use('/seccional', seccional);
//creacion ruta /categoria
app.use('/categoria', categoria);
//creacion ruta /tipoafiliado
app.use('/tipoafiliado', tipoafiliado);
//creacion ruta /tipoempresa
app.use('/tipoempresa', tipoempresa);
//creacion ruta /aspirantejuridico
app.use('/aspirantejuridico', aspirantejuridico);
//creacion ruta /tipocontacto
app.use('/tipocontacto', tipocontacto);
//creacion ruta /contacto
app.use('/contacto', contacto);
//creacion ruta /ciudad
app.use('/ciudad', ciudad);
//creacion ruta /pais
app.use('/pais', pais);
//creacion ruta /log
app.use('/log', log);
//creacion ruta /estudio
app.use('/estudio', estudio);
//creacion ruta /clasificacion
app.use('/clasificacion', clasificacion);
//creacion ruta /grupoafinidad
app.use('/grupoafinidad', grupoafinidad);
//creacion ruta /subafinidad
app.use('/subafinidad', subafinidad);
//creacion ruta /grupocorreos
app.use('/grupocorreos', grupocorreos);
//creacion ruta /tipodocumento
app.use('/tipodocumento', tipodocumento);
//creacion ruta /tipodocumentosoporte
app.use('/tipodocumentosoporte', tipodocumentosoporte);
//creacion ruta /documento
app.use('/documento', documento);
//creacion ruta /aspirantenatural
app.use('/aspirantenatural', aspirantenatural);
//creacion ruta /afiliado
app.use('/afiliado', afiliado);
//creacion ruta /afiliado
app.use('/factura', factura);
//creacion ruta /upload
app.post('/upload', uploader.single('singleFile'),  upload.s3);

app.use('/users', users);
//creacion ruta /
app.use('/', status);
app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.status(500).send('Something broke! ',err);
  });
// puerto de servicio
const port = PORT || 3000;
// inicir la conexion
app.listen(port, () => {
    console.log('Server in port ' + port);
});