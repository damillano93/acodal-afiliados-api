//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Estudio
var EstudioSchema = new Schema({
    nivel: { type: String, required: true },
    titulo: { type: String, required: true },
    universidad: { type: String, required: true },

},

    {
        timestamps: true
    }


);

EstudioSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Estudio', EstudioSchema);