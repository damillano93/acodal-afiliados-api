//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Documento
var DocumentoSchema = new Schema({
tipodocumentosoporte:[ {type:Schema.Types.ObjectId, ref: 'Tipodocumentosoporte' , autopopulate: true }], 
urldocumento: {type:String, required:true}, 

});

DocumentoSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Documento', DocumentoSchema);