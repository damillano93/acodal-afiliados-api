//importar modelo
var Grupo_correos = require('../models/grupo_correos');

//funcion create
exports.grupo_correos_create = function (req, res, next) {
    var grupo_correos = new Grupo_correos(
        req.body
    );

    grupo_correos.save(function (err, grupo_correos) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: grupo_correos._id})
    })
};
//funcion read by id
exports.grupo_correos_details = function (req, res, next) {
    Grupo_correos.findById(req.params.id, function (err, grupo_correos) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(grupo_correos);
    })
};
//funcion read all
exports.grupo_correos_all = function (req, res, next) {
    Grupo_correos.find(req.params.id, function (err, grupo_correos) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(grupo_correos);
    })
};
//funcion update
exports.grupo_correos_update = function (req, res, next) {
    Grupo_correos.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, grupo_correos) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", grupo_correos: grupo_correos });
    });
};

//funcion delete
exports.grupo_correos_delete = function (req, res, next) {
    Grupo_correos.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};