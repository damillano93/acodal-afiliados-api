// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de grupocorreos
var grupocorreos_controller = require('../controllers/grupocorreos');

// GET /:id
router.get('/:id', grupocorreos_controller.grupocorreos_details);
// GET /
router.get('/', grupocorreos_controller.grupocorreos_all);
// POST /
router.post('/', grupocorreos_controller.grupocorreos_create);
// PUT /:id
router.put('/:id', grupocorreos_controller.grupocorreos_update);
// DELETE /:id
router.delete('/:id', grupocorreos_controller.grupocorreos_delete);

//export router
module.exports = router;