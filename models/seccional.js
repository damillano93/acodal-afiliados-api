//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Seccional
var SeccionalSchema = new Schema({
nombre: {type:String, required:true}, 
descripcion: {type:String, required:true}, 
direccion: {type:String, required:true}, 
ciudad: {type:String, required:true}, 
cubrimiento: {type:String, required:true}, 
estado: {type:String, required:true}, 
telefono: {type:String, required:true}, 
email: {type:String, required:true}, 

});

SeccionalSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Seccional', SeccionalSchema);