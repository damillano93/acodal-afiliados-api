const axios = require('axios')
ciudades = [

    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268aa"
        },
        "nombre": "Betulia",
        "descripcion": "Betulia",
        "abreviacion": "Betulia",
        "createdAt": "2020-01-16T13:31:26.315Z",
        "updatedAt": "2020-01-16T13:31:26.315Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268ab"
        },
        "nombre": "Ciudad Bolívar",
        "descripcion": "Ciudad Bolívar",
        "abreviacion": "Ciudad Bolívar",
        "createdAt": "2020-01-16T13:31:26.329Z",
        "updatedAt": "2020-01-16T13:31:26.329Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268ac"
        },
        "nombre": "Abejorral",
        "descripcion": "Abejorral",
        "abreviacion": "Abejorral",
        "createdAt": "2020-01-16T13:31:26.334Z",
        "updatedAt": "2020-01-16T13:31:26.334Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268ad"
        },
        "nombre": "Caucasia",
        "descripcion": "Caucasia",
        "abreviacion": "Caucasia",
        "createdAt": "2020-01-16T13:31:26.336Z",
        "updatedAt": "2020-01-16T13:31:26.336Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268ae"
        },
        "nombre": "Alejandría",
        "descripcion": "Alejandría",
        "abreviacion": "Alejandría",
        "createdAt": "2020-01-16T13:31:26.341Z",
        "updatedAt": "2020-01-16T13:31:26.341Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268af"
        },
        "nombre": "Guadalupe",
        "descripcion": "Guadalupe",
        "abreviacion": "Guadalupe",
        "createdAt": "2020-01-16T13:31:26.347Z",
        "updatedAt": "2020-01-16T13:31:26.347Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268b0"
        },
        "nombre": "Medellín",
        "descripcion": "Medellín",
        "abreviacion": "Medellín",
        "createdAt": "2020-01-16T13:31:26.353Z",
        "updatedAt": "2020-01-16T13:31:26.353Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268b1"
        },
        "nombre": "Cisneros",
        "descripcion": "Cisneros",
        "abreviacion": "Cisneros",
        "createdAt": "2020-01-16T13:31:26.355Z",
        "updatedAt": "2020-01-16T13:31:26.355Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268b2"
        },
        "nombre": "Bello",
        "descripcion": "Bello",
        "abreviacion": "Bello",
        "createdAt": "2020-01-16T13:31:26.357Z",
        "updatedAt": "2020-01-16T13:31:26.357Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268b3"
        },
        "nombre": "Cáceres",
        "descripcion": "Cáceres",
        "abreviacion": "Cáceres",
        "createdAt": "2020-01-16T13:31:26.366Z",
        "updatedAt": "2020-01-16T13:31:26.366Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268b4"
        },
        "nombre": "Amagá",
        "descripcion": "Amagá",
        "abreviacion": "Amagá",
        "createdAt": "2020-01-16T13:31:26.368Z",
        "updatedAt": "2020-01-16T13:31:26.368Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268b5"
        },
        "nombre": "San Francisco",
        "descripcion": "San Francisco",
        "abreviacion": "San Francisco",
        "createdAt": "2020-01-16T13:31:26.369Z",
        "updatedAt": "2020-01-16T13:31:26.369Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268b6"
        },
        "nombre": "Abriaquí",
        "descripcion": "Abriaquí",
        "abreviacion": "Abriaquí",
        "createdAt": "2020-01-16T13:31:26.376Z",
        "updatedAt": "2020-01-16T13:31:26.376Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268b7"
        },
        "nombre": "Amalfi",
        "descripcion": "Amalfi",
        "abreviacion": "Amalfi",
        "createdAt": "2020-01-16T13:31:26.381Z",
        "updatedAt": "2020-01-16T13:31:26.381Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268b8"
        },
        "nombre": "Puerto Berrío",
        "descripcion": "Puerto Berrío",
        "abreviacion": "Puerto Berrío",
        "createdAt": "2020-01-16T13:31:26.383Z",
        "updatedAt": "2020-01-16T13:31:26.383Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268b9"
        },
        "nombre": "Caramanta",
        "descripcion": "Caramanta",
        "abreviacion": "Caramanta",
        "createdAt": "2020-01-16T13:31:26.385Z",
        "updatedAt": "2020-01-16T13:31:26.385Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268ba"
        },
        "nombre": "San Roque",
        "descripcion": "San Roque",
        "abreviacion": "San Roque",
        "createdAt": "2020-01-16T13:31:26.387Z",
        "updatedAt": "2020-01-16T13:31:26.387Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268bb"
        },
        "nombre": "Entrerrios",
        "descripcion": "Entrerrios",
        "abreviacion": "Entrerrios",
        "createdAt": "2020-01-16T13:31:26.393Z",
        "updatedAt": "2020-01-16T13:31:26.393Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268bc"
        },
        "nombre": "Baranoa",
        "descripcion": "Baranoa",
        "abreviacion": "Baranoa",
        "createdAt": "2020-01-16T13:31:26.395Z",
        "updatedAt": "2020-01-16T13:31:26.395Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268bd"
        },
        "nombre": "Envigado",
        "descripcion": "Envigado",
        "abreviacion": "Envigado",
        "createdAt": "2020-01-16T13:31:26.400Z",
        "updatedAt": "2020-01-16T13:31:26.400Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268be"
        },
        "nombre": "Cañasgordas",
        "descripcion": "Cañasgordas",
        "abreviacion": "Cañasgordas",
        "createdAt": "2020-01-16T13:31:26.402Z",
        "updatedAt": "2020-01-16T13:31:26.402Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268bf"
        },
        "nombre": "Pueblorrico",
        "descripcion": "Pueblorrico",
        "abreviacion": "Pueblorrico",
        "createdAt": "2020-01-16T13:31:26.403Z",
        "updatedAt": "2020-01-16T13:31:26.403Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268c0"
        },
        "nombre": "Yolombó",
        "descripcion": "Yolombó",
        "abreviacion": "Yolombó",
        "createdAt": "2020-01-16T13:31:26.413Z",
        "updatedAt": "2020-01-16T13:31:26.413Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268c1"
        },
        "nombre": "Mutatá",
        "descripcion": "Mutatá",
        "abreviacion": "Mutatá",
        "createdAt": "2020-01-16T13:31:26.414Z",
        "updatedAt": "2020-01-16T13:31:26.414Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268c2"
        },
        "nombre": "Fredonia",
        "descripcion": "Fredonia",
        "abreviacion": "Fredonia",
        "createdAt": "2020-01-16T13:31:26.420Z",
        "updatedAt": "2020-01-16T13:31:26.420Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268c3"
        },
        "nombre": "Yavaraté",
        "descripcion": "Yavaraté",
        "abreviacion": "Yavaraté",
        "createdAt": "2020-01-16T13:31:26.426Z",
        "updatedAt": "2020-01-16T13:31:26.426Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268c4"
        },
        "nombre": "Maceo",
        "descripcion": "Maceo",
        "abreviacion": "Maceo",
        "createdAt": "2020-01-16T13:31:26.427Z",
        "updatedAt": "2020-01-16T13:31:26.427Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268c5"
        },
        "nombre": "Anza",
        "descripcion": "Anza",
        "abreviacion": "Anza",
        "createdAt": "2020-01-16T13:31:26.433Z",
        "updatedAt": "2020-01-16T13:31:26.433Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268c6"
        },
        "nombre": "Venecia",
        "descripcion": "Venecia",
        "abreviacion": "Venecia",
        "createdAt": "2020-01-16T13:31:26.441Z",
        "updatedAt": "2020-01-16T13:31:26.441Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268c7"
        },
        "nombre": "Sabanalarga",
        "descripcion": "Sabanalarga",
        "abreviacion": "Sabanalarga",
        "createdAt": "2020-01-16T13:31:26.442Z",
        "updatedAt": "2020-01-16T13:31:26.442Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268c8"
        },
        "nombre": "Piojó",
        "descripcion": "Piojó",
        "abreviacion": "Piojó",
        "createdAt": "2020-01-16T13:31:26.444Z",
        "updatedAt": "2020-01-16T13:31:26.444Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268c9"
        },
        "nombre": "San Jerónimo",
        "descripcion": "San Jerónimo",
        "abreviacion": "San Jerónimo",
        "createdAt": "2020-01-16T13:31:26.445Z",
        "updatedAt": "2020-01-16T13:31:26.445Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268ca"
        },
        "nombre": "Luruaco",
        "descripcion": "Luruaco",
        "abreviacion": "Luruaco",
        "createdAt": "2020-01-16T13:31:26.453Z",
        "updatedAt": "2020-01-16T13:31:26.453Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268cb"
        },
        "nombre": "Marinilla",
        "descripcion": "Marinilla",
        "abreviacion": "Marinilla",
        "createdAt": "2020-01-16T13:31:26.454Z",
        "updatedAt": "2020-01-16T13:31:26.454Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268cc"
        },
        "nombre": "Gómez Plata",
        "descripcion": "Gómez Plata",
        "abreviacion": "Gómez Plata",
        "createdAt": "2020-01-16T13:31:26.456Z",
        "updatedAt": "2020-01-16T13:31:26.456Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268cd"
        },
        "nombre": "Chigorodó",
        "descripcion": "Chigorodó",
        "abreviacion": "Chigorodó",
        "createdAt": "2020-01-16T13:31:26.459Z",
        "updatedAt": "2020-01-16T13:31:26.459Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268ce"
        },
        "nombre": "Retiro",
        "descripcion": "Retiro",
        "abreviacion": "Retiro",
        "createdAt": "2020-01-16T13:31:26.462Z",
        "updatedAt": "2020-01-16T13:31:26.462Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268cf"
        },
        "nombre": "Betania",
        "descripcion": "Betania",
        "abreviacion": "Betania",
        "createdAt": "2020-01-16T13:31:26.466Z",
        "updatedAt": "2020-01-16T13:31:26.466Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268d0"
        },
        "nombre": "Santa Bárbara",
        "descripcion": "Santa Bárbara",
        "abreviacion": "Santa Bárbara",
        "createdAt": "2020-01-16T13:31:26.468Z",
        "updatedAt": "2020-01-16T13:31:26.468Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268d1"
        },
        "nombre": "Jericó",
        "descripcion": "Jericó",
        "abreviacion": "Jericó",
        "createdAt": "2020-01-16T13:31:26.473Z",
        "updatedAt": "2020-01-16T13:31:26.473Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268d2"
        },
        "nombre": "Candelaria",
        "descripcion": "Candelaria",
        "abreviacion": "Candelaria",
        "createdAt": "2020-01-16T13:31:26.475Z",
        "updatedAt": "2020-01-16T13:31:26.475Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268d3"
        },
        "nombre": "San Pedro",
        "descripcion": "San Pedro",
        "abreviacion": "San Pedro",
        "createdAt": "2020-01-16T13:31:26.476Z",
        "updatedAt": "2020-01-16T13:31:26.476Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268d4"
        },
        "nombre": "Tarso",
        "descripcion": "Tarso",
        "abreviacion": "Tarso",
        "createdAt": "2020-01-16T13:31:26.481Z",
        "updatedAt": "2020-01-16T13:31:26.481Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268d5"
        },
        "nombre": "Peque",
        "descripcion": "Peque",
        "abreviacion": "Peque",
        "createdAt": "2020-01-16T13:31:26.482Z",
        "updatedAt": "2020-01-16T13:31:26.482Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268d6"
        },
        "nombre": "Armenia",
        "descripcion": "Armenia",
        "abreviacion": "Armenia",
        "createdAt": "2020-01-16T13:31:26.489Z",
        "updatedAt": "2020-01-16T13:31:26.489Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268d7"
        },
        "nombre": "Toledo",
        "descripcion": "Toledo",
        "abreviacion": "Toledo",
        "createdAt": "2020-01-16T13:31:26.490Z",
        "updatedAt": "2020-01-16T13:31:26.490Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268d8"
        },
        "nombre": "Tuluá",
        "descripcion": "Tuluá",
        "abreviacion": "Tuluá",
        "createdAt": "2020-01-16T13:31:26.493Z",
        "updatedAt": "2020-01-16T13:31:26.493Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268d9"
        },
        "nombre": "Milán",
        "descripcion": "Milán",
        "abreviacion": "Milán",
        "createdAt": "2020-01-16T13:31:26.501Z",
        "updatedAt": "2020-01-16T13:31:26.501Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268da"
        },
        "nombre": "Ebéjico",
        "descripcion": "Ebéjico",
        "abreviacion": "Ebéjico",
        "createdAt": "2020-01-16T13:31:26.502Z",
        "updatedAt": "2020-01-16T13:31:26.502Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268db"
        },
        "nombre": "La Ceja",
        "descripcion": "La Ceja",
        "abreviacion": "La Ceja",
        "createdAt": "2020-01-16T13:31:26.504Z",
        "updatedAt": "2020-01-16T13:31:26.504Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268dc"
        },
        "nombre": "Pinillos",
        "descripcion": "Pinillos",
        "abreviacion": "Pinillos",
        "createdAt": "2020-01-16T13:31:26.505Z",
        "updatedAt": "2020-01-16T13:31:26.505Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268dd"
        },
        "nombre": "Zaragoza",
        "descripcion": "Zaragoza",
        "abreviacion": "Zaragoza",
        "createdAt": "2020-01-16T13:31:26.513Z",
        "updatedAt": "2020-01-16T13:31:26.513Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268de"
        },
        "nombre": "Yarumal",
        "descripcion": "Yarumal",
        "abreviacion": "Yarumal",
        "createdAt": "2020-01-16T13:31:26.514Z",
        "updatedAt": "2020-01-16T13:31:26.514Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268df"
        },
        "nombre": "Manatí",
        "descripcion": "Manatí",
        "abreviacion": "Manatí",
        "createdAt": "2020-01-16T13:31:26.516Z",
        "updatedAt": "2020-01-16T13:31:26.516Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268e0"
        },
        "nombre": "Barbosa",
        "descripcion": "Barbosa",
        "abreviacion": "Barbosa",
        "createdAt": "2020-01-16T13:31:26.517Z",
        "updatedAt": "2020-01-16T13:31:26.517Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268e1"
        },
        "nombre": "Malambo",
        "descripcion": "Malambo",
        "abreviacion": "Malambo",
        "createdAt": "2020-01-16T13:31:26.520Z",
        "updatedAt": "2020-01-16T13:31:26.520Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268e2"
        },
        "nombre": "Morales",
        "descripcion": "Morales",
        "abreviacion": "Morales",
        "createdAt": "2020-01-16T13:31:26.525Z",
        "updatedAt": "2020-01-16T13:31:26.525Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268e3"
        },
        "nombre": "Usiacurí",
        "descripcion": "Usiacurí",
        "abreviacion": "Usiacurí",
        "createdAt": "2020-01-16T13:31:26.526Z",
        "updatedAt": "2020-01-16T13:31:26.526Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268e4"
        },
        "nombre": "Arjona",
        "descripcion": "Arjona",
        "abreviacion": "Arjona",
        "createdAt": "2020-01-16T13:31:26.531Z",
        "updatedAt": "2020-01-16T13:31:26.531Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268e5"
        },
        "nombre": "La Pintada",
        "descripcion": "La Pintada",
        "abreviacion": "La Pintada",
        "createdAt": "2020-01-16T13:31:26.532Z",
        "updatedAt": "2020-01-16T13:31:26.532Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268e6"
        },
        "nombre": "Córdoba",
        "descripcion": "Córdoba",
        "abreviacion": "Córdoba",
        "createdAt": "2020-01-16T13:31:26.534Z",
        "updatedAt": "2020-01-16T13:31:26.534Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268e7"
        },
        "nombre": "Frontino",
        "descripcion": "Frontino",
        "abreviacion": "Frontino",
        "createdAt": "2020-01-16T13:31:26.541Z",
        "updatedAt": "2020-01-16T13:31:26.541Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268e8"
        },
        "nombre": "Arboletes",
        "descripcion": "Arboletes",
        "abreviacion": "Arboletes",
        "createdAt": "2020-01-16T13:31:26.542Z",
        "updatedAt": "2020-01-16T13:31:26.542Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268e9"
        },
        "nombre": "Concordia",
        "descripcion": "Concordia",
        "abreviacion": "Concordia",
        "createdAt": "2020-01-16T13:31:26.544Z",
        "updatedAt": "2020-01-16T13:31:26.544Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268ea"
        },
        "nombre": "Suan",
        "descripcion": "Suan",
        "abreviacion": "Suan",
        "createdAt": "2020-01-16T13:31:26.548Z",
        "updatedAt": "2020-01-16T13:31:26.548Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065ae7c6e3d00085268eb"
        },
        "nombre": "Clemencia",
        "descripcion": "Clemencia",
        "abreviacion": "Clemencia",
        "createdAt": "2020-01-16T13:31:26.551Z",
        "updatedAt": "2020-01-16T13:31:26.551Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d00085268ec"
        },
        "nombre": "Caracolí",
        "descripcion": "Caracolí",
        "abreviacion": "Caracolí",
        "createdAt": "2020-01-16T13:31:28.583Z",
        "updatedAt": "2020-01-16T13:31:28.583Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d00085268ed"
        },
        "nombre": "Angelópolis",
        "descripcion": "Angelópolis",
        "abreviacion": "Angelópolis",
        "createdAt": "2020-01-16T13:31:28.586Z",
        "updatedAt": "2020-01-16T13:31:28.586Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d00085268ee"
        },
        "nombre": "Cicuco",
        "descripcion": "Cicuco",
        "abreviacion": "Cicuco",
        "createdAt": "2020-01-16T13:31:28.593Z",
        "updatedAt": "2020-01-16T13:31:28.593Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d00085268ef"
        },
        "nombre": "Olaya",
        "descripcion": "Olaya",
        "abreviacion": "Olaya",
        "createdAt": "2020-01-16T13:31:28.595Z",
        "updatedAt": "2020-01-16T13:31:28.595Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d00085268f0"
        },
        "nombre": "La Unión",
        "descripcion": "La Unión",
        "abreviacion": "La Unión",
        "createdAt": "2020-01-16T13:31:28.597Z",
        "updatedAt": "2020-01-16T13:31:28.597Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d00085268f1"
        },
        "nombre": "Norosí",
        "descripcion": "Norosí",
        "abreviacion": "Norosí",
        "createdAt": "2020-01-16T13:31:28.605Z",
        "updatedAt": "2020-01-16T13:31:28.605Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d00085268f2"
        },
        "nombre": "Florida",
        "descripcion": "Florida",
        "abreviacion": "Florida",
        "createdAt": "2020-01-16T13:31:28.606Z",
        "updatedAt": "2020-01-16T13:31:28.606Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d00085268f3"
        },
        "nombre": "Anorí",
        "descripcion": "Anorí",
        "abreviacion": "Anorí",
        "createdAt": "2020-01-16T13:31:28.608Z",
        "updatedAt": "2020-01-16T13:31:28.608Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d00085268f4"
        },
        "nombre": "Vegachí",
        "descripcion": "Vegachí",
        "abreviacion": "Vegachí",
        "createdAt": "2020-01-16T13:31:28.609Z",
        "updatedAt": "2020-01-16T13:31:28.609Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d00085268f5"
        },
        "nombre": "Tarazá",
        "descripcion": "Tarazá",
        "abreviacion": "Tarazá",
        "createdAt": "2020-01-16T13:31:28.611Z",
        "updatedAt": "2020-01-16T13:31:28.611Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d00085268f6"
        },
        "nombre": "Támesis",
        "descripcion": "Támesis",
        "abreviacion": "Támesis",
        "createdAt": "2020-01-16T13:31:28.614Z",
        "updatedAt": "2020-01-16T13:31:28.614Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d00085268f7"
        },
        "nombre": "San Vicente",
        "descripcion": "San Vicente",
        "abreviacion": "San Vicente",
        "createdAt": "2020-01-16T13:31:28.621Z",
        "updatedAt": "2020-01-16T13:31:28.621Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d00085268f8"
        },
        "nombre": "Liborina",
        "descripcion": "Liborina",
        "abreviacion": "Liborina",
        "createdAt": "2020-01-16T13:31:28.623Z",
        "updatedAt": "2020-01-16T13:31:28.623Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d00085268f9"
        },
        "nombre": "Murindó",
        "descripcion": "Murindó",
        "abreviacion": "Murindó",
        "createdAt": "2020-01-16T13:31:28.625Z",
        "updatedAt": "2020-01-16T13:31:28.625Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d00085268fa"
        },
        "nombre": "Argelia",
        "descripcion": "Argelia",
        "abreviacion": "Argelia",
        "createdAt": "2020-01-16T13:31:28.626Z",
        "updatedAt": "2020-01-16T13:31:28.626Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d00085268fb"
        },
        "nombre": "Salgar",
        "descripcion": "Salgar",
        "abreviacion": "Salgar",
        "createdAt": "2020-01-16T13:31:28.629Z",
        "updatedAt": "2020-01-16T13:31:28.629Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d00085268fc"
        },
        "nombre": "Apartadó",
        "descripcion": "Apartadó",
        "abreviacion": "Apartadó",
        "createdAt": "2020-01-16T13:31:28.634Z",
        "updatedAt": "2020-01-16T13:31:28.634Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d00085268fd"
        },
        "nombre": "Concepción",
        "descripcion": "Concepción",
        "abreviacion": "Concepción",
        "createdAt": "2020-01-16T13:31:28.636Z",
        "updatedAt": "2020-01-16T13:31:28.636Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d00085268fe"
        },
        "nombre": "San Rafael",
        "descripcion": "San Rafael",
        "abreviacion": "San Rafael",
        "createdAt": "2020-01-16T13:31:28.637Z",
        "updatedAt": "2020-01-16T13:31:28.637Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d00085268ff"
        },
        "nombre": "Caldas",
        "descripcion": "Caldas",
        "abreviacion": "Caldas",
        "createdAt": "2020-01-16T13:31:28.644Z",
        "updatedAt": "2020-01-16T13:31:28.644Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526900"
        },
        "nombre": "Casabianca",
        "descripcion": "Casabianca",
        "abreviacion": "Casabianca",
        "createdAt": "2020-01-16T13:31:28.646Z",
        "updatedAt": "2020-01-16T13:31:28.646Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526901"
        },
        "nombre": "La Estrella",
        "descripcion": "La Estrella",
        "abreviacion": "La Estrella",
        "createdAt": "2020-01-16T13:31:28.649Z",
        "updatedAt": "2020-01-16T13:31:28.649Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526902"
        },
        "nombre": "Ituango",
        "descripcion": "Ituango",
        "abreviacion": "Ituango",
        "createdAt": "2020-01-16T13:31:28.659Z",
        "updatedAt": "2020-01-16T13:31:28.659Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526903"
        },
        "nombre": "El Bagre",
        "descripcion": "El Bagre",
        "abreviacion": "El Bagre",
        "createdAt": "2020-01-16T13:31:28.664Z",
        "updatedAt": "2020-01-16T13:31:28.664Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526904"
        },
        "nombre": "Sampués",
        "descripcion": "Sampués",
        "abreviacion": "Sampués",
        "createdAt": "2020-01-16T13:31:28.666Z",
        "updatedAt": "2020-01-16T13:31:28.666Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526905"
        },
        "nombre": "Dabeiba",
        "descripcion": "Dabeiba",
        "abreviacion": "Dabeiba",
        "createdAt": "2020-01-16T13:31:28.671Z",
        "updatedAt": "2020-01-16T13:31:28.671Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526906"
        },
        "nombre": "Tubará",
        "descripcion": "Tubará",
        "abreviacion": "Tubará",
        "createdAt": "2020-01-16T13:31:28.672Z",
        "updatedAt": "2020-01-16T13:31:28.672Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526907"
        },
        "nombre": "Yondó",
        "descripcion": "Yondó",
        "abreviacion": "Yondó",
        "createdAt": "2020-01-16T13:31:28.674Z",
        "updatedAt": "2020-01-16T13:31:28.674Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526908"
        },
        "nombre": "Briceño",
        "descripcion": "Briceño",
        "abreviacion": "Briceño",
        "createdAt": "2020-01-16T13:31:28.677Z",
        "updatedAt": "2020-01-16T13:31:28.677Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526909"
        },
        "nombre": "Capitanejo",
        "descripcion": "Capitanejo",
        "abreviacion": "Capitanejo",
        "createdAt": "2020-01-16T13:31:28.684Z",
        "updatedAt": "2020-01-16T13:31:28.684Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d000852690a"
        },
        "nombre": "Uramita",
        "descripcion": "Uramita",
        "abreviacion": "Uramita",
        "createdAt": "2020-01-16T13:31:28.686Z",
        "updatedAt": "2020-01-16T13:31:28.686Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d000852690b"
        },
        "nombre": "Cocorná",
        "descripcion": "Cocorná",
        "abreviacion": "Cocorná",
        "createdAt": "2020-01-16T13:31:28.688Z",
        "updatedAt": "2020-01-16T13:31:28.688Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d000852690c"
        },
        "nombre": "Buriticá",
        "descripcion": "Buriticá",
        "abreviacion": "Buriticá",
        "createdAt": "2020-01-16T13:31:28.690Z",
        "updatedAt": "2020-01-16T13:31:28.690Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d000852690d"
        },
        "nombre": "Sabanalarga",
        "descripcion": "Sabanalarga",
        "abreviacion": "Sabanalarga",
        "createdAt": "2020-01-16T13:31:28.709Z",
        "updatedAt": "2020-01-16T13:31:28.709Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d000852690e"
        },
        "nombre": "Guatapé",
        "descripcion": "Guatapé",
        "abreviacion": "Guatapé",
        "createdAt": "2020-01-16T13:31:28.711Z",
        "updatedAt": "2020-01-16T13:31:28.711Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d000852690f"
        },
        "nombre": "Calamar",
        "descripcion": "Calamar",
        "abreviacion": "Calamar",
        "createdAt": "2020-01-16T13:31:28.714Z",
        "updatedAt": "2020-01-16T13:31:28.714Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526910"
        },
        "nombre": "San Luis",
        "descripcion": "San Luis",
        "abreviacion": "San Luis",
        "createdAt": "2020-01-16T13:31:28.724Z",
        "updatedAt": "2020-01-16T13:31:28.724Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526911"
        },
        "nombre": "Arroyohondo",
        "descripcion": "Arroyohondo",
        "abreviacion": "Arroyohondo",
        "createdAt": "2020-01-16T13:31:28.726Z",
        "updatedAt": "2020-01-16T13:31:28.726Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526912"
        },
        "nombre": "Nunchía",
        "descripcion": "Nunchía",
        "abreviacion": "Nunchía",
        "createdAt": "2020-01-16T13:31:28.733Z",
        "updatedAt": "2020-01-16T13:31:28.733Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526913"
        },
        "nombre": "Guarne",
        "descripcion": "Guarne",
        "abreviacion": "Guarne",
        "createdAt": "2020-01-16T13:31:28.737Z",
        "updatedAt": "2020-01-16T13:31:28.737Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526914"
        },
        "nombre": "Necoclí",
        "descripcion": "Necoclí",
        "abreviacion": "Necoclí",
        "createdAt": "2020-01-16T13:31:28.749Z",
        "updatedAt": "2020-01-16T13:31:28.749Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526915"
        },
        "nombre": "Montebello",
        "descripcion": "Montebello",
        "abreviacion": "Montebello",
        "createdAt": "2020-01-16T13:31:28.753Z",
        "updatedAt": "2020-01-16T13:31:28.753Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526916"
        },
        "nombre": "Corozal",
        "descripcion": "Corozal",
        "abreviacion": "Corozal",
        "createdAt": "2020-01-16T13:31:28.754Z",
        "updatedAt": "2020-01-16T13:31:28.754Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526917"
        },
        "nombre": "Albán",
        "descripcion": "Albán",
        "abreviacion": "Albán",
        "createdAt": "2020-01-16T13:31:28.757Z",
        "updatedAt": "2020-01-16T13:31:28.757Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526918"
        },
        "nombre": "Rionegro",
        "descripcion": "Rionegro",
        "abreviacion": "Rionegro",
        "createdAt": "2020-01-16T13:31:28.760Z",
        "updatedAt": "2020-01-16T13:31:28.760Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526919"
        },
        "nombre": "Carolina",
        "descripcion": "Carolina",
        "abreviacion": "Carolina",
        "createdAt": "2020-01-16T13:31:28.761Z",
        "updatedAt": "2020-01-16T13:31:28.761Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d000852691a"
        },
        "nombre": "Soledad",
        "descripcion": "Soledad",
        "abreviacion": "Soledad",
        "createdAt": "2020-01-16T13:31:28.769Z",
        "updatedAt": "2020-01-16T13:31:28.769Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d000852691b"
        },
        "nombre": "Fúquene",
        "descripcion": "Fúquene",
        "abreviacion": "Fúquene",
        "createdAt": "2020-01-16T13:31:28.770Z",
        "updatedAt": "2020-01-16T13:31:28.770Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d000852691c"
        },
        "nombre": "Peñol",
        "descripcion": "Peñol",
        "abreviacion": "Peñol",
        "createdAt": "2020-01-16T13:31:28.771Z",
        "updatedAt": "2020-01-16T13:31:28.771Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d000852691d"
        },
        "nombre": "Sabanagrande",
        "descripcion": "Sabanagrande",
        "abreviacion": "Sabanagrande",
        "createdAt": "2020-01-16T13:31:28.789Z",
        "updatedAt": "2020-01-16T13:31:28.789Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d000852691e"
        },
        "nombre": "Margarita",
        "descripcion": "Margarita",
        "abreviacion": "Margarita",
        "createdAt": "2020-01-16T13:31:28.815Z",
        "updatedAt": "2020-01-16T13:31:28.815Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d000852691f"
        },
        "nombre": "Nechí",
        "descripcion": "Nechí",
        "abreviacion": "Nechí",
        "createdAt": "2020-01-16T13:31:28.824Z",
        "updatedAt": "2020-01-16T13:31:28.824Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526920"
        },
        "nombre": "Campamento",
        "descripcion": "Campamento",
        "abreviacion": "Campamento",
        "createdAt": "2020-01-16T13:31:28.828Z",
        "updatedAt": "2020-01-16T13:31:28.828Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526921"
        },
        "nombre": "El Santuario",
        "descripcion": "El Santuario",
        "abreviacion": "El Santuario",
        "createdAt": "2020-01-16T13:31:28.830Z",
        "updatedAt": "2020-01-16T13:31:28.830Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526922"
        },
        "nombre": "Turbo",
        "descripcion": "Turbo",
        "abreviacion": "Turbo",
        "createdAt": "2020-01-16T13:31:28.837Z",
        "updatedAt": "2020-01-16T13:31:28.837Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526923"
        },
        "nombre": "San Andrés de Tumaco",
        "descripcion": "San Andrés de Tumaco",
        "abreviacion": "San Andrés de Tumaco",
        "createdAt": "2020-01-16T13:31:28.838Z",
        "updatedAt": "2020-01-16T13:31:28.838Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526924"
        },
        "nombre": "Polonuevo",
        "descripcion": "Polonuevo",
        "abreviacion": "Polonuevo",
        "createdAt": "2020-01-16T13:31:28.840Z",
        "updatedAt": "2020-01-16T13:31:28.840Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526925"
        },
        "nombre": "Puerto Nare",
        "descripcion": "Puerto Nare",
        "abreviacion": "Puerto Nare",
        "createdAt": "2020-01-16T13:31:28.841Z",
        "updatedAt": "2020-01-16T13:31:28.841Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526926"
        },
        "nombre": "Chinavita",
        "descripcion": "Chinavita",
        "abreviacion": "Chinavita",
        "createdAt": "2020-01-16T13:31:28.843Z",
        "updatedAt": "2020-01-16T13:31:28.843Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526927"
        },
        "nombre": "Caicedo",
        "descripcion": "Caicedo",
        "abreviacion": "Caicedo",
        "createdAt": "2020-01-16T13:31:28.860Z",
        "updatedAt": "2020-01-16T13:31:28.860Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526928"
        },
        "nombre": "Segovia",
        "descripcion": "Segovia",
        "abreviacion": "Segovia",
        "createdAt": "2020-01-16T13:31:28.869Z",
        "updatedAt": "2020-01-16T13:31:28.869Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526929"
        },
        "nombre": "Tutazá",
        "descripcion": "Tutazá",
        "abreviacion": "Tutazá",
        "createdAt": "2020-01-16T13:31:28.870Z",
        "updatedAt": "2020-01-16T13:31:28.870Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d000852692a"
        },
        "nombre": "Hispania",
        "descripcion": "Hispania",
        "abreviacion": "Hispania",
        "createdAt": "2020-01-16T13:31:28.879Z",
        "updatedAt": "2020-01-16T13:31:28.879Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d000852692b"
        },
        "nombre": "Aguadas",
        "descripcion": "Aguadas",
        "abreviacion": "Aguadas",
        "createdAt": "2020-01-16T13:31:28.889Z",
        "updatedAt": "2020-01-16T13:31:28.889Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d000852692c"
        },
        "nombre": "Sabaneta",
        "descripcion": "Sabaneta",
        "abreviacion": "Sabaneta",
        "createdAt": "2020-01-16T13:31:28.891Z",
        "updatedAt": "2020-01-16T13:31:28.891Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d000852692d"
        },
        "nombre": "Somondoco",
        "descripcion": "Somondoco",
        "abreviacion": "Somondoco",
        "createdAt": "2020-01-16T13:31:28.898Z",
        "updatedAt": "2020-01-16T13:31:28.898Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d000852692e"
        },
        "nombre": "Sopó",
        "descripcion": "Sopó",
        "abreviacion": "Sopó",
        "createdAt": "2020-01-16T13:31:28.904Z",
        "updatedAt": "2020-01-16T13:31:28.904Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d000852692f"
        },
        "nombre": "Heliconia",
        "descripcion": "Heliconia",
        "abreviacion": "Heliconia",
        "createdAt": "2020-01-16T13:31:28.905Z",
        "updatedAt": "2020-01-16T13:31:28.905Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526930"
        },
        "nombre": "Sogamoso",
        "descripcion": "Sogamoso",
        "abreviacion": "Sogamoso",
        "createdAt": "2020-01-16T13:31:28.916Z",
        "updatedAt": "2020-01-16T13:31:28.916Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526931"
        },
        "nombre": "Sativanorte",
        "descripcion": "Sativanorte",
        "abreviacion": "Sativanorte",
        "createdAt": "2020-01-16T13:31:28.920Z",
        "updatedAt": "2020-01-16T13:31:28.920Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526932"
        },
        "nombre": "Socha",
        "descripcion": "Socha",
        "abreviacion": "Socha",
        "createdAt": "2020-01-16T13:31:28.925Z",
        "updatedAt": "2020-01-16T13:31:28.925Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526933"
        },
        "nombre": "Caqueza",
        "descripcion": "Caqueza",
        "abreviacion": "Caqueza",
        "createdAt": "2020-01-16T13:31:28.926Z",
        "updatedAt": "2020-01-16T13:31:28.926Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526934"
        },
        "nombre": "Girardota",
        "descripcion": "Girardota",
        "abreviacion": "Girardota",
        "createdAt": "2020-01-16T13:31:28.927Z",
        "updatedAt": "2020-01-16T13:31:28.927Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526935"
        },
        "nombre": "Don Matías",
        "descripcion": "Don Matías",
        "abreviacion": "Don Matías",
        "createdAt": "2020-01-16T13:31:28.929Z",
        "updatedAt": "2020-01-16T13:31:28.929Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526936"
        },
        "nombre": "Chía",
        "descripcion": "Chía",
        "abreviacion": "Chía",
        "createdAt": "2020-01-16T13:31:28.937Z",
        "updatedAt": "2020-01-16T13:31:28.937Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526937"
        },
        "nombre": "Puerto Asís",
        "descripcion": "Puerto Asís",
        "abreviacion": "Puerto Asís",
        "createdAt": "2020-01-16T13:31:28.938Z",
        "updatedAt": "2020-01-16T13:31:28.938Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526938"
        },
        "nombre": "Montelíbano",
        "descripcion": "Montelíbano",
        "abreviacion": "Montelíbano",
        "createdAt": "2020-01-16T13:31:28.939Z",
        "updatedAt": "2020-01-16T13:31:28.939Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526939"
        },
        "nombre": "Maní",
        "descripcion": "Maní",
        "abreviacion": "Maní",
        "createdAt": "2020-01-16T13:31:28.941Z",
        "updatedAt": "2020-01-16T13:31:28.941Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d000852693a"
        },
        "nombre": "Santa Lucía",
        "descripcion": "Santa Lucía",
        "abreviacion": "Santa Lucía",
        "createdAt": "2020-01-16T13:31:28.942Z",
        "updatedAt": "2020-01-16T13:31:28.942Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d000852693b"
        },
        "nombre": "Gama",
        "descripcion": "Gama",
        "abreviacion": "Gama",
        "createdAt": "2020-01-16T13:31:28.949Z",
        "updatedAt": "2020-01-16T13:31:28.949Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d000852693c"
        },
        "nombre": "Simití",
        "descripcion": "Simití",
        "abreviacion": "Simití",
        "createdAt": "2020-01-16T13:31:28.950Z",
        "updatedAt": "2020-01-16T13:31:28.950Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d000852693d"
        },
        "nombre": "Miraflores",
        "descripcion": "Miraflores",
        "abreviacion": "Miraflores",
        "createdAt": "2020-01-16T13:31:28.951Z",
        "updatedAt": "2020-01-16T13:31:28.951Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d000852693e"
        },
        "nombre": "Boavita",
        "descripcion": "Boavita",
        "abreviacion": "Boavita",
        "createdAt": "2020-01-16T13:31:28.953Z",
        "updatedAt": "2020-01-16T13:31:28.953Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d000852693f"
        },
        "nombre": "Ramiriquí",
        "descripcion": "Ramiriquí",
        "abreviacion": "Ramiriquí",
        "createdAt": "2020-01-16T13:31:28.954Z",
        "updatedAt": "2020-01-16T13:31:28.954Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b07c6e3d0008526940"
        },
        "nombre": "Regidor",
        "descripcion": "Regidor",
        "abreviacion": "Regidor",
        "createdAt": "2020-01-16T13:31:28.957Z",
        "updatedAt": "2020-01-16T13:31:28.957Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526941"
        },
        "nombre": "Campohermoso",
        "descripcion": "Campohermoso",
        "abreviacion": "Campohermoso",
        "createdAt": "2020-01-16T13:31:29.007Z",
        "updatedAt": "2020-01-16T13:31:29.007Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526942"
        },
        "nombre": "Nuevo Colón",
        "descripcion": "Nuevo Colón",
        "abreviacion": "Nuevo Colón",
        "createdAt": "2020-01-16T13:31:29.009Z",
        "updatedAt": "2020-01-16T13:31:29.009Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526943"
        },
        "nombre": "Tunja",
        "descripcion": "Tunja",
        "abreviacion": "Tunja",
        "createdAt": "2020-01-16T13:31:29.013Z",
        "updatedAt": "2020-01-16T13:31:29.013Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526944"
        },
        "nombre": "Chivatá",
        "descripcion": "Chivatá",
        "abreviacion": "Chivatá",
        "createdAt": "2020-01-16T13:31:29.014Z",
        "updatedAt": "2020-01-16T13:31:29.014Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526945"
        },
        "nombre": "Jericó",
        "descripcion": "Jericó",
        "abreviacion": "Jericó",
        "createdAt": "2020-01-16T13:31:29.019Z",
        "updatedAt": "2020-01-16T13:31:29.019Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526946"
        },
        "nombre": "Tausa",
        "descripcion": "Tausa",
        "abreviacion": "Tausa",
        "createdAt": "2020-01-16T13:31:29.021Z",
        "updatedAt": "2020-01-16T13:31:29.021Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526947"
        },
        "nombre": "Soplaviento",
        "descripcion": "Soplaviento",
        "abreviacion": "Soplaviento",
        "createdAt": "2020-01-16T13:31:29.022Z",
        "updatedAt": "2020-01-16T13:31:29.022Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526948"
        },
        "nombre": "Paya",
        "descripcion": "Paya",
        "abreviacion": "Paya",
        "createdAt": "2020-01-16T13:31:29.029Z",
        "updatedAt": "2020-01-16T13:31:29.029Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526949"
        },
        "nombre": "Almeida",
        "descripcion": "Almeida",
        "abreviacion": "Almeida",
        "createdAt": "2020-01-16T13:31:29.030Z",
        "updatedAt": "2020-01-16T13:31:29.030Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852694a"
        },
        "nombre": "Cómbita",
        "descripcion": "Cómbita",
        "abreviacion": "Cómbita",
        "createdAt": "2020-01-16T13:31:29.031Z",
        "updatedAt": "2020-01-16T13:31:29.031Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852694b"
        },
        "nombre": "Guateque",
        "descripcion": "Guateque",
        "abreviacion": "Guateque",
        "createdAt": "2020-01-16T13:31:29.033Z",
        "updatedAt": "2020-01-16T13:31:29.033Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852694c"
        },
        "nombre": "Río Viejo",
        "descripcion": "Río Viejo",
        "abreviacion": "Río Viejo",
        "createdAt": "2020-01-16T13:31:29.034Z",
        "updatedAt": "2020-01-16T13:31:29.034Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852694d"
        },
        "nombre": "Chíquiza",
        "descripcion": "Chíquiza",
        "abreviacion": "Chíquiza",
        "createdAt": "2020-01-16T13:31:29.040Z",
        "updatedAt": "2020-01-16T13:31:29.040Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852694e"
        },
        "nombre": "Pesca",
        "descripcion": "Pesca",
        "abreviacion": "Pesca",
        "createdAt": "2020-01-16T13:31:29.042Z",
        "updatedAt": "2020-01-16T13:31:29.042Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852694f"
        },
        "nombre": "San Juan Nepomuceno",
        "descripcion": "San Juan Nepomuceno",
        "abreviacion": "San Juan Nepomuceno",
        "createdAt": "2020-01-16T13:31:29.043Z",
        "updatedAt": "2020-01-16T13:31:29.043Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526950"
        },
        "nombre": "San Fernando",
        "descripcion": "San Fernando",
        "abreviacion": "San Fernando",
        "createdAt": "2020-01-16T13:31:29.044Z",
        "updatedAt": "2020-01-16T13:31:29.044Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526951"
        },
        "nombre": "Pamplonita",
        "descripcion": "Pamplonita",
        "abreviacion": "Pamplonita",
        "createdAt": "2020-01-16T13:31:29.049Z",
        "updatedAt": "2020-01-16T13:31:29.049Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526952"
        },
        "nombre": "Miriti Paraná",
        "descripcion": "Miriti Paraná",
        "abreviacion": "Miriti Paraná",
        "createdAt": "2020-01-16T13:31:29.050Z",
        "updatedAt": "2020-01-16T13:31:29.050Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526953"
        },
        "nombre": "Turbaco",
        "descripcion": "Turbaco",
        "abreviacion": "Turbaco",
        "createdAt": "2020-01-16T13:31:29.052Z",
        "updatedAt": "2020-01-16T13:31:29.052Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526954"
        },
        "nombre": "Támara",
        "descripcion": "Támara",
        "abreviacion": "Támara",
        "createdAt": "2020-01-16T13:31:29.056Z",
        "updatedAt": "2020-01-16T13:31:29.056Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526955"
        },
        "nombre": "Susa",
        "descripcion": "Susa",
        "abreviacion": "Susa",
        "createdAt": "2020-01-16T13:31:29.057Z",
        "updatedAt": "2020-01-16T13:31:29.057Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526956"
        },
        "nombre": "Güicán",
        "descripcion": "Güicán",
        "abreviacion": "Güicán",
        "createdAt": "2020-01-16T13:31:29.058Z",
        "updatedAt": "2020-01-16T13:31:29.058Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526957"
        },
        "nombre": "Junín",
        "descripcion": "Junín",
        "abreviacion": "Junín",
        "createdAt": "2020-01-16T13:31:29.062Z",
        "updatedAt": "2020-01-16T13:31:29.062Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526958"
        },
        "nombre": "Lenguazaque",
        "descripcion": "Lenguazaque",
        "abreviacion": "Lenguazaque",
        "createdAt": "2020-01-16T13:31:29.069Z",
        "updatedAt": "2020-01-16T13:31:29.069Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526959"
        },
        "nombre": "Gachetá",
        "descripcion": "Gachetá",
        "abreviacion": "Gachetá",
        "createdAt": "2020-01-16T13:31:29.070Z",
        "updatedAt": "2020-01-16T13:31:29.070Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852695a"
        },
        "nombre": "Facatativá",
        "descripcion": "Facatativá",
        "abreviacion": "Facatativá",
        "createdAt": "2020-01-16T13:31:29.071Z",
        "updatedAt": "2020-01-16T13:31:29.071Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852695b"
        },
        "nombre": "Yalí",
        "descripcion": "Yalí",
        "abreviacion": "Yalí",
        "createdAt": "2020-01-16T13:31:29.073Z",
        "updatedAt": "2020-01-16T13:31:29.073Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852695c"
        },
        "nombre": "Riosucio",
        "descripcion": "Riosucio",
        "abreviacion": "Riosucio",
        "createdAt": "2020-01-16T13:31:29.074Z",
        "updatedAt": "2020-01-16T13:31:29.074Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852695d"
        },
        "nombre": "Lloró",
        "descripcion": "Lloró",
        "abreviacion": "Lloró",
        "createdAt": "2020-01-16T13:31:29.080Z",
        "updatedAt": "2020-01-16T13:31:29.080Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852695e"
        },
        "nombre": "Chimá",
        "descripcion": "Chimá",
        "abreviacion": "Chimá",
        "createdAt": "2020-01-16T13:31:29.082Z",
        "updatedAt": "2020-01-16T13:31:29.082Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852695f"
        },
        "nombre": "Itagui",
        "descripcion": "Itagui",
        "abreviacion": "Itagui",
        "createdAt": "2020-01-16T13:31:29.100Z",
        "updatedAt": "2020-01-16T13:31:29.100Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526960"
        },
        "nombre": "Unguía",
        "descripcion": "Unguía",
        "abreviacion": "Unguía",
        "createdAt": "2020-01-16T13:31:29.102Z",
        "updatedAt": "2020-01-16T13:31:29.102Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526961"
        },
        "nombre": "Vianí",
        "descripcion": "Vianí",
        "abreviacion": "Vianí",
        "createdAt": "2020-01-16T13:31:29.111Z",
        "updatedAt": "2020-01-16T13:31:29.111Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526962"
        },
        "nombre": "Briceño",
        "descripcion": "Briceño",
        "abreviacion": "Briceño",
        "createdAt": "2020-01-16T13:31:29.121Z",
        "updatedAt": "2020-01-16T13:31:29.121Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526963"
        },
        "nombre": "Chivor",
        "descripcion": "Chivor",
        "abreviacion": "Chivor",
        "createdAt": "2020-01-16T13:31:29.123Z",
        "updatedAt": "2020-01-16T13:31:29.123Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526964"
        },
        "nombre": "San Estanislao",
        "descripcion": "San Estanislao",
        "abreviacion": "San Estanislao",
        "createdAt": "2020-01-16T13:31:29.127Z",
        "updatedAt": "2020-01-16T13:31:29.127Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526965"
        },
        "nombre": "Sopetrán",
        "descripcion": "Sopetrán",
        "abreviacion": "Sopetrán",
        "createdAt": "2020-01-16T13:31:29.137Z",
        "updatedAt": "2020-01-16T13:31:29.137Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526966"
        },
        "nombre": "Oporapa",
        "descripcion": "Oporapa",
        "abreviacion": "Oporapa",
        "createdAt": "2020-01-16T13:31:29.138Z",
        "updatedAt": "2020-01-16T13:31:29.138Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526967"
        },
        "nombre": "Suárez",
        "descripcion": "Suárez",
        "abreviacion": "Suárez",
        "createdAt": "2020-01-16T13:31:29.146Z",
        "updatedAt": "2020-01-16T13:31:29.146Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526968"
        },
        "nombre": "Útica",
        "descripcion": "Útica",
        "abreviacion": "Útica",
        "createdAt": "2020-01-16T13:31:29.148Z",
        "updatedAt": "2020-01-16T13:31:29.148Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526969"
        },
        "nombre": "Andes",
        "descripcion": "Andes",
        "abreviacion": "Andes",
        "createdAt": "2020-01-16T13:31:29.151Z",
        "updatedAt": "2020-01-16T13:31:29.151Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852696a"
        },
        "nombre": "Sesquilé",
        "descripcion": "Sesquilé",
        "abreviacion": "Sesquilé",
        "createdAt": "2020-01-16T13:31:29.157Z",
        "updatedAt": "2020-01-16T13:31:29.157Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852696b"
        },
        "nombre": "Madrid",
        "descripcion": "Madrid",
        "abreviacion": "Madrid",
        "createdAt": "2020-01-16T13:31:29.197Z",
        "updatedAt": "2020-01-16T13:31:29.197Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852696c"
        },
        "nombre": "Nimaima",
        "descripcion": "Nimaima",
        "abreviacion": "Nimaima",
        "createdAt": "2020-01-16T13:31:29.205Z",
        "updatedAt": "2020-01-16T13:31:29.205Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852696d"
        },
        "nombre": "Tenjo",
        "descripcion": "Tenjo",
        "abreviacion": "Tenjo",
        "createdAt": "2020-01-16T13:31:29.206Z",
        "updatedAt": "2020-01-16T13:31:29.206Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852696e"
        },
        "nombre": "Apulo",
        "descripcion": "Apulo",
        "abreviacion": "Apulo",
        "createdAt": "2020-01-16T13:31:29.208Z",
        "updatedAt": "2020-01-16T13:31:29.208Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852696f"
        },
        "nombre": "Fosca",
        "descripcion": "Fosca",
        "abreviacion": "Fosca",
        "createdAt": "2020-01-16T13:31:29.209Z",
        "updatedAt": "2020-01-16T13:31:29.209Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526970"
        },
        "nombre": "Anapoima",
        "descripcion": "Anapoima",
        "abreviacion": "Anapoima",
        "createdAt": "2020-01-16T13:31:29.212Z",
        "updatedAt": "2020-01-16T13:31:29.212Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526971"
        },
        "nombre": "Puerto Salgar",
        "descripcion": "Puerto Salgar",
        "abreviacion": "Puerto Salgar",
        "createdAt": "2020-01-16T13:31:29.214Z",
        "updatedAt": "2020-01-16T13:31:29.214Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526972"
        },
        "nombre": "Cachipay",
        "descripcion": "Cachipay",
        "abreviacion": "Cachipay",
        "createdAt": "2020-01-16T13:31:29.218Z",
        "updatedAt": "2020-01-16T13:31:29.218Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526973"
        },
        "nombre": "Cantagallo",
        "descripcion": "Cantagallo",
        "abreviacion": "Cantagallo",
        "createdAt": "2020-01-16T13:31:29.220Z",
        "updatedAt": "2020-01-16T13:31:29.220Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526974"
        },
        "nombre": "El Peñón",
        "descripcion": "El Peñón",
        "abreviacion": "El Peñón",
        "createdAt": "2020-01-16T13:31:29.221Z",
        "updatedAt": "2020-01-16T13:31:29.221Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526975"
        },
        "nombre": "Purísima",
        "descripcion": "Purísima",
        "abreviacion": "Purísima",
        "createdAt": "2020-01-16T13:31:29.228Z",
        "updatedAt": "2020-01-16T13:31:29.228Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526976"
        },
        "nombre": "Santo Tomás",
        "descripcion": "Santo Tomás",
        "abreviacion": "Santo Tomás",
        "createdAt": "2020-01-16T13:31:29.230Z",
        "updatedAt": "2020-01-16T13:31:29.230Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526977"
        },
        "nombre": "Marulanda",
        "descripcion": "Marulanda",
        "abreviacion": "Marulanda",
        "createdAt": "2020-01-16T13:31:29.231Z",
        "updatedAt": "2020-01-16T13:31:29.231Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526978"
        },
        "nombre": "Anzoátegui",
        "descripcion": "Anzoátegui",
        "abreviacion": "Anzoátegui",
        "createdAt": "2020-01-16T13:31:29.232Z",
        "updatedAt": "2020-01-16T13:31:29.232Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526979"
        },
        "nombre": "Titiribí",
        "descripcion": "Titiribí",
        "abreviacion": "Titiribí",
        "createdAt": "2020-01-16T13:31:29.234Z",
        "updatedAt": "2020-01-16T13:31:29.234Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852697a"
        },
        "nombre": "Los Córdobas",
        "descripcion": "Los Córdobas",
        "abreviacion": "Los Córdobas",
        "createdAt": "2020-01-16T13:31:29.238Z",
        "updatedAt": "2020-01-16T13:31:29.238Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852697b"
        },
        "nombre": "Monguí",
        "descripcion": "Monguí",
        "abreviacion": "Monguí",
        "createdAt": "2020-01-16T13:31:29.240Z",
        "updatedAt": "2020-01-16T13:31:29.240Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852697c"
        },
        "nombre": "Puerto Boyacá",
        "descripcion": "Puerto Boyacá",
        "abreviacion": "Puerto Boyacá",
        "createdAt": "2020-01-16T13:31:29.248Z",
        "updatedAt": "2020-01-16T13:31:29.248Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852697d"
        },
        "nombre": "Moniquirá",
        "descripcion": "Moniquirá",
        "abreviacion": "Moniquirá",
        "createdAt": "2020-01-16T13:31:29.253Z",
        "updatedAt": "2020-01-16T13:31:29.253Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852697e"
        },
        "nombre": "Siachoque",
        "descripcion": "Siachoque",
        "abreviacion": "Siachoque",
        "createdAt": "2020-01-16T13:31:29.254Z",
        "updatedAt": "2020-01-16T13:31:29.254Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852697f"
        },
        "nombre": "Chiscas",
        "descripcion": "Chiscas",
        "abreviacion": "Chiscas",
        "createdAt": "2020-01-16T13:31:29.256Z",
        "updatedAt": "2020-01-16T13:31:29.256Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526980"
        },
        "nombre": "Guayatá",
        "descripcion": "Guayatá",
        "abreviacion": "Guayatá",
        "createdAt": "2020-01-16T13:31:29.273Z",
        "updatedAt": "2020-01-16T13:31:29.273Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526981"
        },
        "nombre": "Villanueva",
        "descripcion": "Villanueva",
        "abreviacion": "Villanueva",
        "createdAt": "2020-01-16T13:31:29.275Z",
        "updatedAt": "2020-01-16T13:31:29.275Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526982"
        },
        "nombre": "Gachantivá",
        "descripcion": "Gachantivá",
        "abreviacion": "Gachantivá",
        "createdAt": "2020-01-16T13:31:29.280Z",
        "updatedAt": "2020-01-16T13:31:29.280Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526983"
        },
        "nombre": "Panqueba",
        "descripcion": "Panqueba",
        "abreviacion": "Panqueba",
        "createdAt": "2020-01-16T13:31:29.284Z",
        "updatedAt": "2020-01-16T13:31:29.284Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526984"
        },
        "nombre": "Tópaga",
        "descripcion": "Tópaga",
        "abreviacion": "Tópaga",
        "createdAt": "2020-01-16T13:31:29.287Z",
        "updatedAt": "2020-01-16T13:31:29.287Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526985"
        },
        "nombre": "Floresta",
        "descripcion": "Floresta",
        "abreviacion": "Floresta",
        "createdAt": "2020-01-16T13:31:29.294Z",
        "updatedAt": "2020-01-16T13:31:29.294Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526986"
        },
        "nombre": "Cúcuta",
        "descripcion": "Cúcuta",
        "abreviacion": "Cúcuta",
        "createdAt": "2020-01-16T13:31:29.295Z",
        "updatedAt": "2020-01-16T13:31:29.295Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526987"
        },
        "nombre": "Sotaquirá",
        "descripcion": "Sotaquirá",
        "abreviacion": "Sotaquirá",
        "createdAt": "2020-01-16T13:31:29.301Z",
        "updatedAt": "2020-01-16T13:31:29.301Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526988"
        },
        "nombre": "Gameza",
        "descripcion": "Gameza",
        "abreviacion": "Gameza",
        "createdAt": "2020-01-16T13:31:29.302Z",
        "updatedAt": "2020-01-16T13:31:29.302Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526989"
        },
        "nombre": "Betéitiva",
        "descripcion": "Betéitiva",
        "abreviacion": "Betéitiva",
        "createdAt": "2020-01-16T13:31:29.303Z",
        "updatedAt": "2020-01-16T13:31:29.303Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852698a"
        },
        "nombre": "Puerto Colombia",
        "descripcion": "Puerto Colombia",
        "abreviacion": "Puerto Colombia",
        "createdAt": "2020-01-16T13:31:29.305Z",
        "updatedAt": "2020-01-16T13:31:29.305Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852698b"
        },
        "nombre": "Pisba",
        "descripcion": "Pisba",
        "abreviacion": "Pisba",
        "createdAt": "2020-01-16T13:31:29.313Z",
        "updatedAt": "2020-01-16T13:31:29.313Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852698c"
        },
        "nombre": "Pajarito",
        "descripcion": "Pajarito",
        "abreviacion": "Pajarito",
        "createdAt": "2020-01-16T13:31:29.314Z",
        "updatedAt": "2020-01-16T13:31:29.314Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852698d"
        },
        "nombre": "Talaigua Nuevo",
        "descripcion": "Talaigua Nuevo",
        "abreviacion": "Talaigua Nuevo",
        "createdAt": "2020-01-16T13:31:29.315Z",
        "updatedAt": "2020-01-16T13:31:29.315Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852698e"
        },
        "nombre": "Turbaná",
        "descripcion": "Turbaná",
        "abreviacion": "Turbaná",
        "createdAt": "2020-01-16T13:31:29.317Z",
        "updatedAt": "2020-01-16T13:31:29.317Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852698f"
        },
        "nombre": "Berbeo",
        "descripcion": "Berbeo",
        "abreviacion": "Berbeo",
        "createdAt": "2020-01-16T13:31:29.318Z",
        "updatedAt": "2020-01-16T13:31:29.318Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526990"
        },
        "nombre": "Buena Vista",
        "descripcion": "Buena Vista",
        "abreviacion": "Buena Vista",
        "createdAt": "2020-01-16T13:31:29.319Z",
        "updatedAt": "2020-01-16T13:31:29.319Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526991"
        },
        "nombre": "Tibasosa",
        "descripcion": "Tibasosa",
        "abreviacion": "Tibasosa",
        "createdAt": "2020-01-16T13:31:29.325Z",
        "updatedAt": "2020-01-16T13:31:29.325Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526992"
        },
        "nombre": "Arcabuco",
        "descripcion": "Arcabuco",
        "abreviacion": "Arcabuco",
        "createdAt": "2020-01-16T13:31:29.326Z",
        "updatedAt": "2020-01-16T13:31:29.326Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526993"
        },
        "nombre": "Páez",
        "descripcion": "Páez",
        "abreviacion": "Páez",
        "createdAt": "2020-01-16T13:31:29.327Z",
        "updatedAt": "2020-01-16T13:31:29.327Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526994"
        },
        "nombre": "Tibirita",
        "descripcion": "Tibirita",
        "abreviacion": "Tibirita",
        "createdAt": "2020-01-16T13:31:29.329Z",
        "updatedAt": "2020-01-16T13:31:29.329Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526995"
        },
        "nombre": "Medina",
        "descripcion": "Medina",
        "abreviacion": "Medina",
        "createdAt": "2020-01-16T13:31:29.333Z",
        "updatedAt": "2020-01-16T13:31:29.333Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526996"
        },
        "nombre": "Génova",
        "descripcion": "Génova",
        "abreviacion": "Génova",
        "createdAt": "2020-01-16T13:31:29.334Z",
        "updatedAt": "2020-01-16T13:31:29.334Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526997"
        },
        "nombre": "Aipe",
        "descripcion": "Aipe",
        "abreviacion": "Aipe",
        "createdAt": "2020-01-16T13:31:29.336Z",
        "updatedAt": "2020-01-16T13:31:29.336Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526998"
        },
        "nombre": "La Vega",
        "descripcion": "La Vega",
        "abreviacion": "La Vega",
        "createdAt": "2020-01-16T13:31:29.340Z",
        "updatedAt": "2020-01-16T13:31:29.340Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526999"
        },
        "nombre": "San Francisco",
        "descripcion": "San Francisco",
        "abreviacion": "San Francisco",
        "createdAt": "2020-01-16T13:31:29.342Z",
        "updatedAt": "2020-01-16T13:31:29.342Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852699a"
        },
        "nombre": "Galapa",
        "descripcion": "Galapa",
        "abreviacion": "Galapa",
        "createdAt": "2020-01-16T13:31:29.343Z",
        "updatedAt": "2020-01-16T13:31:29.343Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852699b"
        },
        "nombre": "Nuquí",
        "descripcion": "Nuquí",
        "abreviacion": "Nuquí",
        "createdAt": "2020-01-16T13:31:29.349Z",
        "updatedAt": "2020-01-16T13:31:29.349Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852699c"
        },
        "nombre": "La Mesa",
        "descripcion": "La Mesa",
        "abreviacion": "La Mesa",
        "createdAt": "2020-01-16T13:31:29.350Z",
        "updatedAt": "2020-01-16T13:31:29.350Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852699d"
        },
        "nombre": "Pamplona",
        "descripcion": "Pamplona",
        "abreviacion": "Pamplona",
        "createdAt": "2020-01-16T13:31:29.351Z",
        "updatedAt": "2020-01-16T13:31:29.351Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852699e"
        },
        "nombre": "Topaipí",
        "descripcion": "Topaipí",
        "abreviacion": "Topaipí",
        "createdAt": "2020-01-16T13:31:29.353Z",
        "updatedAt": "2020-01-16T13:31:29.353Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d000852699f"
        },
        "nombre": "Supatá",
        "descripcion": "Supatá",
        "abreviacion": "Supatá",
        "createdAt": "2020-01-16T13:31:29.361Z",
        "updatedAt": "2020-01-16T13:31:29.361Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269a0"
        },
        "nombre": "Pueblo Viejo",
        "descripcion": "Pueblo Viejo",
        "abreviacion": "Pueblo Viejo",
        "createdAt": "2020-01-16T13:31:29.362Z",
        "updatedAt": "2020-01-16T13:31:29.362Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269a1"
        },
        "nombre": "Marquetalia",
        "descripcion": "Marquetalia",
        "abreviacion": "Marquetalia",
        "createdAt": "2020-01-16T13:31:29.363Z",
        "updatedAt": "2020-01-16T13:31:29.363Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269a2"
        },
        "nombre": "Guadalupe",
        "descripcion": "Guadalupe",
        "abreviacion": "Guadalupe",
        "createdAt": "2020-01-16T13:31:29.365Z",
        "updatedAt": "2020-01-16T13:31:29.365Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269a3"
        },
        "nombre": "Santa Rosa",
        "descripcion": "Santa Rosa",
        "abreviacion": "Santa Rosa",
        "createdAt": "2020-01-16T13:31:29.372Z",
        "updatedAt": "2020-01-16T13:31:29.372Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269a4"
        },
        "nombre": "Busbanzá",
        "descripcion": "Busbanzá",
        "abreviacion": "Busbanzá",
        "createdAt": "2020-01-16T13:31:29.378Z",
        "updatedAt": "2020-01-16T13:31:29.378Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269a5"
        },
        "nombre": "Colombia",
        "descripcion": "Colombia",
        "abreviacion": "Colombia",
        "createdAt": "2020-01-16T13:31:29.379Z",
        "updatedAt": "2020-01-16T13:31:29.379Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269a6"
        },
        "nombre": "Tiquisio",
        "descripcion": "Tiquisio",
        "abreviacion": "Tiquisio",
        "createdAt": "2020-01-16T13:31:29.381Z",
        "updatedAt": "2020-01-16T13:31:29.381Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269a7"
        },
        "nombre": "Cubará",
        "descripcion": "Cubará",
        "abreviacion": "Cubará",
        "createdAt": "2020-01-16T13:31:29.382Z",
        "updatedAt": "2020-01-16T13:31:29.382Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269a8"
        },
        "nombre": "Ibagué",
        "descripcion": "Ibagué",
        "abreviacion": "Ibagué",
        "createdAt": "2020-01-16T13:31:29.387Z",
        "updatedAt": "2020-01-16T13:31:29.387Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269a9"
        },
        "nombre": "Fusagasugá",
        "descripcion": "Fusagasugá",
        "abreviacion": "Fusagasugá",
        "createdAt": "2020-01-16T13:31:29.394Z",
        "updatedAt": "2020-01-16T13:31:29.394Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269aa"
        },
        "nombre": "Ubaque",
        "descripcion": "Ubaque",
        "abreviacion": "Ubaque",
        "createdAt": "2020-01-16T13:31:29.401Z",
        "updatedAt": "2020-01-16T13:31:29.401Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269ab"
        },
        "nombre": "Subachoque",
        "descripcion": "Subachoque",
        "abreviacion": "Subachoque",
        "createdAt": "2020-01-16T13:31:29.402Z",
        "updatedAt": "2020-01-16T13:31:29.402Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269ac"
        },
        "nombre": "Carepa",
        "descripcion": "Carepa",
        "abreviacion": "Carepa",
        "createdAt": "2020-01-16T13:31:29.403Z",
        "updatedAt": "2020-01-16T13:31:29.403Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269ad"
        },
        "nombre": "Carmen del Darien",
        "descripcion": "Carmen del Darien",
        "abreviacion": "Carmen del Darien",
        "createdAt": "2020-01-16T13:31:29.405Z",
        "updatedAt": "2020-01-16T13:31:29.405Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269ae"
        },
        "nombre": "Chita",
        "descripcion": "Chita",
        "abreviacion": "Chita",
        "createdAt": "2020-01-16T13:31:29.485Z",
        "updatedAt": "2020-01-16T13:31:29.485Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269af"
        },
        "nombre": "Giraldo",
        "descripcion": "Giraldo",
        "abreviacion": "Giraldo",
        "createdAt": "2020-01-16T13:31:29.491Z",
        "updatedAt": "2020-01-16T13:31:29.491Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269b0"
        },
        "nombre": "Bojacá",
        "descripcion": "Bojacá",
        "abreviacion": "Bojacá",
        "createdAt": "2020-01-16T13:31:29.493Z",
        "updatedAt": "2020-01-16T13:31:29.493Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269b1"
        },
        "nombre": "Nemocón",
        "descripcion": "Nemocón",
        "abreviacion": "Nemocón",
        "createdAt": "2020-01-16T13:31:29.505Z",
        "updatedAt": "2020-01-16T13:31:29.505Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269b2"
        },
        "nombre": "Remedios",
        "descripcion": "Remedios",
        "abreviacion": "Remedios",
        "createdAt": "2020-01-16T13:31:29.509Z",
        "updatedAt": "2020-01-16T13:31:29.509Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269b3"
        },
        "nombre": "San Andrés Sotavento",
        "descripcion": "San Andrés Sotavento",
        "abreviacion": "San Andrés Sotavento",
        "createdAt": "2020-01-16T13:31:29.510Z",
        "updatedAt": "2020-01-16T13:31:29.510Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269b4"
        },
        "nombre": "Momil",
        "descripcion": "Momil",
        "abreviacion": "Momil",
        "createdAt": "2020-01-16T13:31:29.514Z",
        "updatedAt": "2020-01-16T13:31:29.514Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269b5"
        },
        "nombre": "Sahagún",
        "descripcion": "Sahagún",
        "abreviacion": "Sahagún",
        "createdAt": "2020-01-16T13:31:29.517Z",
        "updatedAt": "2020-01-16T13:31:29.517Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269b6"
        },
        "nombre": "Nocaima",
        "descripcion": "Nocaima",
        "abreviacion": "Nocaima",
        "createdAt": "2020-01-16T13:31:29.525Z",
        "updatedAt": "2020-01-16T13:31:29.525Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269b7"
        },
        "nombre": "Cota",
        "descripcion": "Cota",
        "abreviacion": "Cota",
        "createdAt": "2020-01-16T13:31:29.529Z",
        "updatedAt": "2020-01-16T13:31:29.529Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269b8"
        },
        "nombre": "La Apartada",
        "descripcion": "La Apartada",
        "abreviacion": "La Apartada",
        "createdAt": "2020-01-16T13:31:29.532Z",
        "updatedAt": "2020-01-16T13:31:29.532Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269b9"
        },
        "nombre": "Chipaque",
        "descripcion": "Chipaque",
        "abreviacion": "Chipaque",
        "createdAt": "2020-01-16T13:31:29.536Z",
        "updatedAt": "2020-01-16T13:31:29.536Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269ba"
        },
        "nombre": "El Colegio",
        "descripcion": "El Colegio",
        "abreviacion": "El Colegio",
        "createdAt": "2020-01-16T13:31:29.537Z",
        "updatedAt": "2020-01-16T13:31:29.537Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269bb"
        },
        "nombre": "Gachancipá",
        "descripcion": "Gachancipá",
        "abreviacion": "Gachancipá",
        "createdAt": "2020-01-16T13:31:29.548Z",
        "updatedAt": "2020-01-16T13:31:29.548Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269bc"
        },
        "nombre": "El Carmen",
        "descripcion": "El Carmen",
        "abreviacion": "El Carmen",
        "createdAt": "2020-01-16T13:31:29.549Z",
        "updatedAt": "2020-01-16T13:31:29.549Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269bd"
        },
        "nombre": "Bituima",
        "descripcion": "Bituima",
        "abreviacion": "Bituima",
        "createdAt": "2020-01-16T13:31:29.551Z",
        "updatedAt": "2020-01-16T13:31:29.551Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269be"
        },
        "nombre": "Cogua",
        "descripcion": "Cogua",
        "abreviacion": "Cogua",
        "createdAt": "2020-01-16T13:31:29.552Z",
        "updatedAt": "2020-01-16T13:31:29.552Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269bf"
        },
        "nombre": "Mosquera",
        "descripcion": "Mosquera",
        "abreviacion": "Mosquera",
        "createdAt": "2020-01-16T13:31:29.553Z",
        "updatedAt": "2020-01-16T13:31:29.553Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269c0"
        },
        "nombre": "Zambrano",
        "descripcion": "Zambrano",
        "abreviacion": "Zambrano",
        "createdAt": "2020-01-16T13:31:29.555Z",
        "updatedAt": "2020-01-16T13:31:29.555Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269c1"
        },
        "nombre": "Venecia",
        "descripcion": "Venecia",
        "abreviacion": "Venecia",
        "createdAt": "2020-01-16T13:31:29.556Z",
        "updatedAt": "2020-01-16T13:31:29.556Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269c4"
        },
        "nombre": "Yacopí",
        "descripcion": "Yacopí",
        "abreviacion": "Yacopí",
        "createdAt": "2020-01-16T13:31:29.559Z",
        "updatedAt": "2020-01-16T13:31:29.559Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269c2"
        },
        "nombre": "Arbeláez",
        "descripcion": "Arbeláez",
        "abreviacion": "Arbeláez",
        "createdAt": "2020-01-16T13:31:29.557Z",
        "updatedAt": "2020-01-16T13:31:29.557Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269c3"
        },
        "nombre": "Guayabetal",
        "descripcion": "Guayabetal",
        "abreviacion": "Guayabetal",
        "createdAt": "2020-01-16T13:31:29.558Z",
        "updatedAt": "2020-01-16T13:31:29.558Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269c5"
        },
        "nombre": "Ricaurte",
        "descripcion": "Ricaurte",
        "abreviacion": "Ricaurte",
        "createdAt": "2020-01-16T13:31:29.560Z",
        "updatedAt": "2020-01-16T13:31:29.560Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269c6"
        },
        "nombre": "Caparrapí",
        "descripcion": "Caparrapí",
        "abreviacion": "Caparrapí",
        "createdAt": "2020-01-16T13:31:29.561Z",
        "updatedAt": "2020-01-16T13:31:29.561Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269c8"
        },
        "nombre": "Sasaima",
        "descripcion": "Sasaima",
        "abreviacion": "Sasaima",
        "createdAt": "2020-01-16T13:31:29.564Z",
        "updatedAt": "2020-01-16T13:31:29.564Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269c7"
        },
        "nombre": "Mongua",
        "descripcion": "Mongua",
        "abreviacion": "Mongua",
        "createdAt": "2020-01-16T13:31:29.563Z",
        "updatedAt": "2020-01-16T13:31:29.563Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269c9"
        },
        "nombre": "Cucunubá",
        "descripcion": "Cucunubá",
        "abreviacion": "Cucunubá",
        "createdAt": "2020-01-16T13:31:29.566Z",
        "updatedAt": "2020-01-16T13:31:29.566Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269ca"
        },
        "nombre": "Norcasia",
        "descripcion": "Norcasia",
        "abreviacion": "Norcasia",
        "createdAt": "2020-01-16T13:31:29.567Z",
        "updatedAt": "2020-01-16T13:31:29.567Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269cb"
        },
        "nombre": "Santa Sofía",
        "descripcion": "Santa Sofía",
        "abreviacion": "Santa Sofía",
        "createdAt": "2020-01-16T13:31:29.568Z",
        "updatedAt": "2020-01-16T13:31:29.568Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269cc"
        },
        "nombre": "Maripí",
        "descripcion": "Maripí",
        "abreviacion": "Maripí",
        "createdAt": "2020-01-16T13:31:29.570Z",
        "updatedAt": "2020-01-16T13:31:29.570Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269cd"
        },
        "nombre": "Miranda",
        "descripcion": "Miranda",
        "abreviacion": "Miranda",
        "createdAt": "2020-01-16T13:31:29.572Z",
        "updatedAt": "2020-01-16T13:31:29.572Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269ce"
        },
        "nombre": "Paipa",
        "descripcion": "Paipa",
        "abreviacion": "Paipa",
        "createdAt": "2020-01-16T13:31:29.573Z",
        "updatedAt": "2020-01-16T13:31:29.573Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269cf"
        },
        "nombre": "Aquitania",
        "descripcion": "Aquitania",
        "abreviacion": "Aquitania",
        "createdAt": "2020-01-16T13:31:29.575Z",
        "updatedAt": "2020-01-16T13:31:29.575Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269d0"
        },
        "nombre": "Nobsa",
        "descripcion": "Nobsa",
        "abreviacion": "Nobsa",
        "createdAt": "2020-01-16T13:31:29.577Z",
        "updatedAt": "2020-01-16T13:31:29.577Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269d1"
        },
        "nombre": "Samaná",
        "descripcion": "Samaná",
        "abreviacion": "Samaná",
        "createdAt": "2020-01-16T13:31:29.580Z",
        "updatedAt": "2020-01-16T13:31:29.580Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269d2"
        },
        "nombre": "Pachavita",
        "descripcion": "Pachavita",
        "abreviacion": "Pachavita",
        "createdAt": "2020-01-16T13:31:29.581Z",
        "updatedAt": "2020-01-16T13:31:29.581Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269d3"
        },
        "nombre": "Ventaquemada",
        "descripcion": "Ventaquemada",
        "abreviacion": "Ventaquemada",
        "createdAt": "2020-01-16T13:31:29.583Z",
        "updatedAt": "2020-01-16T13:31:29.583Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269d4"
        },
        "nombre": "Solano",
        "descripcion": "Solano",
        "abreviacion": "Solano",
        "createdAt": "2020-01-16T13:31:29.585Z",
        "updatedAt": "2020-01-16T13:31:29.585Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269d5"
        },
        "nombre": "Risaralda",
        "descripcion": "Risaralda",
        "abreviacion": "Risaralda",
        "createdAt": "2020-01-16T13:31:29.586Z",
        "updatedAt": "2020-01-16T13:31:29.586Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269d6"
        },
        "nombre": "Jambaló",
        "descripcion": "Jambaló",
        "abreviacion": "Jambaló",
        "createdAt": "2020-01-16T13:31:29.587Z",
        "updatedAt": "2020-01-16T13:31:29.587Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269d7"
        },
        "nombre": "Agua de Dios",
        "descripcion": "Agua de Dios",
        "abreviacion": "Agua de Dios",
        "createdAt": "2020-01-16T13:31:29.589Z",
        "updatedAt": "2020-01-16T13:31:29.589Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269d8"
        },
        "nombre": "La Merced",
        "descripcion": "La Merced",
        "abreviacion": "La Merced",
        "createdAt": "2020-01-16T13:31:29.590Z",
        "updatedAt": "2020-01-16T13:31:29.590Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269d9"
        },
        "nombre": "Morales",
        "descripcion": "Morales",
        "abreviacion": "Morales",
        "createdAt": "2020-01-16T13:31:29.592Z",
        "updatedAt": "2020-01-16T13:31:29.592Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269da"
        },
        "nombre": "Cabrera",
        "descripcion": "Cabrera",
        "abreviacion": "Cabrera",
        "createdAt": "2020-01-16T13:31:29.593Z",
        "updatedAt": "2020-01-16T13:31:29.593Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269db"
        },
        "nombre": "Tipacoque",
        "descripcion": "Tipacoque",
        "abreviacion": "Tipacoque",
        "createdAt": "2020-01-16T13:31:29.597Z",
        "updatedAt": "2020-01-16T13:31:29.597Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269dc"
        },
        "nombre": "Garagoa",
        "descripcion": "Garagoa",
        "abreviacion": "Garagoa",
        "createdAt": "2020-01-16T13:31:29.599Z",
        "updatedAt": "2020-01-16T13:31:29.599Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269dd"
        },
        "nombre": "Guataquí",
        "descripcion": "Guataquí",
        "abreviacion": "Guataquí",
        "createdAt": "2020-01-16T13:31:29.602Z",
        "updatedAt": "2020-01-16T13:31:29.602Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269de"
        },
        "nombre": "Jenesano",
        "descripcion": "Jenesano",
        "abreviacion": "Jenesano",
        "createdAt": "2020-01-16T13:31:29.604Z",
        "updatedAt": "2020-01-16T13:31:29.604Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269df"
        },
        "nombre": "Fomeque",
        "descripcion": "Fomeque",
        "abreviacion": "Fomeque",
        "createdAt": "2020-01-16T13:31:29.605Z",
        "updatedAt": "2020-01-16T13:31:29.605Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269e0"
        },
        "nombre": "Barranco Minas",
        "descripcion": "Barranco Minas",
        "abreviacion": "Barranco Minas",
        "createdAt": "2020-01-16T13:31:29.608Z",
        "updatedAt": "2020-01-16T13:31:29.608Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269e1"
        },
        "nombre": "La Guadalupe",
        "descripcion": "La Guadalupe",
        "abreviacion": "La Guadalupe",
        "createdAt": "2020-01-16T13:31:29.611Z",
        "updatedAt": "2020-01-16T13:31:29.611Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269e2"
        },
        "nombre": "Pandi",
        "descripcion": "Pandi",
        "abreviacion": "Pandi",
        "createdAt": "2020-01-16T13:31:29.615Z",
        "updatedAt": "2020-01-16T13:31:29.615Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269e3"
        },
        "nombre": "San Cayetano",
        "descripcion": "San Cayetano",
        "abreviacion": "San Cayetano",
        "createdAt": "2020-01-16T13:31:29.616Z",
        "updatedAt": "2020-01-16T13:31:29.616Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269e4"
        },
        "nombre": "Soacha",
        "descripcion": "Soacha",
        "abreviacion": "Soacha",
        "createdAt": "2020-01-16T13:31:29.617Z",
        "updatedAt": "2020-01-16T13:31:29.617Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269e5"
        },
        "nombre": "Iquira",
        "descripcion": "Iquira",
        "abreviacion": "Iquira",
        "createdAt": "2020-01-16T13:31:29.624Z",
        "updatedAt": "2020-01-16T13:31:29.624Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269e6"
        },
        "nombre": "Garzón",
        "descripcion": "Garzón",
        "abreviacion": "Garzón",
        "createdAt": "2020-01-16T13:31:29.626Z",
        "updatedAt": "2020-01-16T13:31:29.626Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269e7"
        },
        "nombre": "La Peña",
        "descripcion": "La Peña",
        "abreviacion": "La Peña",
        "createdAt": "2020-01-16T13:31:29.628Z",
        "updatedAt": "2020-01-16T13:31:29.628Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269e8"
        },
        "nombre": "Dibula",
        "descripcion": "Dibula",
        "abreviacion": "Dibula",
        "createdAt": "2020-01-16T13:31:29.629Z",
        "updatedAt": "2020-01-16T13:31:29.629Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269e9"
        },
        "nombre": "Baraya",
        "descripcion": "Baraya",
        "abreviacion": "Baraya",
        "createdAt": "2020-01-16T13:31:29.631Z",
        "updatedAt": "2020-01-16T13:31:29.631Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269ea"
        },
        "nombre": "San Felipe",
        "descripcion": "San Felipe",
        "abreviacion": "San Felipe",
        "createdAt": "2020-01-16T13:31:29.632Z",
        "updatedAt": "2020-01-16T13:31:29.632Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269eb"
        },
        "nombre": "Barranca de Upía",
        "descripcion": "Barranca de Upía",
        "abreviacion": "Barranca de Upía",
        "createdAt": "2020-01-16T13:31:29.634Z",
        "updatedAt": "2020-01-16T13:31:29.634Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269ec"
        },
        "nombre": "El Litoral del San Juan",
        "descripcion": "El Litoral del San Juan",
        "abreviacion": "El Litoral del San Juan",
        "createdAt": "2020-01-16T13:31:29.636Z",
        "updatedAt": "2020-01-16T13:31:29.636Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269ed"
        },
        "nombre": "Gutiérrez",
        "descripcion": "Gutiérrez",
        "abreviacion": "Gutiérrez",
        "createdAt": "2020-01-16T13:31:29.637Z",
        "updatedAt": "2020-01-16T13:31:29.637Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269ee"
        },
        "nombre": "Pacho",
        "descripcion": "Pacho",
        "abreviacion": "Pacho",
        "createdAt": "2020-01-16T13:31:29.639Z",
        "updatedAt": "2020-01-16T13:31:29.639Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269ef"
        },
        "nombre": "Puerto Colombia",
        "descripcion": "Puerto Colombia",
        "abreviacion": "Puerto Colombia",
        "createdAt": "2020-01-16T13:31:29.641Z",
        "updatedAt": "2020-01-16T13:31:29.641Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269f0"
        },
        "nombre": "Juan de Acosta",
        "descripcion": "Juan de Acosta",
        "abreviacion": "Juan de Acosta",
        "createdAt": "2020-01-16T13:31:29.642Z",
        "updatedAt": "2020-01-16T13:31:29.642Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269f1"
        },
        "nombre": "Mitú",
        "descripcion": "Mitú",
        "abreviacion": "Mitú",
        "createdAt": "2020-01-16T13:31:29.644Z",
        "updatedAt": "2020-01-16T13:31:29.644Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269f2"
        },
        "nombre": "San Martín de Loba",
        "descripcion": "San Martín de Loba",
        "abreviacion": "San Martín de Loba",
        "createdAt": "2020-01-16T13:31:29.645Z",
        "updatedAt": "2020-01-16T13:31:29.645Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269f3"
        },
        "nombre": "Calamar",
        "descripcion": "Calamar",
        "abreviacion": "Calamar",
        "createdAt": "2020-01-16T13:31:29.648Z",
        "updatedAt": "2020-01-16T13:31:29.648Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269f4"
        },
        "nombre": "Villeta",
        "descripcion": "Villeta",
        "abreviacion": "Villeta",
        "createdAt": "2020-01-16T13:31:29.650Z",
        "updatedAt": "2020-01-16T13:31:29.650Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269f5"
        },
        "nombre": "La Calera",
        "descripcion": "La Calera",
        "abreviacion": "La Calera",
        "createdAt": "2020-01-16T13:31:29.651Z",
        "updatedAt": "2020-01-16T13:31:29.651Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269f6"
        },
        "nombre": "Guaduas",
        "descripcion": "Guaduas",
        "abreviacion": "Guaduas",
        "createdAt": "2020-01-16T13:31:29.653Z",
        "updatedAt": "2020-01-16T13:31:29.653Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269f7"
        },
        "nombre": "Togüí",
        "descripcion": "Togüí",
        "abreviacion": "Togüí",
        "createdAt": "2020-01-16T13:31:29.654Z",
        "updatedAt": "2020-01-16T13:31:29.654Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269f8"
        },
        "nombre": "Planeta Rica",
        "descripcion": "Planeta Rica",
        "abreviacion": "Planeta Rica",
        "createdAt": "2020-01-16T13:31:29.655Z",
        "updatedAt": "2020-01-16T13:31:29.655Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269f9"
        },
        "nombre": "Beltrán",
        "descripcion": "Beltrán",
        "abreviacion": "Beltrán",
        "createdAt": "2020-01-16T13:31:29.657Z",
        "updatedAt": "2020-01-16T13:31:29.657Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269fa"
        },
        "nombre": "Pueblo Nuevo",
        "descripcion": "Pueblo Nuevo",
        "abreviacion": "Pueblo Nuevo",
        "createdAt": "2020-01-16T13:31:29.658Z",
        "updatedAt": "2020-01-16T13:31:29.658Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269fb"
        },
        "nombre": "Nilo",
        "descripcion": "Nilo",
        "abreviacion": "Nilo",
        "createdAt": "2020-01-16T13:31:29.659Z",
        "updatedAt": "2020-01-16T13:31:29.659Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269fc"
        },
        "nombre": "El Rosal",
        "descripcion": "El Rosal",
        "abreviacion": "El Rosal",
        "createdAt": "2020-01-16T13:31:29.660Z",
        "updatedAt": "2020-01-16T13:31:29.660Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269fd"
        },
        "nombre": "Chocontá",
        "descripcion": "Chocontá",
        "abreviacion": "Chocontá",
        "createdAt": "2020-01-16T13:31:29.662Z",
        "updatedAt": "2020-01-16T13:31:29.662Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269fe"
        },
        "nombre": "Inírida",
        "descripcion": "Inírida",
        "abreviacion": "Inírida",
        "createdAt": "2020-01-16T13:31:29.663Z",
        "updatedAt": "2020-01-16T13:31:29.663Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d00085269ff"
        },
        "nombre": "Granada",
        "descripcion": "Granada",
        "abreviacion": "Granada",
        "createdAt": "2020-01-16T13:31:29.664Z",
        "updatedAt": "2020-01-16T13:31:29.664Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a00"
        },
        "nombre": "San Antero",
        "descripcion": "San Antero",
        "abreviacion": "San Antero",
        "createdAt": "2020-01-16T13:31:29.667Z",
        "updatedAt": "2020-01-16T13:31:29.667Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a01"
        },
        "nombre": "Nariño",
        "descripcion": "Nariño",
        "abreviacion": "Nariño",
        "createdAt": "2020-01-16T13:31:29.669Z",
        "updatedAt": "2020-01-16T13:31:29.669Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a02"
        },
        "nombre": "Calarcá",
        "descripcion": "Calarcá",
        "abreviacion": "Calarcá",
        "createdAt": "2020-01-16T13:31:29.670Z",
        "updatedAt": "2020-01-16T13:31:29.670Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a03"
        },
        "nombre": "Guasca",
        "descripcion": "Guasca",
        "abreviacion": "Guasca",
        "createdAt": "2020-01-16T13:31:29.671Z",
        "updatedAt": "2020-01-16T13:31:29.671Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a04"
        },
        "nombre": "Guachetá",
        "descripcion": "Guachetá",
        "abreviacion": "Guachetá",
        "createdAt": "2020-01-16T13:31:29.673Z",
        "updatedAt": "2020-01-16T13:31:29.673Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a05"
        },
        "nombre": "Cerinza",
        "descripcion": "Cerinza",
        "abreviacion": "Cerinza",
        "createdAt": "2020-01-16T13:31:29.674Z",
        "updatedAt": "2020-01-16T13:31:29.674Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a06"
        },
        "nombre": "Labranzagrande",
        "descripcion": "Labranzagrande",
        "abreviacion": "Labranzagrande",
        "createdAt": "2020-01-16T13:31:29.675Z",
        "updatedAt": "2020-01-16T13:31:29.675Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a07"
        },
        "nombre": "Duitama",
        "descripcion": "Duitama",
        "abreviacion": "Duitama",
        "createdAt": "2020-01-16T13:31:29.677Z",
        "updatedAt": "2020-01-16T13:31:29.677Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a08"
        },
        "nombre": "Sáchica",
        "descripcion": "Sáchica",
        "abreviacion": "Sáchica",
        "createdAt": "2020-01-16T13:31:29.678Z",
        "updatedAt": "2020-01-16T13:31:29.678Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a09"
        },
        "nombre": "Oicatá",
        "descripcion": "Oicatá",
        "abreviacion": "Oicatá",
        "createdAt": "2020-01-16T13:31:29.684Z",
        "updatedAt": "2020-01-16T13:31:29.684Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a0a"
        },
        "nombre": "Puerto Escondido",
        "descripcion": "Puerto Escondido",
        "abreviacion": "Puerto Escondido",
        "createdAt": "2020-01-16T13:31:29.685Z",
        "updatedAt": "2020-01-16T13:31:29.685Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a0b"
        },
        "nombre": "Moñitos",
        "descripcion": "Moñitos",
        "abreviacion": "Moñitos",
        "createdAt": "2020-01-16T13:31:29.687Z",
        "updatedAt": "2020-01-16T13:31:29.687Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a0c"
        },
        "nombre": "Belén",
        "descripcion": "Belén",
        "abreviacion": "Belén",
        "createdAt": "2020-01-16T13:31:29.692Z",
        "updatedAt": "2020-01-16T13:31:29.692Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a0d"
        },
        "nombre": "Tuchín",
        "descripcion": "Tuchín",
        "abreviacion": "Tuchín",
        "createdAt": "2020-01-16T13:31:29.693Z",
        "updatedAt": "2020-01-16T13:31:29.693Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a0e"
        },
        "nombre": "San Vicente del Caguán",
        "descripcion": "San Vicente del Caguán",
        "abreviacion": "San Vicente del Caguán",
        "createdAt": "2020-01-16T13:31:29.694Z",
        "updatedAt": "2020-01-16T13:31:29.694Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a0f"
        },
        "nombre": "Pijiño del Carmen",
        "descripcion": "Pijiño del Carmen",
        "abreviacion": "Pijiño del Carmen",
        "createdAt": "2020-01-16T13:31:29.696Z",
        "updatedAt": "2020-01-16T13:31:29.696Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a10"
        },
        "nombre": "Corinto",
        "descripcion": "Corinto",
        "abreviacion": "Corinto",
        "createdAt": "2020-01-16T13:31:29.697Z",
        "updatedAt": "2020-01-16T13:31:29.697Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a11"
        },
        "nombre": "Macanal",
        "descripcion": "Macanal",
        "abreviacion": "Macanal",
        "createdAt": "2020-01-16T13:31:29.698Z",
        "updatedAt": "2020-01-16T13:31:29.698Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a12"
        },
        "nombre": "Saboyá",
        "descripcion": "Saboyá",
        "abreviacion": "Saboyá",
        "createdAt": "2020-01-16T13:31:29.700Z",
        "updatedAt": "2020-01-16T13:31:29.700Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a13"
        },
        "nombre": "María la Baja",
        "descripcion": "María la Baja",
        "abreviacion": "María la Baja",
        "createdAt": "2020-01-16T13:31:29.701Z",
        "updatedAt": "2020-01-16T13:31:29.701Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a14"
        },
        "nombre": "El Peñón",
        "descripcion": "El Peñón",
        "abreviacion": "El Peñón",
        "createdAt": "2020-01-16T13:31:29.703Z",
        "updatedAt": "2020-01-16T13:31:29.703Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a15"
        },
        "nombre": "Susacón",
        "descripcion": "Susacón",
        "abreviacion": "Susacón",
        "createdAt": "2020-01-16T13:31:29.704Z",
        "updatedAt": "2020-01-16T13:31:29.704Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a16"
        },
        "nombre": "Cacahual",
        "descripcion": "Cacahual",
        "abreviacion": "Cacahual",
        "createdAt": "2020-01-16T13:31:29.705Z",
        "updatedAt": "2020-01-16T13:31:29.705Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a17"
        },
        "nombre": "Valparaíso",
        "descripcion": "Valparaíso",
        "abreviacion": "Valparaíso",
        "createdAt": "2020-01-16T13:31:29.706Z",
        "updatedAt": "2020-01-16T13:31:29.706Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a18"
        },
        "nombre": "San José",
        "descripcion": "San José",
        "abreviacion": "San José",
        "createdAt": "2020-01-16T13:31:29.708Z",
        "updatedAt": "2020-01-16T13:31:29.708Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a19"
        },
        "nombre": "Neira",
        "descripcion": "Neira",
        "abreviacion": "Neira",
        "createdAt": "2020-01-16T13:31:29.709Z",
        "updatedAt": "2020-01-16T13:31:29.709Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a1a"
        },
        "nombre": "Inzá",
        "descripcion": "Inzá",
        "abreviacion": "Inzá",
        "createdAt": "2020-01-16T13:31:29.710Z",
        "updatedAt": "2020-01-16T13:31:29.710Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a1b"
        },
        "nombre": "Sutamarchán",
        "descripcion": "Sutamarchán",
        "abreviacion": "Sutamarchán",
        "createdAt": "2020-01-16T13:31:29.711Z",
        "updatedAt": "2020-01-16T13:31:29.711Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a1c"
        },
        "nombre": "Soatá",
        "descripcion": "Soatá",
        "abreviacion": "Soatá",
        "createdAt": "2020-01-16T13:31:29.713Z",
        "updatedAt": "2020-01-16T13:31:29.713Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a1d"
        },
        "nombre": "El Espino",
        "descripcion": "El Espino",
        "abreviacion": "El Espino",
        "createdAt": "2020-01-16T13:31:29.714Z",
        "updatedAt": "2020-01-16T13:31:29.714Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a1e"
        },
        "nombre": "El Cocuy",
        "descripcion": "El Cocuy",
        "abreviacion": "El Cocuy",
        "createdAt": "2020-01-16T13:31:29.715Z",
        "updatedAt": "2020-01-16T13:31:29.715Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a1f"
        },
        "nombre": "Belén de Los Andaquies",
        "descripcion": "Belén de Los Andaquies",
        "abreviacion": "Belén de Los Andaquies",
        "createdAt": "2020-01-16T13:31:29.716Z",
        "updatedAt": "2020-01-16T13:31:29.716Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a20"
        },
        "nombre": "La Palma",
        "descripcion": "La Palma",
        "abreviacion": "La Palma",
        "createdAt": "2020-01-16T13:31:29.718Z",
        "updatedAt": "2020-01-16T13:31:29.718Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a21"
        },
        "nombre": "Manta",
        "descripcion": "Manta",
        "abreviacion": "Manta",
        "createdAt": "2020-01-16T13:31:29.719Z",
        "updatedAt": "2020-01-16T13:31:29.719Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a22"
        },
        "nombre": "Iza",
        "descripcion": "Iza",
        "abreviacion": "Iza",
        "createdAt": "2020-01-16T13:31:29.720Z",
        "updatedAt": "2020-01-16T13:31:29.720Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a23"
        },
        "nombre": "Viotá",
        "descripcion": "Viotá",
        "abreviacion": "Viotá",
        "createdAt": "2020-01-16T13:31:29.722Z",
        "updatedAt": "2020-01-16T13:31:29.722Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a24"
        },
        "nombre": "Santa Rosa del Sur",
        "descripcion": "Santa Rosa del Sur",
        "abreviacion": "Santa Rosa del Sur",
        "createdAt": "2020-01-16T13:31:29.723Z",
        "updatedAt": "2020-01-16T13:31:29.723Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a25"
        },
        "nombre": "Hobo",
        "descripcion": "Hobo",
        "abreviacion": "Hobo",
        "createdAt": "2020-01-16T13:31:29.724Z",
        "updatedAt": "2020-01-16T13:31:29.724Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a26"
        },
        "nombre": "Tello",
        "descripcion": "Tello",
        "abreviacion": "Tello",
        "createdAt": "2020-01-16T13:31:29.725Z",
        "updatedAt": "2020-01-16T13:31:29.725Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a27"
        },
        "nombre": "Pacoa",
        "descripcion": "Pacoa",
        "abreviacion": "Pacoa",
        "createdAt": "2020-01-16T13:31:29.727Z",
        "updatedAt": "2020-01-16T13:31:29.727Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a28"
        },
        "nombre": "Morichal",
        "descripcion": "Morichal",
        "abreviacion": "Morichal",
        "createdAt": "2020-01-16T13:31:29.728Z",
        "updatedAt": "2020-01-16T13:31:29.728Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a29"
        },
        "nombre": "Suesca",
        "descripcion": "Suesca",
        "abreviacion": "Suesca",
        "createdAt": "2020-01-16T13:31:29.729Z",
        "updatedAt": "2020-01-16T13:31:29.729Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a2a"
        },
        "nombre": "Simijaca",
        "descripcion": "Simijaca",
        "abreviacion": "Simijaca",
        "createdAt": "2020-01-16T13:31:29.731Z",
        "updatedAt": "2020-01-16T13:31:29.731Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a2b"
        },
        "nombre": "Villapinzón",
        "descripcion": "Villapinzón",
        "abreviacion": "Villapinzón",
        "createdAt": "2020-01-16T13:31:29.732Z",
        "updatedAt": "2020-01-16T13:31:29.732Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a2c"
        },
        "nombre": "Tierralta",
        "descripcion": "Tierralta",
        "abreviacion": "Tierralta",
        "createdAt": "2020-01-16T13:31:29.859Z",
        "updatedAt": "2020-01-16T13:31:29.859Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a2d"
        },
        "nombre": "San Pelayo",
        "descripcion": "San Pelayo",
        "abreviacion": "San Pelayo",
        "createdAt": "2020-01-16T13:31:29.860Z",
        "updatedAt": "2020-01-16T13:31:29.860Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a2e"
        },
        "nombre": "Choachí",
        "descripcion": "Choachí",
        "abreviacion": "Choachí",
        "createdAt": "2020-01-16T13:31:29.868Z",
        "updatedAt": "2020-01-16T13:31:29.868Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a2f"
        },
        "nombre": "Funza",
        "descripcion": "Funza",
        "abreviacion": "Funza",
        "createdAt": "2020-01-16T13:31:29.870Z",
        "updatedAt": "2020-01-16T13:31:29.870Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a30"
        },
        "nombre": "San Cristóbal",
        "descripcion": "San Cristóbal",
        "abreviacion": "San Cristóbal",
        "createdAt": "2020-01-16T13:31:29.871Z",
        "updatedAt": "2020-01-16T13:31:29.871Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a31"
        },
        "nombre": "Paime",
        "descripcion": "Paime",
        "abreviacion": "Paime",
        "createdAt": "2020-01-16T13:31:29.877Z",
        "updatedAt": "2020-01-16T13:31:29.877Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a32"
        },
        "nombre": "San Bernardo",
        "descripcion": "San Bernardo",
        "abreviacion": "San Bernardo",
        "createdAt": "2020-01-16T13:31:29.878Z",
        "updatedAt": "2020-01-16T13:31:29.878Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a33"
        },
        "nombre": "Pana Pana",
        "descripcion": "Pana Pana",
        "abreviacion": "Pana Pana",
        "createdAt": "2020-01-16T13:31:29.880Z",
        "updatedAt": "2020-01-16T13:31:29.880Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a34"
        },
        "nombre": "Zipacón",
        "descripcion": "Zipacón",
        "abreviacion": "Zipacón",
        "createdAt": "2020-01-16T13:31:29.881Z",
        "updatedAt": "2020-01-16T13:31:29.881Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a35"
        },
        "nombre": "Paicol",
        "descripcion": "Paicol",
        "abreviacion": "Paicol",
        "createdAt": "2020-01-16T13:31:29.887Z",
        "updatedAt": "2020-01-16T13:31:29.887Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a36"
        },
        "nombre": "Mapiripana",
        "descripcion": "Mapiripana",
        "abreviacion": "Mapiripana",
        "createdAt": "2020-01-16T13:31:29.889Z",
        "updatedAt": "2020-01-16T13:31:29.889Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a37"
        },
        "nombre": "San Luis de Gaceno",
        "descripcion": "San Luis de Gaceno",
        "abreviacion": "San Luis de Gaceno",
        "createdAt": "2020-01-16T13:31:29.891Z",
        "updatedAt": "2020-01-16T13:31:29.891Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a38"
        },
        "nombre": "San Juan del Cesar",
        "descripcion": "San Juan del Cesar",
        "abreviacion": "San Juan del Cesar",
        "createdAt": "2020-01-16T13:31:29.893Z",
        "updatedAt": "2020-01-16T13:31:29.893Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a39"
        },
        "nombre": "San José del Guaviare",
        "descripcion": "San José del Guaviare",
        "abreviacion": "San José del Guaviare",
        "createdAt": "2020-01-16T13:31:29.894Z",
        "updatedAt": "2020-01-16T13:31:29.894Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a3a"
        },
        "nombre": "San Carlos de Guaroa",
        "descripcion": "San Carlos de Guaroa",
        "abreviacion": "San Carlos de Guaroa",
        "createdAt": "2020-01-16T13:31:29.895Z",
        "updatedAt": "2020-01-16T13:31:29.895Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a3b"
        },
        "nombre": "Palmar de Varela",
        "descripcion": "Palmar de Varela",
        "abreviacion": "Palmar de Varela",
        "createdAt": "2020-01-16T13:31:29.896Z",
        "updatedAt": "2020-01-16T13:31:29.896Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a3c"
        },
        "nombre": "Carmen de Carupa",
        "descripcion": "Carmen de Carupa",
        "abreviacion": "Carmen de Carupa",
        "createdAt": "2020-01-16T13:31:29.898Z",
        "updatedAt": "2020-01-16T13:31:29.898Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a3d"
        },
        "nombre": "Valencia",
        "descripcion": "Valencia",
        "abreviacion": "Valencia",
        "createdAt": "2020-01-16T13:31:29.900Z",
        "updatedAt": "2020-01-16T13:31:29.900Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a3e"
        },
        "nombre": "San Jacinto",
        "descripcion": "San Jacinto",
        "abreviacion": "San Jacinto",
        "createdAt": "2020-01-16T13:31:29.901Z",
        "updatedAt": "2020-01-16T13:31:29.901Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a3f"
        },
        "nombre": "Cachirá",
        "descripcion": "Cachirá",
        "abreviacion": "Cachirá",
        "createdAt": "2020-01-16T13:31:29.903Z",
        "updatedAt": "2020-01-16T13:31:29.903Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a40"
        },
        "nombre": "Cartago",
        "descripcion": "Cartago",
        "abreviacion": "Cartago",
        "createdAt": "2020-01-16T13:31:29.904Z",
        "updatedAt": "2020-01-16T13:31:29.904Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a41"
        },
        "nombre": "Ragonvalia",
        "descripcion": "Ragonvalia",
        "abreviacion": "Ragonvalia",
        "createdAt": "2020-01-16T13:31:29.905Z",
        "updatedAt": "2020-01-16T13:31:29.905Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a42"
        },
        "nombre": "La Playa",
        "descripcion": "La Playa",
        "abreviacion": "La Playa",
        "createdAt": "2020-01-16T13:31:29.907Z",
        "updatedAt": "2020-01-16T13:31:29.907Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a43"
        },
        "nombre": "San Pablo de Borbur",
        "descripcion": "San Pablo de Borbur",
        "abreviacion": "San Pablo de Borbur",
        "createdAt": "2020-01-16T13:31:29.908Z",
        "updatedAt": "2020-01-16T13:31:29.908Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a44"
        },
        "nombre": "Valle de Guamez",
        "descripcion": "Valle de Guamez",
        "abreviacion": "Valle de Guamez",
        "createdAt": "2020-01-16T13:31:29.910Z",
        "updatedAt": "2020-01-16T13:31:29.910Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a45"
        },
        "nombre": "Guacarí",
        "descripcion": "Guacarí",
        "abreviacion": "Guacarí",
        "createdAt": "2020-01-16T13:31:29.912Z",
        "updatedAt": "2020-01-16T13:31:29.912Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a46"
        },
        "nombre": "Galán",
        "descripcion": "Galán",
        "abreviacion": "Galán",
        "createdAt": "2020-01-16T13:31:29.913Z",
        "updatedAt": "2020-01-16T13:31:29.913Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a47"
        },
        "nombre": "Pradera",
        "descripcion": "Pradera",
        "abreviacion": "Pradera",
        "createdAt": "2020-01-16T13:31:29.915Z",
        "updatedAt": "2020-01-16T13:31:29.915Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a48"
        },
        "nombre": "Restrepo",
        "descripcion": "Restrepo",
        "abreviacion": "Restrepo",
        "createdAt": "2020-01-16T13:31:29.916Z",
        "updatedAt": "2020-01-16T13:31:29.916Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a49"
        },
        "nombre": "Valle de San José",
        "descripcion": "Valle de San José",
        "abreviacion": "Valle de San José",
        "createdAt": "2020-01-16T13:31:29.918Z",
        "updatedAt": "2020-01-16T13:31:29.918Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a4a"
        },
        "nombre": "El Carmen de Viboral",
        "descripcion": "El Carmen de Viboral",
        "abreviacion": "El Carmen de Viboral",
        "createdAt": "2020-01-16T13:31:29.919Z",
        "updatedAt": "2020-01-16T13:31:29.919Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a4b"
        },
        "nombre": "Cabrera",
        "descripcion": "Cabrera",
        "abreviacion": "Cabrera",
        "createdAt": "2020-01-16T13:31:29.920Z",
        "updatedAt": "2020-01-16T13:31:29.920Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a4c"
        },
        "nombre": "Tarapacá",
        "descripcion": "Tarapacá",
        "abreviacion": "Tarapacá",
        "createdAt": "2020-01-16T13:31:29.922Z",
        "updatedAt": "2020-01-16T13:31:29.922Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a4d"
        },
        "nombre": "Enciso",
        "descripcion": "Enciso",
        "abreviacion": "Enciso",
        "createdAt": "2020-01-16T13:31:29.923Z",
        "updatedAt": "2020-01-16T13:31:29.923Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a4e"
        },
        "nombre": "San Antonio",
        "descripcion": "San Antonio",
        "abreviacion": "San Antonio",
        "createdAt": "2020-01-16T13:31:29.925Z",
        "updatedAt": "2020-01-16T13:31:29.925Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a4f"
        },
        "nombre": "Güepsa",
        "descripcion": "Güepsa",
        "abreviacion": "Güepsa",
        "createdAt": "2020-01-16T13:31:29.926Z",
        "updatedAt": "2020-01-16T13:31:29.926Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a50"
        },
        "nombre": "Montenegro",
        "descripcion": "Montenegro",
        "abreviacion": "Montenegro",
        "createdAt": "2020-01-16T13:31:29.928Z",
        "updatedAt": "2020-01-16T13:31:29.928Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a51"
        },
        "nombre": "San José de Pare",
        "descripcion": "San José de Pare",
        "abreviacion": "San José de Pare",
        "createdAt": "2020-01-16T13:31:29.930Z",
        "updatedAt": "2020-01-16T13:31:29.930Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a52"
        },
        "nombre": "Tangua",
        "descripcion": "Tangua",
        "abreviacion": "Tangua",
        "createdAt": "2020-01-16T13:31:29.931Z",
        "updatedAt": "2020-01-16T13:31:29.931Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a53"
        },
        "nombre": "Buenavista",
        "descripcion": "Buenavista",
        "abreviacion": "Buenavista",
        "createdAt": "2020-01-16T13:31:29.933Z",
        "updatedAt": "2020-01-16T13:31:29.933Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a54"
        },
        "nombre": "Yacuanquer",
        "descripcion": "Yacuanquer",
        "abreviacion": "Yacuanquer",
        "createdAt": "2020-01-16T13:31:29.934Z",
        "updatedAt": "2020-01-16T13:31:29.934Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a55"
        },
        "nombre": "Calima",
        "descripcion": "Calima",
        "abreviacion": "Calima",
        "createdAt": "2020-01-16T13:31:29.942Z",
        "updatedAt": "2020-01-16T13:31:29.942Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a56"
        },
        "nombre": "San Luis de Sincé",
        "descripcion": "San Luis de Sincé",
        "abreviacion": "San Luis de Sincé",
        "createdAt": "2020-01-16T13:31:29.945Z",
        "updatedAt": "2020-01-16T13:31:29.945Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a57"
        },
        "nombre": "Ricaurte",
        "descripcion": "Ricaurte",
        "abreviacion": "Ricaurte",
        "createdAt": "2020-01-16T13:31:29.949Z",
        "updatedAt": "2020-01-16T13:31:29.949Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a58"
        },
        "nombre": "Albania",
        "descripcion": "Albania",
        "abreviacion": "Albania",
        "createdAt": "2020-01-16T13:31:29.950Z",
        "updatedAt": "2020-01-16T13:31:29.950Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a59"
        },
        "nombre": "Guaitarilla",
        "descripcion": "Guaitarilla",
        "abreviacion": "Guaitarilla",
        "createdAt": "2020-01-16T13:31:29.955Z",
        "updatedAt": "2020-01-16T13:31:29.955Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a5a"
        },
        "nombre": "San Bernardo",
        "descripcion": "San Bernardo",
        "abreviacion": "San Bernardo",
        "createdAt": "2020-01-16T13:31:29.957Z",
        "updatedAt": "2020-01-16T13:31:29.957Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a5b"
        },
        "nombre": "Filandia",
        "descripcion": "Filandia",
        "abreviacion": "Filandia",
        "createdAt": "2020-01-16T13:31:29.959Z",
        "updatedAt": "2020-01-16T13:31:29.959Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a5c"
        },
        "nombre": "Ciénega",
        "descripcion": "Ciénega",
        "abreviacion": "Ciénega",
        "createdAt": "2020-01-16T13:31:29.961Z",
        "updatedAt": "2020-01-16T13:31:29.961Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a5d"
        },
        "nombre": "Mahates",
        "descripcion": "Mahates",
        "abreviacion": "Mahates",
        "createdAt": "2020-01-16T13:31:29.965Z",
        "updatedAt": "2020-01-16T13:31:29.965Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a5e"
        },
        "nombre": "Sabana de Torres",
        "descripcion": "Sabana de Torres",
        "abreviacion": "Sabana de Torres",
        "createdAt": "2020-01-16T13:31:29.969Z",
        "updatedAt": "2020-01-16T13:31:29.969Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a5f"
        },
        "nombre": "Sardinata",
        "descripcion": "Sardinata",
        "abreviacion": "Sardinata",
        "createdAt": "2020-01-16T13:31:29.970Z",
        "updatedAt": "2020-01-16T13:31:29.970Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a60"
        },
        "nombre": "Maicao",
        "descripcion": "Maicao",
        "abreviacion": "Maicao",
        "createdAt": "2020-01-16T13:31:29.971Z",
        "updatedAt": "2020-01-16T13:31:29.971Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a61"
        },
        "nombre": "Túquerres",
        "descripcion": "Túquerres",
        "abreviacion": "Túquerres",
        "createdAt": "2020-01-16T13:31:29.977Z",
        "updatedAt": "2020-01-16T13:31:29.977Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a62"
        },
        "nombre": "Quebradanegra",
        "descripcion": "Quebradanegra",
        "abreviacion": "Quebradanegra",
        "createdAt": "2020-01-16T13:31:29.978Z",
        "updatedAt": "2020-01-16T13:31:29.978Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a63"
        },
        "nombre": "Macheta",
        "descripcion": "Macheta",
        "abreviacion": "Macheta",
        "createdAt": "2020-01-16T13:31:29.979Z",
        "updatedAt": "2020-01-16T13:31:29.979Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a64"
        },
        "nombre": "Arboledas",
        "descripcion": "Arboledas",
        "abreviacion": "Arboledas",
        "createdAt": "2020-01-16T13:31:29.981Z",
        "updatedAt": "2020-01-16T13:31:29.981Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a65"
        },
        "nombre": "Ariguaní",
        "descripcion": "Ariguaní",
        "abreviacion": "Ariguaní",
        "createdAt": "2020-01-16T13:31:29.988Z",
        "updatedAt": "2020-01-16T13:31:29.988Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a66"
        },
        "nombre": "Líbano",
        "descripcion": "Líbano",
        "abreviacion": "Líbano",
        "createdAt": "2020-01-16T13:31:29.989Z",
        "updatedAt": "2020-01-16T13:31:29.989Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a67"
        },
        "nombre": "Quípama",
        "descripcion": "Quípama",
        "abreviacion": "Quípama",
        "createdAt": "2020-01-16T13:31:29.990Z",
        "updatedAt": "2020-01-16T13:31:29.990Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a68"
        },
        "nombre": "San Diego",
        "descripcion": "San Diego",
        "abreviacion": "San Diego",
        "createdAt": "2020-01-16T13:31:29.992Z",
        "updatedAt": "2020-01-16T13:31:29.992Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a69"
        },
        "nombre": "Quipile",
        "descripcion": "Quipile",
        "abreviacion": "Quipile",
        "createdAt": "2020-01-16T13:31:29.993Z",
        "updatedAt": "2020-01-16T13:31:29.993Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a6a"
        },
        "nombre": "Tuta",
        "descripcion": "Tuta",
        "abreviacion": "Tuta",
        "createdAt": "2020-01-16T13:31:29.994Z",
        "updatedAt": "2020-01-16T13:31:29.994Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a6b"
        },
        "nombre": "Santana",
        "descripcion": "Santana",
        "abreviacion": "Santana",
        "createdAt": "2020-01-16T13:31:29.995Z",
        "updatedAt": "2020-01-16T13:31:29.995Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a6c"
        },
        "nombre": "San Martín",
        "descripcion": "San Martín",
        "abreviacion": "San Martín",
        "createdAt": "2020-01-16T13:31:29.997Z",
        "updatedAt": "2020-01-16T13:31:29.997Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a6d"
        },
        "nombre": "Une",
        "descripcion": "Une",
        "abreviacion": "Une",
        "createdAt": "2020-01-16T13:31:29.998Z",
        "updatedAt": "2020-01-16T13:31:29.998Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b17c6e3d0008526a6e"
        },
        "nombre": "Altos del Rosario",
        "descripcion": "Altos del Rosario",
        "abreviacion": "Altos del Rosario",
        "createdAt": "2020-01-16T13:31:29.999Z",
        "updatedAt": "2020-01-16T13:31:29.999Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a6f"
        },
        "nombre": "Cartagena del Chairá",
        "descripcion": "Cartagena del Chairá",
        "abreviacion": "Cartagena del Chairá",
        "createdAt": "2020-01-16T13:31:30.001Z",
        "updatedAt": "2020-01-16T13:31:30.001Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a70"
        },
        "nombre": "Santiago de Tolú",
        "descripcion": "Santiago de Tolú",
        "abreviacion": "Santiago de Tolú",
        "createdAt": "2020-01-16T13:31:30.003Z",
        "updatedAt": "2020-01-16T13:31:30.003Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a71"
        },
        "nombre": "Santa Rosalía",
        "descripcion": "Santa Rosalía",
        "abreviacion": "Santa Rosalía",
        "createdAt": "2020-01-16T13:31:30.004Z",
        "updatedAt": "2020-01-16T13:31:30.004Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a72"
        },
        "nombre": "Olaya Herrera",
        "descripcion": "Olaya Herrera",
        "abreviacion": "Olaya Herrera",
        "createdAt": "2020-01-16T13:31:30.005Z",
        "updatedAt": "2020-01-16T13:31:30.005Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a73"
        },
        "nombre": "Santiago",
        "descripcion": "Santiago",
        "abreviacion": "Santiago",
        "createdAt": "2020-01-16T13:31:30.006Z",
        "updatedAt": "2020-01-16T13:31:30.006Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a74"
        },
        "nombre": "Hatillo de Loba",
        "descripcion": "Hatillo de Loba",
        "abreviacion": "Hatillo de Loba",
        "createdAt": "2020-01-16T13:31:30.008Z",
        "updatedAt": "2020-01-16T13:31:30.008Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a75"
        },
        "nombre": "San Juan de Río Seco",
        "descripcion": "San Juan de Río Seco",
        "abreviacion": "San Juan de Río Seco",
        "createdAt": "2020-01-16T13:31:30.009Z",
        "updatedAt": "2020-01-16T13:31:30.009Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a76"
        },
        "nombre": "Guadalajara de Buga",
        "descripcion": "Guadalajara de Buga",
        "abreviacion": "Guadalajara de Buga",
        "createdAt": "2020-01-16T13:31:30.010Z",
        "updatedAt": "2020-01-16T13:31:30.010Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a77"
        },
        "nombre": "Roldanillo",
        "descripcion": "Roldanillo",
        "abreviacion": "Roldanillo",
        "createdAt": "2020-01-16T13:31:30.012Z",
        "updatedAt": "2020-01-16T13:31:30.012Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a78"
        },
        "nombre": "Cerrito",
        "descripcion": "Cerrito",
        "abreviacion": "Cerrito",
        "createdAt": "2020-01-16T13:31:30.013Z",
        "updatedAt": "2020-01-16T13:31:30.013Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a79"
        },
        "nombre": "Cali",
        "descripcion": "Cali",
        "abreviacion": "Cali",
        "createdAt": "2020-01-16T13:31:30.014Z",
        "updatedAt": "2020-01-16T13:31:30.014Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a7a"
        },
        "nombre": "Barichara",
        "descripcion": "Barichara",
        "abreviacion": "Barichara",
        "createdAt": "2020-01-16T13:31:30.015Z",
        "updatedAt": "2020-01-16T13:31:30.015Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a7b"
        },
        "nombre": "Carcasí",
        "descripcion": "Carcasí",
        "abreviacion": "Carcasí",
        "createdAt": "2020-01-16T13:31:30.017Z",
        "updatedAt": "2020-01-16T13:31:30.017Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a7c"
        },
        "nombre": "Coromoro",
        "descripcion": "Coromoro",
        "abreviacion": "Coromoro",
        "createdAt": "2020-01-16T13:31:30.018Z",
        "updatedAt": "2020-01-16T13:31:30.018Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a7d"
        },
        "nombre": "Bucarasica",
        "descripcion": "Bucarasica",
        "abreviacion": "Bucarasica",
        "createdAt": "2020-01-16T13:31:30.019Z",
        "updatedAt": "2020-01-16T13:31:30.019Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a7e"
        },
        "nombre": "Teorama",
        "descripcion": "Teorama",
        "abreviacion": "Teorama",
        "createdAt": "2020-01-16T13:31:30.020Z",
        "updatedAt": "2020-01-16T13:31:30.020Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a7f"
        },
        "nombre": "El Tarra",
        "descripcion": "El Tarra",
        "abreviacion": "El Tarra",
        "createdAt": "2020-01-16T13:31:30.022Z",
        "updatedAt": "2020-01-16T13:31:30.022Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a80"
        },
        "nombre": "Cimitarra",
        "descripcion": "Cimitarra",
        "abreviacion": "Cimitarra",
        "createdAt": "2020-01-16T13:31:30.024Z",
        "updatedAt": "2020-01-16T13:31:30.024Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a81"
        },
        "nombre": "El Playón",
        "descripcion": "El Playón",
        "abreviacion": "El Playón",
        "createdAt": "2020-01-16T13:31:30.025Z",
        "updatedAt": "2020-01-16T13:31:30.025Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a82"
        },
        "nombre": "Gramalote",
        "descripcion": "Gramalote",
        "abreviacion": "Gramalote",
        "createdAt": "2020-01-16T13:31:30.026Z",
        "updatedAt": "2020-01-16T13:31:30.026Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a83"
        },
        "nombre": "San Luis de Gaceno",
        "descripcion": "San Luis de Gaceno",
        "abreviacion": "San Luis de Gaceno",
        "createdAt": "2020-01-16T13:31:30.027Z",
        "updatedAt": "2020-01-16T13:31:30.027Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a84"
        },
        "nombre": "Ciénaga de Oro",
        "descripcion": "Ciénaga de Oro",
        "abreviacion": "Ciénaga de Oro",
        "createdAt": "2020-01-16T13:31:30.029Z",
        "updatedAt": "2020-01-16T13:31:30.029Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a85"
        },
        "nombre": "San Agustín",
        "descripcion": "San Agustín",
        "abreviacion": "San Agustín",
        "createdAt": "2020-01-16T13:31:30.030Z",
        "updatedAt": "2020-01-16T13:31:30.030Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a86"
        },
        "nombre": "Sandoná",
        "descripcion": "Sandoná",
        "abreviacion": "Sandoná",
        "createdAt": "2020-01-16T13:31:30.031Z",
        "updatedAt": "2020-01-16T13:31:30.031Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a87"
        },
        "nombre": "Guática",
        "descripcion": "Guática",
        "abreviacion": "Guática",
        "createdAt": "2020-01-16T13:31:30.037Z",
        "updatedAt": "2020-01-16T13:31:30.037Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a88"
        },
        "nombre": "Apía",
        "descripcion": "Apía",
        "abreviacion": "Apía",
        "createdAt": "2020-01-16T13:31:30.038Z",
        "updatedAt": "2020-01-16T13:31:30.038Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a89"
        },
        "nombre": "Taminango",
        "descripcion": "Taminango",
        "abreviacion": "Taminango",
        "createdAt": "2020-01-16T13:31:30.039Z",
        "updatedAt": "2020-01-16T13:31:30.039Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a8a"
        },
        "nombre": "Bolívar",
        "descripcion": "Bolívar",
        "abreviacion": "Bolívar",
        "createdAt": "2020-01-16T13:31:30.040Z",
        "updatedAt": "2020-01-16T13:31:30.040Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a8b"
        },
        "nombre": "Barbosa",
        "descripcion": "Barbosa",
        "abreviacion": "Barbosa",
        "createdAt": "2020-01-16T13:31:30.045Z",
        "updatedAt": "2020-01-16T13:31:30.045Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a8c"
        },
        "nombre": "San Benito",
        "descripcion": "San Benito",
        "abreviacion": "San Benito",
        "createdAt": "2020-01-16T13:31:30.047Z",
        "updatedAt": "2020-01-16T13:31:30.047Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a8d"
        },
        "nombre": "Armenia",
        "descripcion": "Armenia",
        "abreviacion": "Armenia",
        "createdAt": "2020-01-16T13:31:30.048Z",
        "updatedAt": "2020-01-16T13:31:30.048Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a8e"
        },
        "nombre": "Belmira",
        "descripcion": "Belmira",
        "abreviacion": "Belmira",
        "createdAt": "2020-01-16T13:31:30.049Z",
        "updatedAt": "2020-01-16T13:31:30.049Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a8f"
        },
        "nombre": "Samaniego",
        "descripcion": "Samaniego",
        "abreviacion": "Samaniego",
        "createdAt": "2020-01-16T13:31:30.051Z",
        "updatedAt": "2020-01-16T13:31:30.051Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a90"
        },
        "nombre": "La Jagua de Ibirico",
        "descripcion": "La Jagua de Ibirico",
        "abreviacion": "La Jagua de Ibirico",
        "createdAt": "2020-01-16T13:31:30.052Z",
        "updatedAt": "2020-01-16T13:31:30.052Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a91"
        },
        "nombre": "Santuario",
        "descripcion": "Santuario",
        "abreviacion": "Santuario",
        "createdAt": "2020-01-16T13:31:30.053Z",
        "updatedAt": "2020-01-16T13:31:30.053Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a92"
        },
        "nombre": "La Tebaida",
        "descripcion": "La Tebaida",
        "abreviacion": "La Tebaida",
        "createdAt": "2020-01-16T13:31:30.054Z",
        "updatedAt": "2020-01-16T13:31:30.054Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a93"
        },
        "nombre": "Mistrató",
        "descripcion": "Mistrató",
        "abreviacion": "Mistrató",
        "createdAt": "2020-01-16T13:31:30.056Z",
        "updatedAt": "2020-01-16T13:31:30.056Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a94"
        },
        "nombre": "San Sebastián",
        "descripcion": "San Sebastián",
        "abreviacion": "San Sebastián",
        "createdAt": "2020-01-16T13:31:30.057Z",
        "updatedAt": "2020-01-16T13:31:30.057Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a95"
        },
        "nombre": "San Pedro",
        "descripcion": "San Pedro",
        "abreviacion": "San Pedro",
        "createdAt": "2020-01-16T13:31:30.058Z",
        "updatedAt": "2020-01-16T13:31:30.058Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a96"
        },
        "nombre": "Campo de La Cruz",
        "descripcion": "Campo de La Cruz",
        "abreviacion": "Campo de La Cruz",
        "createdAt": "2020-01-16T13:31:30.059Z",
        "updatedAt": "2020-01-16T13:31:30.059Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a97"
        },
        "nombre": "Potosí",
        "descripcion": "Potosí",
        "abreviacion": "Potosí",
        "createdAt": "2020-01-16T13:31:30.061Z",
        "updatedAt": "2020-01-16T13:31:30.061Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a98"
        },
        "nombre": "Salento",
        "descripcion": "Salento",
        "abreviacion": "Salento",
        "createdAt": "2020-01-16T13:31:30.062Z",
        "updatedAt": "2020-01-16T13:31:30.062Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a99"
        },
        "nombre": "Lourdes",
        "descripcion": "Lourdes",
        "abreviacion": "Lourdes",
        "createdAt": "2020-01-16T13:31:30.063Z",
        "updatedAt": "2020-01-16T13:31:30.063Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a9a"
        },
        "nombre": "Linares",
        "descripcion": "Linares",
        "abreviacion": "Linares",
        "createdAt": "2020-01-16T13:31:30.064Z",
        "updatedAt": "2020-01-16T13:31:30.064Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a9b"
        },
        "nombre": "Tamalameque",
        "descripcion": "Tamalameque",
        "abreviacion": "Tamalameque",
        "createdAt": "2020-01-16T13:31:30.066Z",
        "updatedAt": "2020-01-16T13:31:30.066Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a9c"
        },
        "nombre": "Lorica",
        "descripcion": "Lorica",
        "abreviacion": "Lorica",
        "createdAt": "2020-01-16T13:31:30.067Z",
        "updatedAt": "2020-01-16T13:31:30.067Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a9d"
        },
        "nombre": "Suárez",
        "descripcion": "Suárez",
        "abreviacion": "Suárez",
        "createdAt": "2020-01-16T13:31:30.068Z",
        "updatedAt": "2020-01-16T13:31:30.068Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a9e"
        },
        "nombre": "Nariño",
        "descripcion": "Nariño",
        "abreviacion": "Nariño",
        "createdAt": "2020-01-16T13:31:30.069Z",
        "updatedAt": "2020-01-16T13:31:30.069Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526a9f"
        },
        "nombre": "San Jacinto del Cauca",
        "descripcion": "San Jacinto del Cauca",
        "abreviacion": "San Jacinto del Cauca",
        "createdAt": "2020-01-16T13:31:30.071Z",
        "updatedAt": "2020-01-16T13:31:30.071Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526aa0"
        },
        "nombre": "Santander de Quilichao",
        "descripcion": "Santander de Quilichao",
        "abreviacion": "Santander de Quilichao",
        "createdAt": "2020-01-16T13:31:30.072Z",
        "updatedAt": "2020-01-16T13:31:30.072Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526aa1"
        },
        "nombre": "El Copey",
        "descripcion": "El Copey",
        "abreviacion": "El Copey",
        "createdAt": "2020-01-16T13:31:30.073Z",
        "updatedAt": "2020-01-16T13:31:30.073Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526aa2"
        },
        "nombre": "Fundación",
        "descripcion": "Fundación",
        "abreviacion": "Fundación",
        "createdAt": "2020-01-16T13:31:30.074Z",
        "updatedAt": "2020-01-16T13:31:30.074Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526aa3"
        },
        "nombre": "Gamarra",
        "descripcion": "Gamarra",
        "abreviacion": "Gamarra",
        "createdAt": "2020-01-16T13:31:30.076Z",
        "updatedAt": "2020-01-16T13:31:30.076Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526aa4"
        },
        "nombre": "Astrea",
        "descripcion": "Astrea",
        "abreviacion": "Astrea",
        "createdAt": "2020-01-16T13:31:30.077Z",
        "updatedAt": "2020-01-16T13:31:30.077Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526aa5"
        },
        "nombre": "Santa María",
        "descripcion": "Santa María",
        "abreviacion": "Santa María",
        "createdAt": "2020-01-16T13:31:30.078Z",
        "updatedAt": "2020-01-16T13:31:30.078Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526aa6"
        },
        "nombre": "San Juan de Arama",
        "descripcion": "San Juan de Arama",
        "abreviacion": "San Juan de Arama",
        "createdAt": "2020-01-16T13:31:30.079Z",
        "updatedAt": "2020-01-16T13:31:30.079Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526aa7"
        },
        "nombre": "Silvania",
        "descripcion": "Silvania",
        "abreviacion": "Silvania",
        "createdAt": "2020-01-16T13:31:30.081Z",
        "updatedAt": "2020-01-16T13:31:30.081Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526aa8"
        },
        "nombre": "Santa María",
        "descripcion": "Santa María",
        "abreviacion": "Santa María",
        "createdAt": "2020-01-16T13:31:30.082Z",
        "updatedAt": "2020-01-16T13:31:30.082Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526aa9"
        },
        "nombre": "Caicedonia",
        "descripcion": "Caicedonia",
        "abreviacion": "Caicedonia",
        "createdAt": "2020-01-16T13:31:30.083Z",
        "updatedAt": "2020-01-16T13:31:30.083Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526aaa"
        },
        "nombre": "Córdoba",
        "descripcion": "Córdoba",
        "abreviacion": "Córdoba",
        "createdAt": "2020-01-16T13:31:30.087Z",
        "updatedAt": "2020-01-16T13:31:30.087Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526aab"
        },
        "nombre": "Campoalegre",
        "descripcion": "Campoalegre",
        "abreviacion": "Campoalegre",
        "createdAt": "2020-01-16T13:31:30.088Z",
        "updatedAt": "2020-01-16T13:31:30.088Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526aac"
        },
        "nombre": "Mosquera",
        "descripcion": "Mosquera",
        "abreviacion": "Mosquera",
        "createdAt": "2020-01-16T13:31:30.089Z",
        "updatedAt": "2020-01-16T13:31:30.089Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526aad"
        },
        "nombre": "Pelaya",
        "descripcion": "Pelaya",
        "abreviacion": "Pelaya",
        "createdAt": "2020-01-16T13:31:30.090Z",
        "updatedAt": "2020-01-16T13:31:30.090Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526aae"
        },
        "nombre": "Sora",
        "descripcion": "Sora",
        "abreviacion": "Sora",
        "createdAt": "2020-01-16T13:31:30.092Z",
        "updatedAt": "2020-01-16T13:31:30.092Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526aaf"
        },
        "nombre": "San Francisco",
        "descripcion": "San Francisco",
        "abreviacion": "San Francisco",
        "createdAt": "2020-01-16T13:31:30.093Z",
        "updatedAt": "2020-01-16T13:31:30.093Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526ab0"
        },
        "nombre": "Puerto Caicedo",
        "descripcion": "Puerto Caicedo",
        "abreviacion": "Puerto Caicedo",
        "createdAt": "2020-01-16T13:31:30.094Z",
        "updatedAt": "2020-01-16T13:31:30.094Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526ab1"
        },
        "nombre": "Puerto Nariño",
        "descripcion": "Puerto Nariño",
        "abreviacion": "Puerto Nariño",
        "createdAt": "2020-01-16T13:31:30.095Z",
        "updatedAt": "2020-01-16T13:31:30.095Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526ab2"
        },
        "nombre": "La Salina",
        "descripcion": "La Salina",
        "abreviacion": "La Salina",
        "createdAt": "2020-01-16T13:31:30.097Z",
        "updatedAt": "2020-01-16T13:31:30.097Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526ab3"
        },
        "nombre": "Sabanalarga",
        "descripcion": "Sabanalarga",
        "abreviacion": "Sabanalarga",
        "createdAt": "2020-01-16T13:31:30.098Z",
        "updatedAt": "2020-01-16T13:31:30.098Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526ab4"
        },
        "nombre": "Monterrey",
        "descripcion": "Monterrey",
        "abreviacion": "Monterrey",
        "createdAt": "2020-01-16T13:31:30.099Z",
        "updatedAt": "2020-01-16T13:31:30.099Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526ab5"
        },
        "nombre": "Puerto Guzmán",
        "descripcion": "Puerto Guzmán",
        "abreviacion": "Puerto Guzmán",
        "createdAt": "2020-01-16T13:31:30.104Z",
        "updatedAt": "2020-01-16T13:31:30.104Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526ab6"
        },
        "nombre": "Arauca",
        "descripcion": "Arauca",
        "abreviacion": "Arauca",
        "createdAt": "2020-01-16T13:31:30.105Z",
        "updatedAt": "2020-01-16T13:31:30.105Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526ab7"
        },
        "nombre": "Cunday",
        "descripcion": "Cunday",
        "abreviacion": "Cunday",
        "createdAt": "2020-01-16T13:31:30.107Z",
        "updatedAt": "2020-01-16T13:31:30.107Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526ab8"
        },
        "nombre": "Palocabildo",
        "descripcion": "Palocabildo",
        "abreviacion": "Palocabildo",
        "createdAt": "2020-01-16T13:31:30.108Z",
        "updatedAt": "2020-01-16T13:31:30.108Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526ab9"
        },
        "nombre": "Bolívar",
        "descripcion": "Bolívar",
        "abreviacion": "Bolívar",
        "createdAt": "2020-01-16T13:31:30.109Z",
        "updatedAt": "2020-01-16T13:31:30.109Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526aba"
        },
        "nombre": "Honda",
        "descripcion": "Honda",
        "abreviacion": "Honda",
        "createdAt": "2020-01-16T13:31:30.111Z",
        "updatedAt": "2020-01-16T13:31:30.111Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526abb"
        },
        "nombre": "Socotá",
        "descripcion": "Socotá",
        "abreviacion": "Socotá",
        "createdAt": "2020-01-16T13:31:30.112Z",
        "updatedAt": "2020-01-16T13:31:30.112Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526abc"
        },
        "nombre": "Algarrobo",
        "descripcion": "Algarrobo",
        "abreviacion": "Algarrobo",
        "createdAt": "2020-01-16T13:31:30.113Z",
        "updatedAt": "2020-01-16T13:31:30.113Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526abd"
        },
        "nombre": "Sapuyes",
        "descripcion": "Sapuyes",
        "abreviacion": "Sapuyes",
        "createdAt": "2020-01-16T13:31:30.114Z",
        "updatedAt": "2020-01-16T13:31:30.114Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526abe"
        },
        "nombre": "San Cayetano",
        "descripcion": "San Cayetano",
        "abreviacion": "San Cayetano",
        "createdAt": "2020-01-16T13:31:30.116Z",
        "updatedAt": "2020-01-16T13:31:30.116Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526abf"
        },
        "nombre": "Chinácota",
        "descripcion": "Chinácota",
        "abreviacion": "Chinácota",
        "createdAt": "2020-01-16T13:31:30.117Z",
        "updatedAt": "2020-01-16T13:31:30.117Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526ac0"
        },
        "nombre": "Betulia",
        "descripcion": "Betulia",
        "abreviacion": "Betulia",
        "createdAt": "2020-01-16T13:31:30.118Z",
        "updatedAt": "2020-01-16T13:31:30.118Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526ac1"
        },
        "nombre": "Charalá",
        "descripcion": "Charalá",
        "abreviacion": "Charalá",
        "createdAt": "2020-01-16T13:31:30.119Z",
        "updatedAt": "2020-01-16T13:31:30.119Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526ac2"
        },
        "nombre": "Jerusalén",
        "descripcion": "Jerusalén",
        "abreviacion": "Jerusalén",
        "createdAt": "2020-01-16T13:31:30.121Z",
        "updatedAt": "2020-01-16T13:31:30.121Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526ac3"
        },
        "nombre": "Puerto Santander",
        "descripcion": "Puerto Santander",
        "abreviacion": "Puerto Santander",
        "createdAt": "2020-01-16T13:31:30.122Z",
        "updatedAt": "2020-01-16T13:31:30.122Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526ac4"
        },
        "nombre": "Cucutilla",
        "descripcion": "Cucutilla",
        "abreviacion": "Cucutilla",
        "createdAt": "2020-01-16T13:31:30.123Z",
        "updatedAt": "2020-01-16T13:31:30.123Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b27c6e3d0008526ac5"
        },
        "nombre": "Pasca",
        "descripcion": "Pasca",
        "abreviacion": "Pasca",
        "createdAt": "2020-01-16T13:31:30.129Z",
        "updatedAt": "2020-01-16T13:31:30.129Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b67c6e3d0008526ac6"
        },
        "nombre": "Puerto Rico",
        "descripcion": "Puerto Rico",
        "abreviacion": "Puerto Rico",
        "createdAt": "2020-01-16T13:31:34.037Z",
        "updatedAt": "2020-01-16T13:31:34.037Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b67c6e3d0008526ac7"
        },
        "nombre": "Chiquinquirá",
        "descripcion": "Chiquinquirá",
        "abreviacion": "Chiquinquirá",
        "createdAt": "2020-01-16T13:31:34.038Z",
        "updatedAt": "2020-01-16T13:31:34.038Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b67c6e3d0008526ac8"
        },
        "nombre": "Marmato",
        "descripcion": "Marmato",
        "abreviacion": "Marmato",
        "createdAt": "2020-01-16T13:31:34.100Z",
        "updatedAt": "2020-01-16T13:31:34.100Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526ac9"
        },
        "nombre": "Caldas",
        "descripcion": "Caldas",
        "abreviacion": "Caldas",
        "createdAt": "2020-01-16T13:31:36.159Z",
        "updatedAt": "2020-01-16T13:31:36.159Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526aca"
        },
        "nombre": "Cucaita",
        "descripcion": "Cucaita",
        "abreviacion": "Cucaita",
        "createdAt": "2020-01-16T13:31:36.162Z",
        "updatedAt": "2020-01-16T13:31:36.162Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526acb"
        },
        "nombre": "Tinjacá",
        "descripcion": "Tinjacá",
        "abreviacion": "Tinjacá",
        "createdAt": "2020-01-16T13:31:36.163Z",
        "updatedAt": "2020-01-16T13:31:36.163Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526acc"
        },
        "nombre": "Río Quito",
        "descripcion": "Río Quito",
        "abreviacion": "Río Quito",
        "createdAt": "2020-01-16T13:31:36.310Z",
        "updatedAt": "2020-01-16T13:31:36.310Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526acd"
        },
        "nombre": "Puerto Libertador",
        "descripcion": "Puerto Libertador",
        "abreviacion": "Puerto Libertador",
        "createdAt": "2020-01-16T13:31:36.313Z",
        "updatedAt": "2020-01-16T13:31:36.313Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526ace"
        },
        "nombre": "Pital",
        "descripcion": "Pital",
        "abreviacion": "Pital",
        "createdAt": "2020-01-16T13:31:36.318Z",
        "updatedAt": "2020-01-16T13:31:36.318Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526acf"
        },
        "nombre": "La Primavera",
        "descripcion": "La Primavera",
        "abreviacion": "La Primavera",
        "createdAt": "2020-01-16T13:31:36.320Z",
        "updatedAt": "2020-01-16T13:31:36.320Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526ad0"
        },
        "nombre": "Fuente de Oro",
        "descripcion": "Fuente de Oro",
        "abreviacion": "Fuente de Oro",
        "createdAt": "2020-01-16T13:31:36.326Z",
        "updatedAt": "2020-01-16T13:31:36.326Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526ad1"
        },
        "nombre": "Magangué",
        "descripcion": "Magangué",
        "abreviacion": "Magangué",
        "createdAt": "2020-01-16T13:31:36.349Z",
        "updatedAt": "2020-01-16T13:31:36.349Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526ad2"
        },
        "nombre": "Buenaventura",
        "descripcion": "Buenaventura",
        "abreviacion": "Buenaventura",
        "createdAt": "2020-01-16T13:31:36.351Z",
        "updatedAt": "2020-01-16T13:31:36.351Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526ad3"
        },
        "nombre": "Villavicencio",
        "descripcion": "Villavicencio",
        "abreviacion": "Villavicencio",
        "createdAt": "2020-01-16T13:31:36.353Z",
        "updatedAt": "2020-01-16T13:31:36.353Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526ad4"
        },
        "nombre": "Condoto",
        "descripcion": "Condoto",
        "abreviacion": "Condoto",
        "createdAt": "2020-01-16T13:31:36.357Z",
        "updatedAt": "2020-01-16T13:31:36.357Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526ad5"
        },
        "nombre": "Cumaribo",
        "descripcion": "Cumaribo",
        "abreviacion": "Cumaribo",
        "createdAt": "2020-01-16T13:31:36.359Z",
        "updatedAt": "2020-01-16T13:31:36.359Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526ad6"
        },
        "nombre": "Aguachica",
        "descripcion": "Aguachica",
        "abreviacion": "Aguachica",
        "createdAt": "2020-01-16T13:31:36.368Z",
        "updatedAt": "2020-01-16T13:31:36.368Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526ad7"
        },
        "nombre": "Bojaya",
        "descripcion": "Bojaya",
        "abreviacion": "Bojaya",
        "createdAt": "2020-01-16T13:31:36.376Z",
        "updatedAt": "2020-01-16T13:31:36.376Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526ad8"
        },
        "nombre": "Cuaspud",
        "descripcion": "Cuaspud",
        "abreviacion": "Cuaspud",
        "createdAt": "2020-01-16T13:31:36.378Z",
        "updatedAt": "2020-01-16T13:31:36.378Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526ad9"
        },
        "nombre": "Villagarzón",
        "descripcion": "Villagarzón",
        "abreviacion": "Villagarzón",
        "createdAt": "2020-01-16T13:31:36.380Z",
        "updatedAt": "2020-01-16T13:31:36.380Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526ada"
        },
        "nombre": "Nueva Granada",
        "descripcion": "Nueva Granada",
        "abreviacion": "Nueva Granada",
        "createdAt": "2020-01-16T13:31:36.387Z",
        "updatedAt": "2020-01-16T13:31:36.387Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526adb"
        },
        "nombre": "Teruel",
        "descripcion": "Teruel",
        "abreviacion": "Teruel",
        "createdAt": "2020-01-16T13:31:36.388Z",
        "updatedAt": "2020-01-16T13:31:36.388Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526adc"
        },
        "nombre": "Rivera",
        "descripcion": "Rivera",
        "abreviacion": "Rivera",
        "createdAt": "2020-01-16T13:31:36.397Z",
        "updatedAt": "2020-01-16T13:31:36.397Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526add"
        },
        "nombre": "Unión Panamericana",
        "descripcion": "Unión Panamericana",
        "abreviacion": "Unión Panamericana",
        "createdAt": "2020-01-16T13:31:36.398Z",
        "updatedAt": "2020-01-16T13:31:36.398Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526ade"
        },
        "nombre": "Muzo",
        "descripcion": "Muzo",
        "abreviacion": "Muzo",
        "createdAt": "2020-01-16T13:31:36.400Z",
        "updatedAt": "2020-01-16T13:31:36.400Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526adf"
        },
        "nombre": "Cumbitara",
        "descripcion": "Cumbitara",
        "abreviacion": "Cumbitara",
        "createdAt": "2020-01-16T13:31:36.407Z",
        "updatedAt": "2020-01-16T13:31:36.407Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526ae0"
        },
        "nombre": "El Piñon",
        "descripcion": "El Piñon",
        "abreviacion": "El Piñon",
        "createdAt": "2020-01-16T13:31:36.408Z",
        "updatedAt": "2020-01-16T13:31:36.408Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526ae1"
        },
        "nombre": "Algeciras",
        "descripcion": "Algeciras",
        "abreviacion": "Algeciras",
        "createdAt": "2020-01-16T13:31:36.410Z",
        "updatedAt": "2020-01-16T13:31:36.410Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526ae2"
        },
        "nombre": "Almaguer",
        "descripcion": "Almaguer",
        "abreviacion": "Almaguer",
        "createdAt": "2020-01-16T13:31:36.413Z",
        "updatedAt": "2020-01-16T13:31:36.413Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526ae3"
        },
        "nombre": "La Montañita",
        "descripcion": "La Montañita",
        "abreviacion": "La Montañita",
        "createdAt": "2020-01-16T13:31:36.417Z",
        "updatedAt": "2020-01-16T13:31:36.417Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526ae4"
        },
        "nombre": "Aranzazu",
        "descripcion": "Aranzazu",
        "abreviacion": "Aranzazu",
        "createdAt": "2020-01-16T13:31:36.418Z",
        "updatedAt": "2020-01-16T13:31:36.418Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526ae5"
        },
        "nombre": "Argelia",
        "descripcion": "Argelia",
        "abreviacion": "Argelia",
        "createdAt": "2020-01-16T13:31:36.421Z",
        "updatedAt": "2020-01-16T13:31:36.421Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526ae6"
        },
        "nombre": "Belalcázar",
        "descripcion": "Belalcázar",
        "abreviacion": "Belalcázar",
        "createdAt": "2020-01-16T13:31:36.424Z",
        "updatedAt": "2020-01-16T13:31:36.424Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526ae7"
        },
        "nombre": "Puerres",
        "descripcion": "Puerres",
        "abreviacion": "Puerres",
        "createdAt": "2020-01-16T13:31:36.426Z",
        "updatedAt": "2020-01-16T13:31:36.426Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526ae8"
        },
        "nombre": "Balboa",
        "descripcion": "Balboa",
        "abreviacion": "Balboa",
        "createdAt": "2020-01-16T13:31:36.427Z",
        "updatedAt": "2020-01-16T13:31:36.427Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526ae9"
        },
        "nombre": "Riosucio",
        "descripcion": "Riosucio",
        "abreviacion": "Riosucio",
        "createdAt": "2020-01-16T13:31:36.429Z",
        "updatedAt": "2020-01-16T13:31:36.429Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526aea"
        },
        "nombre": "Cumbal",
        "descripcion": "Cumbal",
        "abreviacion": "Cumbal",
        "createdAt": "2020-01-16T13:31:36.431Z",
        "updatedAt": "2020-01-16T13:31:36.431Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526aeb"
        },
        "nombre": "Chiriguaná",
        "descripcion": "Chiriguaná",
        "abreviacion": "Chiriguaná",
        "createdAt": "2020-01-16T13:31:36.433Z",
        "updatedAt": "2020-01-16T13:31:36.433Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526aec"
        },
        "nombre": "Agustín Codazzi",
        "descripcion": "Agustín Codazzi",
        "abreviacion": "Agustín Codazzi",
        "createdAt": "2020-01-16T13:31:36.434Z",
        "updatedAt": "2020-01-16T13:31:36.434Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526aed"
        },
        "nombre": "Bosconia",
        "descripcion": "Bosconia",
        "abreviacion": "Bosconia",
        "createdAt": "2020-01-16T13:31:36.435Z",
        "updatedAt": "2020-01-16T13:31:36.435Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526aee"
        },
        "nombre": "Tadó",
        "descripcion": "Tadó",
        "abreviacion": "Tadó",
        "createdAt": "2020-01-16T13:31:36.437Z",
        "updatedAt": "2020-01-16T13:31:36.437Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526aef"
        },
        "nombre": "Becerril",
        "descripcion": "Becerril",
        "abreviacion": "Becerril",
        "createdAt": "2020-01-16T13:31:36.438Z",
        "updatedAt": "2020-01-16T13:31:36.438Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526af0"
        },
        "nombre": "Tibaná",
        "descripcion": "Tibaná",
        "abreviacion": "Tibaná",
        "createdAt": "2020-01-16T13:31:36.439Z",
        "updatedAt": "2020-01-16T13:31:36.439Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526af1"
        },
        "nombre": "Manizales",
        "descripcion": "Manizales",
        "abreviacion": "Manizales",
        "createdAt": "2020-01-16T13:31:36.441Z",
        "updatedAt": "2020-01-16T13:31:36.441Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526af2"
        },
        "nombre": "Manaure",
        "descripcion": "Manaure",
        "abreviacion": "Manaure",
        "createdAt": "2020-01-16T13:31:36.442Z",
        "updatedAt": "2020-01-16T13:31:36.442Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526af3"
        },
        "nombre": "Granada",
        "descripcion": "Granada",
        "abreviacion": "Granada",
        "createdAt": "2020-01-16T13:31:36.443Z",
        "updatedAt": "2020-01-16T13:31:36.443Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526af4"
        },
        "nombre": "Páez",
        "descripcion": "Páez",
        "abreviacion": "Páez",
        "createdAt": "2020-01-16T13:31:36.446Z",
        "updatedAt": "2020-01-16T13:31:36.446Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526af5"
        },
        "nombre": "Tibacuy",
        "descripcion": "Tibacuy",
        "abreviacion": "Tibacuy",
        "createdAt": "2020-01-16T13:31:36.447Z",
        "updatedAt": "2020-01-16T13:31:36.447Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526af6"
        },
        "nombre": "El Calvario",
        "descripcion": "El Calvario",
        "abreviacion": "El Calvario",
        "createdAt": "2020-01-16T13:31:36.449Z",
        "updatedAt": "2020-01-16T13:31:36.449Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526af7"
        },
        "nombre": "Bolívar",
        "descripcion": "Bolívar",
        "abreviacion": "Bolívar",
        "createdAt": "2020-01-16T13:31:36.450Z",
        "updatedAt": "2020-01-16T13:31:36.450Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526af8"
        },
        "nombre": "Suaza",
        "descripcion": "Suaza",
        "abreviacion": "Suaza",
        "createdAt": "2020-01-16T13:31:36.452Z",
        "updatedAt": "2020-01-16T13:31:36.452Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526af9"
        },
        "nombre": "Villagómez",
        "descripcion": "Villagómez",
        "abreviacion": "Villagómez",
        "createdAt": "2020-01-16T13:31:36.453Z",
        "updatedAt": "2020-01-16T13:31:36.453Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526afa"
        },
        "nombre": "La Capilla",
        "descripcion": "La Capilla",
        "abreviacion": "La Capilla",
        "createdAt": "2020-01-16T13:31:36.465Z",
        "updatedAt": "2020-01-16T13:31:36.465Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526afb"
        },
        "nombre": "Montecristo",
        "descripcion": "Montecristo",
        "abreviacion": "Montecristo",
        "createdAt": "2020-01-16T13:31:36.472Z",
        "updatedAt": "2020-01-16T13:31:36.472Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526afc"
        },
        "nombre": "Puerto López",
        "descripcion": "Puerto López",
        "abreviacion": "Puerto López",
        "createdAt": "2020-01-16T13:31:36.473Z",
        "updatedAt": "2020-01-16T13:31:36.473Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526afd"
        },
        "nombre": "Umbita",
        "descripcion": "Umbita",
        "abreviacion": "Umbita",
        "createdAt": "2020-01-16T13:31:36.475Z",
        "updatedAt": "2020-01-16T13:31:36.475Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526afe"
        },
        "nombre": "Barranquilla",
        "descripcion": "Barranquilla",
        "abreviacion": "Barranquilla",
        "createdAt": "2020-01-16T13:31:36.476Z",
        "updatedAt": "2020-01-16T13:31:36.476Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526aff"
        },
        "nombre": "Juradó",
        "descripcion": "Juradó",
        "abreviacion": "Juradó",
        "createdAt": "2020-01-16T13:31:36.478Z",
        "updatedAt": "2020-01-16T13:31:36.478Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b00"
        },
        "nombre": "Tununguá",
        "descripcion": "Tununguá",
        "abreviacion": "Tununguá",
        "createdAt": "2020-01-16T13:31:36.479Z",
        "updatedAt": "2020-01-16T13:31:36.479Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b01"
        },
        "nombre": "Ciénaga",
        "descripcion": "Ciénaga",
        "abreviacion": "Ciénaga",
        "createdAt": "2020-01-16T13:31:36.480Z",
        "updatedAt": "2020-01-16T13:31:36.480Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b02"
        },
        "nombre": "Copacabana",
        "descripcion": "Copacabana",
        "abreviacion": "Copacabana",
        "createdAt": "2020-01-16T13:31:36.482Z",
        "updatedAt": "2020-01-16T13:31:36.482Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b03"
        },
        "nombre": "Anserma",
        "descripcion": "Anserma",
        "abreviacion": "Anserma",
        "createdAt": "2020-01-16T13:31:36.483Z",
        "updatedAt": "2020-01-16T13:31:36.483Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b04"
        },
        "nombre": "Aldana",
        "descripcion": "Aldana",
        "abreviacion": "Aldana",
        "createdAt": "2020-01-16T13:31:36.484Z",
        "updatedAt": "2020-01-16T13:31:36.484Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b05"
        },
        "nombre": "Cumaral",
        "descripcion": "Cumaral",
        "abreviacion": "Cumaral",
        "createdAt": "2020-01-16T13:31:36.487Z",
        "updatedAt": "2020-01-16T13:31:36.487Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b06"
        },
        "nombre": "San Zenón",
        "descripcion": "San Zenón",
        "abreviacion": "San Zenón",
        "createdAt": "2020-01-16T13:31:36.488Z",
        "updatedAt": "2020-01-16T13:31:36.488Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b07"
        },
        "nombre": "Santa Catalina",
        "descripcion": "Santa Catalina",
        "abreviacion": "Santa Catalina",
        "createdAt": "2020-01-16T13:31:36.490Z",
        "updatedAt": "2020-01-16T13:31:36.490Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b08"
        },
        "nombre": "Tena",
        "descripcion": "Tena",
        "abreviacion": "Tena",
        "createdAt": "2020-01-16T13:31:36.491Z",
        "updatedAt": "2020-01-16T13:31:36.491Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b09"
        },
        "nombre": "Mercaderes",
        "descripcion": "Mercaderes",
        "abreviacion": "Mercaderes",
        "createdAt": "2020-01-16T13:31:36.492Z",
        "updatedAt": "2020-01-16T13:31:36.492Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b0a"
        },
        "nombre": "Pácora",
        "descripcion": "Pácora",
        "abreviacion": "Pácora",
        "createdAt": "2020-01-16T13:31:36.493Z",
        "updatedAt": "2020-01-16T13:31:36.493Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b0b"
        },
        "nombre": "Sucre",
        "descripcion": "Sucre",
        "abreviacion": "Sucre",
        "createdAt": "2020-01-16T13:31:36.495Z",
        "updatedAt": "2020-01-16T13:31:36.495Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b0c"
        },
        "nombre": "Totoró",
        "descripcion": "Totoró",
        "abreviacion": "Totoró",
        "createdAt": "2020-01-16T13:31:36.496Z",
        "updatedAt": "2020-01-16T13:31:36.496Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b0d"
        },
        "nombre": "Bagadó",
        "descripcion": "Bagadó",
        "abreviacion": "Bagadó",
        "createdAt": "2020-01-16T13:31:36.497Z",
        "updatedAt": "2020-01-16T13:31:36.497Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b0e"
        },
        "nombre": "Valledupar",
        "descripcion": "Valledupar",
        "abreviacion": "Valledupar",
        "createdAt": "2020-01-16T13:31:36.499Z",
        "updatedAt": "2020-01-16T13:31:36.499Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b0f"
        },
        "nombre": "El Peñol",
        "descripcion": "El Peñol",
        "abreviacion": "El Peñol",
        "createdAt": "2020-01-16T13:31:36.500Z",
        "updatedAt": "2020-01-16T13:31:36.500Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b10"
        },
        "nombre": "La Paz",
        "descripcion": "La Paz",
        "abreviacion": "La Paz",
        "createdAt": "2020-01-16T13:31:36.502Z",
        "updatedAt": "2020-01-16T13:31:36.502Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b11"
        },
        "nombre": "Tocaima",
        "descripcion": "Tocaima",
        "abreviacion": "Tocaima",
        "createdAt": "2020-01-16T13:31:36.503Z",
        "updatedAt": "2020-01-16T13:31:36.503Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b12"
        },
        "nombre": "Agrado",
        "descripcion": "Agrado",
        "abreviacion": "Agrado",
        "createdAt": "2020-01-16T13:31:36.505Z",
        "updatedAt": "2020-01-16T13:31:36.505Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b13"
        },
        "nombre": "Nátaga",
        "descripcion": "Nátaga",
        "abreviacion": "Nátaga",
        "createdAt": "2020-01-16T13:31:36.507Z",
        "updatedAt": "2020-01-16T13:31:36.507Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b14"
        },
        "nombre": "El Paujil",
        "descripcion": "El Paujil",
        "abreviacion": "El Paujil",
        "createdAt": "2020-01-16T13:31:36.511Z",
        "updatedAt": "2020-01-16T13:31:36.511Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b15"
        },
        "nombre": "Elías",
        "descripcion": "Elías",
        "abreviacion": "Elías",
        "createdAt": "2020-01-16T13:31:36.513Z",
        "updatedAt": "2020-01-16T13:31:36.513Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b16"
        },
        "nombre": "Tota",
        "descripcion": "Tota",
        "abreviacion": "Tota",
        "createdAt": "2020-01-16T13:31:36.514Z",
        "updatedAt": "2020-01-16T13:31:36.514Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b17"
        },
        "nombre": "Guachucal",
        "descripcion": "Guachucal",
        "abreviacion": "Guachucal",
        "createdAt": "2020-01-16T13:31:36.516Z",
        "updatedAt": "2020-01-16T13:31:36.516Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b18"
        },
        "nombre": "Santa Marta",
        "descripcion": "Santa Marta",
        "abreviacion": "Santa Marta",
        "createdAt": "2020-01-16T13:31:36.517Z",
        "updatedAt": "2020-01-16T13:31:36.517Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b19"
        },
        "nombre": "El Tambo",
        "descripcion": "El Tambo",
        "abreviacion": "El Tambo",
        "createdAt": "2020-01-16T13:31:36.518Z",
        "updatedAt": "2020-01-16T13:31:36.518Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b1a"
        },
        "nombre": "Chivolo",
        "descripcion": "Chivolo",
        "abreviacion": "Chivolo",
        "createdAt": "2020-01-16T13:31:36.519Z",
        "updatedAt": "2020-01-16T13:31:36.519Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b1b"
        },
        "nombre": "Consaca",
        "descripcion": "Consaca",
        "abreviacion": "Consaca",
        "createdAt": "2020-01-16T13:31:36.523Z",
        "updatedAt": "2020-01-16T13:31:36.523Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b1c"
        },
        "nombre": "Albán",
        "descripcion": "Albán",
        "abreviacion": "Albán",
        "createdAt": "2020-01-16T13:31:36.524Z",
        "updatedAt": "2020-01-16T13:31:36.524Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b1d"
        },
        "nombre": "Contadero",
        "descripcion": "Contadero",
        "abreviacion": "Contadero",
        "createdAt": "2020-01-16T13:31:36.526Z",
        "updatedAt": "2020-01-16T13:31:36.526Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b1e"
        },
        "nombre": "Medio San Juan",
        "descripcion": "Medio San Juan",
        "abreviacion": "Medio San Juan",
        "createdAt": "2020-01-16T13:31:36.527Z",
        "updatedAt": "2020-01-16T13:31:36.527Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b1f"
        },
        "nombre": "Distracción",
        "descripcion": "Distracción",
        "abreviacion": "Distracción",
        "createdAt": "2020-01-16T13:31:36.528Z",
        "updatedAt": "2020-01-16T13:31:36.528Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b20"
        },
        "nombre": "Manaure",
        "descripcion": "Manaure",
        "abreviacion": "Manaure",
        "createdAt": "2020-01-16T13:31:36.531Z",
        "updatedAt": "2020-01-16T13:31:36.531Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b21"
        },
        "nombre": "La Argentina",
        "descripcion": "La Argentina",
        "abreviacion": "La Argentina",
        "createdAt": "2020-01-16T13:31:36.532Z",
        "updatedAt": "2020-01-16T13:31:36.532Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b22"
        },
        "nombre": "Puerto Concordia",
        "descripcion": "Puerto Concordia",
        "abreviacion": "Puerto Concordia",
        "createdAt": "2020-01-16T13:31:36.538Z",
        "updatedAt": "2020-01-16T13:31:36.538Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b23"
        },
        "nombre": "Popayán",
        "descripcion": "Popayán",
        "abreviacion": "Popayán",
        "createdAt": "2020-01-16T13:31:36.539Z",
        "updatedAt": "2020-01-16T13:31:36.539Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b24"
        },
        "nombre": "La Cruz",
        "descripcion": "La Cruz",
        "abreviacion": "La Cruz",
        "createdAt": "2020-01-16T13:31:36.570Z",
        "updatedAt": "2020-01-16T13:31:36.570Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b25"
        },
        "nombre": "Guaranda",
        "descripcion": "Guaranda",
        "abreviacion": "Guaranda",
        "createdAt": "2020-01-16T13:31:36.603Z",
        "updatedAt": "2020-01-16T13:31:36.603Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b26"
        },
        "nombre": "Palmar",
        "descripcion": "Palmar",
        "abreviacion": "Palmar",
        "createdAt": "2020-01-16T13:31:36.608Z",
        "updatedAt": "2020-01-16T13:31:36.608Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b27"
        },
        "nombre": "La Tola",
        "descripcion": "La Tola",
        "abreviacion": "La Tola",
        "createdAt": "2020-01-16T13:31:36.609Z",
        "updatedAt": "2020-01-16T13:31:36.609Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b28"
        },
        "nombre": "Uribia",
        "descripcion": "Uribia",
        "abreviacion": "Uribia",
        "createdAt": "2020-01-16T13:31:36.610Z",
        "updatedAt": "2020-01-16T13:31:36.610Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b29"
        },
        "nombre": "Albania",
        "descripcion": "Albania",
        "abreviacion": "Albania",
        "createdAt": "2020-01-16T13:31:36.613Z",
        "updatedAt": "2020-01-16T13:31:36.613Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b2a"
        },
        "nombre": "Zapatoca",
        "descripcion": "Zapatoca",
        "abreviacion": "Zapatoca",
        "createdAt": "2020-01-16T13:31:36.617Z",
        "updatedAt": "2020-01-16T13:31:36.617Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b2b"
        },
        "nombre": "Funes",
        "descripcion": "Funes",
        "abreviacion": "Funes",
        "createdAt": "2020-01-16T13:31:36.618Z",
        "updatedAt": "2020-01-16T13:31:36.618Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b2c"
        },
        "nombre": "Pereira",
        "descripcion": "Pereira",
        "abreviacion": "Pereira",
        "createdAt": "2020-01-16T13:31:36.619Z",
        "updatedAt": "2020-01-16T13:31:36.619Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b2d"
        },
        "nombre": "San Alberto",
        "descripcion": "San Alberto",
        "abreviacion": "San Alberto",
        "createdAt": "2020-01-16T13:31:36.624Z",
        "updatedAt": "2020-01-16T13:31:36.624Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b2e"
        },
        "nombre": "San Mateo",
        "descripcion": "San Mateo",
        "abreviacion": "San Mateo",
        "createdAt": "2020-01-16T13:31:36.625Z",
        "updatedAt": "2020-01-16T13:31:36.625Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b2f"
        },
        "nombre": "Piedecuesta",
        "descripcion": "Piedecuesta",
        "abreviacion": "Piedecuesta",
        "createdAt": "2020-01-16T13:31:36.628Z",
        "updatedAt": "2020-01-16T13:31:36.628Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b30"
        },
        "nombre": "Salamina",
        "descripcion": "Salamina",
        "abreviacion": "Salamina",
        "createdAt": "2020-01-16T13:31:36.630Z",
        "updatedAt": "2020-01-16T13:31:36.630Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b31"
        },
        "nombre": "Sativasur",
        "descripcion": "Sativasur",
        "abreviacion": "Sativasur",
        "createdAt": "2020-01-16T13:31:36.631Z",
        "updatedAt": "2020-01-16T13:31:36.631Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b32"
        },
        "nombre": "Neiva",
        "descripcion": "Neiva",
        "abreviacion": "Neiva",
        "createdAt": "2020-01-16T13:31:36.634Z",
        "updatedAt": "2020-01-16T13:31:36.634Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b33"
        },
        "nombre": "Santa Bárbara",
        "descripcion": "Santa Bárbara",
        "abreviacion": "Santa Bárbara",
        "createdAt": "2020-01-16T13:31:36.644Z",
        "updatedAt": "2020-01-16T13:31:36.644Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b34"
        },
        "nombre": "Aratoca",
        "descripcion": "Aratoca",
        "abreviacion": "Aratoca",
        "createdAt": "2020-01-16T13:31:36.648Z",
        "updatedAt": "2020-01-16T13:31:36.648Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b35"
        },
        "nombre": "Ospina",
        "descripcion": "Ospina",
        "abreviacion": "Ospina",
        "createdAt": "2020-01-16T13:31:36.649Z",
        "updatedAt": "2020-01-16T13:31:36.649Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b36"
        },
        "nombre": "Macaravita",
        "descripcion": "Macaravita",
        "abreviacion": "Macaravita",
        "createdAt": "2020-01-16T13:31:36.665Z",
        "updatedAt": "2020-01-16T13:31:36.665Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b37"
        },
        "nombre": "Molagavita",
        "descripcion": "Molagavita",
        "abreviacion": "Molagavita",
        "createdAt": "2020-01-16T13:31:36.666Z",
        "updatedAt": "2020-01-16T13:31:36.666Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b38"
        },
        "nombre": "Alvarado",
        "descripcion": "Alvarado",
        "abreviacion": "Alvarado",
        "createdAt": "2020-01-16T13:31:36.667Z",
        "updatedAt": "2020-01-16T13:31:36.667Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b39"
        },
        "nombre": "El Retén",
        "descripcion": "El Retén",
        "abreviacion": "El Retén",
        "createdAt": "2020-01-16T13:31:36.669Z",
        "updatedAt": "2020-01-16T13:31:36.669Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b3a"
        },
        "nombre": "Tolú Viejo",
        "descripcion": "Tolú Viejo",
        "abreviacion": "Tolú Viejo",
        "createdAt": "2020-01-16T13:31:36.670Z",
        "updatedAt": "2020-01-16T13:31:36.670Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b3b"
        },
        "nombre": "Socorro",
        "descripcion": "Socorro",
        "abreviacion": "Socorro",
        "createdAt": "2020-01-16T13:31:36.671Z",
        "updatedAt": "2020-01-16T13:31:36.671Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b3c"
        },
        "nombre": "San Benito Abad",
        "descripcion": "San Benito Abad",
        "abreviacion": "San Benito Abad",
        "createdAt": "2020-01-16T13:31:36.672Z",
        "updatedAt": "2020-01-16T13:31:36.672Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b3d"
        },
        "nombre": "Girón",
        "descripcion": "Girón",
        "abreviacion": "Girón",
        "createdAt": "2020-01-16T13:31:36.674Z",
        "updatedAt": "2020-01-16T13:31:36.674Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b3e"
        },
        "nombre": "Suaita",
        "descripcion": "Suaita",
        "abreviacion": "Suaita",
        "createdAt": "2020-01-16T13:31:36.675Z",
        "updatedAt": "2020-01-16T13:31:36.675Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b3f"
        },
        "nombre": "Santa Bárbara",
        "descripcion": "Santa Bárbara",
        "abreviacion": "Santa Bárbara",
        "createdAt": "2020-01-16T13:31:36.676Z",
        "updatedAt": "2020-01-16T13:31:36.676Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b40"
        },
        "nombre": "Oiba",
        "descripcion": "Oiba",
        "abreviacion": "Oiba",
        "createdAt": "2020-01-16T13:31:36.677Z",
        "updatedAt": "2020-01-16T13:31:36.677Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b41"
        },
        "nombre": "La Unión",
        "descripcion": "La Unión",
        "abreviacion": "La Unión",
        "createdAt": "2020-01-16T13:31:36.679Z",
        "updatedAt": "2020-01-16T13:31:36.679Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b42"
        },
        "nombre": "Mapiripán",
        "descripcion": "Mapiripán",
        "abreviacion": "Mapiripán",
        "createdAt": "2020-01-16T13:31:36.684Z",
        "updatedAt": "2020-01-16T13:31:36.684Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b43"
        },
        "nombre": "Lebríja",
        "descripcion": "Lebríja",
        "abreviacion": "Lebríja",
        "createdAt": "2020-01-16T13:31:36.685Z",
        "updatedAt": "2020-01-16T13:31:36.685Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b44"
        },
        "nombre": "San Pedro",
        "descripcion": "San Pedro",
        "abreviacion": "San Pedro",
        "createdAt": "2020-01-16T13:31:36.686Z",
        "updatedAt": "2020-01-16T13:31:36.686Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b45"
        },
        "nombre": "La Celia",
        "descripcion": "La Celia",
        "abreviacion": "La Celia",
        "createdAt": "2020-01-16T13:31:36.688Z",
        "updatedAt": "2020-01-16T13:31:36.688Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b46"
        },
        "nombre": "Istmina",
        "descripcion": "Istmina",
        "abreviacion": "Istmina",
        "createdAt": "2020-01-16T13:31:36.690Z",
        "updatedAt": "2020-01-16T13:31:36.690Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b47"
        },
        "nombre": "Pasto",
        "descripcion": "Pasto",
        "abreviacion": "Pasto",
        "createdAt": "2020-01-16T13:31:36.691Z",
        "updatedAt": "2020-01-16T13:31:36.691Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b48"
        },
        "nombre": "Cajibío",
        "descripcion": "Cajibío",
        "abreviacion": "Cajibío",
        "createdAt": "2020-01-16T13:31:36.692Z",
        "updatedAt": "2020-01-16T13:31:36.692Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b49"
        },
        "nombre": "Cubarral",
        "descripcion": "Cubarral",
        "abreviacion": "Cubarral",
        "createdAt": "2020-01-16T13:31:36.694Z",
        "updatedAt": "2020-01-16T13:31:36.694Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b4a"
        },
        "nombre": "Restrepo",
        "descripcion": "Restrepo",
        "abreviacion": "Restrepo",
        "createdAt": "2020-01-16T13:31:36.695Z",
        "updatedAt": "2020-01-16T13:31:36.695Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b4b"
        },
        "nombre": "Boyacá",
        "descripcion": "Boyacá",
        "abreviacion": "Boyacá",
        "createdAt": "2020-01-16T13:31:36.696Z",
        "updatedAt": "2020-01-16T13:31:36.696Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b4c"
        },
        "nombre": "Remolino",
        "descripcion": "Remolino",
        "abreviacion": "Remolino",
        "createdAt": "2020-01-16T13:31:36.698Z",
        "updatedAt": "2020-01-16T13:31:36.698Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b4d"
        },
        "nombre": "Santa Ana",
        "descripcion": "Santa Ana",
        "abreviacion": "Santa Ana",
        "createdAt": "2020-01-16T13:31:36.699Z",
        "updatedAt": "2020-01-16T13:31:36.699Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b4e"
        },
        "nombre": "San Martín",
        "descripcion": "San Martín",
        "abreviacion": "San Martín",
        "createdAt": "2020-01-16T13:31:36.701Z",
        "updatedAt": "2020-01-16T13:31:36.701Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b4f"
        },
        "nombre": "Granada",
        "descripcion": "Granada",
        "abreviacion": "Granada",
        "createdAt": "2020-01-16T13:31:36.703Z",
        "updatedAt": "2020-01-16T13:31:36.703Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b50"
        },
        "nombre": "La Macarena",
        "descripcion": "La Macarena",
        "abreviacion": "La Macarena",
        "createdAt": "2020-01-16T13:31:36.704Z",
        "updatedAt": "2020-01-16T13:31:36.704Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b51"
        },
        "nombre": "Villanueva",
        "descripcion": "Villanueva",
        "abreviacion": "Villanueva",
        "createdAt": "2020-01-16T13:31:36.710Z",
        "updatedAt": "2020-01-16T13:31:36.710Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b52"
        },
        "nombre": "Sitionuevo",
        "descripcion": "Sitionuevo",
        "abreviacion": "Sitionuevo",
        "createdAt": "2020-01-16T13:31:36.712Z",
        "updatedAt": "2020-01-16T13:31:36.712Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b53"
        },
        "nombre": "Los Andes",
        "descripcion": "Los Andes",
        "abreviacion": "Los Andes",
        "createdAt": "2020-01-16T13:31:36.713Z",
        "updatedAt": "2020-01-16T13:31:36.713Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b54"
        },
        "nombre": "Cotorra",
        "descripcion": "Cotorra",
        "abreviacion": "Cotorra",
        "createdAt": "2020-01-16T13:31:36.715Z",
        "updatedAt": "2020-01-16T13:31:36.715Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b55"
        },
        "nombre": "Cartagena",
        "descripcion": "Cartagena",
        "abreviacion": "Cartagena",
        "createdAt": "2020-01-16T13:31:36.716Z",
        "updatedAt": "2020-01-16T13:31:36.716Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b56"
        },
        "nombre": "Santa Bárbara de Pinto",
        "descripcion": "Santa Bárbara de Pinto",
        "abreviacion": "Santa Bárbara de Pinto",
        "createdAt": "2020-01-16T13:31:36.718Z",
        "updatedAt": "2020-01-16T13:31:36.718Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b57"
        },
        "nombre": "Firavitoba",
        "descripcion": "Firavitoba",
        "abreviacion": "Firavitoba",
        "createdAt": "2020-01-16T13:31:36.719Z",
        "updatedAt": "2020-01-16T13:31:36.719Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b58"
        },
        "nombre": "Tenerife",
        "descripcion": "Tenerife",
        "abreviacion": "Tenerife",
        "createdAt": "2020-01-16T13:31:36.721Z",
        "updatedAt": "2020-01-16T13:31:36.721Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b59"
        },
        "nombre": "San Sebastián de Buenavista",
        "descripcion": "San Sebastián de Buenavista",
        "abreviacion": "San Sebastián de Buenavista",
        "createdAt": "2020-01-16T13:31:36.722Z",
        "updatedAt": "2020-01-16T13:31:36.722Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b5a"
        },
        "nombre": "Vélez",
        "descripcion": "Vélez",
        "abreviacion": "Vélez",
        "createdAt": "2020-01-16T13:31:36.723Z",
        "updatedAt": "2020-01-16T13:31:36.723Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b5b"
        },
        "nombre": "San José de La Montaña",
        "descripcion": "San José de La Montaña",
        "abreviacion": "San José de La Montaña",
        "createdAt": "2020-01-16T13:31:36.726Z",
        "updatedAt": "2020-01-16T13:31:36.726Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b5c"
        },
        "nombre": "San Miguel de Sema",
        "descripcion": "San Miguel de Sema",
        "abreviacion": "San Miguel de Sema",
        "createdAt": "2020-01-16T13:31:36.728Z",
        "updatedAt": "2020-01-16T13:31:36.728Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b5d"
        },
        "nombre": "Iles",
        "descripcion": "Iles",
        "abreviacion": "Iles",
        "createdAt": "2020-01-16T13:31:36.730Z",
        "updatedAt": "2020-01-16T13:31:36.730Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b5e"
        },
        "nombre": "Dosquebradas",
        "descripcion": "Dosquebradas",
        "abreviacion": "Dosquebradas",
        "createdAt": "2020-01-16T13:31:36.733Z",
        "updatedAt": "2020-01-16T13:31:36.733Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b5f"
        },
        "nombre": "Obando",
        "descripcion": "Obando",
        "abreviacion": "Obando",
        "createdAt": "2020-01-16T13:31:36.734Z",
        "updatedAt": "2020-01-16T13:31:36.734Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b60"
        },
        "nombre": "Riofrío",
        "descripcion": "Riofrío",
        "abreviacion": "Riofrío",
        "createdAt": "2020-01-16T13:31:36.736Z",
        "updatedAt": "2020-01-16T13:31:36.736Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b61"
        },
        "nombre": "Villa del Rosario",
        "descripcion": "Villa del Rosario",
        "abreviacion": "Villa del Rosario",
        "createdAt": "2020-01-16T13:31:36.738Z",
        "updatedAt": "2020-01-16T13:31:36.738Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b62"
        },
        "nombre": "Versalles",
        "descripcion": "Versalles",
        "abreviacion": "Versalles",
        "createdAt": "2020-01-16T13:31:36.741Z",
        "updatedAt": "2020-01-16T13:31:36.741Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b63"
        },
        "nombre": "Rosas",
        "descripcion": "Rosas",
        "abreviacion": "Rosas",
        "createdAt": "2020-01-16T13:31:36.743Z",
        "updatedAt": "2020-01-16T13:31:36.743Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b64"
        },
        "nombre": "Providencia",
        "descripcion": "Providencia",
        "abreviacion": "Providencia",
        "createdAt": "2020-01-16T13:31:36.749Z",
        "updatedAt": "2020-01-16T13:31:36.749Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b65"
        },
        "nombre": "Toledo",
        "descripcion": "Toledo",
        "abreviacion": "Toledo",
        "createdAt": "2020-01-16T13:31:36.750Z",
        "updatedAt": "2020-01-16T13:31:36.750Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b66"
        },
        "nombre": "Vijes",
        "descripcion": "Vijes",
        "abreviacion": "Vijes",
        "createdAt": "2020-01-16T13:31:36.755Z",
        "updatedAt": "2020-01-16T13:31:36.755Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b67"
        },
        "nombre": "Cerro San Antonio",
        "descripcion": "Cerro San Antonio",
        "abreviacion": "Cerro San Antonio",
        "createdAt": "2020-01-16T13:31:36.757Z",
        "updatedAt": "2020-01-16T13:31:36.757Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b68"
        },
        "nombre": "Leiva",
        "descripcion": "Leiva",
        "abreviacion": "Leiva",
        "createdAt": "2020-01-16T13:31:36.758Z",
        "updatedAt": "2020-01-16T13:31:36.758Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b69"
        },
        "nombre": "Palestina",
        "descripcion": "Palestina",
        "abreviacion": "Palestina",
        "createdAt": "2020-01-16T13:31:36.759Z",
        "updatedAt": "2020-01-16T13:31:36.759Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b6a"
        },
        "nombre": "La Llanada",
        "descripcion": "La Llanada",
        "abreviacion": "La Llanada",
        "createdAt": "2020-01-16T13:31:36.760Z",
        "updatedAt": "2020-01-16T13:31:36.760Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b6b"
        },
        "nombre": "Colón",
        "descripcion": "Colón",
        "abreviacion": "Colón",
        "createdAt": "2020-01-16T13:31:36.762Z",
        "updatedAt": "2020-01-16T13:31:36.762Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b6c"
        },
        "nombre": "Ancuyá",
        "descripcion": "Ancuyá",
        "abreviacion": "Ancuyá",
        "createdAt": "2020-01-16T13:31:36.763Z",
        "updatedAt": "2020-01-16T13:31:36.763Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b6d"
        },
        "nombre": "Nóvita",
        "descripcion": "Nóvita",
        "abreviacion": "Nóvita",
        "createdAt": "2020-01-16T13:31:36.765Z",
        "updatedAt": "2020-01-16T13:31:36.765Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b6e"
        },
        "nombre": "Guamal",
        "descripcion": "Guamal",
        "abreviacion": "Guamal",
        "createdAt": "2020-01-16T13:31:36.766Z",
        "updatedAt": "2020-01-16T13:31:36.766Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b6f"
        },
        "nombre": "Valparaíso",
        "descripcion": "Valparaíso",
        "abreviacion": "Valparaíso",
        "createdAt": "2020-01-16T13:31:36.767Z",
        "updatedAt": "2020-01-16T13:31:36.767Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b70"
        },
        "nombre": "Charta",
        "descripcion": "Charta",
        "abreviacion": "Charta",
        "createdAt": "2020-01-16T13:31:36.768Z",
        "updatedAt": "2020-01-16T13:31:36.768Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b71"
        },
        "nombre": "Tibú",
        "descripcion": "Tibú",
        "abreviacion": "Tibú",
        "createdAt": "2020-01-16T13:31:36.770Z",
        "updatedAt": "2020-01-16T13:31:36.770Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b72"
        },
        "nombre": "Concordia",
        "descripcion": "Concordia",
        "abreviacion": "Concordia",
        "createdAt": "2020-01-16T13:31:36.771Z",
        "updatedAt": "2020-01-16T13:31:36.771Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b73"
        },
        "nombre": "Montería",
        "descripcion": "Montería",
        "abreviacion": "Montería",
        "createdAt": "2020-01-16T13:31:36.773Z",
        "updatedAt": "2020-01-16T13:31:36.773Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b74"
        },
        "nombre": "Curití",
        "descripcion": "Curití",
        "abreviacion": "Curití",
        "createdAt": "2020-01-16T13:31:36.774Z",
        "updatedAt": "2020-01-16T13:31:36.774Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b75"
        },
        "nombre": "El Rosario",
        "descripcion": "El Rosario",
        "abreviacion": "El Rosario",
        "createdAt": "2020-01-16T13:31:36.775Z",
        "updatedAt": "2020-01-16T13:31:36.775Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b76"
        },
        "nombre": "Matanza",
        "descripcion": "Matanza",
        "abreviacion": "Matanza",
        "createdAt": "2020-01-16T13:31:36.777Z",
        "updatedAt": "2020-01-16T13:31:36.777Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b77"
        },
        "nombre": "Pijao",
        "descripcion": "Pijao",
        "abreviacion": "Pijao",
        "createdAt": "2020-01-16T13:31:36.778Z",
        "updatedAt": "2020-01-16T13:31:36.778Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b78"
        },
        "nombre": "Guamal",
        "descripcion": "Guamal",
        "abreviacion": "Guamal",
        "createdAt": "2020-01-16T13:31:36.779Z",
        "updatedAt": "2020-01-16T13:31:36.779Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b79"
        },
        "nombre": "Ipiales",
        "descripcion": "Ipiales",
        "abreviacion": "Ipiales",
        "createdAt": "2020-01-16T13:31:36.781Z",
        "updatedAt": "2020-01-16T13:31:36.781Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b7a"
        },
        "nombre": "Ambalema",
        "descripcion": "Ambalema",
        "abreviacion": "Ambalema",
        "createdAt": "2020-01-16T13:31:36.826Z",
        "updatedAt": "2020-01-16T13:31:36.826Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b7b"
        },
        "nombre": "Coello",
        "descripcion": "Coello",
        "abreviacion": "Coello",
        "createdAt": "2020-01-16T13:31:36.827Z",
        "updatedAt": "2020-01-16T13:31:36.827Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b7c"
        },
        "nombre": "Villanueva",
        "descripcion": "Villanueva",
        "abreviacion": "Villanueva",
        "createdAt": "2020-01-16T13:31:36.838Z",
        "updatedAt": "2020-01-16T13:31:36.838Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b7d"
        },
        "nombre": "Landázuri",
        "descripcion": "Landázuri",
        "abreviacion": "Landázuri",
        "createdAt": "2020-01-16T13:31:36.840Z",
        "updatedAt": "2020-01-16T13:31:36.840Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b7e"
        },
        "nombre": "Cajamarca",
        "descripcion": "Cajamarca",
        "abreviacion": "Cajamarca",
        "createdAt": "2020-01-16T13:31:36.841Z",
        "updatedAt": "2020-01-16T13:31:36.841Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b7f"
        },
        "nombre": "Chaparral",
        "descripcion": "Chaparral",
        "abreviacion": "Chaparral",
        "createdAt": "2020-01-16T13:31:36.842Z",
        "updatedAt": "2020-01-16T13:31:36.842Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b80"
        },
        "nombre": "Guapotá",
        "descripcion": "Guapotá",
        "abreviacion": "Guapotá",
        "createdAt": "2020-01-16T13:31:36.850Z",
        "updatedAt": "2020-01-16T13:31:36.850Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b81"
        },
        "nombre": "La Virginia",
        "descripcion": "La Virginia",
        "abreviacion": "La Virginia",
        "createdAt": "2020-01-16T13:31:36.851Z",
        "updatedAt": "2020-01-16T13:31:36.851Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b82"
        },
        "nombre": "Piendamó",
        "descripcion": "Piendamó",
        "abreviacion": "Piendamó",
        "createdAt": "2020-01-16T13:31:36.852Z",
        "updatedAt": "2020-01-16T13:31:36.852Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b83"
        },
        "nombre": "El Roble",
        "descripcion": "El Roble",
        "abreviacion": "El Roble",
        "createdAt": "2020-01-16T13:31:36.854Z",
        "updatedAt": "2020-01-16T13:31:36.854Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b84"
        },
        "nombre": "Málaga",
        "descripcion": "Málaga",
        "abreviacion": "Málaga",
        "createdAt": "2020-01-16T13:31:36.855Z",
        "updatedAt": "2020-01-16T13:31:36.855Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b85"
        },
        "nombre": "La Belleza",
        "descripcion": "La Belleza",
        "abreviacion": "La Belleza",
        "createdAt": "2020-01-16T13:31:36.856Z",
        "updatedAt": "2020-01-16T13:31:36.856Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b86"
        },
        "nombre": "Coveñas",
        "descripcion": "Coveñas",
        "abreviacion": "Coveñas",
        "createdAt": "2020-01-16T13:31:36.857Z",
        "updatedAt": "2020-01-16T13:31:36.857Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b87"
        },
        "nombre": "Ansermanuevo",
        "descripcion": "Ansermanuevo",
        "abreviacion": "Ansermanuevo",
        "createdAt": "2020-01-16T13:31:36.859Z",
        "updatedAt": "2020-01-16T13:31:36.859Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b88"
        },
        "nombre": "San Andrés",
        "descripcion": "San Andrés",
        "abreviacion": "San Andrés",
        "createdAt": "2020-01-16T13:31:36.860Z",
        "updatedAt": "2020-01-16T13:31:36.860Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b89"
        },
        "nombre": "Palmira",
        "descripcion": "Palmira",
        "abreviacion": "Palmira",
        "createdAt": "2020-01-16T13:31:36.861Z",
        "updatedAt": "2020-01-16T13:31:36.861Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b8a"
        },
        "nombre": "Barrancabermeja",
        "descripcion": "Barrancabermeja",
        "abreviacion": "Barrancabermeja",
        "createdAt": "2020-01-16T13:31:36.862Z",
        "updatedAt": "2020-01-16T13:31:36.862Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b8b"
        },
        "nombre": "La Cumbre",
        "descripcion": "La Cumbre",
        "abreviacion": "La Cumbre",
        "createdAt": "2020-01-16T13:31:36.864Z",
        "updatedAt": "2020-01-16T13:31:36.864Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b8c"
        },
        "nombre": "Anolaima",
        "descripcion": "Anolaima",
        "abreviacion": "Anolaima",
        "createdAt": "2020-01-16T13:31:36.865Z",
        "updatedAt": "2020-01-16T13:31:36.865Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b8d"
        },
        "nombre": "González",
        "descripcion": "González",
        "abreviacion": "González",
        "createdAt": "2020-01-16T13:31:36.866Z",
        "updatedAt": "2020-01-16T13:31:36.866Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b8e"
        },
        "nombre": "Rionegro",
        "descripcion": "Rionegro",
        "abreviacion": "Rionegro",
        "createdAt": "2020-01-16T13:31:36.867Z",
        "updatedAt": "2020-01-16T13:31:36.867Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b8f"
        },
        "nombre": "Labateca",
        "descripcion": "Labateca",
        "abreviacion": "Labateca",
        "createdAt": "2020-01-16T13:31:36.898Z",
        "updatedAt": "2020-01-16T13:31:36.898Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b90"
        },
        "nombre": "Alpujarra",
        "descripcion": "Alpujarra",
        "abreviacion": "Alpujarra",
        "createdAt": "2020-01-16T13:31:36.900Z",
        "updatedAt": "2020-01-16T13:31:36.900Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b91"
        },
        "nombre": "El Castillo",
        "descripcion": "El Castillo",
        "abreviacion": "El Castillo",
        "createdAt": "2020-01-16T13:31:36.901Z",
        "updatedAt": "2020-01-16T13:31:36.901Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b92"
        },
        "nombre": "Orocué",
        "descripcion": "Orocué",
        "abreviacion": "Orocué",
        "createdAt": "2020-01-16T13:31:36.902Z",
        "updatedAt": "2020-01-16T13:31:36.902Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b93"
        },
        "nombre": "Córdoba",
        "descripcion": "Córdoba",
        "abreviacion": "Córdoba",
        "createdAt": "2020-01-16T13:31:36.904Z",
        "updatedAt": "2020-01-16T13:31:36.904Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b94"
        },
        "nombre": "Sucre",
        "descripcion": "Sucre",
        "abreviacion": "Sucre",
        "createdAt": "2020-01-16T13:31:36.905Z",
        "updatedAt": "2020-01-16T13:31:36.905Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b95"
        },
        "nombre": "Angostura",
        "descripcion": "Angostura",
        "abreviacion": "Angostura",
        "createdAt": "2020-01-16T13:31:36.907Z",
        "updatedAt": "2020-01-16T13:31:36.907Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b96"
        },
        "nombre": "Onzaga",
        "descripcion": "Onzaga",
        "abreviacion": "Onzaga",
        "createdAt": "2020-01-16T13:31:36.908Z",
        "updatedAt": "2020-01-16T13:31:36.908Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b97"
        },
        "nombre": "Marsella",
        "descripcion": "Marsella",
        "abreviacion": "Marsella",
        "createdAt": "2020-01-16T13:31:36.909Z",
        "updatedAt": "2020-01-16T13:31:36.909Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b98"
        },
        "nombre": "Convención",
        "descripcion": "Convención",
        "abreviacion": "Convención",
        "createdAt": "2020-01-16T13:31:36.911Z",
        "updatedAt": "2020-01-16T13:31:36.911Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b99"
        },
        "nombre": "Yumbo",
        "descripcion": "Yumbo",
        "abreviacion": "Yumbo",
        "createdAt": "2020-01-16T13:31:36.912Z",
        "updatedAt": "2020-01-16T13:31:36.912Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b9a"
        },
        "nombre": "El Cairo",
        "descripcion": "El Cairo",
        "abreviacion": "El Cairo",
        "createdAt": "2020-01-16T13:31:36.913Z",
        "updatedAt": "2020-01-16T13:31:36.913Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065b87c6e3d0008526b9b"
        },
        "nombre": "Bochalema",
        "descripcion": "Bochalema",
        "abreviacion": "Bochalema",
        "createdAt": "2020-01-16T13:31:36.914Z",
        "updatedAt": "2020-01-16T13:31:36.914Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c07c6e3d0008526b9c"
        },
        "nombre": "Cácota",
        "descripcion": "Cácota",
        "abreviacion": "Cácota",
        "createdAt": "2020-01-16T13:31:44.789Z",
        "updatedAt": "2020-01-16T13:31:44.789Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c07c6e3d0008526b9d"
        },
        "nombre": "Zarzal",
        "descripcion": "Zarzal",
        "abreviacion": "Zarzal",
        "createdAt": "2020-01-16T13:31:44.791Z",
        "updatedAt": "2020-01-16T13:31:44.791Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c07c6e3d0008526b9e"
        },
        "nombre": "San Juan de Urabá",
        "descripcion": "San Juan de Urabá",
        "abreviacion": "San Juan de Urabá",
        "createdAt": "2020-01-16T13:31:44.793Z",
        "updatedAt": "2020-01-16T13:31:44.793Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c27c6e3d0008526b9f"
        },
        "nombre": "Sevilla",
        "descripcion": "Sevilla",
        "abreviacion": "Sevilla",
        "createdAt": "2020-01-16T13:31:46.746Z",
        "updatedAt": "2020-01-16T13:31:46.746Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c27c6e3d0008526ba0"
        },
        "nombre": "Valle de San Juan",
        "descripcion": "Valle de San Juan",
        "abreviacion": "Valle de San Juan",
        "createdAt": "2020-01-16T13:31:46.749Z",
        "updatedAt": "2020-01-16T13:31:46.749Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c27c6e3d0008526ba1"
        },
        "nombre": "San Pedro de Cartago",
        "descripcion": "San Pedro de Cartago",
        "abreviacion": "San Pedro de Cartago",
        "createdAt": "2020-01-16T13:31:46.753Z",
        "updatedAt": "2020-01-16T13:31:46.753Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c27c6e3d0008526ba2"
        },
        "nombre": "Zipaquirá",
        "descripcion": "Zipaquirá",
        "abreviacion": "Zipaquirá",
        "createdAt": "2020-01-16T13:31:46.770Z",
        "updatedAt": "2020-01-16T13:31:46.770Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c27c6e3d0008526ba3"
        },
        "nombre": "La Uvita",
        "descripcion": "La Uvita",
        "abreviacion": "La Uvita",
        "createdAt": "2020-01-16T13:31:46.773Z",
        "updatedAt": "2020-01-16T13:31:46.773Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c27c6e3d0008526ba4"
        },
        "nombre": "San Pedro de Uraba",
        "descripcion": "San Pedro de Uraba",
        "abreviacion": "San Pedro de Uraba",
        "createdAt": "2020-01-16T13:31:46.861Z",
        "updatedAt": "2020-01-16T13:31:46.861Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c27c6e3d0008526ba5"
        },
        "nombre": "Santa Helena del Opón",
        "descripcion": "Santa Helena del Opón",
        "abreviacion": "Santa Helena del Opón",
        "createdAt": "2020-01-16T13:31:46.862Z",
        "updatedAt": "2020-01-16T13:31:46.862Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c27c6e3d0008526ba6"
        },
        "nombre": "El Carmen de Atrato",
        "descripcion": "El Carmen de Atrato",
        "abreviacion": "El Carmen de Atrato",
        "createdAt": "2020-01-16T13:31:46.864Z",
        "updatedAt": "2020-01-16T13:31:46.864Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c27c6e3d0008526ba7"
        },
        "nombre": "El Carmen de Chucurí",
        "descripcion": "El Carmen de Chucurí",
        "abreviacion": "El Carmen de Chucurí",
        "createdAt": "2020-01-16T13:31:46.869Z",
        "updatedAt": "2020-01-16T13:31:46.869Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c27c6e3d0008526ba8"
        },
        "nombre": "El Retorno",
        "descripcion": "El Retorno",
        "abreviacion": "El Retorno",
        "createdAt": "2020-01-16T13:31:46.881Z",
        "updatedAt": "2020-01-16T13:31:46.881Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c27c6e3d0008526ba9"
        },
        "nombre": "San Pablo de Borbur",
        "descripcion": "San Pablo de Borbur",
        "abreviacion": "San Pablo de Borbur",
        "createdAt": "2020-01-16T13:31:46.885Z",
        "updatedAt": "2020-01-16T13:31:46.885Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c27c6e3d0008526baa"
        },
        "nombre": "Río de Oro",
        "descripcion": "Río de Oro",
        "abreviacion": "Río de Oro",
        "createdAt": "2020-01-16T13:31:46.886Z",
        "updatedAt": "2020-01-16T13:31:46.886Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c27c6e3d0008526bab"
        },
        "nombre": "Vergara",
        "descripcion": "Vergara",
        "abreviacion": "Vergara",
        "createdAt": "2020-01-16T13:31:46.889Z",
        "updatedAt": "2020-01-16T13:31:46.889Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c27c6e3d0008526bac"
        },
        "nombre": "Sabanas de San Angel",
        "descripcion": "Sabanas de San Angel",
        "abreviacion": "Sabanas de San Angel",
        "createdAt": "2020-01-16T13:31:46.892Z",
        "updatedAt": "2020-01-16T13:31:46.892Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c27c6e3d0008526bad"
        },
        "nombre": "San Antonio del Tequendama",
        "descripcion": "San Antonio del Tequendama",
        "abreviacion": "San Antonio del Tequendama",
        "createdAt": "2020-01-16T13:31:46.893Z",
        "updatedAt": "2020-01-16T13:31:46.893Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c27c6e3d0008526bae"
        },
        "nombre": "San Carlos",
        "descripcion": "San Carlos",
        "abreviacion": "San Carlos",
        "createdAt": "2020-01-16T13:31:46.900Z",
        "updatedAt": "2020-01-16T13:31:46.900Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c27c6e3d0008526baf"
        },
        "nombre": "El Dovio",
        "descripcion": "El Dovio",
        "abreviacion": "El Dovio",
        "createdAt": "2020-01-16T13:31:46.903Z",
        "updatedAt": "2020-01-16T13:31:46.903Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c27c6e3d0008526bb0"
        },
        "nombre": "Argelia",
        "descripcion": "Argelia",
        "abreviacion": "Argelia",
        "createdAt": "2020-01-16T13:31:46.904Z",
        "updatedAt": "2020-01-16T13:31:46.904Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c27c6e3d0008526bb1"
        },
        "nombre": "San Juan de Betulia",
        "descripcion": "San Juan de Betulia",
        "abreviacion": "San Juan de Betulia",
        "createdAt": "2020-01-16T13:31:46.906Z",
        "updatedAt": "2020-01-16T13:31:46.906Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c27c6e3d0008526bb2"
        },
        "nombre": "Cértegui",
        "descripcion": "Cértegui",
        "abreviacion": "Cértegui",
        "createdAt": "2020-01-16T13:31:46.909Z",
        "updatedAt": "2020-01-16T13:31:46.909Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c27c6e3d0008526bb3"
        },
        "nombre": "Bogotá D.C.",
        "descripcion": "Bogotá D.C.",
        "abreviacion": "Bogotá D.C.",
        "createdAt": "2020-01-16T13:31:46.912Z",
        "updatedAt": "2020-01-16T13:31:46.912Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c37c6e3d0008526bb4"
        },
        "nombre": "Soracá",
        "descripcion": "Soracá",
        "abreviacion": "Soracá",
        "createdAt": "2020-01-16T13:31:47.993Z",
        "updatedAt": "2020-01-16T13:31:47.993Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bb5"
        },
        "nombre": "Ráquira",
        "descripcion": "Ráquira",
        "abreviacion": "Ráquira",
        "createdAt": "2020-01-16T13:31:48.055Z",
        "updatedAt": "2020-01-16T13:31:48.055Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bb6"
        },
        "nombre": "San Carlos",
        "descripcion": "San Carlos",
        "abreviacion": "San Carlos",
        "createdAt": "2020-01-16T13:31:48.061Z",
        "updatedAt": "2020-01-16T13:31:48.061Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bb7"
        },
        "nombre": "Cajicá",
        "descripcion": "Cajicá",
        "abreviacion": "Cajicá",
        "createdAt": "2020-01-16T13:31:48.062Z",
        "updatedAt": "2020-01-16T13:31:48.062Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bb8"
        },
        "nombre": "Paz de Río",
        "descripcion": "Paz de Río",
        "abreviacion": "Paz de Río",
        "createdAt": "2020-01-16T13:31:48.063Z",
        "updatedAt": "2020-01-16T13:31:48.063Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bb9"
        },
        "nombre": "El Tablón de Gómez",
        "descripcion": "El Tablón de Gómez",
        "abreviacion": "El Tablón de Gómez",
        "createdAt": "2020-01-16T13:31:48.065Z",
        "updatedAt": "2020-01-16T13:31:48.065Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bba"
        },
        "nombre": "El Carmen de Bolívar",
        "descripcion": "El Carmen de Bolívar",
        "abreviacion": "El Carmen de Bolívar",
        "createdAt": "2020-01-16T13:31:48.073Z",
        "updatedAt": "2020-01-16T13:31:48.073Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bbb"
        },
        "nombre": "Puerto Triunfo",
        "descripcion": "Puerto Triunfo",
        "abreviacion": "Puerto Triunfo",
        "createdAt": "2020-01-16T13:31:48.074Z",
        "updatedAt": "2020-01-16T13:31:48.074Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bbc"
        },
        "nombre": "Villa de San Diego de Ubate",
        "descripcion": "Villa de San Diego de Ubate",
        "abreviacion": "Villa de San Diego de Ubate",
        "createdAt": "2020-01-16T13:31:48.075Z",
        "updatedAt": "2020-01-16T13:31:48.075Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bbd"
        },
        "nombre": "San José de Miranda",
        "descripcion": "San José de Miranda",
        "abreviacion": "San José de Miranda",
        "createdAt": "2020-01-16T13:31:48.077Z",
        "updatedAt": "2020-01-16T13:31:48.077Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bbe"
        },
        "nombre": "Silvia",
        "descripcion": "Silvia",
        "abreviacion": "Silvia",
        "createdAt": "2020-01-16T13:31:48.078Z",
        "updatedAt": "2020-01-16T13:31:48.078Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bbf"
        },
        "nombre": "Samacá",
        "descripcion": "Samacá",
        "abreviacion": "Samacá",
        "createdAt": "2020-01-16T13:31:48.079Z",
        "updatedAt": "2020-01-16T13:31:48.079Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bc0"
        },
        "nombre": "Pulí",
        "descripcion": "Pulí",
        "abreviacion": "Pulí",
        "createdAt": "2020-01-16T13:31:48.091Z",
        "updatedAt": "2020-01-16T13:31:48.091Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bc1"
        },
        "nombre": "Guapi",
        "descripcion": "Guapi",
        "abreviacion": "Guapi",
        "createdAt": "2020-01-16T13:31:48.097Z",
        "updatedAt": "2020-01-16T13:31:48.097Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bc2"
        },
        "nombre": "Caloto",
        "descripcion": "Caloto",
        "abreviacion": "Caloto",
        "createdAt": "2020-01-16T13:31:48.099Z",
        "updatedAt": "2020-01-16T13:31:48.099Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bc3"
        },
        "nombre": "Guachené",
        "descripcion": "Guachené",
        "abreviacion": "Guachené",
        "createdAt": "2020-01-16T13:31:48.100Z",
        "updatedAt": "2020-01-16T13:31:48.100Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bc4"
        },
        "nombre": "Patía",
        "descripcion": "Patía",
        "abreviacion": "Patía",
        "createdAt": "2020-01-16T13:31:48.105Z",
        "updatedAt": "2020-01-16T13:31:48.105Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bc5"
        },
        "nombre": "Curillo",
        "descripcion": "Curillo",
        "abreviacion": "Curillo",
        "createdAt": "2020-01-16T13:31:48.106Z",
        "updatedAt": "2020-01-16T13:31:48.106Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bc6"
        },
        "nombre": "Sotara",
        "descripcion": "Sotara",
        "abreviacion": "Sotara",
        "createdAt": "2020-01-16T13:31:48.107Z",
        "updatedAt": "2020-01-16T13:31:48.107Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bc7"
        },
        "nombre": "Chinchiná",
        "descripcion": "Chinchiná",
        "abreviacion": "Chinchiná",
        "createdAt": "2020-01-16T13:31:48.113Z",
        "updatedAt": "2020-01-16T13:31:48.113Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bc8"
        },
        "nombre": "Miraflores",
        "descripcion": "Miraflores",
        "abreviacion": "Miraflores",
        "createdAt": "2020-01-16T13:31:48.114Z",
        "updatedAt": "2020-01-16T13:31:48.114Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bc9"
        },
        "nombre": "Papunaua",
        "descripcion": "Papunaua",
        "abreviacion": "Papunaua",
        "createdAt": "2020-01-16T13:31:48.116Z",
        "updatedAt": "2020-01-16T13:31:48.116Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bca"
        },
        "nombre": "Sibaté",
        "descripcion": "Sibaté",
        "abreviacion": "Sibaté",
        "createdAt": "2020-01-16T13:31:48.121Z",
        "updatedAt": "2020-01-16T13:31:48.121Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bcb"
        },
        "nombre": "Taraira",
        "descripcion": "Taraira",
        "abreviacion": "Taraira",
        "createdAt": "2020-01-16T13:31:48.124Z",
        "updatedAt": "2020-01-16T13:31:48.124Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bcc"
        },
        "nombre": "Ayapel",
        "descripcion": "Ayapel",
        "abreviacion": "Ayapel",
        "createdAt": "2020-01-16T13:31:48.126Z",
        "updatedAt": "2020-01-16T13:31:48.126Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bcd"
        },
        "nombre": "Quetame",
        "descripcion": "Quetame",
        "abreviacion": "Quetame",
        "createdAt": "2020-01-16T13:31:48.129Z",
        "updatedAt": "2020-01-16T13:31:48.129Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bce"
        },
        "nombre": "Piamonte",
        "descripcion": "Piamonte",
        "abreviacion": "Piamonte",
        "createdAt": "2020-01-16T13:31:48.133Z",
        "updatedAt": "2020-01-16T13:31:48.133Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bcf"
        },
        "nombre": "Villamaría",
        "descripcion": "Villamaría",
        "abreviacion": "Villamaría",
        "createdAt": "2020-01-16T13:31:48.146Z",
        "updatedAt": "2020-01-16T13:31:48.146Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bd0"
        },
        "nombre": "Santa Rosa de Viterbo",
        "descripcion": "Santa Rosa de Viterbo",
        "abreviacion": "Santa Rosa de Viterbo",
        "createdAt": "2020-01-16T13:31:48.149Z",
        "updatedAt": "2020-01-16T13:31:48.149Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bd1"
        },
        "nombre": "Caldono",
        "descripcion": "Caldono",
        "abreviacion": "Caldono",
        "createdAt": "2020-01-16T13:31:48.150Z",
        "updatedAt": "2020-01-16T13:31:48.150Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bd2"
        },
        "nombre": "Quibdó",
        "descripcion": "Quibdó",
        "abreviacion": "Quibdó",
        "createdAt": "2020-01-16T13:31:48.153Z",
        "updatedAt": "2020-01-16T13:31:48.153Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bd3"
        },
        "nombre": "Tocancipá",
        "descripcion": "Tocancipá",
        "abreviacion": "Tocancipá",
        "createdAt": "2020-01-16T13:31:48.156Z",
        "updatedAt": "2020-01-16T13:31:48.156Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bd4"
        },
        "nombre": "Caruru",
        "descripcion": "Caruru",
        "abreviacion": "Caruru",
        "createdAt": "2020-01-16T13:31:48.158Z",
        "updatedAt": "2020-01-16T13:31:48.158Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bd5"
        },
        "nombre": "Cuítiva",
        "descripcion": "Cuítiva",
        "abreviacion": "Cuítiva",
        "createdAt": "2020-01-16T13:31:48.165Z",
        "updatedAt": "2020-01-16T13:31:48.165Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bd6"
        },
        "nombre": "Carmen de Apicala",
        "descripcion": "Carmen de Apicala",
        "abreviacion": "Carmen de Apicala",
        "createdAt": "2020-01-16T13:31:48.169Z",
        "updatedAt": "2020-01-16T13:31:48.169Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bd7"
        },
        "nombre": "La Jagua del Pilar",
        "descripcion": "La Jagua del Pilar",
        "abreviacion": "La Jagua del Pilar",
        "createdAt": "2020-01-16T13:31:48.170Z",
        "updatedAt": "2020-01-16T13:31:48.170Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bd8"
        },
        "nombre": "San Eduardo",
        "descripcion": "San Eduardo",
        "abreviacion": "San Eduardo",
        "createdAt": "2020-01-16T13:31:48.173Z",
        "updatedAt": "2020-01-16T13:31:48.173Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bd9"
        },
        "nombre": "Pueblo Bello",
        "descripcion": "Pueblo Bello",
        "abreviacion": "Pueblo Bello",
        "createdAt": "2020-01-16T13:31:48.175Z",
        "updatedAt": "2020-01-16T13:31:48.175Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bda"
        },
        "nombre": "Sutatenza",
        "descripcion": "Sutatenza",
        "abreviacion": "Sutatenza",
        "createdAt": "2020-01-16T13:31:48.180Z",
        "updatedAt": "2020-01-16T13:31:48.180Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bdb"
        },
        "nombre": "Pitalito",
        "descripcion": "Pitalito",
        "abreviacion": "Pitalito",
        "createdAt": "2020-01-16T13:31:48.182Z",
        "updatedAt": "2020-01-16T13:31:48.182Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bdc"
        },
        "nombre": "Victoria",
        "descripcion": "Victoria",
        "abreviacion": "Victoria",
        "createdAt": "2020-01-16T13:31:48.183Z",
        "updatedAt": "2020-01-16T13:31:48.183Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bdd"
        },
        "nombre": "Valdivia",
        "descripcion": "Valdivia",
        "abreviacion": "Valdivia",
        "createdAt": "2020-01-16T13:31:48.209Z",
        "updatedAt": "2020-01-16T13:31:48.209Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bde"
        },
        "nombre": "Barrancas",
        "descripcion": "Barrancas",
        "abreviacion": "Barrancas",
        "createdAt": "2020-01-16T13:31:48.220Z",
        "updatedAt": "2020-01-16T13:31:48.220Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bdf"
        },
        "nombre": "Puerto Tejada",
        "descripcion": "Puerto Tejada",
        "abreviacion": "Puerto Tejada",
        "createdAt": "2020-01-16T13:31:48.221Z",
        "updatedAt": "2020-01-16T13:31:48.221Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526be0"
        },
        "nombre": "Chinú",
        "descripcion": "Chinú",
        "abreviacion": "Chinú",
        "createdAt": "2020-01-16T13:31:48.254Z",
        "updatedAt": "2020-01-16T13:31:48.254Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526be1"
        },
        "nombre": "El Tambo",
        "descripcion": "El Tambo",
        "abreviacion": "El Tambo",
        "createdAt": "2020-01-16T13:31:48.257Z",
        "updatedAt": "2020-01-16T13:31:48.257Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526be2"
        },
        "nombre": "Santo Domingo",
        "descripcion": "Santo Domingo",
        "abreviacion": "Santo Domingo",
        "createdAt": "2020-01-16T13:31:48.258Z",
        "updatedAt": "2020-01-16T13:31:48.258Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526be3"
        },
        "nombre": "Chachagüí",
        "descripcion": "Chachagüí",
        "abreviacion": "Chachagüí",
        "createdAt": "2020-01-16T13:31:48.268Z",
        "updatedAt": "2020-01-16T13:31:48.268Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526be4"
        },
        "nombre": "Vigía del Fuerte",
        "descripcion": "Vigía del Fuerte",
        "abreviacion": "Vigía del Fuerte",
        "createdAt": "2020-01-16T13:31:48.273Z",
        "updatedAt": "2020-01-16T13:31:48.273Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526be5"
        },
        "nombre": "Salamina",
        "descripcion": "Salamina",
        "abreviacion": "Salamina",
        "createdAt": "2020-01-16T13:31:48.275Z",
        "updatedAt": "2020-01-16T13:31:48.275Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526be6"
        },
        "nombre": "Albania",
        "descripcion": "Albania",
        "abreviacion": "Albania",
        "createdAt": "2020-01-16T13:31:48.276Z",
        "updatedAt": "2020-01-16T13:31:48.276Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526be7"
        },
        "nombre": "Providencia",
        "descripcion": "Providencia",
        "abreviacion": "Providencia",
        "createdAt": "2020-01-16T13:31:48.279Z",
        "updatedAt": "2020-01-16T13:31:48.279Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526be8"
        },
        "nombre": "Florencia",
        "descripcion": "Florencia",
        "abreviacion": "Florencia",
        "createdAt": "2020-01-16T13:31:48.280Z",
        "updatedAt": "2020-01-16T13:31:48.280Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526be9"
        },
        "nombre": "San Luis",
        "descripcion": "San Luis",
        "abreviacion": "San Luis",
        "createdAt": "2020-01-16T13:31:48.283Z",
        "updatedAt": "2020-01-16T13:31:48.283Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bea"
        },
        "nombre": "Toribio",
        "descripcion": "Toribio",
        "abreviacion": "Toribio",
        "createdAt": "2020-01-16T13:31:48.285Z",
        "updatedAt": "2020-01-16T13:31:48.285Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526beb"
        },
        "nombre": "La Vega",
        "descripcion": "La Vega",
        "abreviacion": "La Vega",
        "createdAt": "2020-01-16T13:31:48.288Z",
        "updatedAt": "2020-01-16T13:31:48.288Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bec"
        },
        "nombre": "Chimichagua",
        "descripcion": "Chimichagua",
        "abreviacion": "Chimichagua",
        "createdAt": "2020-01-16T13:31:48.289Z",
        "updatedAt": "2020-01-16T13:31:48.289Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bed"
        },
        "nombre": "Chitagá",
        "descripcion": "Chitagá",
        "abreviacion": "Chitagá",
        "createdAt": "2020-01-16T13:31:48.292Z",
        "updatedAt": "2020-01-16T13:31:48.292Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bee"
        },
        "nombre": "Trujillo",
        "descripcion": "Trujillo",
        "abreviacion": "Trujillo",
        "createdAt": "2020-01-16T13:31:48.295Z",
        "updatedAt": "2020-01-16T13:31:48.295Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bef"
        },
        "nombre": "Mompós",
        "descripcion": "Mompós",
        "abreviacion": "Mompós",
        "createdAt": "2020-01-16T13:31:48.300Z",
        "updatedAt": "2020-01-16T13:31:48.300Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bf0"
        },
        "nombre": "Tenza",
        "descripcion": "Tenza",
        "abreviacion": "Tenza",
        "createdAt": "2020-01-16T13:31:48.319Z",
        "updatedAt": "2020-01-16T13:31:48.319Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bf1"
        },
        "nombre": "Repelón",
        "descripcion": "Repelón",
        "abreviacion": "Repelón",
        "createdAt": "2020-01-16T13:31:48.321Z",
        "updatedAt": "2020-01-16T13:31:48.321Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bf2"
        },
        "nombre": "Palmas del Socorro",
        "descripcion": "Palmas del Socorro",
        "abreviacion": "Palmas del Socorro",
        "createdAt": "2020-01-16T13:31:48.325Z",
        "updatedAt": "2020-01-16T13:31:48.325Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bf3"
        },
        "nombre": "Candelaria",
        "descripcion": "Candelaria",
        "abreviacion": "Candelaria",
        "createdAt": "2020-01-16T13:31:48.327Z",
        "updatedAt": "2020-01-16T13:31:48.327Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bf4"
        },
        "nombre": "Bahía Solano",
        "descripcion": "Bahía Solano",
        "abreviacion": "Bahía Solano",
        "createdAt": "2020-01-16T13:31:48.330Z",
        "updatedAt": "2020-01-16T13:31:48.330Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bf5"
        },
        "nombre": "Sipí",
        "descripcion": "Sipí",
        "abreviacion": "Sipí",
        "createdAt": "2020-01-16T13:31:48.333Z",
        "updatedAt": "2020-01-16T13:31:48.333Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bf6"
        },
        "nombre": "Guatavita",
        "descripcion": "Guatavita",
        "abreviacion": "Guatavita",
        "createdAt": "2020-01-16T13:31:48.334Z",
        "updatedAt": "2020-01-16T13:31:48.334Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bf7"
        },
        "nombre": "Rondón",
        "descripcion": "Rondón",
        "abreviacion": "Rondón",
        "createdAt": "2020-01-16T13:31:48.336Z",
        "updatedAt": "2020-01-16T13:31:48.336Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bf8"
        },
        "nombre": "Timbío",
        "descripcion": "Timbío",
        "abreviacion": "Timbío",
        "createdAt": "2020-01-16T13:31:48.339Z",
        "updatedAt": "2020-01-16T13:31:48.339Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bf9"
        },
        "nombre": "Otanche",
        "descripcion": "Otanche",
        "abreviacion": "Otanche",
        "createdAt": "2020-01-16T13:31:48.341Z",
        "updatedAt": "2020-01-16T13:31:48.341Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bfa"
        },
        "nombre": "Durania",
        "descripcion": "Durania",
        "abreviacion": "Durania",
        "createdAt": "2020-01-16T13:31:48.343Z",
        "updatedAt": "2020-01-16T13:31:48.343Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bfb"
        },
        "nombre": "Hacarí",
        "descripcion": "Hacarí",
        "abreviacion": "Hacarí",
        "createdAt": "2020-01-16T13:31:48.349Z",
        "updatedAt": "2020-01-16T13:31:48.349Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bfc"
        },
        "nombre": "Barranco de Loba",
        "descripcion": "Barranco de Loba",
        "abreviacion": "Barranco de Loba",
        "createdAt": "2020-01-16T13:31:48.359Z",
        "updatedAt": "2020-01-16T13:31:48.359Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bfd"
        },
        "nombre": "Belén de Umbría",
        "descripcion": "Belén de Umbría",
        "abreviacion": "Belén de Umbría",
        "createdAt": "2020-01-16T13:31:48.361Z",
        "updatedAt": "2020-01-16T13:31:48.361Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bfe"
        },
        "nombre": "Galeras",
        "descripcion": "Galeras",
        "abreviacion": "Galeras",
        "createdAt": "2020-01-16T13:31:48.365Z",
        "updatedAt": "2020-01-16T13:31:48.365Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526bff"
        },
        "nombre": "Chalán",
        "descripcion": "Chalán",
        "abreviacion": "Chalán",
        "createdAt": "2020-01-16T13:31:48.366Z",
        "updatedAt": "2020-01-16T13:31:48.366Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c00"
        },
        "nombre": "La Victoria",
        "descripcion": "La Victoria",
        "abreviacion": "La Victoria",
        "createdAt": "2020-01-16T13:31:48.367Z",
        "updatedAt": "2020-01-16T13:31:48.367Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c01"
        },
        "nombre": "Salazar",
        "descripcion": "Salazar",
        "abreviacion": "Salazar",
        "createdAt": "2020-01-16T13:31:48.370Z",
        "updatedAt": "2020-01-16T13:31:48.370Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c02"
        },
        "nombre": "Timaná",
        "descripcion": "Timaná",
        "abreviacion": "Timaná",
        "createdAt": "2020-01-16T13:31:48.373Z",
        "updatedAt": "2020-01-16T13:31:48.373Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c03"
        },
        "nombre": "Palermo",
        "descripcion": "Palermo",
        "abreviacion": "Palermo",
        "createdAt": "2020-01-16T13:31:48.376Z",
        "updatedAt": "2020-01-16T13:31:48.376Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c04"
        },
        "nombre": "Medio Atrato",
        "descripcion": "Medio Atrato",
        "abreviacion": "Medio Atrato",
        "createdAt": "2020-01-16T13:31:48.377Z",
        "updatedAt": "2020-01-16T13:31:48.377Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c05"
        },
        "nombre": "Uribe",
        "descripcion": "Uribe",
        "abreviacion": "Uribe",
        "createdAt": "2020-01-16T13:31:48.380Z",
        "updatedAt": "2020-01-16T13:31:48.380Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c06"
        },
        "nombre": "La Plata",
        "descripcion": "La Plata",
        "abreviacion": "La Plata",
        "createdAt": "2020-01-16T13:31:48.386Z",
        "updatedAt": "2020-01-16T13:31:48.386Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c07"
        },
        "nombre": "San José del Fragua",
        "descripcion": "San José del Fragua",
        "abreviacion": "San José del Fragua",
        "createdAt": "2020-01-16T13:31:48.388Z",
        "updatedAt": "2020-01-16T13:31:48.388Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c08"
        },
        "nombre": "El Guamo",
        "descripcion": "El Guamo",
        "abreviacion": "El Guamo",
        "createdAt": "2020-01-16T13:31:48.390Z",
        "updatedAt": "2020-01-16T13:31:48.390Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c09"
        },
        "nombre": "Santa Rosa de Osos",
        "descripcion": "Santa Rosa de Osos",
        "abreviacion": "Santa Rosa de Osos",
        "createdAt": "2020-01-16T13:31:48.392Z",
        "updatedAt": "2020-01-16T13:31:48.392Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c0a"
        },
        "nombre": "Tame",
        "descripcion": "Tame",
        "abreviacion": "Tame",
        "createdAt": "2020-01-16T13:31:48.395Z",
        "updatedAt": "2020-01-16T13:31:48.395Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c0b"
        },
        "nombre": "Buenavista",
        "descripcion": "Buenavista",
        "abreviacion": "Buenavista",
        "createdAt": "2020-01-16T13:31:48.398Z",
        "updatedAt": "2020-01-16T13:31:48.398Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c0c"
        },
        "nombre": "Villahermosa",
        "descripcion": "Villahermosa",
        "abreviacion": "Villahermosa",
        "createdAt": "2020-01-16T13:31:48.399Z",
        "updatedAt": "2020-01-16T13:31:48.399Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c0d"
        },
        "nombre": "El Peñón",
        "descripcion": "El Peñón",
        "abreviacion": "El Peñón",
        "createdAt": "2020-01-16T13:31:48.401Z",
        "updatedAt": "2020-01-16T13:31:48.401Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c0e"
        },
        "nombre": "Ocaña",
        "descripcion": "Ocaña",
        "abreviacion": "Ocaña",
        "createdAt": "2020-01-16T13:31:48.427Z",
        "updatedAt": "2020-01-16T13:31:48.427Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c0f"
        },
        "nombre": "Dagua",
        "descripcion": "Dagua",
        "abreviacion": "Dagua",
        "createdAt": "2020-01-16T13:31:48.428Z",
        "updatedAt": "2020-01-16T13:31:48.428Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c10"
        },
        "nombre": "Villa Caro",
        "descripcion": "Villa Caro",
        "abreviacion": "Villa Caro",
        "createdAt": "2020-01-16T13:31:48.430Z",
        "updatedAt": "2020-01-16T13:31:48.430Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c11"
        },
        "nombre": "Los Patios",
        "descripcion": "Los Patios",
        "abreviacion": "Los Patios",
        "createdAt": "2020-01-16T13:31:48.431Z",
        "updatedAt": "2020-01-16T13:31:48.431Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c12"
        },
        "nombre": "El Cantón del San Pablo",
        "descripcion": "El Cantón del San Pablo",
        "abreviacion": "El Cantón del San Pablo",
        "createdAt": "2020-01-16T13:31:48.434Z",
        "updatedAt": "2020-01-16T13:31:48.434Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c13"
        },
        "nombre": "Hato",
        "descripcion": "Hato",
        "abreviacion": "Hato",
        "createdAt": "2020-01-16T13:31:48.435Z",
        "updatedAt": "2020-01-16T13:31:48.435Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c14"
        },
        "nombre": "Andalucía",
        "descripcion": "Andalucía",
        "abreviacion": "Andalucía",
        "createdAt": "2020-01-16T13:31:48.436Z",
        "updatedAt": "2020-01-16T13:31:48.436Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c15"
        },
        "nombre": "El Zulia",
        "descripcion": "El Zulia",
        "abreviacion": "El Zulia",
        "createdAt": "2020-01-16T13:31:48.438Z",
        "updatedAt": "2020-01-16T13:31:48.438Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c16"
        },
        "nombre": "Puerto Alegría",
        "descripcion": "Puerto Alegría",
        "abreviacion": "Puerto Alegría",
        "createdAt": "2020-01-16T13:31:48.439Z",
        "updatedAt": "2020-01-16T13:31:48.439Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c17"
        },
        "nombre": "Santafé de Antioquia",
        "descripcion": "Santafé de Antioquia",
        "abreviacion": "Santafé de Antioquia",
        "createdAt": "2020-01-16T13:31:48.440Z",
        "updatedAt": "2020-01-16T13:31:48.440Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c18"
        },
        "nombre": "Villarrica",
        "descripcion": "Villarrica",
        "abreviacion": "Villarrica",
        "createdAt": "2020-01-16T13:31:48.441Z",
        "updatedAt": "2020-01-16T13:31:48.441Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c19"
        },
        "nombre": "La Victoria",
        "descripcion": "La Victoria",
        "abreviacion": "La Victoria",
        "createdAt": "2020-01-16T13:31:48.443Z",
        "updatedAt": "2020-01-16T13:31:48.443Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c1a"
        },
        "nombre": "Silos",
        "descripcion": "Silos",
        "abreviacion": "Silos",
        "createdAt": "2020-01-16T13:31:48.444Z",
        "updatedAt": "2020-01-16T13:31:48.444Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c1b"
        },
        "nombre": "Mutiscua",
        "descripcion": "Mutiscua",
        "abreviacion": "Mutiscua",
        "createdAt": "2020-01-16T13:31:48.446Z",
        "updatedAt": "2020-01-16T13:31:48.446Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c1c"
        },
        "nombre": "San Lorenzo",
        "descripcion": "San Lorenzo",
        "abreviacion": "San Lorenzo",
        "createdAt": "2020-01-16T13:31:48.452Z",
        "updatedAt": "2020-01-16T13:31:48.452Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c1d"
        },
        "nombre": "Purificación",
        "descripcion": "Purificación",
        "abreviacion": "Purificación",
        "createdAt": "2020-01-16T13:31:48.454Z",
        "updatedAt": "2020-01-16T13:31:48.454Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c1e"
        },
        "nombre": "Coloso",
        "descripcion": "Coloso",
        "abreviacion": "Coloso",
        "createdAt": "2020-01-16T13:31:48.456Z",
        "updatedAt": "2020-01-16T13:31:48.456Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c1f"
        },
        "nombre": "Murillo",
        "descripcion": "Murillo",
        "abreviacion": "Murillo",
        "createdAt": "2020-01-16T13:31:48.457Z",
        "updatedAt": "2020-01-16T13:31:48.457Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c20"
        },
        "nombre": "Guamo",
        "descripcion": "Guamo",
        "abreviacion": "Guamo",
        "createdAt": "2020-01-16T13:31:48.458Z",
        "updatedAt": "2020-01-16T13:31:48.458Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c21"
        },
        "nombre": "Natagaima",
        "descripcion": "Natagaima",
        "abreviacion": "Natagaima",
        "createdAt": "2020-01-16T13:31:48.460Z",
        "updatedAt": "2020-01-16T13:31:48.460Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c22"
        },
        "nombre": "Fortul",
        "descripcion": "Fortul",
        "abreviacion": "Fortul",
        "createdAt": "2020-01-16T13:31:48.461Z",
        "updatedAt": "2020-01-16T13:31:48.461Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c23"
        },
        "nombre": "Coyaima",
        "descripcion": "Coyaima",
        "abreviacion": "Coyaima",
        "createdAt": "2020-01-16T13:31:48.464Z",
        "updatedAt": "2020-01-16T13:31:48.464Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c24"
        },
        "nombre": "Cepitá",
        "descripcion": "Cepitá",
        "abreviacion": "Cepitá",
        "createdAt": "2020-01-16T13:31:48.466Z",
        "updatedAt": "2020-01-16T13:31:48.466Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c25"
        },
        "nombre": "Francisco Pizarro",
        "descripcion": "Francisco Pizarro",
        "abreviacion": "Francisco Pizarro",
        "createdAt": "2020-01-16T13:31:48.468Z",
        "updatedAt": "2020-01-16T13:31:48.468Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c26"
        },
        "nombre": "Flandes",
        "descripcion": "Flandes",
        "abreviacion": "Flandes",
        "createdAt": "2020-01-16T13:31:48.469Z",
        "updatedAt": "2020-01-16T13:31:48.469Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c27"
        },
        "nombre": "San Joaquín",
        "descripcion": "San Joaquín",
        "abreviacion": "San Joaquín",
        "createdAt": "2020-01-16T13:31:48.470Z",
        "updatedAt": "2020-01-16T13:31:48.470Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c28"
        },
        "nombre": "Bucaramanga",
        "descripcion": "Bucaramanga",
        "abreviacion": "Bucaramanga",
        "createdAt": "2020-01-16T13:31:48.471Z",
        "updatedAt": "2020-01-16T13:31:48.471Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c29"
        },
        "nombre": "Florián",
        "descripcion": "Florián",
        "abreviacion": "Florián",
        "createdAt": "2020-01-16T13:31:48.472Z",
        "updatedAt": "2020-01-16T13:31:48.472Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c2a"
        },
        "nombre": "Lejanías",
        "descripcion": "Lejanías",
        "abreviacion": "Lejanías",
        "createdAt": "2020-01-16T13:31:48.475Z",
        "updatedAt": "2020-01-16T13:31:48.475Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c2b"
        },
        "nombre": "Morroa",
        "descripcion": "Morroa",
        "abreviacion": "Morroa",
        "createdAt": "2020-01-16T13:31:48.476Z",
        "updatedAt": "2020-01-16T13:31:48.476Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c2c"
        },
        "nombre": "Circasia",
        "descripcion": "Circasia",
        "abreviacion": "Circasia",
        "createdAt": "2020-01-16T13:31:48.478Z",
        "updatedAt": "2020-01-16T13:31:48.478Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c2d"
        },
        "nombre": "Puerto Gaitán",
        "descripcion": "Puerto Gaitán",
        "abreviacion": "Puerto Gaitán",
        "createdAt": "2020-01-16T13:31:48.479Z",
        "updatedAt": "2020-01-16T13:31:48.479Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c2e"
        },
        "nombre": "Aguada",
        "descripcion": "Aguada",
        "abreviacion": "Aguada",
        "createdAt": "2020-01-16T13:31:48.480Z",
        "updatedAt": "2020-01-16T13:31:48.480Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c2f"
        },
        "nombre": "Fresno",
        "descripcion": "Fresno",
        "abreviacion": "Fresno",
        "createdAt": "2020-01-16T13:31:48.481Z",
        "updatedAt": "2020-01-16T13:31:48.481Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c30"
        },
        "nombre": "Puerto Lleras",
        "descripcion": "Puerto Lleras",
        "abreviacion": "Puerto Lleras",
        "createdAt": "2020-01-16T13:31:48.483Z",
        "updatedAt": "2020-01-16T13:31:48.483Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c31"
        },
        "nombre": "Policarpa",
        "descripcion": "Policarpa",
        "abreviacion": "Policarpa",
        "createdAt": "2020-01-16T13:31:48.486Z",
        "updatedAt": "2020-01-16T13:31:48.486Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c32"
        },
        "nombre": "Chámeza",
        "descripcion": "Chámeza",
        "abreviacion": "Chámeza",
        "createdAt": "2020-01-16T13:31:48.488Z",
        "updatedAt": "2020-01-16T13:31:48.488Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c33"
        },
        "nombre": "Barbacoas",
        "descripcion": "Barbacoas",
        "abreviacion": "Barbacoas",
        "createdAt": "2020-01-16T13:31:48.490Z",
        "updatedAt": "2020-01-16T13:31:48.490Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c34"
        },
        "nombre": "Melgar",
        "descripcion": "Melgar",
        "abreviacion": "Melgar",
        "createdAt": "2020-01-16T13:31:48.491Z",
        "updatedAt": "2020-01-16T13:31:48.491Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c35"
        },
        "nombre": "Ovejas",
        "descripcion": "Ovejas",
        "abreviacion": "Ovejas",
        "createdAt": "2020-01-16T13:31:48.494Z",
        "updatedAt": "2020-01-16T13:31:48.494Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c36"
        },
        "nombre": "San José del Palmar",
        "descripcion": "San José del Palmar",
        "abreviacion": "San José del Palmar",
        "createdAt": "2020-01-16T13:31:48.496Z",
        "updatedAt": "2020-01-16T13:31:48.496Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c37"
        },
        "nombre": "Gambita",
        "descripcion": "Gambita",
        "abreviacion": "Gambita",
        "createdAt": "2020-01-16T13:31:48.498Z",
        "updatedAt": "2020-01-16T13:31:48.498Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c38"
        },
        "nombre": "Suratá",
        "descripcion": "Suratá",
        "abreviacion": "Suratá",
        "createdAt": "2020-01-16T13:31:48.500Z",
        "updatedAt": "2020-01-16T13:31:48.500Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c39"
        },
        "nombre": "Bajo Baudó",
        "descripcion": "Bajo Baudó",
        "abreviacion": "Bajo Baudó",
        "createdAt": "2020-01-16T13:31:48.501Z",
        "updatedAt": "2020-01-16T13:31:48.501Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c3a"
        },
        "nombre": "El Cerrito",
        "descripcion": "El Cerrito",
        "abreviacion": "El Cerrito",
        "createdAt": "2020-01-16T13:31:48.504Z",
        "updatedAt": "2020-01-16T13:31:48.504Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c3b"
        },
        "nombre": "Aracataca",
        "descripcion": "Aracataca",
        "abreviacion": "Aracataca",
        "createdAt": "2020-01-16T13:31:48.505Z",
        "updatedAt": "2020-01-16T13:31:48.505Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c3c"
        },
        "nombre": "Arboleda",
        "descripcion": "Arboleda",
        "abreviacion": "Arboleda",
        "createdAt": "2020-01-16T13:31:48.506Z",
        "updatedAt": "2020-01-16T13:31:48.506Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c3d"
        },
        "nombre": "Leticia",
        "descripcion": "Leticia",
        "abreviacion": "Leticia",
        "createdAt": "2020-01-16T13:31:48.512Z",
        "updatedAt": "2020-01-16T13:31:48.512Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c3e"
        },
        "nombre": "San Calixto",
        "descripcion": "San Calixto",
        "abreviacion": "San Calixto",
        "createdAt": "2020-01-16T13:31:48.513Z",
        "updatedAt": "2020-01-16T13:31:48.513Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c3f"
        },
        "nombre": "Bugalagrande",
        "descripcion": "Bugalagrande",
        "abreviacion": "Bugalagrande",
        "createdAt": "2020-01-16T13:31:48.514Z",
        "updatedAt": "2020-01-16T13:31:48.514Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c40"
        },
        "nombre": "Santa Rosa de Cabal",
        "descripcion": "Santa Rosa de Cabal",
        "abreviacion": "Santa Rosa de Cabal",
        "createdAt": "2020-01-16T13:31:48.516Z",
        "updatedAt": "2020-01-16T13:31:48.516Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c41"
        },
        "nombre": "Mallama",
        "descripcion": "Mallama",
        "abreviacion": "Mallama",
        "createdAt": "2020-01-16T13:31:48.517Z",
        "updatedAt": "2020-01-16T13:31:48.517Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c42"
        },
        "nombre": "La Esperanza",
        "descripcion": "La Esperanza",
        "abreviacion": "La Esperanza",
        "createdAt": "2020-01-16T13:31:48.519Z",
        "updatedAt": "2020-01-16T13:31:48.519Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c43"
        },
        "nombre": "La Florida",
        "descripcion": "La Florida",
        "abreviacion": "La Florida",
        "createdAt": "2020-01-16T13:31:48.520Z",
        "updatedAt": "2020-01-16T13:31:48.520Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c44"
        },
        "nombre": "Quimbaya",
        "descripcion": "Quimbaya",
        "abreviacion": "Quimbaya",
        "createdAt": "2020-01-16T13:31:48.521Z",
        "updatedAt": "2020-01-16T13:31:48.521Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c45"
        },
        "nombre": "Puerto Carreño",
        "descripcion": "Puerto Carreño",
        "abreviacion": "Puerto Carreño",
        "createdAt": "2020-01-16T13:31:48.523Z",
        "updatedAt": "2020-01-16T13:31:48.523Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c46"
        },
        "nombre": "Belén de Bajira",
        "descripcion": "Belén de Bajira",
        "abreviacion": "Belén de Bajira",
        "createdAt": "2020-01-16T13:31:48.524Z",
        "updatedAt": "2020-01-16T13:31:48.524Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c47"
        },
        "nombre": "Riohacha",
        "descripcion": "Riohacha",
        "abreviacion": "Riohacha",
        "createdAt": "2020-01-16T13:31:48.525Z",
        "updatedAt": "2020-01-16T13:31:48.525Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c48"
        },
        "nombre": "Tesalia",
        "descripcion": "Tesalia",
        "abreviacion": "Tesalia",
        "createdAt": "2020-01-16T13:31:48.526Z",
        "updatedAt": "2020-01-16T13:31:48.526Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c49"
        },
        "nombre": "Vista Hermosa",
        "descripcion": "Vista Hermosa",
        "abreviacion": "Vista Hermosa",
        "createdAt": "2020-01-16T13:31:48.528Z",
        "updatedAt": "2020-01-16T13:31:48.528Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c4a"
        },
        "nombre": "Chimá",
        "descripcion": "Chimá",
        "abreviacion": "Chimá",
        "createdAt": "2020-01-16T13:31:48.529Z",
        "updatedAt": "2020-01-16T13:31:48.529Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c4b"
        },
        "nombre": "Acevedo",
        "descripcion": "Acevedo",
        "abreviacion": "Acevedo",
        "createdAt": "2020-01-16T13:31:48.530Z",
        "updatedAt": "2020-01-16T13:31:48.530Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c4d"
        },
        "nombre": "Toca",
        "descripcion": "Toca",
        "abreviacion": "Toca",
        "createdAt": "2020-01-16T13:31:48.533Z",
        "updatedAt": "2020-01-16T13:31:48.533Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c4c"
        },
        "nombre": "Achí",
        "descripcion": "Achí",
        "abreviacion": "Achí",
        "createdAt": "2020-01-16T13:31:48.531Z",
        "updatedAt": "2020-01-16T13:31:48.531Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c4e"
        },
        "nombre": "Altamira",
        "descripcion": "Altamira",
        "abreviacion": "Altamira",
        "createdAt": "2020-01-16T13:31:48.534Z",
        "updatedAt": "2020-01-16T13:31:48.534Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c4f"
        },
        "nombre": "Guaca",
        "descripcion": "Guaca",
        "abreviacion": "Guaca",
        "createdAt": "2020-01-16T13:31:48.543Z",
        "updatedAt": "2020-01-16T13:31:48.543Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c50"
        },
        "nombre": "Alcalá",
        "descripcion": "Alcalá",
        "abreviacion": "Alcalá",
        "createdAt": "2020-01-16T13:31:48.545Z",
        "updatedAt": "2020-01-16T13:31:48.545Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c51"
        },
        "nombre": "El Encanto",
        "descripcion": "El Encanto",
        "abreviacion": "El Encanto",
        "createdAt": "2020-01-16T13:31:48.546Z",
        "updatedAt": "2020-01-16T13:31:48.546Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c52"
        },
        "nombre": "Puente Nacional",
        "descripcion": "Puente Nacional",
        "abreviacion": "Puente Nacional",
        "createdAt": "2020-01-16T13:31:48.553Z",
        "updatedAt": "2020-01-16T13:31:48.553Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c53"
        },
        "nombre": "Ulloa",
        "descripcion": "Ulloa",
        "abreviacion": "Ulloa",
        "createdAt": "2020-01-16T13:31:48.555Z",
        "updatedAt": "2020-01-16T13:31:48.555Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c54"
        },
        "nombre": "Yotoco",
        "descripcion": "Yotoco",
        "abreviacion": "Yotoco",
        "createdAt": "2020-01-16T13:31:48.556Z",
        "updatedAt": "2020-01-16T13:31:48.556Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c55"
        },
        "nombre": "Aguazul",
        "descripcion": "Aguazul",
        "abreviacion": "Aguazul",
        "createdAt": "2020-01-16T13:31:48.557Z",
        "updatedAt": "2020-01-16T13:31:48.557Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c56"
        },
        "nombre": "Ocamonte",
        "descripcion": "Ocamonte",
        "abreviacion": "Ocamonte",
        "createdAt": "2020-01-16T13:31:48.558Z",
        "updatedAt": "2020-01-16T13:31:48.558Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c57"
        },
        "nombre": "Santa Isabel",
        "descripcion": "Santa Isabel",
        "abreviacion": "Santa Isabel",
        "createdAt": "2020-01-16T13:31:48.559Z",
        "updatedAt": "2020-01-16T13:31:48.559Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c58"
        },
        "nombre": "Hato Corozal",
        "descripcion": "Hato Corozal",
        "abreviacion": "Hato Corozal",
        "createdAt": "2020-01-16T13:31:48.561Z",
        "updatedAt": "2020-01-16T13:31:48.561Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c59"
        },
        "nombre": "Páramo",
        "descripcion": "Páramo",
        "abreviacion": "Páramo",
        "createdAt": "2020-01-16T13:31:48.562Z",
        "updatedAt": "2020-01-16T13:31:48.562Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c5a"
        },
        "nombre": "Saravena",
        "descripcion": "Saravena",
        "abreviacion": "Saravena",
        "createdAt": "2020-01-16T13:31:48.563Z",
        "updatedAt": "2020-01-16T13:31:48.563Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c5b"
        },
        "nombre": "San Onofre",
        "descripcion": "San Onofre",
        "abreviacion": "San Onofre",
        "createdAt": "2020-01-16T13:31:48.564Z",
        "updatedAt": "2020-01-16T13:31:48.564Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c5c"
        },
        "nombre": "San Pablo",
        "descripcion": "San Pablo",
        "abreviacion": "San Pablo",
        "createdAt": "2020-01-16T13:31:48.566Z",
        "updatedAt": "2020-01-16T13:31:48.566Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c5d"
        },
        "nombre": "Jesús María",
        "descripcion": "Jesús María",
        "abreviacion": "Jesús María",
        "createdAt": "2020-01-16T13:31:48.567Z",
        "updatedAt": "2020-01-16T13:31:48.567Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c5e"
        },
        "nombre": "Espinal",
        "descripcion": "Espinal",
        "abreviacion": "Espinal",
        "createdAt": "2020-01-16T13:31:48.568Z",
        "updatedAt": "2020-01-16T13:31:48.568Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c5f"
        },
        "nombre": "Puerto Santander",
        "descripcion": "Puerto Santander",
        "abreviacion": "Puerto Santander",
        "createdAt": "2020-01-16T13:31:48.570Z",
        "updatedAt": "2020-01-16T13:31:48.570Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c60"
        },
        "nombre": "Toro",
        "descripcion": "Toro",
        "abreviacion": "Toro",
        "createdAt": "2020-01-16T13:31:48.571Z",
        "updatedAt": "2020-01-16T13:31:48.571Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c61"
        },
        "nombre": "Colón",
        "descripcion": "Colón",
        "abreviacion": "Colón",
        "createdAt": "2020-01-16T13:31:48.572Z",
        "updatedAt": "2020-01-16T13:31:48.572Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c62"
        },
        "nombre": "Buenavista",
        "descripcion": "Buenavista",
        "abreviacion": "Buenavista",
        "createdAt": "2020-01-16T13:31:48.573Z",
        "updatedAt": "2020-01-16T13:31:48.573Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c63"
        },
        "nombre": "La Chorrera",
        "descripcion": "La Chorrera",
        "abreviacion": "La Chorrera",
        "createdAt": "2020-01-16T13:31:48.575Z",
        "updatedAt": "2020-01-16T13:31:48.575Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c64"
        },
        "nombre": "Quinchía",
        "descripcion": "Quinchía",
        "abreviacion": "Quinchía",
        "createdAt": "2020-01-16T13:31:48.576Z",
        "updatedAt": "2020-01-16T13:31:48.576Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c65"
        },
        "nombre": "Recetor",
        "descripcion": "Recetor",
        "abreviacion": "Recetor",
        "createdAt": "2020-01-16T13:31:48.577Z",
        "updatedAt": "2020-01-16T13:31:48.577Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c66"
        },
        "nombre": "Sucre",
        "descripcion": "Sucre",
        "abreviacion": "Sucre",
        "createdAt": "2020-01-16T13:31:48.578Z",
        "updatedAt": "2020-01-16T13:31:48.578Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c67"
        },
        "nombre": "Prado",
        "descripcion": "Prado",
        "abreviacion": "Prado",
        "createdAt": "2020-01-16T13:31:48.580Z",
        "updatedAt": "2020-01-16T13:31:48.580Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c68"
        },
        "nombre": "Floridablanca",
        "descripcion": "Floridablanca",
        "abreviacion": "Floridablanca",
        "createdAt": "2020-01-16T13:31:48.581Z",
        "updatedAt": "2020-01-16T13:31:48.581Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c69"
        },
        "nombre": "Guavatá",
        "descripcion": "Guavatá",
        "abreviacion": "Guavatá",
        "createdAt": "2020-01-16T13:31:48.582Z",
        "updatedAt": "2020-01-16T13:31:48.582Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c6a"
        },
        "nombre": "Planadas",
        "descripcion": "Planadas",
        "abreviacion": "Planadas",
        "createdAt": "2020-01-16T13:31:48.583Z",
        "updatedAt": "2020-01-16T13:31:48.583Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c6b"
        },
        "nombre": "Puerto Rondón",
        "descripcion": "Puerto Rondón",
        "abreviacion": "Puerto Rondón",
        "createdAt": "2020-01-16T13:31:48.585Z",
        "updatedAt": "2020-01-16T13:31:48.585Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c6c"
        },
        "nombre": "Urumita",
        "descripcion": "Urumita",
        "abreviacion": "Urumita",
        "createdAt": "2020-01-16T13:31:48.586Z",
        "updatedAt": "2020-01-16T13:31:48.586Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c6d"
        },
        "nombre": "Santiago",
        "descripcion": "Santiago",
        "abreviacion": "Santiago",
        "createdAt": "2020-01-16T13:31:48.587Z",
        "updatedAt": "2020-01-16T13:31:48.587Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c6e"
        },
        "nombre": "Orito",
        "descripcion": "Orito",
        "abreviacion": "Orito",
        "createdAt": "2020-01-16T13:31:48.591Z",
        "updatedAt": "2020-01-16T13:31:48.591Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c6f"
        },
        "nombre": "Balboa",
        "descripcion": "Balboa",
        "abreviacion": "Balboa",
        "createdAt": "2020-01-16T13:31:48.593Z",
        "updatedAt": "2020-01-16T13:31:48.593Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c70"
        },
        "nombre": "Piedras",
        "descripcion": "Piedras",
        "abreviacion": "Piedras",
        "createdAt": "2020-01-16T13:31:48.664Z",
        "updatedAt": "2020-01-16T13:31:48.664Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c71"
        },
        "nombre": "Puerto Arica",
        "descripcion": "Puerto Arica",
        "abreviacion": "Puerto Arica",
        "createdAt": "2020-01-16T13:31:48.666Z",
        "updatedAt": "2020-01-16T13:31:48.666Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c72"
        },
        "nombre": "San Miguel",
        "descripcion": "San Miguel",
        "abreviacion": "San Miguel",
        "createdAt": "2020-01-16T13:31:48.667Z",
        "updatedAt": "2020-01-16T13:31:48.667Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c73"
        },
        "nombre": "Majagual",
        "descripcion": "Majagual",
        "abreviacion": "Majagual",
        "createdAt": "2020-01-16T13:31:48.669Z",
        "updatedAt": "2020-01-16T13:31:48.669Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c74"
        },
        "nombre": "Arauquita",
        "descripcion": "Arauquita",
        "abreviacion": "Arauquita",
        "createdAt": "2020-01-16T13:31:48.670Z",
        "updatedAt": "2020-01-16T13:31:48.670Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c75"
        },
        "nombre": "Los Palmitos",
        "descripcion": "Los Palmitos",
        "abreviacion": "Los Palmitos",
        "createdAt": "2020-01-16T13:31:48.671Z",
        "updatedAt": "2020-01-16T13:31:48.671Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c76"
        },
        "nombre": "Falan",
        "descripcion": "Falan",
        "abreviacion": "Falan",
        "createdAt": "2020-01-16T13:31:48.672Z",
        "updatedAt": "2020-01-16T13:31:48.672Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c77"
        },
        "nombre": "Caimito",
        "descripcion": "Caimito",
        "abreviacion": "Caimito",
        "createdAt": "2020-01-16T13:31:48.674Z",
        "updatedAt": "2020-01-16T13:31:48.674Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c78"
        },
        "nombre": "Herveo",
        "descripcion": "Herveo",
        "abreviacion": "Herveo",
        "createdAt": "2020-01-16T13:31:48.675Z",
        "updatedAt": "2020-01-16T13:31:48.675Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c79"
        },
        "nombre": "Guadalupe",
        "descripcion": "Guadalupe",
        "abreviacion": "Guadalupe",
        "createdAt": "2020-01-16T13:31:48.676Z",
        "updatedAt": "2020-01-16T13:31:48.676Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c7a"
        },
        "nombre": "Vetas",
        "descripcion": "Vetas",
        "abreviacion": "Vetas",
        "createdAt": "2020-01-16T13:31:48.680Z",
        "updatedAt": "2020-01-16T13:31:48.680Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c7b"
        },
        "nombre": "Venadillo",
        "descripcion": "Venadillo",
        "abreviacion": "Venadillo",
        "createdAt": "2020-01-16T13:31:48.681Z",
        "updatedAt": "2020-01-16T13:31:48.681Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c7c"
        },
        "nombre": "Abrego",
        "descripcion": "Abrego",
        "abreviacion": "Abrego",
        "createdAt": "2020-01-16T13:31:48.682Z",
        "updatedAt": "2020-01-16T13:31:48.682Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c7d"
        },
        "nombre": "Tona",
        "descripcion": "Tona",
        "abreviacion": "Tona",
        "createdAt": "2020-01-16T13:31:48.684Z",
        "updatedAt": "2020-01-16T13:31:48.684Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c7e"
        },
        "nombre": "Sincelejo",
        "descripcion": "Sincelejo",
        "abreviacion": "Sincelejo",
        "createdAt": "2020-01-16T13:31:48.685Z",
        "updatedAt": "2020-01-16T13:31:48.685Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c7f"
        },
        "nombre": "Saldaña",
        "descripcion": "Saldaña",
        "abreviacion": "Saldaña",
        "createdAt": "2020-01-16T13:31:48.686Z",
        "updatedAt": "2020-01-16T13:31:48.686Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c80"
        },
        "nombre": "Puerto Parra",
        "descripcion": "Puerto Parra",
        "abreviacion": "Puerto Parra",
        "createdAt": "2020-01-16T13:31:48.687Z",
        "updatedAt": "2020-01-16T13:31:48.687Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c81"
        },
        "nombre": "Sácama",
        "descripcion": "Sácama",
        "abreviacion": "Sácama",
        "createdAt": "2020-01-16T13:31:48.688Z",
        "updatedAt": "2020-01-16T13:31:48.688Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c82"
        },
        "nombre": "Rio Blanco",
        "descripcion": "Rio Blanco",
        "abreviacion": "Rio Blanco",
        "createdAt": "2020-01-16T13:31:48.689Z",
        "updatedAt": "2020-01-16T13:31:48.689Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c83"
        },
        "nombre": "Yopal",
        "descripcion": "Yopal",
        "abreviacion": "Yopal",
        "createdAt": "2020-01-16T13:31:48.695Z",
        "updatedAt": "2020-01-16T13:31:48.695Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c84"
        },
        "nombre": "San Gil",
        "descripcion": "San Gil",
        "abreviacion": "San Gil",
        "createdAt": "2020-01-16T13:31:48.698Z",
        "updatedAt": "2020-01-16T13:31:48.698Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c85"
        },
        "nombre": "La Unión",
        "descripcion": "La Unión",
        "abreviacion": "La Unión",
        "createdAt": "2020-01-16T13:31:48.699Z",
        "updatedAt": "2020-01-16T13:31:48.699Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c86"
        },
        "nombre": "San Juanito",
        "descripcion": "San Juanito",
        "abreviacion": "San Juanito",
        "createdAt": "2020-01-16T13:31:48.700Z",
        "updatedAt": "2020-01-16T13:31:48.700Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c87"
        },
        "nombre": "Simacota",
        "descripcion": "Simacota",
        "abreviacion": "Simacota",
        "createdAt": "2020-01-16T13:31:48.702Z",
        "updatedAt": "2020-01-16T13:31:48.702Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c88"
        },
        "nombre": "Roberto Payán",
        "descripcion": "Roberto Payán",
        "abreviacion": "Roberto Payán",
        "createdAt": "2020-01-16T13:31:48.703Z",
        "updatedAt": "2020-01-16T13:31:48.703Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c89"
        },
        "nombre": "Pupiales",
        "descripcion": "Pupiales",
        "abreviacion": "Pupiales",
        "createdAt": "2020-01-16T13:31:48.704Z",
        "updatedAt": "2020-01-16T13:31:48.704Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c8a"
        },
        "nombre": "Jordán",
        "descripcion": "Jordán",
        "abreviacion": "Jordán",
        "createdAt": "2020-01-16T13:31:48.705Z",
        "updatedAt": "2020-01-16T13:31:48.705Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c8b"
        },
        "nombre": "Saladoblanco",
        "descripcion": "Saladoblanco",
        "abreviacion": "Saladoblanco",
        "createdAt": "2020-01-16T13:31:48.706Z",
        "updatedAt": "2020-01-16T13:31:48.706Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c8c"
        },
        "nombre": "Canalete",
        "descripcion": "Canalete",
        "abreviacion": "Canalete",
        "createdAt": "2020-01-16T13:31:48.708Z",
        "updatedAt": "2020-01-16T13:31:48.708Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c8d"
        },
        "nombre": "Los Santos",
        "descripcion": "Los Santos",
        "abreviacion": "Los Santos",
        "createdAt": "2020-01-16T13:31:48.709Z",
        "updatedAt": "2020-01-16T13:31:48.709Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c8e"
        },
        "nombre": "La Paz",
        "descripcion": "La Paz",
        "abreviacion": "La Paz",
        "createdAt": "2020-01-16T13:31:48.711Z",
        "updatedAt": "2020-01-16T13:31:48.711Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c8f"
        },
        "nombre": "Pinchote",
        "descripcion": "Pinchote",
        "abreviacion": "Pinchote",
        "createdAt": "2020-01-16T13:31:48.712Z",
        "updatedAt": "2020-01-16T13:31:48.712Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c90"
        },
        "nombre": "Plato",
        "descripcion": "Plato",
        "abreviacion": "Plato",
        "createdAt": "2020-01-16T13:31:48.714Z",
        "updatedAt": "2020-01-16T13:31:48.714Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c91"
        },
        "nombre": "Castilla la Nueva",
        "descripcion": "Castilla la Nueva",
        "abreviacion": "Castilla la Nueva",
        "createdAt": "2020-01-16T13:31:48.715Z",
        "updatedAt": "2020-01-16T13:31:48.715Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c92"
        },
        "nombre": "Ponedera",
        "descripcion": "Ponedera",
        "abreviacion": "Ponedera",
        "createdAt": "2020-01-16T13:31:48.717Z",
        "updatedAt": "2020-01-16T13:31:48.717Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c93"
        },
        "nombre": "San Andrés",
        "descripcion": "San Andrés",
        "abreviacion": "San Andrés",
        "createdAt": "2020-01-16T13:31:48.718Z",
        "updatedAt": "2020-01-16T13:31:48.718Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c94"
        },
        "nombre": "San José de Uré",
        "descripcion": "San José de Uré",
        "abreviacion": "San José de Uré",
        "createdAt": "2020-01-16T13:31:48.720Z",
        "updatedAt": "2020-01-16T13:31:48.720Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c95"
        },
        "nombre": "El Águila",
        "descripcion": "El Águila",
        "abreviacion": "El Águila",
        "createdAt": "2020-01-16T13:31:48.721Z",
        "updatedAt": "2020-01-16T13:31:48.721Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c96"
        },
        "nombre": "San Vicente de Chucurí",
        "descripcion": "San Vicente de Chucurí",
        "abreviacion": "San Vicente de Chucurí",
        "createdAt": "2020-01-16T13:31:48.722Z",
        "updatedAt": "2020-01-16T13:31:48.722Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c97"
        },
        "nombre": "Paz de Ariporo",
        "descripcion": "Paz de Ariporo",
        "abreviacion": "Paz de Ariporo",
        "createdAt": "2020-01-16T13:31:48.724Z",
        "updatedAt": "2020-01-16T13:31:48.724Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c98"
        },
        "nombre": "Buenos Aires",
        "descripcion": "Buenos Aires",
        "abreviacion": "Buenos Aires",
        "createdAt": "2020-01-16T13:31:48.727Z",
        "updatedAt": "2020-01-16T13:31:48.727Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c99"
        },
        "nombre": "Villavieja",
        "descripcion": "Villavieja",
        "abreviacion": "Villavieja",
        "createdAt": "2020-01-16T13:31:48.728Z",
        "updatedAt": "2020-01-16T13:31:48.728Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c9a"
        },
        "nombre": "Guayabal de Siquima",
        "descripcion": "Guayabal de Siquima",
        "abreviacion": "Guayabal de Siquima",
        "createdAt": "2020-01-16T13:31:48.730Z",
        "updatedAt": "2020-01-16T13:31:48.730Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c9b"
        },
        "nombre": "Confines",
        "descripcion": "Confines",
        "abreviacion": "Confines",
        "createdAt": "2020-01-16T13:31:48.731Z",
        "updatedAt": "2020-01-16T13:31:48.731Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c9c"
        },
        "nombre": "Buesaco",
        "descripcion": "Buesaco",
        "abreviacion": "Buesaco",
        "createdAt": "2020-01-16T13:31:48.732Z",
        "updatedAt": "2020-01-16T13:31:48.732Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c9d"
        },
        "nombre": "Arenal",
        "descripcion": "Arenal",
        "abreviacion": "Arenal",
        "createdAt": "2020-01-16T13:31:48.734Z",
        "updatedAt": "2020-01-16T13:31:48.734Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c9e"
        },
        "nombre": "Roncesvalles",
        "descripcion": "Roncesvalles",
        "abreviacion": "Roncesvalles",
        "createdAt": "2020-01-16T13:31:48.735Z",
        "updatedAt": "2020-01-16T13:31:48.735Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526c9f"
        },
        "nombre": "Tauramena",
        "descripcion": "Tauramena",
        "abreviacion": "Tauramena",
        "createdAt": "2020-01-16T13:31:48.736Z",
        "updatedAt": "2020-01-16T13:31:48.736Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526ca0"
        },
        "nombre": "Urrao",
        "descripcion": "Urrao",
        "abreviacion": "Urrao",
        "createdAt": "2020-01-16T13:31:48.738Z",
        "updatedAt": "2020-01-16T13:31:48.738Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526ca1"
        },
        "nombre": "Sibundoy",
        "descripcion": "Sibundoy",
        "abreviacion": "Sibundoy",
        "createdAt": "2020-01-16T13:31:48.739Z",
        "updatedAt": "2020-01-16T13:31:48.739Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526ca2"
        },
        "nombre": "Ginebra",
        "descripcion": "Ginebra",
        "abreviacion": "Ginebra",
        "createdAt": "2020-01-16T13:31:48.740Z",
        "updatedAt": "2020-01-16T13:31:48.740Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526ca3"
        },
        "nombre": "Icononzo",
        "descripcion": "Icononzo",
        "abreviacion": "Icononzo",
        "createdAt": "2020-01-16T13:31:48.742Z",
        "updatedAt": "2020-01-16T13:31:48.742Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526ca4"
        },
        "nombre": "Dolores",
        "descripcion": "Dolores",
        "abreviacion": "Dolores",
        "createdAt": "2020-01-16T13:31:48.743Z",
        "updatedAt": "2020-01-16T13:31:48.743Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526ca5"
        },
        "nombre": "Rovira",
        "descripcion": "Rovira",
        "abreviacion": "Rovira",
        "createdAt": "2020-01-16T13:31:48.745Z",
        "updatedAt": "2020-01-16T13:31:48.745Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526ca6"
        },
        "nombre": "Trinidad",
        "descripcion": "Trinidad",
        "abreviacion": "Trinidad",
        "createdAt": "2020-01-16T13:31:48.746Z",
        "updatedAt": "2020-01-16T13:31:48.746Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526ca7"
        },
        "nombre": "La Pedrera",
        "descripcion": "La Pedrera",
        "abreviacion": "La Pedrera",
        "createdAt": "2020-01-16T13:31:48.748Z",
        "updatedAt": "2020-01-16T13:31:48.748Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526ca8"
        },
        "nombre": "Leguízamo",
        "descripcion": "Leguízamo",
        "abreviacion": "Leguízamo",
        "createdAt": "2020-01-16T13:31:48.749Z",
        "updatedAt": "2020-01-16T13:31:48.749Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526ca9"
        },
        "nombre": "Ortega",
        "descripcion": "Ortega",
        "abreviacion": "Ortega",
        "createdAt": "2020-01-16T13:31:48.750Z",
        "updatedAt": "2020-01-16T13:31:48.750Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526caa"
        },
        "nombre": "Herrán",
        "descripcion": "Herrán",
        "abreviacion": "Herrán",
        "createdAt": "2020-01-16T13:31:48.751Z",
        "updatedAt": "2020-01-16T13:31:48.751Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526cab"
        },
        "nombre": "Mogotes",
        "descripcion": "Mogotes",
        "abreviacion": "Mogotes",
        "createdAt": "2020-01-16T13:31:48.753Z",
        "updatedAt": "2020-01-16T13:31:48.753Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526cac"
        },
        "nombre": "Concepción",
        "descripcion": "Concepción",
        "abreviacion": "Concepción",
        "createdAt": "2020-01-16T13:31:48.757Z",
        "updatedAt": "2020-01-16T13:31:48.757Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526cad"
        },
        "nombre": "La Unión",
        "descripcion": "La Unión",
        "abreviacion": "La Unión",
        "createdAt": "2020-01-16T13:31:48.758Z",
        "updatedAt": "2020-01-16T13:31:48.758Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526cae"
        },
        "nombre": "Santacruz",
        "descripcion": "Santacruz",
        "abreviacion": "Santacruz",
        "createdAt": "2020-01-16T13:31:48.759Z",
        "updatedAt": "2020-01-16T13:31:48.759Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526caf"
        },
        "nombre": "Pueblo Rico",
        "descripcion": "Pueblo Rico",
        "abreviacion": "Pueblo Rico",
        "createdAt": "2020-01-16T13:31:48.844Z",
        "updatedAt": "2020-01-16T13:31:48.844Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526cb0"
        },
        "nombre": "Encino",
        "descripcion": "Encino",
        "abreviacion": "Encino",
        "createdAt": "2020-01-16T13:31:48.846Z",
        "updatedAt": "2020-01-16T13:31:48.846Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526cb1"
        },
        "nombre": "Pore",
        "descripcion": "Pore",
        "abreviacion": "Pore",
        "createdAt": "2020-01-16T13:31:48.847Z",
        "updatedAt": "2020-01-16T13:31:48.847Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526cb2"
        },
        "nombre": "Contratación",
        "descripcion": "Contratación",
        "abreviacion": "Contratación",
        "createdAt": "2020-01-16T13:31:48.848Z",
        "updatedAt": "2020-01-16T13:31:48.848Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526cb3"
        },
        "nombre": "Mariquita",
        "descripcion": "Mariquita",
        "abreviacion": "Mariquita",
        "createdAt": "2020-01-16T13:31:48.849Z",
        "updatedAt": "2020-01-16T13:31:48.849Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526cb4"
        },
        "nombre": "Chipatá",
        "descripcion": "Chipatá",
        "abreviacion": "Chipatá",
        "createdAt": "2020-01-16T13:31:48.850Z",
        "updatedAt": "2020-01-16T13:31:48.850Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526cb5"
        },
        "nombre": "Cravo Norte",
        "descripcion": "Cravo Norte",
        "abreviacion": "Cravo Norte",
        "createdAt": "2020-01-16T13:31:48.852Z",
        "updatedAt": "2020-01-16T13:31:48.852Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526cb6"
        },
        "nombre": "San Andrés de Cuerquía",
        "descripcion": "San Andrés de Cuerquía",
        "abreviacion": "San Andrés de Cuerquía",
        "createdAt": "2020-01-16T13:31:48.853Z",
        "updatedAt": "2020-01-16T13:31:48.853Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526cb7"
        },
        "nombre": "Magüí",
        "descripcion": "Magüí",
        "abreviacion": "Magüí",
        "createdAt": "2020-01-16T13:31:48.854Z",
        "updatedAt": "2020-01-16T13:31:48.854Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526cb8"
        },
        "nombre": "Mocoa",
        "descripcion": "Mocoa",
        "abreviacion": "Mocoa",
        "createdAt": "2020-01-16T13:31:48.855Z",
        "updatedAt": "2020-01-16T13:31:48.855Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526cb9"
        },
        "nombre": "Medio Baudó",
        "descripcion": "Medio Baudó",
        "abreviacion": "Medio Baudó",
        "createdAt": "2020-01-16T13:31:48.856Z",
        "updatedAt": "2020-01-16T13:31:48.856Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526cba"
        },
        "nombre": "Palmito",
        "descripcion": "Palmito",
        "abreviacion": "Palmito",
        "createdAt": "2020-01-16T13:31:48.858Z",
        "updatedAt": "2020-01-16T13:31:48.858Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526cbb"
        },
        "nombre": "Villanueva",
        "descripcion": "Villanueva",
        "abreviacion": "Villanueva",
        "createdAt": "2020-01-16T13:31:48.859Z",
        "updatedAt": "2020-01-16T13:31:48.859Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526cbc"
        },
        "nombre": "Armero",
        "descripcion": "Armero",
        "abreviacion": "Armero",
        "createdAt": "2020-01-16T13:31:48.860Z",
        "updatedAt": "2020-01-16T13:31:48.860Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526cbd"
        },
        "nombre": "San Miguel",
        "descripcion": "San Miguel",
        "abreviacion": "San Miguel",
        "createdAt": "2020-01-16T13:31:48.861Z",
        "updatedAt": "2020-01-16T13:31:48.861Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526cbe"
        },
        "nombre": "San Marcos",
        "descripcion": "San Marcos",
        "abreviacion": "San Marcos",
        "createdAt": "2020-01-16T13:31:48.862Z",
        "updatedAt": "2020-01-16T13:31:48.862Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526cbf"
        },
        "nombre": "Ataco",
        "descripcion": "Ataco",
        "abreviacion": "Ataco",
        "createdAt": "2020-01-16T13:31:48.864Z",
        "updatedAt": "2020-01-16T13:31:48.864Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e2065c47c6e3d0008526cc0"
        },
        "nombre": "Zona Bananera",
        "descripcion": "Zona Bananera",
        "abreviacion": "Zona Bananera",
        "createdAt": "2020-01-16T13:31:48.865Z",
        "updatedAt": "2020-01-16T13:31:48.865Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608086"
        },
        "nombre": "Medellín",
        "descripcion": "Medellín",
        "abreviacion": "Medellín",
        "createdAt": "2020-01-16T13:33:35.488Z",
        "updatedAt": "2020-01-16T13:33:35.488Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608088"
        },
        "nombre": "Abriaquí",
        "descripcion": "Abriaquí",
        "abreviacion": "Abriaquí",
        "createdAt": "2020-01-16T13:33:35.494Z",
        "updatedAt": "2020-01-16T13:33:35.494Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608087"
        },
        "nombre": "Abejorral",
        "descripcion": "Abejorral",
        "abreviacion": "Abejorral",
        "createdAt": "2020-01-16T13:33:35.493Z",
        "updatedAt": "2020-01-16T13:33:35.493Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608089"
        },
        "nombre": "Alejandría",
        "descripcion": "Alejandría",
        "abreviacion": "Alejandría",
        "createdAt": "2020-01-16T13:33:35.496Z",
        "updatedAt": "2020-01-16T13:33:35.496Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60808b"
        },
        "nombre": "Amalfi",
        "descripcion": "Amalfi",
        "abreviacion": "Amalfi",
        "createdAt": "2020-01-16T13:33:35.499Z",
        "updatedAt": "2020-01-16T13:33:35.499Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60808a"
        },
        "nombre": "Amagá",
        "descripcion": "Amagá",
        "abreviacion": "Amagá",
        "createdAt": "2020-01-16T13:33:35.498Z",
        "updatedAt": "2020-01-16T13:33:35.498Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60808c"
        },
        "nombre": "Andes",
        "descripcion": "Andes",
        "abreviacion": "Andes",
        "createdAt": "2020-01-16T13:33:35.500Z",
        "updatedAt": "2020-01-16T13:33:35.500Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60808e"
        },
        "nombre": "Angostura",
        "descripcion": "Angostura",
        "abreviacion": "Angostura",
        "createdAt": "2020-01-16T13:33:35.504Z",
        "updatedAt": "2020-01-16T13:33:35.504Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60808d"
        },
        "nombre": "Angelópolis",
        "descripcion": "Angelópolis",
        "abreviacion": "Angelópolis",
        "createdAt": "2020-01-16T13:33:35.503Z",
        "updatedAt": "2020-01-16T13:33:35.503Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60808f"
        },
        "nombre": "Anorí",
        "descripcion": "Anorí",
        "abreviacion": "Anorí",
        "createdAt": "2020-01-16T13:33:35.506Z",
        "updatedAt": "2020-01-16T13:33:35.506Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608090"
        },
        "nombre": "Chimá",
        "descripcion": "Chimá",
        "abreviacion": "Chimá",
        "createdAt": "2020-01-16T13:33:35.507Z",
        "updatedAt": "2020-01-16T13:33:35.507Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608094"
        },
        "nombre": "Argelia",
        "descripcion": "Argelia",
        "abreviacion": "Argelia",
        "createdAt": "2020-01-16T13:33:35.513Z",
        "updatedAt": "2020-01-16T13:33:35.513Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608093"
        },
        "nombre": "Arboletes",
        "descripcion": "Arboletes",
        "abreviacion": "Arboletes",
        "createdAt": "2020-01-16T13:33:35.511Z",
        "updatedAt": "2020-01-16T13:33:35.511Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608095"
        },
        "nombre": "Armenia",
        "descripcion": "Armenia",
        "abreviacion": "Armenia",
        "createdAt": "2020-01-16T13:33:35.515Z",
        "updatedAt": "2020-01-16T13:33:35.515Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608091"
        },
        "nombre": "Anza",
        "descripcion": "Anza",
        "abreviacion": "Anza",
        "createdAt": "2020-01-16T13:33:35.508Z",
        "updatedAt": "2020-01-16T13:33:35.508Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608096"
        },
        "nombre": "Barbosa",
        "descripcion": "Barbosa",
        "abreviacion": "Barbosa",
        "createdAt": "2020-01-16T13:33:35.516Z",
        "updatedAt": "2020-01-16T13:33:35.516Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608099"
        },
        "nombre": "Betulia",
        "descripcion": "Betulia",
        "abreviacion": "Betulia",
        "createdAt": "2020-01-16T13:33:35.518Z",
        "updatedAt": "2020-01-16T13:33:35.518Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60809b"
        },
        "nombre": "Briceño",
        "descripcion": "Briceño",
        "abreviacion": "Briceño",
        "createdAt": "2020-01-16T13:33:35.520Z",
        "updatedAt": "2020-01-16T13:33:35.520Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60809e"
        },
        "nombre": "Caicedo",
        "descripcion": "Caicedo",
        "abreviacion": "Caicedo",
        "createdAt": "2020-01-16T13:33:35.523Z",
        "updatedAt": "2020-01-16T13:33:35.523Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080a0"
        },
        "nombre": "Campamento",
        "descripcion": "Campamento",
        "abreviacion": "Campamento",
        "createdAt": "2020-01-16T13:33:35.525Z",
        "updatedAt": "2020-01-16T13:33:35.525Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60809a"
        },
        "nombre": "Ciudad Bolívar",
        "descripcion": "Ciudad Bolívar",
        "abreviacion": "Ciudad Bolívar",
        "createdAt": "2020-01-16T13:33:35.519Z",
        "updatedAt": "2020-01-16T13:33:35.519Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080a3"
        },
        "nombre": "Caramanta",
        "descripcion": "Caramanta",
        "abreviacion": "Caramanta",
        "createdAt": "2020-01-16T13:33:35.528Z",
        "updatedAt": "2020-01-16T13:33:35.528Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60809f"
        },
        "nombre": "Caldas",
        "descripcion": "Caldas",
        "abreviacion": "Caldas",
        "createdAt": "2020-01-16T13:33:35.524Z",
        "updatedAt": "2020-01-16T13:33:35.524Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608097"
        },
        "nombre": "Bello",
        "descripcion": "Bello",
        "abreviacion": "Bello",
        "createdAt": "2020-01-16T13:33:35.516Z",
        "updatedAt": "2020-01-16T13:33:35.516Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60809c"
        },
        "nombre": "Buriticá",
        "descripcion": "Buriticá",
        "abreviacion": "Buriticá",
        "createdAt": "2020-01-16T13:33:35.521Z",
        "updatedAt": "2020-01-16T13:33:35.521Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080a5"
        },
        "nombre": "Sampués",
        "descripcion": "Sampués",
        "abreviacion": "Sampués",
        "createdAt": "2020-01-16T13:33:35.532Z",
        "updatedAt": "2020-01-16T13:33:35.532Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080a8"
        },
        "nombre": "Chigorodó",
        "descripcion": "Chigorodó",
        "abreviacion": "Chigorodó",
        "createdAt": "2020-01-16T13:33:35.536Z",
        "updatedAt": "2020-01-16T13:33:35.536Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080a4"
        },
        "nombre": "Carepa",
        "descripcion": "Carepa",
        "abreviacion": "Carepa",
        "createdAt": "2020-01-16T13:33:35.529Z",
        "updatedAt": "2020-01-16T13:33:35.529Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080a1"
        },
        "nombre": "Cañasgordas",
        "descripcion": "Cañasgordas",
        "abreviacion": "Cañasgordas",
        "createdAt": "2020-01-16T13:33:35.526Z",
        "updatedAt": "2020-01-16T13:33:35.526Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080a6"
        },
        "nombre": "Carolina",
        "descripcion": "Carolina",
        "abreviacion": "Carolina",
        "createdAt": "2020-01-16T13:33:35.533Z",
        "updatedAt": "2020-01-16T13:33:35.533Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608092"
        },
        "nombre": "Apartadó",
        "descripcion": "Apartadó",
        "abreviacion": "Apartadó",
        "createdAt": "2020-01-16T13:33:35.510Z",
        "updatedAt": "2020-01-16T13:33:35.510Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608098"
        },
        "nombre": "Betania",
        "descripcion": "Betania",
        "abreviacion": "Betania",
        "createdAt": "2020-01-16T13:33:35.517Z",
        "updatedAt": "2020-01-16T13:33:35.517Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60809d"
        },
        "nombre": "Cáceres",
        "descripcion": "Cáceres",
        "abreviacion": "Cáceres",
        "createdAt": "2020-01-16T13:33:35.522Z",
        "updatedAt": "2020-01-16T13:33:35.522Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080a2"
        },
        "nombre": "Caracolí",
        "descripcion": "Caracolí",
        "abreviacion": "Caracolí",
        "createdAt": "2020-01-16T13:33:35.527Z",
        "updatedAt": "2020-01-16T13:33:35.527Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080a7"
        },
        "nombre": "Caucasia",
        "descripcion": "Caucasia",
        "abreviacion": "Caucasia",
        "createdAt": "2020-01-16T13:33:35.534Z",
        "updatedAt": "2020-01-16T13:33:35.534Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080aa"
        },
        "nombre": "Cocorná",
        "descripcion": "Cocorná",
        "abreviacion": "Cocorná",
        "createdAt": "2020-01-16T13:33:35.538Z",
        "updatedAt": "2020-01-16T13:33:35.538Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080a9"
        },
        "nombre": "Cisneros",
        "descripcion": "Cisneros",
        "abreviacion": "Cisneros",
        "createdAt": "2020-01-16T13:33:35.537Z",
        "updatedAt": "2020-01-16T13:33:35.537Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080af"
        },
        "nombre": "Don Matías",
        "descripcion": "Don Matías",
        "abreviacion": "Don Matías",
        "createdAt": "2020-01-16T13:33:35.543Z",
        "updatedAt": "2020-01-16T13:33:35.543Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080ae"
        },
        "nombre": "Dabeiba",
        "descripcion": "Dabeiba",
        "abreviacion": "Dabeiba",
        "createdAt": "2020-01-16T13:33:35.542Z",
        "updatedAt": "2020-01-16T13:33:35.542Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080b4"
        },
        "nombre": "Fredonia",
        "descripcion": "Fredonia",
        "abreviacion": "Fredonia",
        "createdAt": "2020-01-16T13:33:35.548Z",
        "updatedAt": "2020-01-16T13:33:35.548Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080b3"
        },
        "nombre": "Envigado",
        "descripcion": "Envigado",
        "abreviacion": "Envigado",
        "createdAt": "2020-01-16T13:33:35.547Z",
        "updatedAt": "2020-01-16T13:33:35.547Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080b9"
        },
        "nombre": "Guadalupe",
        "descripcion": "Guadalupe",
        "abreviacion": "Guadalupe",
        "createdAt": "2020-01-16T13:33:35.552Z",
        "updatedAt": "2020-01-16T13:33:35.552Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080ad"
        },
        "nombre": "Copacabana",
        "descripcion": "Copacabana",
        "abreviacion": "Copacabana",
        "createdAt": "2020-01-16T13:33:35.541Z",
        "updatedAt": "2020-01-16T13:33:35.541Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080b8"
        },
        "nombre": "Nunchía",
        "descripcion": "Nunchía",
        "abreviacion": "Nunchía",
        "createdAt": "2020-01-16T13:33:35.552Z",
        "updatedAt": "2020-01-16T13:33:35.552Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080b2"
        },
        "nombre": "Entrerrios",
        "descripcion": "Entrerrios",
        "abreviacion": "Entrerrios",
        "createdAt": "2020-01-16T13:33:35.546Z",
        "updatedAt": "2020-01-16T13:33:35.546Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080bd"
        },
        "nombre": "Hispania",
        "descripcion": "Hispania",
        "abreviacion": "Hispania",
        "createdAt": "2020-01-16T13:33:35.555Z",
        "updatedAt": "2020-01-16T13:33:35.555Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080b7"
        },
        "nombre": "Gómez Plata",
        "descripcion": "Gómez Plata",
        "abreviacion": "Gómez Plata",
        "createdAt": "2020-01-16T13:33:35.551Z",
        "updatedAt": "2020-01-16T13:33:35.551Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080be"
        },
        "nombre": "Itagui",
        "descripcion": "Itagui",
        "abreviacion": "Itagui",
        "createdAt": "2020-01-16T13:33:35.556Z",
        "updatedAt": "2020-01-16T13:33:35.556Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080bc"
        },
        "nombre": "Heliconia",
        "descripcion": "Heliconia",
        "abreviacion": "Heliconia",
        "createdAt": "2020-01-16T13:33:35.555Z",
        "updatedAt": "2020-01-16T13:33:35.555Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080c2"
        },
        "nombre": "La Ceja",
        "descripcion": "La Ceja",
        "abreviacion": "La Ceja",
        "createdAt": "2020-01-16T13:33:35.559Z",
        "updatedAt": "2020-01-16T13:33:35.559Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080c1"
        },
        "nombre": "Jericó",
        "descripcion": "Jericó",
        "abreviacion": "Jericó",
        "createdAt": "2020-01-16T13:33:35.558Z",
        "updatedAt": "2020-01-16T13:33:35.558Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080c7"
        },
        "nombre": "Maceo",
        "descripcion": "Maceo",
        "abreviacion": "Maceo",
        "createdAt": "2020-01-16T13:33:35.563Z",
        "updatedAt": "2020-01-16T13:33:35.563Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080c6"
        },
        "nombre": "Liborina",
        "descripcion": "Liborina",
        "abreviacion": "Liborina",
        "createdAt": "2020-01-16T13:33:35.562Z",
        "updatedAt": "2020-01-16T13:33:35.562Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080c3"
        },
        "nombre": "La Estrella",
        "descripcion": "La Estrella",
        "abreviacion": "La Estrella",
        "createdAt": "2020-01-16T13:33:35.560Z",
        "updatedAt": "2020-01-16T13:33:35.560Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080cb"
        },
        "nombre": "Mutatá",
        "descripcion": "Mutatá",
        "abreviacion": "Mutatá",
        "createdAt": "2020-01-16T13:33:35.567Z",
        "updatedAt": "2020-01-16T13:33:35.567Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080cc"
        },
        "nombre": "Nariño",
        "descripcion": "Nariño",
        "abreviacion": "Nariño",
        "createdAt": "2020-01-16T13:33:35.568Z",
        "updatedAt": "2020-01-16T13:33:35.568Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080c8"
        },
        "nombre": "Marinilla",
        "descripcion": "Marinilla",
        "abreviacion": "Marinilla",
        "createdAt": "2020-01-16T13:33:35.564Z",
        "updatedAt": "2020-01-16T13:33:35.564Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080d0"
        },
        "nombre": "Peñol",
        "descripcion": "Peñol",
        "abreviacion": "Peñol",
        "createdAt": "2020-01-16T13:33:35.572Z",
        "updatedAt": "2020-01-16T13:33:35.572Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080d1"
        },
        "nombre": "Peque",
        "descripcion": "Peque",
        "abreviacion": "Peque",
        "createdAt": "2020-01-16T13:33:35.572Z",
        "updatedAt": "2020-01-16T13:33:35.572Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080cd"
        },
        "nombre": "Necoclí",
        "descripcion": "Necoclí",
        "abreviacion": "Necoclí",
        "createdAt": "2020-01-16T13:33:35.569Z",
        "updatedAt": "2020-01-16T13:33:35.569Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080d5"
        },
        "nombre": "Puerto Triunfo",
        "descripcion": "Puerto Triunfo",
        "abreviacion": "Puerto Triunfo",
        "createdAt": "2020-01-16T13:33:35.576Z",
        "updatedAt": "2020-01-16T13:33:35.576Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080d6"
        },
        "nombre": "Remedios",
        "descripcion": "Remedios",
        "abreviacion": "Remedios",
        "createdAt": "2020-01-16T13:33:35.577Z",
        "updatedAt": "2020-01-16T13:33:35.577Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080d2"
        },
        "nombre": "Pueblorrico",
        "descripcion": "Pueblorrico",
        "abreviacion": "Pueblorrico",
        "createdAt": "2020-01-16T13:33:35.573Z",
        "updatedAt": "2020-01-16T13:33:35.573Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080da"
        },
        "nombre": "Sabaneta",
        "descripcion": "Sabaneta",
        "abreviacion": "Sabaneta",
        "createdAt": "2020-01-16T13:33:35.581Z",
        "updatedAt": "2020-01-16T13:33:35.581Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080db"
        },
        "nombre": "Salgar",
        "descripcion": "Salgar",
        "abreviacion": "Salgar",
        "createdAt": "2020-01-16T13:33:35.582Z",
        "updatedAt": "2020-01-16T13:33:35.582Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080d7"
        },
        "nombre": "Retiro",
        "descripcion": "Retiro",
        "abreviacion": "Retiro",
        "createdAt": "2020-01-16T13:33:35.578Z",
        "updatedAt": "2020-01-16T13:33:35.578Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080df"
        },
        "nombre": "San Jerónimo",
        "descripcion": "San Jerónimo",
        "abreviacion": "San Jerónimo",
        "createdAt": "2020-01-16T13:33:35.585Z",
        "updatedAt": "2020-01-16T13:33:35.585Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080e0"
        },
        "nombre": "Montelíbano",
        "descripcion": "Montelíbano",
        "abreviacion": "Montelíbano",
        "createdAt": "2020-01-16T13:33:35.586Z",
        "updatedAt": "2020-01-16T13:33:35.586Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080e4"
        },
        "nombre": "Corozal",
        "descripcion": "Corozal",
        "abreviacion": "Corozal",
        "createdAt": "2020-01-16T13:33:35.589Z",
        "updatedAt": "2020-01-16T13:33:35.589Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080dc"
        },
        "nombre": "Albán",
        "descripcion": "Albán",
        "abreviacion": "Albán",
        "createdAt": "2020-01-16T13:33:35.583Z",
        "updatedAt": "2020-01-16T13:33:35.583Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080e5"
        },
        "nombre": "San Rafael",
        "descripcion": "San Rafael",
        "abreviacion": "San Rafael",
        "createdAt": "2020-01-16T13:33:35.590Z",
        "updatedAt": "2020-01-16T13:33:35.590Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080e9"
        },
        "nombre": "Buesaco",
        "descripcion": "Buesaco",
        "abreviacion": "Buesaco",
        "createdAt": "2020-01-16T13:33:35.593Z",
        "updatedAt": "2020-01-16T13:33:35.593Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080e1"
        },
        "nombre": "Puerto Asís",
        "descripcion": "Puerto Asís",
        "abreviacion": "Puerto Asís",
        "createdAt": "2020-01-16T13:33:35.587Z",
        "updatedAt": "2020-01-16T13:33:35.587Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080ee"
        },
        "nombre": "Támesis",
        "descripcion": "Támesis",
        "abreviacion": "Támesis",
        "createdAt": "2020-01-16T13:33:35.598Z",
        "updatedAt": "2020-01-16T13:33:35.598Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080e6"
        },
        "nombre": "San Roque",
        "descripcion": "San Roque",
        "abreviacion": "San Roque",
        "createdAt": "2020-01-16T13:33:35.591Z",
        "updatedAt": "2020-01-16T13:33:35.591Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080ea"
        },
        "nombre": "Santo Domingo",
        "descripcion": "Santo Domingo",
        "abreviacion": "Santo Domingo",
        "createdAt": "2020-01-16T13:33:35.594Z",
        "updatedAt": "2020-01-16T13:33:35.594Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080f3"
        },
        "nombre": "Turbo",
        "descripcion": "Turbo",
        "abreviacion": "Turbo",
        "createdAt": "2020-01-16T13:33:35.602Z",
        "updatedAt": "2020-01-16T13:33:35.602Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080eb"
        },
        "nombre": "El Santuario",
        "descripcion": "El Santuario",
        "abreviacion": "El Santuario",
        "createdAt": "2020-01-16T13:33:35.595Z",
        "updatedAt": "2020-01-16T13:33:35.595Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080ef"
        },
        "nombre": "Tarazá",
        "descripcion": "Tarazá",
        "abreviacion": "Tarazá",
        "createdAt": "2020-01-16T13:33:35.598Z",
        "updatedAt": "2020-01-16T13:33:35.598Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080f8"
        },
        "nombre": "Vegachí",
        "descripcion": "Vegachí",
        "abreviacion": "Vegachí",
        "createdAt": "2020-01-16T13:33:35.607Z",
        "updatedAt": "2020-01-16T13:33:35.607Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080f0"
        },
        "nombre": "Tarso",
        "descripcion": "Tarso",
        "abreviacion": "Tarso",
        "createdAt": "2020-01-16T13:33:35.599Z",
        "updatedAt": "2020-01-16T13:33:35.599Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080fd"
        },
        "nombre": "Yolombó",
        "descripcion": "Yolombó",
        "abreviacion": "Yolombó",
        "createdAt": "2020-01-16T13:33:35.614Z",
        "updatedAt": "2020-01-16T13:33:35.614Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080f4"
        },
        "nombre": "Uramita",
        "descripcion": "Uramita",
        "abreviacion": "Uramita",
        "createdAt": "2020-01-16T13:33:35.603Z",
        "updatedAt": "2020-01-16T13:33:35.603Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080f5"
        },
        "nombre": "Urrao",
        "descripcion": "Urrao",
        "abreviacion": "Urrao",
        "createdAt": "2020-01-16T13:33:35.604Z",
        "updatedAt": "2020-01-16T13:33:35.604Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080ac"
        },
        "nombre": "Concordia",
        "descripcion": "Concordia",
        "abreviacion": "Concordia",
        "createdAt": "2020-01-16T13:33:35.540Z",
        "updatedAt": "2020-01-16T13:33:35.540Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080b1"
        },
        "nombre": "El Bagre",
        "descripcion": "El Bagre",
        "abreviacion": "El Bagre",
        "createdAt": "2020-01-16T13:33:35.545Z",
        "updatedAt": "2020-01-16T13:33:35.545Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080b6"
        },
        "nombre": "Girardota",
        "descripcion": "Girardota",
        "abreviacion": "Girardota",
        "createdAt": "2020-01-16T13:33:35.550Z",
        "updatedAt": "2020-01-16T13:33:35.550Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608102"
        },
        "nombre": "El Peñón",
        "descripcion": "El Peñón",
        "abreviacion": "El Peñón",
        "createdAt": "2020-01-16T13:33:35.617Z",
        "updatedAt": "2020-01-16T13:33:35.617Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080bb"
        },
        "nombre": "Guatapé",
        "descripcion": "Guatapé",
        "abreviacion": "Guatapé",
        "createdAt": "2020-01-16T13:33:35.554Z",
        "updatedAt": "2020-01-16T13:33:35.554Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080f9"
        },
        "nombre": "Venecia",
        "descripcion": "Venecia",
        "abreviacion": "Venecia",
        "createdAt": "2020-01-16T13:33:35.608Z",
        "updatedAt": "2020-01-16T13:33:35.608Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080c0"
        },
        "nombre": "Pamplona",
        "descripcion": "Pamplona",
        "abreviacion": "Pamplona",
        "createdAt": "2020-01-16T13:33:35.558Z",
        "updatedAt": "2020-01-16T13:33:35.558Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608107"
        },
        "nombre": "Luruaco",
        "descripcion": "Luruaco",
        "abreviacion": "Luruaco",
        "createdAt": "2020-01-16T13:33:35.620Z",
        "updatedAt": "2020-01-16T13:33:35.620Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080c5"
        },
        "nombre": "La Unión",
        "descripcion": "La Unión",
        "abreviacion": "La Unión",
        "createdAt": "2020-01-16T13:33:35.561Z",
        "updatedAt": "2020-01-16T13:33:35.561Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080ca"
        },
        "nombre": "Murindó",
        "descripcion": "Murindó",
        "abreviacion": "Murindó",
        "createdAt": "2020-01-16T13:33:35.566Z",
        "updatedAt": "2020-01-16T13:33:35.566Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080fa"
        },
        "nombre": "Maní",
        "descripcion": "Maní",
        "abreviacion": "Maní",
        "createdAt": "2020-01-16T13:33:35.609Z",
        "updatedAt": "2020-01-16T13:33:35.609Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080cf"
        },
        "nombre": "Olaya",
        "descripcion": "Olaya",
        "abreviacion": "Olaya",
        "createdAt": "2020-01-16T13:33:35.571Z",
        "updatedAt": "2020-01-16T13:33:35.571Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080fe"
        },
        "nombre": "Yondó",
        "descripcion": "Yondó",
        "abreviacion": "Yondó",
        "createdAt": "2020-01-16T13:33:35.614Z",
        "updatedAt": "2020-01-16T13:33:35.614Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080d4"
        },
        "nombre": "Puerto Nare",
        "descripcion": "Puerto Nare",
        "abreviacion": "Puerto Nare",
        "createdAt": "2020-01-16T13:33:35.575Z",
        "updatedAt": "2020-01-16T13:33:35.575Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608103"
        },
        "nombre": "Candelaria",
        "descripcion": "Candelaria",
        "abreviacion": "Candelaria",
        "createdAt": "2020-01-16T13:33:35.618Z",
        "updatedAt": "2020-01-16T13:33:35.618Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080d9"
        },
        "nombre": "Sabanalarga",
        "descripcion": "Sabanalarga",
        "abreviacion": "Sabanalarga",
        "createdAt": "2020-01-16T13:33:35.580Z",
        "updatedAt": "2020-01-16T13:33:35.580Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60810c"
        },
        "nombre": "Polonuevo",
        "descripcion": "Polonuevo",
        "abreviacion": "Polonuevo",
        "createdAt": "2020-01-16T13:33:35.624Z",
        "updatedAt": "2020-01-16T13:33:35.624Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080de"
        },
        "nombre": "San Francisco",
        "descripcion": "San Francisco",
        "abreviacion": "San Francisco",
        "createdAt": "2020-01-16T13:33:35.584Z",
        "updatedAt": "2020-01-16T13:33:35.584Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080ff"
        },
        "nombre": "Zaragoza",
        "descripcion": "Zaragoza",
        "abreviacion": "Zaragoza",
        "createdAt": "2020-01-16T13:33:35.615Z",
        "updatedAt": "2020-01-16T13:33:35.615Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080e3"
        },
        "nombre": "San Pedro",
        "descripcion": "San Pedro",
        "abreviacion": "San Pedro",
        "createdAt": "2020-01-16T13:33:35.589Z",
        "updatedAt": "2020-01-16T13:33:35.589Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608108"
        },
        "nombre": "Malambo",
        "descripcion": "Malambo",
        "abreviacion": "Malambo",
        "createdAt": "2020-01-16T13:33:35.621Z",
        "updatedAt": "2020-01-16T13:33:35.621Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080e8"
        },
        "nombre": "Santa Bárbara",
        "descripcion": "Santa Bárbara",
        "abreviacion": "Santa Bárbara",
        "createdAt": "2020-01-16T13:33:35.592Z",
        "updatedAt": "2020-01-16T13:33:35.592Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608104"
        },
        "nombre": "Galapa",
        "descripcion": "Galapa",
        "abreviacion": "Galapa",
        "createdAt": "2020-01-16T13:33:35.618Z",
        "updatedAt": "2020-01-16T13:33:35.618Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080ed"
        },
        "nombre": "Sopetrán",
        "descripcion": "Sopetrán",
        "abreviacion": "Sopetrán",
        "createdAt": "2020-01-16T13:33:35.597Z",
        "updatedAt": "2020-01-16T13:33:35.597Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608111"
        },
        "nombre": "Santa Lucía",
        "descripcion": "Santa Lucía",
        "abreviacion": "Santa Lucía",
        "createdAt": "2020-01-16T13:33:35.627Z",
        "updatedAt": "2020-01-16T13:33:35.627Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080f2"
        },
        "nombre": "Toledo",
        "descripcion": "Toledo",
        "abreviacion": "Toledo",
        "createdAt": "2020-01-16T13:33:35.600Z",
        "updatedAt": "2020-01-16T13:33:35.600Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60810d"
        },
        "nombre": "Chía",
        "descripcion": "Chía",
        "abreviacion": "Chía",
        "createdAt": "2020-01-16T13:33:35.624Z",
        "updatedAt": "2020-01-16T13:33:35.624Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080f7"
        },
        "nombre": "Valparaíso",
        "descripcion": "Valparaíso",
        "abreviacion": "Valparaíso",
        "createdAt": "2020-01-16T13:33:35.605Z",
        "updatedAt": "2020-01-16T13:33:35.605Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608109"
        },
        "nombre": "Manatí",
        "descripcion": "Manatí",
        "abreviacion": "Manatí",
        "createdAt": "2020-01-16T13:33:35.622Z",
        "updatedAt": "2020-01-16T13:33:35.622Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080fc"
        },
        "nombre": "Yarumal",
        "descripcion": "Yarumal",
        "abreviacion": "Yarumal",
        "createdAt": "2020-01-16T13:33:35.613Z",
        "updatedAt": "2020-01-16T13:33:35.613Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608116"
        },
        "nombre": "Usiacurí",
        "descripcion": "Usiacurí",
        "abreviacion": "Usiacurí",
        "createdAt": "2020-01-16T13:33:35.631Z",
        "updatedAt": "2020-01-16T13:33:35.631Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608101"
        },
        "nombre": "Baranoa",
        "descripcion": "Baranoa",
        "abreviacion": "Baranoa",
        "createdAt": "2020-01-16T13:33:35.616Z",
        "updatedAt": "2020-01-16T13:33:35.616Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608112"
        },
        "nombre": "Santo Tomás",
        "descripcion": "Santo Tomás",
        "abreviacion": "Santo Tomás",
        "createdAt": "2020-01-16T13:33:35.628Z",
        "updatedAt": "2020-01-16T13:33:35.628Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608106"
        },
        "nombre": "Casabianca",
        "descripcion": "Casabianca",
        "abreviacion": "Casabianca",
        "createdAt": "2020-01-16T13:33:35.620Z",
        "updatedAt": "2020-01-16T13:33:35.620Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60811b"
        },
        "nombre": "Arenal",
        "descripcion": "Arenal",
        "abreviacion": "Arenal",
        "createdAt": "2020-01-16T13:33:35.636Z",
        "updatedAt": "2020-01-16T13:33:35.636Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60810b"
        },
        "nombre": "Piojó",
        "descripcion": "Piojó",
        "abreviacion": "Piojó",
        "createdAt": "2020-01-16T13:33:35.623Z",
        "updatedAt": "2020-01-16T13:33:35.623Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60810e"
        },
        "nombre": "San Andrés de Tumaco",
        "descripcion": "San Andrés de Tumaco",
        "abreviacion": "San Andrés de Tumaco",
        "createdAt": "2020-01-16T13:33:35.625Z",
        "updatedAt": "2020-01-16T13:33:35.625Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608117"
        },
        "nombre": "Milán",
        "descripcion": "Milán",
        "abreviacion": "Milán",
        "createdAt": "2020-01-16T13:33:35.632Z",
        "updatedAt": "2020-01-16T13:33:35.632Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608120"
        },
        "nombre": "Cantagallo",
        "descripcion": "Cantagallo",
        "abreviacion": "Cantagallo",
        "createdAt": "2020-01-16T13:33:35.640Z",
        "updatedAt": "2020-01-16T13:33:35.640Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608110"
        },
        "nombre": "Sabanalarga",
        "descripcion": "Sabanalarga",
        "abreviacion": "Sabanalarga",
        "createdAt": "2020-01-16T13:33:35.626Z",
        "updatedAt": "2020-01-16T13:33:35.626Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608113"
        },
        "nombre": "Soledad",
        "descripcion": "Soledad",
        "abreviacion": "Soledad",
        "createdAt": "2020-01-16T13:33:35.628Z",
        "updatedAt": "2020-01-16T13:33:35.628Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60811c"
        },
        "nombre": "Arjona",
        "descripcion": "Arjona",
        "abreviacion": "Arjona",
        "createdAt": "2020-01-16T13:33:35.636Z",
        "updatedAt": "2020-01-16T13:33:35.636Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608125"
        },
        "nombre": "El Guamo",
        "descripcion": "El Guamo",
        "abreviacion": "El Guamo",
        "createdAt": "2020-01-16T13:33:35.645Z",
        "updatedAt": "2020-01-16T13:33:35.645Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608115"
        },
        "nombre": "Tubará",
        "descripcion": "Tubará",
        "abreviacion": "Tubará",
        "createdAt": "2020-01-16T13:33:35.631Z",
        "updatedAt": "2020-01-16T13:33:35.631Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608121"
        },
        "nombre": "Cicuco",
        "descripcion": "Cicuco",
        "abreviacion": "Cicuco",
        "createdAt": "2020-01-16T13:33:35.641Z",
        "updatedAt": "2020-01-16T13:33:35.641Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60812a"
        },
        "nombre": "Montecristo",
        "descripcion": "Montecristo",
        "abreviacion": "Montecristo",
        "createdAt": "2020-01-16T13:33:35.648Z",
        "updatedAt": "2020-01-16T13:33:35.648Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60811a"
        },
        "nombre": "Anzoátegui",
        "descripcion": "Anzoátegui",
        "abreviacion": "Anzoátegui",
        "createdAt": "2020-01-16T13:33:35.635Z",
        "updatedAt": "2020-01-16T13:33:35.635Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608118"
        },
        "nombre": "Capitanejo",
        "descripcion": "Capitanejo",
        "abreviacion": "Capitanejo",
        "createdAt": "2020-01-16T13:33:35.633Z",
        "updatedAt": "2020-01-16T13:33:35.633Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608126"
        },
        "nombre": "Frontino",
        "descripcion": "Frontino",
        "abreviacion": "Frontino",
        "createdAt": "2020-01-16T13:33:35.646Z",
        "updatedAt": "2020-01-16T13:33:35.646Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60812f"
        },
        "nombre": "Regidor",
        "descripcion": "Regidor",
        "abreviacion": "Regidor",
        "createdAt": "2020-01-16T13:33:35.651Z",
        "updatedAt": "2020-01-16T13:33:35.651Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60811f"
        },
        "nombre": "Calamar",
        "descripcion": "Calamar",
        "abreviacion": "Calamar",
        "createdAt": "2020-01-16T13:33:35.639Z",
        "updatedAt": "2020-01-16T13:33:35.639Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60811d"
        },
        "nombre": "Arroyohondo",
        "descripcion": "Arroyohondo",
        "abreviacion": "Arroyohondo",
        "createdAt": "2020-01-16T13:33:35.637Z",
        "updatedAt": "2020-01-16T13:33:35.637Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60812b"
        },
        "nombre": "Mompós",
        "descripcion": "Mompós",
        "abreviacion": "Mompós",
        "createdAt": "2020-01-16T13:33:35.649Z",
        "updatedAt": "2020-01-16T13:33:35.649Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608134"
        },
        "nombre": "Pamplonita",
        "descripcion": "Pamplonita",
        "abreviacion": "Pamplonita",
        "createdAt": "2020-01-16T13:33:35.654Z",
        "updatedAt": "2020-01-16T13:33:35.654Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608124"
        },
        "nombre": "Repelón",
        "descripcion": "Repelón",
        "abreviacion": "Repelón",
        "createdAt": "2020-01-16T13:33:35.644Z",
        "updatedAt": "2020-01-16T13:33:35.644Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608122"
        },
        "nombre": "Córdoba",
        "descripcion": "Córdoba",
        "abreviacion": "Córdoba",
        "createdAt": "2020-01-16T13:33:35.641Z",
        "updatedAt": "2020-01-16T13:33:35.641Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608130"
        },
        "nombre": "Río Viejo",
        "descripcion": "Río Viejo",
        "abreviacion": "Río Viejo",
        "createdAt": "2020-01-16T13:33:35.652Z",
        "updatedAt": "2020-01-16T13:33:35.652Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608139"
        },
        "nombre": "Santa Rosa",
        "descripcion": "Santa Rosa",
        "abreviacion": "Santa Rosa",
        "createdAt": "2020-01-16T13:33:35.658Z",
        "updatedAt": "2020-01-16T13:33:35.658Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608127"
        },
        "nombre": "Magangué",
        "descripcion": "Magangué",
        "abreviacion": "Magangué",
        "createdAt": "2020-01-16T13:33:35.646Z",
        "updatedAt": "2020-01-16T13:33:35.646Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608135"
        },
        "nombre": "San Juan Nepomuceno",
        "descripcion": "San Juan Nepomuceno",
        "abreviacion": "San Juan Nepomuceno",
        "createdAt": "2020-01-16T13:33:35.655Z",
        "updatedAt": "2020-01-16T13:33:35.655Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608129"
        },
        "nombre": "Margarita",
        "descripcion": "Margarita",
        "abreviacion": "Margarita",
        "createdAt": "2020-01-16T13:33:35.647Z",
        "updatedAt": "2020-01-16T13:33:35.647Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60813e"
        },
        "nombre": "Tiquisio",
        "descripcion": "Tiquisio",
        "abreviacion": "Tiquisio",
        "createdAt": "2020-01-16T13:33:35.663Z",
        "updatedAt": "2020-01-16T13:33:35.663Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60812c"
        },
        "nombre": "Morales",
        "descripcion": "Morales",
        "abreviacion": "Morales",
        "createdAt": "2020-01-16T13:33:35.649Z",
        "updatedAt": "2020-01-16T13:33:35.649Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60813a"
        },
        "nombre": "Tibasosa",
        "descripcion": "Tibasosa",
        "abreviacion": "Tibasosa",
        "createdAt": "2020-01-16T13:33:35.658Z",
        "updatedAt": "2020-01-16T13:33:35.658Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60812e"
        },
        "nombre": "Pinillos",
        "descripcion": "Pinillos",
        "abreviacion": "Pinillos",
        "createdAt": "2020-01-16T13:33:35.650Z",
        "updatedAt": "2020-01-16T13:33:35.650Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080ab"
        },
        "nombre": "Concepción",
        "descripcion": "Concepción",
        "abreviacion": "Concepción",
        "createdAt": "2020-01-16T13:33:35.539Z",
        "updatedAt": "2020-01-16T13:33:35.539Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608143"
        },
        "nombre": "Tunja",
        "descripcion": "Tunja",
        "abreviacion": "Tunja",
        "createdAt": "2020-01-16T13:33:35.666Z",
        "updatedAt": "2020-01-16T13:33:35.666Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608131"
        },
        "nombre": "San Estanislao",
        "descripcion": "San Estanislao",
        "abreviacion": "San Estanislao",
        "createdAt": "2020-01-16T13:33:35.652Z",
        "updatedAt": "2020-01-16T13:33:35.652Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60813f"
        },
        "nombre": "Turbaco",
        "descripcion": "Turbaco",
        "abreviacion": "Turbaco",
        "createdAt": "2020-01-16T13:33:35.663Z",
        "updatedAt": "2020-01-16T13:33:35.663Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608133"
        },
        "nombre": "El Peñón",
        "descripcion": "El Peñón",
        "abreviacion": "El Peñón",
        "createdAt": "2020-01-16T13:33:35.654Z",
        "updatedAt": "2020-01-16T13:33:35.654Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608136"
        },
        "nombre": "Miriti Paraná",
        "descripcion": "Miriti Paraná",
        "abreviacion": "Miriti Paraná",
        "createdAt": "2020-01-16T13:33:35.656Z",
        "updatedAt": "2020-01-16T13:33:35.656Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080b0"
        },
        "nombre": "Ebéjico",
        "descripcion": "Ebéjico",
        "abreviacion": "Ebéjico",
        "createdAt": "2020-01-16T13:33:35.544Z",
        "updatedAt": "2020-01-16T13:33:35.544Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608144"
        },
        "nombre": "Almeida",
        "descripcion": "Almeida",
        "abreviacion": "Almeida",
        "createdAt": "2020-01-16T13:33:35.667Z",
        "updatedAt": "2020-01-16T13:33:35.667Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608138"
        },
        "nombre": "Santa Catalina",
        "descripcion": "Santa Catalina",
        "abreviacion": "Santa Catalina",
        "createdAt": "2020-01-16T13:33:35.657Z",
        "updatedAt": "2020-01-16T13:33:35.657Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608148"
        },
        "nombre": "Berbeo",
        "descripcion": "Berbeo",
        "abreviacion": "Berbeo",
        "createdAt": "2020-01-16T13:33:35.672Z",
        "updatedAt": "2020-01-16T13:33:35.672Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080b5"
        },
        "nombre": "Giraldo",
        "descripcion": "Giraldo",
        "abreviacion": "Giraldo",
        "createdAt": "2020-01-16T13:33:35.549Z",
        "updatedAt": "2020-01-16T13:33:35.549Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60813b"
        },
        "nombre": "Simití",
        "descripcion": "Simití",
        "abreviacion": "Simití",
        "createdAt": "2020-01-16T13:33:35.660Z",
        "updatedAt": "2020-01-16T13:33:35.660Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608149"
        },
        "nombre": "Betéitiva",
        "descripcion": "Betéitiva",
        "abreviacion": "Betéitiva",
        "createdAt": "2020-01-16T13:33:35.673Z",
        "updatedAt": "2020-01-16T13:33:35.673Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60813d"
        },
        "nombre": "Talaigua Nuevo",
        "descripcion": "Talaigua Nuevo",
        "abreviacion": "Talaigua Nuevo",
        "createdAt": "2020-01-16T13:33:35.662Z",
        "updatedAt": "2020-01-16T13:33:35.662Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60814d"
        },
        "nombre": "Buena Vista",
        "descripcion": "Buena Vista",
        "abreviacion": "Buena Vista",
        "createdAt": "2020-01-16T13:33:35.675Z",
        "updatedAt": "2020-01-16T13:33:35.675Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080ba"
        },
        "nombre": "Guarne",
        "descripcion": "Guarne",
        "abreviacion": "Guarne",
        "createdAt": "2020-01-16T13:33:35.553Z",
        "updatedAt": "2020-01-16T13:33:35.553Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60814e"
        },
        "nombre": "Busbanzá",
        "descripcion": "Busbanzá",
        "abreviacion": "Busbanzá",
        "createdAt": "2020-01-16T13:33:35.676Z",
        "updatedAt": "2020-01-16T13:33:35.676Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608142"
        },
        "nombre": "Páez",
        "descripcion": "Páez",
        "abreviacion": "Páez",
        "createdAt": "2020-01-16T13:33:35.666Z",
        "updatedAt": "2020-01-16T13:33:35.666Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608140"
        },
        "nombre": "Turbaná",
        "descripcion": "Turbaná",
        "abreviacion": "Turbaná",
        "createdAt": "2020-01-16T13:33:35.664Z",
        "updatedAt": "2020-01-16T13:33:35.664Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608152"
        },
        "nombre": "Chinavita",
        "descripcion": "Chinavita",
        "abreviacion": "Chinavita",
        "createdAt": "2020-01-16T13:33:35.678Z",
        "updatedAt": "2020-01-16T13:33:35.678Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608147"
        },
        "nombre": "Ibagué",
        "descripcion": "Ibagué",
        "abreviacion": "Ibagué",
        "createdAt": "2020-01-16T13:33:35.671Z",
        "updatedAt": "2020-01-16T13:33:35.671Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608145"
        },
        "nombre": "Aquitania",
        "descripcion": "Aquitania",
        "abreviacion": "Aquitania",
        "createdAt": "2020-01-16T13:33:35.670Z",
        "updatedAt": "2020-01-16T13:33:35.670Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608157"
        },
        "nombre": "Chivatá",
        "descripcion": "Chivatá",
        "abreviacion": "Chivatá",
        "createdAt": "2020-01-16T13:33:35.682Z",
        "updatedAt": "2020-01-16T13:33:35.682Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608153"
        },
        "nombre": "Chiquinquirá",
        "descripcion": "Chiquinquirá",
        "abreviacion": "Chiquinquirá",
        "createdAt": "2020-01-16T13:33:35.679Z",
        "updatedAt": "2020-01-16T13:33:35.679Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080bf"
        },
        "nombre": "Ituango",
        "descripcion": "Ituango",
        "abreviacion": "Ituango",
        "createdAt": "2020-01-16T13:33:35.557Z",
        "updatedAt": "2020-01-16T13:33:35.557Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60814a"
        },
        "nombre": "Boavita",
        "descripcion": "Boavita",
        "abreviacion": "Boavita",
        "createdAt": "2020-01-16T13:33:35.673Z",
        "updatedAt": "2020-01-16T13:33:35.673Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60814c"
        },
        "nombre": "Briceño",
        "descripcion": "Briceño",
        "abreviacion": "Briceño",
        "createdAt": "2020-01-16T13:33:35.674Z",
        "updatedAt": "2020-01-16T13:33:35.674Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60815c"
        },
        "nombre": "Cubará",
        "descripcion": "Cubará",
        "abreviacion": "Cubará",
        "createdAt": "2020-01-16T13:33:35.687Z",
        "updatedAt": "2020-01-16T13:33:35.687Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608158"
        },
        "nombre": "Cómbita",
        "descripcion": "Cómbita",
        "abreviacion": "Cómbita",
        "createdAt": "2020-01-16T13:33:35.683Z",
        "updatedAt": "2020-01-16T13:33:35.683Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080c4"
        },
        "nombre": "La Pintada",
        "descripcion": "La Pintada",
        "abreviacion": "La Pintada",
        "createdAt": "2020-01-16T13:33:35.561Z",
        "updatedAt": "2020-01-16T13:33:35.561Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608151"
        },
        "nombre": "Cerinza",
        "descripcion": "Cerinza",
        "abreviacion": "Cerinza",
        "createdAt": "2020-01-16T13:33:35.677Z",
        "updatedAt": "2020-01-16T13:33:35.677Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608161"
        },
        "nombre": "Duitama",
        "descripcion": "Duitama",
        "abreviacion": "Duitama",
        "createdAt": "2020-01-16T13:33:35.690Z",
        "updatedAt": "2020-01-16T13:33:35.690Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60814f"
        },
        "nombre": "Caldas",
        "descripcion": "Caldas",
        "abreviacion": "Caldas",
        "createdAt": "2020-01-16T13:33:35.676Z",
        "updatedAt": "2020-01-16T13:33:35.676Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60815d"
        },
        "nombre": "Cucaita",
        "descripcion": "Cucaita",
        "abreviacion": "Cucaita",
        "createdAt": "2020-01-16T13:33:35.687Z",
        "updatedAt": "2020-01-16T13:33:35.687Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080c9"
        },
        "nombre": "Montebello",
        "descripcion": "Montebello",
        "abreviacion": "Montebello",
        "createdAt": "2020-01-16T13:33:35.565Z",
        "updatedAt": "2020-01-16T13:33:35.565Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608166"
        },
        "nombre": "Gachantivá",
        "descripcion": "Gachantivá",
        "abreviacion": "Gachantivá",
        "createdAt": "2020-01-16T13:33:35.694Z",
        "updatedAt": "2020-01-16T13:33:35.694Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608154"
        },
        "nombre": "Chiscas",
        "descripcion": "Chiscas",
        "abreviacion": "Chiscas",
        "createdAt": "2020-01-16T13:33:35.680Z",
        "updatedAt": "2020-01-16T13:33:35.680Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608162"
        },
        "nombre": "El Cocuy",
        "descripcion": "El Cocuy",
        "abreviacion": "El Cocuy",
        "createdAt": "2020-01-16T13:33:35.691Z",
        "updatedAt": "2020-01-16T13:33:35.691Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608156"
        },
        "nombre": "Chitaraque",
        "descripcion": "Chitaraque",
        "abreviacion": "Chitaraque",
        "createdAt": "2020-01-16T13:33:35.681Z",
        "updatedAt": "2020-01-16T13:33:35.681Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080ce"
        },
        "nombre": "Nechí",
        "descripcion": "Nechí",
        "abreviacion": "Nechí",
        "createdAt": "2020-01-16T13:33:35.570Z",
        "updatedAt": "2020-01-16T13:33:35.570Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608167"
        },
        "nombre": "Gameza",
        "descripcion": "Gameza",
        "abreviacion": "Gameza",
        "createdAt": "2020-01-16T13:33:35.694Z",
        "updatedAt": "2020-01-16T13:33:35.694Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608159"
        },
        "nombre": "Coper",
        "descripcion": "Coper",
        "abreviacion": "Coper",
        "createdAt": "2020-01-16T13:33:35.685Z",
        "updatedAt": "2020-01-16T13:33:35.685Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60816b"
        },
        "nombre": "Guayatá",
        "descripcion": "Guayatá",
        "abreviacion": "Guayatá",
        "createdAt": "2020-01-16T13:33:35.700Z",
        "updatedAt": "2020-01-16T13:33:35.700Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60815b"
        },
        "nombre": "Covarachía",
        "descripcion": "Covarachía",
        "abreviacion": "Covarachía",
        "createdAt": "2020-01-16T13:33:35.686Z",
        "updatedAt": "2020-01-16T13:33:35.686Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60815e"
        },
        "nombre": "Cuítiva",
        "descripcion": "Cuítiva",
        "abreviacion": "Cuítiva",
        "createdAt": "2020-01-16T13:33:35.688Z",
        "updatedAt": "2020-01-16T13:33:35.688Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608170"
        },
        "nombre": "Labranzagrande",
        "descripcion": "Labranzagrande",
        "abreviacion": "Labranzagrande",
        "createdAt": "2020-01-16T13:33:35.703Z",
        "updatedAt": "2020-01-16T13:33:35.703Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608160"
        },
        "nombre": "Chivor",
        "descripcion": "Chivor",
        "abreviacion": "Chivor",
        "createdAt": "2020-01-16T13:33:35.689Z",
        "updatedAt": "2020-01-16T13:33:35.689Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60816c"
        },
        "nombre": "Güicán",
        "descripcion": "Güicán",
        "abreviacion": "Güicán",
        "createdAt": "2020-01-16T13:33:35.701Z",
        "updatedAt": "2020-01-16T13:33:35.701Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080d3"
        },
        "nombre": "Puerto Berrío",
        "descripcion": "Puerto Berrío",
        "abreviacion": "Puerto Berrío",
        "createdAt": "2020-01-16T13:33:35.574Z",
        "updatedAt": "2020-01-16T13:33:35.574Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608163"
        },
        "nombre": "El Espino",
        "descripcion": "El Espino",
        "abreviacion": "El Espino",
        "createdAt": "2020-01-16T13:33:35.692Z",
        "updatedAt": "2020-01-16T13:33:35.692Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608175"
        },
        "nombre": "Macanal",
        "descripcion": "Macanal",
        "abreviacion": "Macanal",
        "createdAt": "2020-01-16T13:33:35.706Z",
        "updatedAt": "2020-01-16T13:33:35.706Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608171"
        },
        "nombre": "La Capilla",
        "descripcion": "La Capilla",
        "abreviacion": "La Capilla",
        "createdAt": "2020-01-16T13:33:35.704Z",
        "updatedAt": "2020-01-16T13:33:35.704Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608165"
        },
        "nombre": "Floresta",
        "descripcion": "Floresta",
        "abreviacion": "Floresta",
        "createdAt": "2020-01-16T13:33:35.693Z",
        "updatedAt": "2020-01-16T13:33:35.693Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080d8"
        },
        "nombre": "Rionegro",
        "descripcion": "Rionegro",
        "abreviacion": "Rionegro",
        "createdAt": "2020-01-16T13:33:35.578Z",
        "updatedAt": "2020-01-16T13:33:35.578Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60817a"
        },
        "nombre": "Moniquirá",
        "descripcion": "Moniquirá",
        "abreviacion": "Moniquirá",
        "createdAt": "2020-01-16T13:33:35.709Z",
        "updatedAt": "2020-01-16T13:33:35.709Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608168"
        },
        "nombre": "Garagoa",
        "descripcion": "Garagoa",
        "abreviacion": "Garagoa",
        "createdAt": "2020-01-16T13:33:35.697Z",
        "updatedAt": "2020-01-16T13:33:35.697Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60816a"
        },
        "nombre": "Guateque",
        "descripcion": "Guateque",
        "abreviacion": "Guateque",
        "createdAt": "2020-01-16T13:33:35.699Z",
        "updatedAt": "2020-01-16T13:33:35.699Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608176"
        },
        "nombre": "Maripí",
        "descripcion": "Maripí",
        "abreviacion": "Maripí",
        "createdAt": "2020-01-16T13:33:35.707Z",
        "updatedAt": "2020-01-16T13:33:35.707Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60816d"
        },
        "nombre": "Iza",
        "descripcion": "Iza",
        "abreviacion": "Iza",
        "createdAt": "2020-01-16T13:33:35.701Z",
        "updatedAt": "2020-01-16T13:33:35.701Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60816f"
        },
        "nombre": "Jericó",
        "descripcion": "Jericó",
        "abreviacion": "Jericó",
        "createdAt": "2020-01-16T13:33:35.703Z",
        "updatedAt": "2020-01-16T13:33:35.703Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080dd"
        },
        "nombre": "Yavaraté",
        "descripcion": "Yavaraté",
        "abreviacion": "Yavaraté",
        "createdAt": "2020-01-16T13:33:35.584Z",
        "updatedAt": "2020-01-16T13:33:35.584Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60817b"
        },
        "nombre": "Muzo",
        "descripcion": "Muzo",
        "abreviacion": "Muzo",
        "createdAt": "2020-01-16T13:33:35.710Z",
        "updatedAt": "2020-01-16T13:33:35.710Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60817f"
        },
        "nombre": "Otanche",
        "descripcion": "Otanche",
        "abreviacion": "Otanche",
        "createdAt": "2020-01-16T13:33:35.712Z",
        "updatedAt": "2020-01-16T13:33:35.712Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608172"
        },
        "nombre": "La Victoria",
        "descripcion": "La Victoria",
        "abreviacion": "La Victoria",
        "createdAt": "2020-01-16T13:33:35.705Z",
        "updatedAt": "2020-01-16T13:33:35.705Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608174"
        },
        "nombre": "Belén",
        "descripcion": "Belén",
        "abreviacion": "Belén",
        "createdAt": "2020-01-16T13:33:35.706Z",
        "updatedAt": "2020-01-16T13:33:35.706Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080e2"
        },
        "nombre": "San Luis",
        "descripcion": "San Luis",
        "abreviacion": "San Luis",
        "createdAt": "2020-01-16T13:33:35.588Z",
        "updatedAt": "2020-01-16T13:33:35.588Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608184"
        },
        "nombre": "Panqueba",
        "descripcion": "Panqueba",
        "abreviacion": "Panqueba",
        "createdAt": "2020-01-16T13:33:35.715Z",
        "updatedAt": "2020-01-16T13:33:35.715Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608180"
        },
        "nombre": "Pachavita",
        "descripcion": "Pachavita",
        "abreviacion": "Pachavita",
        "createdAt": "2020-01-16T13:33:35.713Z",
        "updatedAt": "2020-01-16T13:33:35.713Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608177"
        },
        "nombre": "Miraflores",
        "descripcion": "Miraflores",
        "abreviacion": "Miraflores",
        "createdAt": "2020-01-16T13:33:35.708Z",
        "updatedAt": "2020-01-16T13:33:35.708Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608179"
        },
        "nombre": "Monguí",
        "descripcion": "Monguí",
        "abreviacion": "Monguí",
        "createdAt": "2020-01-16T13:33:35.709Z",
        "updatedAt": "2020-01-16T13:33:35.709Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080e7"
        },
        "nombre": "San Vicente",
        "descripcion": "San Vicente",
        "abreviacion": "San Vicente",
        "createdAt": "2020-01-16T13:33:35.591Z",
        "updatedAt": "2020-01-16T13:33:35.591Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608189"
        },
        "nombre": "Pisba",
        "descripcion": "Pisba",
        "abreviacion": "Pisba",
        "createdAt": "2020-01-16T13:33:35.718Z",
        "updatedAt": "2020-01-16T13:33:35.718Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608185"
        },
        "nombre": "Pauna",
        "descripcion": "Pauna",
        "abreviacion": "Pauna",
        "createdAt": "2020-01-16T13:33:35.716Z",
        "updatedAt": "2020-01-16T13:33:35.716Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60817c"
        },
        "nombre": "Nobsa",
        "descripcion": "Nobsa",
        "abreviacion": "Nobsa",
        "createdAt": "2020-01-16T13:33:35.710Z",
        "updatedAt": "2020-01-16T13:33:35.710Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60817e"
        },
        "nombre": "Oicatá",
        "descripcion": "Oicatá",
        "abreviacion": "Oicatá",
        "createdAt": "2020-01-16T13:33:35.712Z",
        "updatedAt": "2020-01-16T13:33:35.712Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60818e"
        },
        "nombre": "Rondón",
        "descripcion": "Rondón",
        "abreviacion": "Rondón",
        "createdAt": "2020-01-16T13:33:35.723Z",
        "updatedAt": "2020-01-16T13:33:35.723Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080ec"
        },
        "nombre": "Segovia",
        "descripcion": "Segovia",
        "abreviacion": "Segovia",
        "createdAt": "2020-01-16T13:33:35.596Z",
        "updatedAt": "2020-01-16T13:33:35.596Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60818a"
        },
        "nombre": "Puerto Boyacá",
        "descripcion": "Puerto Boyacá",
        "abreviacion": "Puerto Boyacá",
        "createdAt": "2020-01-16T13:33:35.719Z",
        "updatedAt": "2020-01-16T13:33:35.719Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608183"
        },
        "nombre": "Pajarito",
        "descripcion": "Pajarito",
        "abreviacion": "Pajarito",
        "createdAt": "2020-01-16T13:33:35.715Z",
        "updatedAt": "2020-01-16T13:33:35.715Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080f1"
        },
        "nombre": "Titiribí",
        "descripcion": "Titiribí",
        "abreviacion": "Titiribí",
        "createdAt": "2020-01-16T13:33:35.599Z",
        "updatedAt": "2020-01-16T13:33:35.599Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608193"
        },
        "nombre": "Carmen del Darien",
        "descripcion": "Carmen del Darien",
        "abreviacion": "Carmen del Darien",
        "createdAt": "2020-01-16T13:33:35.727Z",
        "updatedAt": "2020-01-16T13:33:35.727Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608181"
        },
        "nombre": "Páez",
        "descripcion": "Páez",
        "abreviacion": "Páez",
        "createdAt": "2020-01-16T13:33:35.713Z",
        "updatedAt": "2020-01-16T13:33:35.713Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60818f"
        },
        "nombre": "Saboyá",
        "descripcion": "Saboyá",
        "abreviacion": "Saboyá",
        "createdAt": "2020-01-16T13:33:35.724Z",
        "updatedAt": "2020-01-16T13:33:35.724Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080f6"
        },
        "nombre": "Valdivia",
        "descripcion": "Valdivia",
        "abreviacion": "Valdivia",
        "createdAt": "2020-01-16T13:33:35.604Z",
        "updatedAt": "2020-01-16T13:33:35.604Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608198"
        },
        "nombre": "Santana",
        "descripcion": "Santana",
        "abreviacion": "Santana",
        "createdAt": "2020-01-16T13:33:35.730Z",
        "updatedAt": "2020-01-16T13:33:35.730Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608188"
        },
        "nombre": "Pesca",
        "descripcion": "Pesca",
        "abreviacion": "Pesca",
        "createdAt": "2020-01-16T13:33:35.717Z",
        "updatedAt": "2020-01-16T13:33:35.717Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608186"
        },
        "nombre": "Paya",
        "descripcion": "Paya",
        "abreviacion": "Paya",
        "createdAt": "2020-01-16T13:33:35.716Z",
        "updatedAt": "2020-01-16T13:33:35.716Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608194"
        },
        "nombre": "Gama",
        "descripcion": "Gama",
        "abreviacion": "Gama",
        "createdAt": "2020-01-16T13:33:35.727Z",
        "updatedAt": "2020-01-16T13:33:35.727Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60819d"
        },
        "nombre": "Sativasur",
        "descripcion": "Sativasur",
        "abreviacion": "Sativasur",
        "createdAt": "2020-01-16T13:33:35.733Z",
        "updatedAt": "2020-01-16T13:33:35.733Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60818d"
        },
        "nombre": "Ráquira",
        "descripcion": "Ráquira",
        "abreviacion": "Ráquira",
        "createdAt": "2020-01-16T13:33:35.722Z",
        "updatedAt": "2020-01-16T13:33:35.722Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6080fb"
        },
        "nombre": "Yalí",
        "descripcion": "Yalí",
        "abreviacion": "Yalí",
        "createdAt": "2020-01-16T13:33:35.611Z",
        "updatedAt": "2020-01-16T13:33:35.611Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60818b"
        },
        "nombre": "Quípama",
        "descripcion": "Quípama",
        "abreviacion": "Quípama",
        "createdAt": "2020-01-16T13:33:35.721Z",
        "updatedAt": "2020-01-16T13:33:35.721Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608199"
        },
        "nombre": "Santa María",
        "descripcion": "Santa María",
        "abreviacion": "Santa María",
        "createdAt": "2020-01-16T13:33:35.731Z",
        "updatedAt": "2020-01-16T13:33:35.731Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081a2"
        },
        "nombre": "Sogamoso",
        "descripcion": "Sogamoso",
        "abreviacion": "Sogamoso",
        "createdAt": "2020-01-16T13:33:35.736Z",
        "updatedAt": "2020-01-16T13:33:35.736Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608192"
        },
        "nombre": "San Eduardo",
        "descripcion": "San Eduardo",
        "abreviacion": "San Eduardo",
        "createdAt": "2020-01-16T13:33:35.726Z",
        "updatedAt": "2020-01-16T13:33:35.726Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608100"
        },
        "nombre": "Barranquilla",
        "descripcion": "Barranquilla",
        "abreviacion": "Barranquilla",
        "createdAt": "2020-01-16T13:33:35.616Z",
        "updatedAt": "2020-01-16T13:33:35.616Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608190"
        },
        "nombre": "Sáchica",
        "descripcion": "Sáchica",
        "abreviacion": "Sáchica",
        "createdAt": "2020-01-16T13:33:35.724Z",
        "updatedAt": "2020-01-16T13:33:35.724Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60819e"
        },
        "nombre": "Siachoque",
        "descripcion": "Siachoque",
        "abreviacion": "Siachoque",
        "createdAt": "2020-01-16T13:33:35.734Z",
        "updatedAt": "2020-01-16T13:33:35.734Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081a7"
        },
        "nombre": "Susacón",
        "descripcion": "Susacón",
        "abreviacion": "Susacón",
        "createdAt": "2020-01-16T13:33:35.739Z",
        "updatedAt": "2020-01-16T13:33:35.739Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608195"
        },
        "nombre": "San Mateo",
        "descripcion": "San Mateo",
        "abreviacion": "San Mateo",
        "createdAt": "2020-01-16T13:33:35.728Z",
        "updatedAt": "2020-01-16T13:33:35.728Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608105"
        },
        "nombre": "Tuluá",
        "descripcion": "Tuluá",
        "abreviacion": "Tuluá",
        "createdAt": "2020-01-16T13:33:35.619Z",
        "updatedAt": "2020-01-16T13:33:35.619Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608197"
        },
        "nombre": "Chachagüí",
        "descripcion": "Chachagüí",
        "abreviacion": "Chachagüí",
        "createdAt": "2020-01-16T13:33:35.729Z",
        "updatedAt": "2020-01-16T13:33:35.729Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081a3"
        },
        "nombre": "Somondoco",
        "descripcion": "Somondoco",
        "abreviacion": "Somondoco",
        "createdAt": "2020-01-16T13:33:35.737Z",
        "updatedAt": "2020-01-16T13:33:35.737Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081ac"
        },
        "nombre": "Tibaná",
        "descripcion": "Tibaná",
        "abreviacion": "Tibaná",
        "createdAt": "2020-01-16T13:33:35.742Z",
        "updatedAt": "2020-01-16T13:33:35.742Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60819a"
        },
        "nombre": "Cúcuta",
        "descripcion": "Cúcuta",
        "abreviacion": "Cúcuta",
        "createdAt": "2020-01-16T13:33:35.731Z",
        "updatedAt": "2020-01-16T13:33:35.731Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60810a"
        },
        "nombre": "Anolaima",
        "descripcion": "Anolaima",
        "abreviacion": "Anolaima",
        "createdAt": "2020-01-16T13:33:35.622Z",
        "updatedAt": "2020-01-16T13:33:35.622Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60819c"
        },
        "nombre": "Sativanorte",
        "descripcion": "Sativanorte",
        "abreviacion": "Sativanorte",
        "createdAt": "2020-01-16T13:33:35.733Z",
        "updatedAt": "2020-01-16T13:33:35.733Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081a8"
        },
        "nombre": "Sutamarchán",
        "descripcion": "Sutamarchán",
        "abreviacion": "Sutamarchán",
        "createdAt": "2020-01-16T13:33:35.740Z",
        "updatedAt": "2020-01-16T13:33:35.740Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081b1"
        },
        "nombre": "Tópaga",
        "descripcion": "Tópaga",
        "abreviacion": "Tópaga",
        "createdAt": "2020-01-16T13:33:35.745Z",
        "updatedAt": "2020-01-16T13:33:35.745Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60819f"
        },
        "nombre": "Soatá",
        "descripcion": "Soatá",
        "abreviacion": "Soatá",
        "createdAt": "2020-01-16T13:33:35.734Z",
        "updatedAt": "2020-01-16T13:33:35.734Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081a1"
        },
        "nombre": "Socha",
        "descripcion": "Socha",
        "abreviacion": "Socha",
        "createdAt": "2020-01-16T13:33:35.736Z",
        "updatedAt": "2020-01-16T13:33:35.736Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081ad"
        },
        "nombre": "Tinjacá",
        "descripcion": "Tinjacá",
        "abreviacion": "Tinjacá",
        "createdAt": "2020-01-16T13:33:35.743Z",
        "updatedAt": "2020-01-16T13:33:35.743Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60810f"
        },
        "nombre": "Sabanagrande",
        "descripcion": "Sabanagrande",
        "abreviacion": "Sabanagrande",
        "createdAt": "2020-01-16T13:33:35.626Z",
        "updatedAt": "2020-01-16T13:33:35.626Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081b6"
        },
        "nombre": "Umbita",
        "descripcion": "Umbita",
        "abreviacion": "Umbita",
        "createdAt": "2020-01-16T13:33:35.749Z",
        "updatedAt": "2020-01-16T13:33:35.749Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081a4"
        },
        "nombre": "Sora",
        "descripcion": "Sora",
        "abreviacion": "Sora",
        "createdAt": "2020-01-16T13:33:35.737Z",
        "updatedAt": "2020-01-16T13:33:35.737Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081a6"
        },
        "nombre": "Soracá",
        "descripcion": "Soracá",
        "abreviacion": "Soracá",
        "createdAt": "2020-01-16T13:33:35.739Z",
        "updatedAt": "2020-01-16T13:33:35.739Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081b2"
        },
        "nombre": "Tota",
        "descripcion": "Tota",
        "abreviacion": "Tota",
        "createdAt": "2020-01-16T13:33:35.746Z",
        "updatedAt": "2020-01-16T13:33:35.746Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608114"
        },
        "nombre": "Suan",
        "descripcion": "Suan",
        "abreviacion": "Suan",
        "createdAt": "2020-01-16T13:33:35.630Z",
        "updatedAt": "2020-01-16T13:33:35.630Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081bb"
        },
        "nombre": "Aguadas",
        "descripcion": "Aguadas",
        "abreviacion": "Aguadas",
        "createdAt": "2020-01-16T13:33:35.752Z",
        "updatedAt": "2020-01-16T13:33:35.752Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081a9"
        },
        "nombre": "Sutatenza",
        "descripcion": "Sutatenza",
        "abreviacion": "Sutatenza",
        "createdAt": "2020-01-16T13:33:35.741Z",
        "updatedAt": "2020-01-16T13:33:35.741Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081ab"
        },
        "nombre": "Tenza",
        "descripcion": "Tenza",
        "abreviacion": "Tenza",
        "createdAt": "2020-01-16T13:33:35.742Z",
        "updatedAt": "2020-01-16T13:33:35.742Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608119"
        },
        "nombre": "Achí",
        "descripcion": "Achí",
        "abreviacion": "Achí",
        "createdAt": "2020-01-16T13:33:35.634Z",
        "updatedAt": "2020-01-16T13:33:35.634Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081b7"
        },
        "nombre": "Ventaquemada",
        "descripcion": "Ventaquemada",
        "abreviacion": "Ventaquemada",
        "createdAt": "2020-01-16T13:33:35.749Z",
        "updatedAt": "2020-01-16T13:33:35.749Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081c0"
        },
        "nombre": "Filadelfia",
        "descripcion": "Filadelfia",
        "abreviacion": "Filadelfia",
        "createdAt": "2020-01-16T13:33:35.755Z",
        "updatedAt": "2020-01-16T13:33:35.755Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081ae"
        },
        "nombre": "Tipacoque",
        "descripcion": "Tipacoque",
        "abreviacion": "Tipacoque",
        "createdAt": "2020-01-16T13:33:35.744Z",
        "updatedAt": "2020-01-16T13:33:35.744Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081b0"
        },
        "nombre": "Cartagena",
        "descripcion": "Cartagena",
        "abreviacion": "Cartagena",
        "createdAt": "2020-01-16T13:33:35.745Z",
        "updatedAt": "2020-01-16T13:33:35.745Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60811e"
        },
        "nombre": "Florida",
        "descripcion": "Florida",
        "abreviacion": "Florida",
        "createdAt": "2020-01-16T13:33:35.638Z",
        "updatedAt": "2020-01-16T13:33:35.638Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081bc"
        },
        "nombre": "Anserma",
        "descripcion": "Anserma",
        "abreviacion": "Anserma",
        "createdAt": "2020-01-16T13:33:35.752Z",
        "updatedAt": "2020-01-16T13:33:35.752Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081c5"
        },
        "nombre": "Marulanda",
        "descripcion": "Marulanda",
        "abreviacion": "Marulanda",
        "createdAt": "2020-01-16T13:33:35.758Z",
        "updatedAt": "2020-01-16T13:33:35.758Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081b3"
        },
        "nombre": "Turmequé",
        "descripcion": "Turmequé",
        "abreviacion": "Turmequé",
        "createdAt": "2020-01-16T13:33:35.747Z",
        "updatedAt": "2020-01-16T13:33:35.747Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081b5"
        },
        "nombre": "Tutazá",
        "descripcion": "Tutazá",
        "abreviacion": "Tutazá",
        "createdAt": "2020-01-16T13:33:35.748Z",
        "updatedAt": "2020-01-16T13:33:35.748Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081c1"
        },
        "nombre": "La Dorada",
        "descripcion": "La Dorada",
        "abreviacion": "La Dorada",
        "createdAt": "2020-01-16T13:33:35.755Z",
        "updatedAt": "2020-01-16T13:33:35.755Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608123"
        },
        "nombre": "Clemencia",
        "descripcion": "Clemencia",
        "abreviacion": "Clemencia",
        "createdAt": "2020-01-16T13:33:35.644Z",
        "updatedAt": "2020-01-16T13:33:35.644Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081ca"
        },
        "nombre": "Pensilvania",
        "descripcion": "Pensilvania",
        "abreviacion": "Pensilvania",
        "createdAt": "2020-01-16T13:33:35.761Z",
        "updatedAt": "2020-01-16T13:33:35.761Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081b8"
        },
        "nombre": "Viracachá",
        "descripcion": "Viracachá",
        "abreviacion": "Viracachá",
        "createdAt": "2020-01-16T13:33:35.750Z",
        "updatedAt": "2020-01-16T13:33:35.750Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081c6"
        },
        "nombre": "Neira",
        "descripcion": "Neira",
        "abreviacion": "Neira",
        "createdAt": "2020-01-16T13:33:35.758Z",
        "updatedAt": "2020-01-16T13:33:35.758Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081ba"
        },
        "nombre": "Manizales",
        "descripcion": "Manizales",
        "abreviacion": "Manizales",
        "createdAt": "2020-01-16T13:33:35.751Z",
        "updatedAt": "2020-01-16T13:33:35.751Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608128"
        },
        "nombre": "Mahates",
        "descripcion": "Mahates",
        "abreviacion": "Mahates",
        "createdAt": "2020-01-16T13:33:35.647Z",
        "updatedAt": "2020-01-16T13:33:35.647Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081cf"
        },
        "nombre": "San José",
        "descripcion": "San José",
        "abreviacion": "San José",
        "createdAt": "2020-01-16T13:33:35.763Z",
        "updatedAt": "2020-01-16T13:33:35.763Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081bd"
        },
        "nombre": "Aranzazu",
        "descripcion": "Aranzazu",
        "abreviacion": "Aranzazu",
        "createdAt": "2020-01-16T13:33:35.753Z",
        "updatedAt": "2020-01-16T13:33:35.753Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081cb"
        },
        "nombre": "Riosucio",
        "descripcion": "Riosucio",
        "abreviacion": "Riosucio",
        "createdAt": "2020-01-16T13:33:35.761Z",
        "updatedAt": "2020-01-16T13:33:35.761Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081bf"
        },
        "nombre": "Chinchiná",
        "descripcion": "Chinchiná",
        "abreviacion": "Chinchiná",
        "createdAt": "2020-01-16T13:33:35.754Z",
        "updatedAt": "2020-01-16T13:33:35.754Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60812d"
        },
        "nombre": "Norosí",
        "descripcion": "Norosí",
        "abreviacion": "Norosí",
        "createdAt": "2020-01-16T13:33:35.650Z",
        "updatedAt": "2020-01-16T13:33:35.650Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081d4"
        },
        "nombre": "Florencia",
        "descripcion": "Florencia",
        "abreviacion": "Florencia",
        "createdAt": "2020-01-16T13:33:35.767Z",
        "updatedAt": "2020-01-16T13:33:35.767Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081c2"
        },
        "nombre": "La Merced",
        "descripcion": "La Merced",
        "abreviacion": "La Merced",
        "createdAt": "2020-01-16T13:33:35.756Z",
        "updatedAt": "2020-01-16T13:33:35.756Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081d0"
        },
        "nombre": "Supía",
        "descripcion": "Supía",
        "abreviacion": "Supía",
        "createdAt": "2020-01-16T13:33:35.765Z",
        "updatedAt": "2020-01-16T13:33:35.765Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081c4"
        },
        "nombre": "Marmato",
        "descripcion": "Marmato",
        "abreviacion": "Marmato",
        "createdAt": "2020-01-16T13:33:35.757Z",
        "updatedAt": "2020-01-16T13:33:35.757Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608132"
        },
        "nombre": "San Fernando",
        "descripcion": "San Fernando",
        "abreviacion": "San Fernando",
        "createdAt": "2020-01-16T13:33:35.653Z",
        "updatedAt": "2020-01-16T13:33:35.653Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081d9"
        },
        "nombre": "El Doncello",
        "descripcion": "El Doncello",
        "abreviacion": "El Doncello",
        "createdAt": "2020-01-16T13:33:35.771Z",
        "updatedAt": "2020-01-16T13:33:35.771Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081c7"
        },
        "nombre": "Norcasia",
        "descripcion": "Norcasia",
        "abreviacion": "Norcasia",
        "createdAt": "2020-01-16T13:33:35.759Z",
        "updatedAt": "2020-01-16T13:33:35.759Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081d5"
        },
        "nombre": "Albania",
        "descripcion": "Albania",
        "abreviacion": "Albania",
        "createdAt": "2020-01-16T13:33:35.768Z",
        "updatedAt": "2020-01-16T13:33:35.768Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081c9"
        },
        "nombre": "Palestina",
        "descripcion": "Palestina",
        "abreviacion": "Palestina",
        "createdAt": "2020-01-16T13:33:35.760Z",
        "updatedAt": "2020-01-16T13:33:35.760Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608137"
        },
        "nombre": "Támara",
        "descripcion": "Támara",
        "abreviacion": "Támara",
        "createdAt": "2020-01-16T13:33:35.656Z",
        "updatedAt": "2020-01-16T13:33:35.656Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081de"
        },
        "nombre": "San Vicente del Caguán",
        "descripcion": "San Vicente del Caguán",
        "abreviacion": "San Vicente del Caguán",
        "createdAt": "2020-01-16T13:33:35.776Z",
        "updatedAt": "2020-01-16T13:33:35.776Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081cc"
        },
        "nombre": "Risaralda",
        "descripcion": "Risaralda",
        "abreviacion": "Risaralda",
        "createdAt": "2020-01-16T13:33:35.762Z",
        "updatedAt": "2020-01-16T13:33:35.762Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081da"
        },
        "nombre": "El Paujil",
        "descripcion": "El Paujil",
        "abreviacion": "El Paujil",
        "createdAt": "2020-01-16T13:33:35.771Z",
        "updatedAt": "2020-01-16T13:33:35.771Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081ce"
        },
        "nombre": "Samaná",
        "descripcion": "Samaná",
        "abreviacion": "Samaná",
        "createdAt": "2020-01-16T13:33:35.763Z",
        "updatedAt": "2020-01-16T13:33:35.763Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60813c"
        },
        "nombre": "Soplaviento",
        "descripcion": "Soplaviento",
        "abreviacion": "Soplaviento",
        "createdAt": "2020-01-16T13:33:35.661Z",
        "updatedAt": "2020-01-16T13:33:35.661Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081e3"
        },
        "nombre": "Almaguer",
        "descripcion": "Almaguer",
        "abreviacion": "Almaguer",
        "createdAt": "2020-01-16T13:33:35.780Z",
        "updatedAt": "2020-01-16T13:33:35.780Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081d1"
        },
        "nombre": "Victoria",
        "descripcion": "Victoria",
        "abreviacion": "Victoria",
        "createdAt": "2020-01-16T13:33:35.765Z",
        "updatedAt": "2020-01-16T13:33:35.765Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081df"
        },
        "nombre": "Solano",
        "descripcion": "Solano",
        "abreviacion": "Solano",
        "createdAt": "2020-01-16T13:33:35.777Z",
        "updatedAt": "2020-01-16T13:33:35.777Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081d3"
        },
        "nombre": "Viterbo",
        "descripcion": "Viterbo",
        "abreviacion": "Viterbo",
        "createdAt": "2020-01-16T13:33:35.767Z",
        "updatedAt": "2020-01-16T13:33:35.767Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608141"
        },
        "nombre": "Villanueva",
        "descripcion": "Villanueva",
        "abreviacion": "Villanueva",
        "createdAt": "2020-01-16T13:33:35.665Z",
        "updatedAt": "2020-01-16T13:33:35.665Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081e8"
        },
        "nombre": "Cajibío",
        "descripcion": "Cajibío",
        "abreviacion": "Cajibío",
        "createdAt": "2020-01-16T13:33:35.786Z",
        "updatedAt": "2020-01-16T13:33:35.786Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081d6"
        },
        "nombre": "Santa Bárbara de Pinto",
        "descripcion": "Santa Bárbara de Pinto",
        "abreviacion": "Santa Bárbara de Pinto",
        "createdAt": "2020-01-16T13:33:35.769Z",
        "updatedAt": "2020-01-16T13:33:35.769Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081e4"
        },
        "nombre": "Argelia",
        "descripcion": "Argelia",
        "abreviacion": "Argelia",
        "createdAt": "2020-01-16T13:33:35.781Z",
        "updatedAt": "2020-01-16T13:33:35.781Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081d8"
        },
        "nombre": "Curillo",
        "descripcion": "Curillo",
        "abreviacion": "Curillo",
        "createdAt": "2020-01-16T13:33:35.770Z",
        "updatedAt": "2020-01-16T13:33:35.770Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608146"
        },
        "nombre": "Arcabuco",
        "descripcion": "Arcabuco",
        "abreviacion": "Arcabuco",
        "createdAt": "2020-01-16T13:33:35.671Z",
        "updatedAt": "2020-01-16T13:33:35.671Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081ed"
        },
        "nombre": "Florencia",
        "descripcion": "Florencia",
        "abreviacion": "Florencia",
        "createdAt": "2020-01-16T13:33:35.789Z",
        "updatedAt": "2020-01-16T13:33:35.789Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081db"
        },
        "nombre": "Morelia",
        "descripcion": "Morelia",
        "abreviacion": "Morelia",
        "createdAt": "2020-01-16T13:33:35.774Z",
        "updatedAt": "2020-01-16T13:33:35.774Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081dd"
        },
        "nombre": "La Montañita",
        "descripcion": "La Montañita",
        "abreviacion": "La Montañita",
        "createdAt": "2020-01-16T13:33:35.775Z",
        "updatedAt": "2020-01-16T13:33:35.775Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081e9"
        },
        "nombre": "Caldono",
        "descripcion": "Caldono",
        "abreviacion": "Caldono",
        "createdAt": "2020-01-16T13:33:35.787Z",
        "updatedAt": "2020-01-16T13:33:35.787Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60814b"
        },
        "nombre": "Boyacá",
        "descripcion": "Boyacá",
        "abreviacion": "Boyacá",
        "createdAt": "2020-01-16T13:33:35.674Z",
        "updatedAt": "2020-01-16T13:33:35.674Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081f2"
        },
        "nombre": "Rosas",
        "descripcion": "Rosas",
        "abreviacion": "Rosas",
        "createdAt": "2020-01-16T13:33:35.793Z",
        "updatedAt": "2020-01-16T13:33:35.793Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081e0"
        },
        "nombre": "Solita",
        "descripcion": "Solita",
        "abreviacion": "Solita",
        "createdAt": "2020-01-16T13:33:35.778Z",
        "updatedAt": "2020-01-16T13:33:35.778Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081e2"
        },
        "nombre": "Popayán",
        "descripcion": "Popayán",
        "abreviacion": "Popayán",
        "createdAt": "2020-01-16T13:33:35.779Z",
        "updatedAt": "2020-01-16T13:33:35.779Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081ee"
        },
        "nombre": "Guachené",
        "descripcion": "Guachené",
        "abreviacion": "Guachené",
        "createdAt": "2020-01-16T13:33:35.790Z",
        "updatedAt": "2020-01-16T13:33:35.790Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608150"
        },
        "nombre": "Campohermoso",
        "descripcion": "Campohermoso",
        "abreviacion": "Campohermoso",
        "createdAt": "2020-01-16T13:33:35.677Z",
        "updatedAt": "2020-01-16T13:33:35.677Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081f7"
        },
        "nombre": "Valledupar",
        "descripcion": "Valledupar",
        "abreviacion": "Valledupar",
        "createdAt": "2020-01-16T13:33:35.796Z",
        "updatedAt": "2020-01-16T13:33:35.796Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081e5"
        },
        "nombre": "Balboa",
        "descripcion": "Balboa",
        "abreviacion": "Balboa",
        "createdAt": "2020-01-16T13:33:35.782Z",
        "updatedAt": "2020-01-16T13:33:35.782Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081f3"
        },
        "nombre": "Jardín",
        "descripcion": "Jardín",
        "abreviacion": "Jardín",
        "createdAt": "2020-01-16T13:33:35.793Z",
        "updatedAt": "2020-01-16T13:33:35.793Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081e7"
        },
        "nombre": "Buenos Aires",
        "descripcion": "Buenos Aires",
        "abreviacion": "Buenos Aires",
        "createdAt": "2020-01-16T13:33:35.785Z",
        "updatedAt": "2020-01-16T13:33:35.785Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608155"
        },
        "nombre": "Chita",
        "descripcion": "Chita",
        "abreviacion": "Chita",
        "createdAt": "2020-01-16T13:33:35.680Z",
        "updatedAt": "2020-01-16T13:33:35.680Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081fc"
        },
        "nombre": "La Gloria",
        "descripcion": "La Gloria",
        "abreviacion": "La Gloria",
        "createdAt": "2020-01-16T13:33:35.799Z",
        "updatedAt": "2020-01-16T13:33:35.799Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081ea"
        },
        "nombre": "Caloto",
        "descripcion": "Caloto",
        "abreviacion": "Caloto",
        "createdAt": "2020-01-16T13:33:35.787Z",
        "updatedAt": "2020-01-16T13:33:35.787Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081f8"
        },
        "nombre": "Agustín Codazzi",
        "descripcion": "Agustín Codazzi",
        "abreviacion": "Agustín Codazzi",
        "createdAt": "2020-01-16T13:33:35.796Z",
        "updatedAt": "2020-01-16T13:33:35.796Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081ec"
        },
        "nombre": "El Tambo",
        "descripcion": "El Tambo",
        "abreviacion": "El Tambo",
        "createdAt": "2020-01-16T13:33:35.789Z",
        "updatedAt": "2020-01-16T13:33:35.789Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60815a"
        },
        "nombre": "Corrales",
        "descripcion": "Corrales",
        "abreviacion": "Corrales",
        "createdAt": "2020-01-16T13:33:35.685Z",
        "updatedAt": "2020-01-16T13:33:35.685Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608201"
        },
        "nombre": "Ayapel",
        "descripcion": "Ayapel",
        "abreviacion": "Ayapel",
        "createdAt": "2020-01-16T13:33:35.802Z",
        "updatedAt": "2020-01-16T13:33:35.802Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081ef"
        },
        "nombre": "Jambaló",
        "descripcion": "Jambaló",
        "abreviacion": "Jambaló",
        "createdAt": "2020-01-16T13:33:35.791Z",
        "updatedAt": "2020-01-16T13:33:35.791Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081fd"
        },
        "nombre": "Manaure",
        "descripcion": "Manaure",
        "abreviacion": "Manaure",
        "createdAt": "2020-01-16T13:33:35.799Z",
        "updatedAt": "2020-01-16T13:33:35.799Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081f1"
        },
        "nombre": "Piendamó",
        "descripcion": "Piendamó",
        "abreviacion": "Piendamó",
        "createdAt": "2020-01-16T13:33:35.792Z",
        "updatedAt": "2020-01-16T13:33:35.792Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60815f"
        },
        "nombre": "Chíquiza",
        "descripcion": "Chíquiza",
        "abreviacion": "Chíquiza",
        "createdAt": "2020-01-16T13:33:35.689Z",
        "updatedAt": "2020-01-16T13:33:35.689Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608206"
        },
        "nombre": "Momil",
        "descripcion": "Momil",
        "abreviacion": "Momil",
        "createdAt": "2020-01-16T13:33:35.805Z",
        "updatedAt": "2020-01-16T13:33:35.805Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081f4"
        },
        "nombre": "Sotara",
        "descripcion": "Sotara",
        "abreviacion": "Sotara",
        "createdAt": "2020-01-16T13:33:35.794Z",
        "updatedAt": "2020-01-16T13:33:35.794Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081f6"
        },
        "nombre": "Toribio",
        "descripcion": "Toribio",
        "abreviacion": "Toribio",
        "createdAt": "2020-01-16T13:33:35.795Z",
        "updatedAt": "2020-01-16T13:33:35.795Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608202"
        },
        "nombre": "Canalete",
        "descripcion": "Canalete",
        "abreviacion": "Canalete",
        "createdAt": "2020-01-16T13:33:35.802Z",
        "updatedAt": "2020-01-16T13:33:35.802Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608164"
        },
        "nombre": "Firavitoba",
        "descripcion": "Firavitoba",
        "abreviacion": "Firavitoba",
        "createdAt": "2020-01-16T13:33:35.692Z",
        "updatedAt": "2020-01-16T13:33:35.692Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60820b"
        },
        "nombre": "Yacopí",
        "descripcion": "Yacopí",
        "abreviacion": "Yacopí",
        "createdAt": "2020-01-16T13:33:35.808Z",
        "updatedAt": "2020-01-16T13:33:35.808Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081f9"
        },
        "nombre": "Bosconia",
        "descripcion": "Bosconia",
        "abreviacion": "Bosconia",
        "createdAt": "2020-01-16T13:33:35.797Z",
        "updatedAt": "2020-01-16T13:33:35.797Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081fb"
        },
        "nombre": "El Paso",
        "descripcion": "El Paso",
        "abreviacion": "El Paso",
        "createdAt": "2020-01-16T13:33:35.798Z",
        "updatedAt": "2020-01-16T13:33:35.798Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608207"
        },
        "nombre": "Moñitos",
        "descripcion": "Moñitos",
        "abreviacion": "Moñitos",
        "createdAt": "2020-01-16T13:33:35.805Z",
        "updatedAt": "2020-01-16T13:33:35.805Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608169"
        },
        "nombre": "Guacamayas",
        "descripcion": "Guacamayas",
        "abreviacion": "Guacamayas",
        "createdAt": "2020-01-16T13:33:35.698Z",
        "updatedAt": "2020-01-16T13:33:35.698Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608210"
        },
        "nombre": "Calarcá",
        "descripcion": "Calarcá",
        "abreviacion": "Calarcá",
        "createdAt": "2020-01-16T13:33:35.812Z",
        "updatedAt": "2020-01-16T13:33:35.812Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081fe"
        },
        "nombre": "Pueblo Bello",
        "descripcion": "Pueblo Bello",
        "abreviacion": "Pueblo Bello",
        "createdAt": "2020-01-16T13:33:35.800Z",
        "updatedAt": "2020-01-16T13:33:35.800Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60820c"
        },
        "nombre": "Purísima",
        "descripcion": "Purísima",
        "abreviacion": "Purísima",
        "createdAt": "2020-01-16T13:33:35.808Z",
        "updatedAt": "2020-01-16T13:33:35.808Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608200"
        },
        "nombre": "San Martín",
        "descripcion": "San Martín",
        "abreviacion": "San Martín",
        "createdAt": "2020-01-16T13:33:35.801Z",
        "updatedAt": "2020-01-16T13:33:35.801Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60816e"
        },
        "nombre": "Jenesano",
        "descripcion": "Jenesano",
        "abreviacion": "Jenesano",
        "createdAt": "2020-01-16T13:33:35.702Z",
        "updatedAt": "2020-01-16T13:33:35.702Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608215"
        },
        "nombre": "Tuchín",
        "descripcion": "Tuchín",
        "abreviacion": "Tuchín",
        "createdAt": "2020-01-16T13:33:35.817Z",
        "updatedAt": "2020-01-16T13:33:35.817Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608203"
        },
        "nombre": "Chinú",
        "descripcion": "Chinú",
        "abreviacion": "Chinú",
        "createdAt": "2020-01-16T13:33:35.803Z",
        "updatedAt": "2020-01-16T13:33:35.803Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608211"
        },
        "nombre": "Sonsón",
        "descripcion": "Sonsón",
        "abreviacion": "Sonsón",
        "createdAt": "2020-01-16T13:33:35.813Z",
        "updatedAt": "2020-01-16T13:33:35.813Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608205"
        },
        "nombre": "Los Córdobas",
        "descripcion": "Los Córdobas",
        "abreviacion": "Los Córdobas",
        "createdAt": "2020-01-16T13:33:35.804Z",
        "updatedAt": "2020-01-16T13:33:35.804Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608173"
        },
        "nombre": "Puerto Colombia",
        "descripcion": "Puerto Colombia",
        "abreviacion": "Puerto Colombia",
        "createdAt": "2020-01-16T13:33:35.705Z",
        "updatedAt": "2020-01-16T13:33:35.705Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60821a"
        },
        "nombre": "Beltrán",
        "descripcion": "Beltrán",
        "abreviacion": "Beltrán",
        "createdAt": "2020-01-16T13:33:35.822Z",
        "updatedAt": "2020-01-16T13:33:35.822Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608208"
        },
        "nombre": "Planeta Rica",
        "descripcion": "Planeta Rica",
        "abreviacion": "Planeta Rica",
        "createdAt": "2020-01-16T13:33:35.806Z",
        "updatedAt": "2020-01-16T13:33:35.806Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608216"
        },
        "nombre": "Valencia",
        "descripcion": "Valencia",
        "abreviacion": "Valencia",
        "createdAt": "2020-01-16T13:33:35.818Z",
        "updatedAt": "2020-01-16T13:33:35.818Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60820a"
        },
        "nombre": "Puerto Escondido",
        "descripcion": "Puerto Escondido",
        "abreviacion": "Puerto Escondido",
        "createdAt": "2020-01-16T13:33:35.807Z",
        "updatedAt": "2020-01-16T13:33:35.807Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608178"
        },
        "nombre": "Mongua",
        "descripcion": "Mongua",
        "abreviacion": "Mongua",
        "createdAt": "2020-01-16T13:33:35.708Z",
        "updatedAt": "2020-01-16T13:33:35.708Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60821f"
        },
        "nombre": "Cajicá",
        "descripcion": "Cajicá",
        "abreviacion": "Cajicá",
        "createdAt": "2020-01-16T13:33:35.825Z",
        "updatedAt": "2020-01-16T13:33:35.825Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60820d"
        },
        "nombre": "Sahagún",
        "descripcion": "Sahagún",
        "abreviacion": "Sahagún",
        "createdAt": "2020-01-16T13:33:35.809Z",
        "updatedAt": "2020-01-16T13:33:35.809Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60821b"
        },
        "nombre": "Bituima",
        "descripcion": "Bituima",
        "abreviacion": "Bituima",
        "createdAt": "2020-01-16T13:33:35.822Z",
        "updatedAt": "2020-01-16T13:33:35.822Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60820f"
        },
        "nombre": "San Antero",
        "descripcion": "San Antero",
        "abreviacion": "San Antero",
        "createdAt": "2020-01-16T13:33:35.810Z",
        "updatedAt": "2020-01-16T13:33:35.810Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60817d"
        },
        "nombre": "Nuevo Colón",
        "descripcion": "Nuevo Colón",
        "abreviacion": "Nuevo Colón",
        "createdAt": "2020-01-16T13:33:35.711Z",
        "updatedAt": "2020-01-16T13:33:35.711Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608224"
        },
        "nombre": "Chipaque",
        "descripcion": "Chipaque",
        "abreviacion": "Chipaque",
        "createdAt": "2020-01-16T13:33:35.834Z",
        "updatedAt": "2020-01-16T13:33:35.834Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608212"
        },
        "nombre": "El Carmen",
        "descripcion": "El Carmen",
        "abreviacion": "El Carmen",
        "createdAt": "2020-01-16T13:33:35.814Z",
        "updatedAt": "2020-01-16T13:33:35.814Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608220"
        },
        "nombre": "Caparrapí",
        "descripcion": "Caparrapí",
        "abreviacion": "Caparrapí",
        "createdAt": "2020-01-16T13:33:35.829Z",
        "updatedAt": "2020-01-16T13:33:35.829Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608214"
        },
        "nombre": "Tierralta",
        "descripcion": "Tierralta",
        "abreviacion": "Tierralta",
        "createdAt": "2020-01-16T13:33:35.816Z",
        "updatedAt": "2020-01-16T13:33:35.816Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608182"
        },
        "nombre": "Paipa",
        "descripcion": "Paipa",
        "abreviacion": "Paipa",
        "createdAt": "2020-01-16T13:33:35.714Z",
        "updatedAt": "2020-01-16T13:33:35.714Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608229"
        },
        "nombre": "Cucunubá",
        "descripcion": "Cucunubá",
        "abreviacion": "Cucunubá",
        "createdAt": "2020-01-16T13:33:35.840Z",
        "updatedAt": "2020-01-16T13:33:35.840Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608217"
        },
        "nombre": "Lérida",
        "descripcion": "Lérida",
        "abreviacion": "Lérida",
        "createdAt": "2020-01-16T13:33:35.819Z",
        "updatedAt": "2020-01-16T13:33:35.819Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608225"
        },
        "nombre": "Choachí",
        "descripcion": "Choachí",
        "abreviacion": "Choachí",
        "createdAt": "2020-01-16T13:33:35.835Z",
        "updatedAt": "2020-01-16T13:33:35.835Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608219"
        },
        "nombre": "Arbeláez",
        "descripcion": "Arbeláez",
        "abreviacion": "Arbeláez",
        "createdAt": "2020-01-16T13:33:35.821Z",
        "updatedAt": "2020-01-16T13:33:35.821Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608187"
        },
        "nombre": "Sopó",
        "descripcion": "Sopó",
        "abreviacion": "Sopó",
        "createdAt": "2020-01-16T13:33:35.717Z",
        "updatedAt": "2020-01-16T13:33:35.717Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60822e"
        },
        "nombre": "Funza",
        "descripcion": "Funza",
        "abreviacion": "Funza",
        "createdAt": "2020-01-16T13:33:35.847Z",
        "updatedAt": "2020-01-16T13:33:35.847Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60821c"
        },
        "nombre": "Bojacá",
        "descripcion": "Bojacá",
        "abreviacion": "Bojacá",
        "createdAt": "2020-01-16T13:33:35.823Z",
        "updatedAt": "2020-01-16T13:33:35.823Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60822a"
        },
        "nombre": "El Colegio",
        "descripcion": "El Colegio",
        "abreviacion": "El Colegio",
        "createdAt": "2020-01-16T13:33:35.841Z",
        "updatedAt": "2020-01-16T13:33:35.841Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60821e"
        },
        "nombre": "Cachipay",
        "descripcion": "Cachipay",
        "abreviacion": "Cachipay",
        "createdAt": "2020-01-16T13:33:35.825Z",
        "updatedAt": "2020-01-16T13:33:35.825Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60818c"
        },
        "nombre": "Ramiriquí",
        "descripcion": "Ramiriquí",
        "abreviacion": "Ramiriquí",
        "createdAt": "2020-01-16T13:33:35.722Z",
        "updatedAt": "2020-01-16T13:33:35.722Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608233"
        },
        "nombre": "San Cristóbal",
        "descripcion": "San Cristóbal",
        "abreviacion": "San Cristóbal",
        "createdAt": "2020-01-16T13:33:35.851Z",
        "updatedAt": "2020-01-16T13:33:35.851Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60822f"
        },
        "nombre": "Fúquene",
        "descripcion": "Fúquene",
        "abreviacion": "Fúquene",
        "createdAt": "2020-01-16T13:33:35.848Z",
        "updatedAt": "2020-01-16T13:33:35.848Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608221"
        },
        "nombre": "Caqueza",
        "descripcion": "Caqueza",
        "abreviacion": "Caqueza",
        "createdAt": "2020-01-16T13:33:35.830Z",
        "updatedAt": "2020-01-16T13:33:35.830Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608223"
        },
        "nombre": "Chaguaní",
        "descripcion": "Chaguaní",
        "abreviacion": "Chaguaní",
        "createdAt": "2020-01-16T13:33:35.832Z",
        "updatedAt": "2020-01-16T13:33:35.832Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608191"
        },
        "nombre": "Samacá",
        "descripcion": "Samacá",
        "abreviacion": "Samacá",
        "createdAt": "2020-01-16T13:33:35.725Z",
        "updatedAt": "2020-01-16T13:33:35.725Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608238"
        },
        "nombre": "Guasca",
        "descripcion": "Guasca",
        "abreviacion": "Guasca",
        "createdAt": "2020-01-16T13:33:35.854Z",
        "updatedAt": "2020-01-16T13:33:35.854Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608226"
        },
        "nombre": "Chocontá",
        "descripcion": "Chocontá",
        "abreviacion": "Chocontá",
        "createdAt": "2020-01-16T13:33:35.836Z",
        "updatedAt": "2020-01-16T13:33:35.836Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608234"
        },
        "nombre": "Girardot",
        "descripcion": "Girardot",
        "abreviacion": "Girardot",
        "createdAt": "2020-01-16T13:33:35.851Z",
        "updatedAt": "2020-01-16T13:33:35.851Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608228"
        },
        "nombre": "Cota",
        "descripcion": "Cota",
        "abreviacion": "Cota",
        "createdAt": "2020-01-16T13:33:35.838Z",
        "updatedAt": "2020-01-16T13:33:35.838Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608196"
        },
        "nombre": "Sasaima",
        "descripcion": "Sasaima",
        "abreviacion": "Sasaima",
        "createdAt": "2020-01-16T13:33:35.729Z",
        "updatedAt": "2020-01-16T13:33:35.729Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60823d"
        },
        "nombre": "Gutiérrez",
        "descripcion": "Gutiérrez",
        "abreviacion": "Gutiérrez",
        "createdAt": "2020-01-16T13:33:35.857Z",
        "updatedAt": "2020-01-16T13:33:35.857Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60822b"
        },
        "nombre": "El Rosal",
        "descripcion": "El Rosal",
        "abreviacion": "El Rosal",
        "createdAt": "2020-01-16T13:33:35.842Z",
        "updatedAt": "2020-01-16T13:33:35.842Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608239"
        },
        "nombre": "Guataquí",
        "descripcion": "Guataquí",
        "abreviacion": "Guataquí",
        "createdAt": "2020-01-16T13:33:35.854Z",
        "updatedAt": "2020-01-16T13:33:35.854Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60819b"
        },
        "nombre": "Santa Sofía",
        "descripcion": "Santa Sofía",
        "abreviacion": "Santa Sofía",
        "createdAt": "2020-01-16T13:33:35.732Z",
        "updatedAt": "2020-01-16T13:33:35.732Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60822d"
        },
        "nombre": "Fosca",
        "descripcion": "Fosca",
        "abreviacion": "Fosca",
        "createdAt": "2020-01-16T13:33:35.846Z",
        "updatedAt": "2020-01-16T13:33:35.846Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608242"
        },
        "nombre": "La Palma",
        "descripcion": "La Palma",
        "abreviacion": "La Palma",
        "createdAt": "2020-01-16T13:33:35.860Z",
        "updatedAt": "2020-01-16T13:33:35.860Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608230"
        },
        "nombre": "Gachala",
        "descripcion": "Gachala",
        "abreviacion": "Gachala",
        "createdAt": "2020-01-16T13:33:35.848Z",
        "updatedAt": "2020-01-16T13:33:35.848Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60823e"
        },
        "nombre": "Jerusalén",
        "descripcion": "Jerusalén",
        "abreviacion": "Jerusalén",
        "createdAt": "2020-01-16T13:33:35.857Z",
        "updatedAt": "2020-01-16T13:33:35.857Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081a0"
        },
        "nombre": "Socotá",
        "descripcion": "Socotá",
        "abreviacion": "Socotá",
        "createdAt": "2020-01-16T13:33:35.735Z",
        "updatedAt": "2020-01-16T13:33:35.735Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608232"
        },
        "nombre": "Gachetá",
        "descripcion": "Gachetá",
        "abreviacion": "Gachetá",
        "createdAt": "2020-01-16T13:33:35.850Z",
        "updatedAt": "2020-01-16T13:33:35.850Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608243"
        },
        "nombre": "La Peña",
        "descripcion": "La Peña",
        "abreviacion": "La Peña",
        "createdAt": "2020-01-16T13:33:35.860Z",
        "updatedAt": "2020-01-16T13:33:35.860Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608235"
        },
        "nombre": "Granada",
        "descripcion": "Granada",
        "abreviacion": "Granada",
        "createdAt": "2020-01-16T13:33:35.852Z",
        "updatedAt": "2020-01-16T13:33:35.852Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081a5"
        },
        "nombre": "Sotaquirá",
        "descripcion": "Sotaquirá",
        "abreviacion": "Sotaquirá",
        "createdAt": "2020-01-16T13:33:35.738Z",
        "updatedAt": "2020-01-16T13:33:35.738Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608237"
        },
        "nombre": "Guaduas",
        "descripcion": "Guaduas",
        "abreviacion": "Guaduas",
        "createdAt": "2020-01-16T13:33:35.853Z",
        "updatedAt": "2020-01-16T13:33:35.853Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608248"
        },
        "nombre": "Manta",
        "descripcion": "Manta",
        "abreviacion": "Manta",
        "createdAt": "2020-01-16T13:33:35.863Z",
        "updatedAt": "2020-01-16T13:33:35.863Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60823a"
        },
        "nombre": "Guatavita",
        "descripcion": "Guatavita",
        "abreviacion": "Guatavita",
        "createdAt": "2020-01-16T13:33:35.855Z",
        "updatedAt": "2020-01-16T13:33:35.855Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081aa"
        },
        "nombre": "Tasco",
        "descripcion": "Tasco",
        "abreviacion": "Tasco",
        "createdAt": "2020-01-16T13:33:35.741Z",
        "updatedAt": "2020-01-16T13:33:35.741Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60823c"
        },
        "nombre": "Guayabetal",
        "descripcion": "Guayabetal",
        "abreviacion": "Guayabetal",
        "createdAt": "2020-01-16T13:33:35.856Z",
        "updatedAt": "2020-01-16T13:33:35.856Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60824d"
        },
        "nombre": "Nilo",
        "descripcion": "Nilo",
        "abreviacion": "Nilo",
        "createdAt": "2020-01-16T13:33:35.866Z",
        "updatedAt": "2020-01-16T13:33:35.866Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60823f"
        },
        "nombre": "Junín",
        "descripcion": "Junín",
        "abreviacion": "Junín",
        "createdAt": "2020-01-16T13:33:35.858Z",
        "updatedAt": "2020-01-16T13:33:35.858Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081af"
        },
        "nombre": "Toca",
        "descripcion": "Toca",
        "abreviacion": "Toca",
        "createdAt": "2020-01-16T13:33:35.744Z",
        "updatedAt": "2020-01-16T13:33:35.744Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608241"
        },
        "nombre": "La Mesa",
        "descripcion": "La Mesa",
        "abreviacion": "La Mesa",
        "createdAt": "2020-01-16T13:33:35.859Z",
        "updatedAt": "2020-01-16T13:33:35.859Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608244"
        },
        "nombre": "La Vega",
        "descripcion": "La Vega",
        "abreviacion": "La Vega",
        "createdAt": "2020-01-16T13:33:35.861Z",
        "updatedAt": "2020-01-16T13:33:35.861Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081b4"
        },
        "nombre": "Granada",
        "descripcion": "Granada",
        "abreviacion": "Granada",
        "createdAt": "2020-01-16T13:33:35.747Z",
        "updatedAt": "2020-01-16T13:33:35.747Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608246"
        },
        "nombre": "Macheta",
        "descripcion": "Macheta",
        "abreviacion": "Macheta",
        "createdAt": "2020-01-16T13:33:35.862Z",
        "updatedAt": "2020-01-16T13:33:35.862Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608249"
        },
        "nombre": "Medina",
        "descripcion": "Medina",
        "abreviacion": "Medina",
        "createdAt": "2020-01-16T13:33:35.864Z",
        "updatedAt": "2020-01-16T13:33:35.864Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60824b"
        },
        "nombre": "Nariño",
        "descripcion": "Nariño",
        "abreviacion": "Nariño",
        "createdAt": "2020-01-16T13:33:35.865Z",
        "updatedAt": "2020-01-16T13:33:35.865Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081b9"
        },
        "nombre": "Zetaquira",
        "descripcion": "Zetaquira",
        "abreviacion": "Zetaquira",
        "createdAt": "2020-01-16T13:33:35.750Z",
        "updatedAt": "2020-01-16T13:33:35.750Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60824e"
        },
        "nombre": "Nimaima",
        "descripcion": "Nimaima",
        "abreviacion": "Nimaima",
        "createdAt": "2020-01-16T13:33:35.866Z",
        "updatedAt": "2020-01-16T13:33:35.866Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608250"
        },
        "nombre": "Venecia",
        "descripcion": "Venecia",
        "abreviacion": "Venecia",
        "createdAt": "2020-01-16T13:33:35.868Z",
        "updatedAt": "2020-01-16T13:33:35.868Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081be"
        },
        "nombre": "Belalcázar",
        "descripcion": "Belalcázar",
        "abreviacion": "Belalcázar",
        "createdAt": "2020-01-16T13:33:35.753Z",
        "updatedAt": "2020-01-16T13:33:35.753Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081c3"
        },
        "nombre": "Manzanares",
        "descripcion": "Manzanares",
        "abreviacion": "Manzanares",
        "createdAt": "2020-01-16T13:33:35.756Z",
        "updatedAt": "2020-01-16T13:33:35.756Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081c8"
        },
        "nombre": "Pácora",
        "descripcion": "Pácora",
        "abreviacion": "Pácora",
        "createdAt": "2020-01-16T13:33:35.759Z",
        "updatedAt": "2020-01-16T13:33:35.759Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081cd"
        },
        "nombre": "Salamina",
        "descripcion": "Salamina",
        "abreviacion": "Salamina",
        "createdAt": "2020-01-16T13:33:35.762Z",
        "updatedAt": "2020-01-16T13:33:35.762Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081d2"
        },
        "nombre": "Villamaría",
        "descripcion": "Villamaría",
        "abreviacion": "Villamaría",
        "createdAt": "2020-01-16T13:33:35.766Z",
        "updatedAt": "2020-01-16T13:33:35.766Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081d7"
        },
        "nombre": "María la Baja",
        "descripcion": "María la Baja",
        "abreviacion": "María la Baja",
        "createdAt": "2020-01-16T13:33:35.770Z",
        "updatedAt": "2020-01-16T13:33:35.770Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081dc"
        },
        "nombre": "Puerto Rico",
        "descripcion": "Puerto Rico",
        "abreviacion": "Puerto Rico",
        "createdAt": "2020-01-16T13:33:35.775Z",
        "updatedAt": "2020-01-16T13:33:35.775Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081e1"
        },
        "nombre": "Valparaíso",
        "descripcion": "Valparaíso",
        "abreviacion": "Valparaíso",
        "createdAt": "2020-01-16T13:33:35.779Z",
        "updatedAt": "2020-01-16T13:33:35.779Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081e6"
        },
        "nombre": "Bolívar",
        "descripcion": "Bolívar",
        "abreviacion": "Bolívar",
        "createdAt": "2020-01-16T13:33:35.784Z",
        "updatedAt": "2020-01-16T13:33:35.784Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081eb"
        },
        "nombre": "Corinto",
        "descripcion": "Corinto",
        "abreviacion": "Corinto",
        "createdAt": "2020-01-16T13:33:35.788Z",
        "updatedAt": "2020-01-16T13:33:35.788Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081f0"
        },
        "nombre": "Morales",
        "descripcion": "Morales",
        "abreviacion": "Morales",
        "createdAt": "2020-01-16T13:33:35.791Z",
        "updatedAt": "2020-01-16T13:33:35.791Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081f5"
        },
        "nombre": "Sucre",
        "descripcion": "Sucre",
        "abreviacion": "Sucre",
        "createdAt": "2020-01-16T13:33:35.794Z",
        "updatedAt": "2020-01-16T13:33:35.794Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081fa"
        },
        "nombre": "Curumaní",
        "descripcion": "Curumaní",
        "abreviacion": "Curumaní",
        "createdAt": "2020-01-16T13:33:35.798Z",
        "updatedAt": "2020-01-16T13:33:35.798Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6081ff"
        },
        "nombre": "San Alberto",
        "descripcion": "San Alberto",
        "abreviacion": "San Alberto",
        "createdAt": "2020-01-16T13:33:35.801Z",
        "updatedAt": "2020-01-16T13:33:35.801Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608204"
        },
        "nombre": "Cotorra",
        "descripcion": "Cotorra",
        "abreviacion": "Cotorra",
        "createdAt": "2020-01-16T13:33:35.803Z",
        "updatedAt": "2020-01-16T13:33:35.803Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608209"
        },
        "nombre": "Pueblo Nuevo",
        "descripcion": "Pueblo Nuevo",
        "abreviacion": "Pueblo Nuevo",
        "createdAt": "2020-01-16T13:33:35.806Z",
        "updatedAt": "2020-01-16T13:33:35.806Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60820e"
        },
        "nombre": "San Andrés Sotavento",
        "descripcion": "San Andrés Sotavento",
        "abreviacion": "San Andrés Sotavento",
        "createdAt": "2020-01-16T13:33:35.809Z",
        "updatedAt": "2020-01-16T13:33:35.809Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608213"
        },
        "nombre": "San Pelayo",
        "descripcion": "San Pelayo",
        "abreviacion": "San Pelayo",
        "createdAt": "2020-01-16T13:33:35.815Z",
        "updatedAt": "2020-01-16T13:33:35.815Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608218"
        },
        "nombre": "Anapoima",
        "descripcion": "Anapoima",
        "abreviacion": "Anapoima",
        "createdAt": "2020-01-16T13:33:35.820Z",
        "updatedAt": "2020-01-16T13:33:35.820Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60821d"
        },
        "nombre": "Cabrera",
        "descripcion": "Cabrera",
        "abreviacion": "Cabrera",
        "createdAt": "2020-01-16T13:33:35.824Z",
        "updatedAt": "2020-01-16T13:33:35.824Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608222"
        },
        "nombre": "La Apartada",
        "descripcion": "La Apartada",
        "abreviacion": "La Apartada",
        "createdAt": "2020-01-16T13:33:35.831Z",
        "updatedAt": "2020-01-16T13:33:35.831Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608227"
        },
        "nombre": "Cogua",
        "descripcion": "Cogua",
        "abreviacion": "Cogua",
        "createdAt": "2020-01-16T13:33:35.837Z",
        "updatedAt": "2020-01-16T13:33:35.837Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60822c"
        },
        "nombre": "Fomeque",
        "descripcion": "Fomeque",
        "abreviacion": "Fomeque",
        "createdAt": "2020-01-16T13:33:35.845Z",
        "updatedAt": "2020-01-16T13:33:35.845Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608231"
        },
        "nombre": "Gachancipá",
        "descripcion": "Gachancipá",
        "abreviacion": "Gachancipá",
        "createdAt": "2020-01-16T13:33:35.849Z",
        "updatedAt": "2020-01-16T13:33:35.849Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608236"
        },
        "nombre": "Guachetá",
        "descripcion": "Guachetá",
        "abreviacion": "Guachetá",
        "createdAt": "2020-01-16T13:33:35.853Z",
        "updatedAt": "2020-01-16T13:33:35.853Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60823b"
        },
        "nombre": "Fusagasugá",
        "descripcion": "Fusagasugá",
        "abreviacion": "Fusagasugá",
        "createdAt": "2020-01-16T13:33:35.856Z",
        "updatedAt": "2020-01-16T13:33:35.856Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608240"
        },
        "nombre": "La Calera",
        "descripcion": "La Calera",
        "abreviacion": "La Calera",
        "createdAt": "2020-01-16T13:33:35.859Z",
        "updatedAt": "2020-01-16T13:33:35.859Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608245"
        },
        "nombre": "Lenguazaque",
        "descripcion": "Lenguazaque",
        "abreviacion": "Lenguazaque",
        "createdAt": "2020-01-16T13:33:35.861Z",
        "updatedAt": "2020-01-16T13:33:35.861Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60824a"
        },
        "nombre": "Mosquera",
        "descripcion": "Mosquera",
        "abreviacion": "Mosquera",
        "createdAt": "2020-01-16T13:33:35.864Z",
        "updatedAt": "2020-01-16T13:33:35.864Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60824f"
        },
        "nombre": "Nocaima",
        "descripcion": "Nocaima",
        "abreviacion": "Nocaima",
        "createdAt": "2020-01-16T13:33:35.867Z",
        "updatedAt": "2020-01-16T13:33:35.867Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608253"
        },
        "nombre": "Pandi",
        "descripcion": "Pandi",
        "abreviacion": "Pandi",
        "createdAt": "2020-01-16T13:33:35.869Z",
        "updatedAt": "2020-01-16T13:33:35.869Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608258"
        },
        "nombre": "Quebradanegra",
        "descripcion": "Quebradanegra",
        "abreviacion": "Quebradanegra",
        "createdAt": "2020-01-16T13:33:35.872Z",
        "updatedAt": "2020-01-16T13:33:35.872Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60825d"
        },
        "nombre": "Zambrano",
        "descripcion": "Zambrano",
        "abreviacion": "Zambrano",
        "createdAt": "2020-01-16T13:33:35.875Z",
        "updatedAt": "2020-01-16T13:33:35.875Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608262"
        },
        "nombre": "Zipaquirá",
        "descripcion": "Zipaquirá",
        "abreviacion": "Zipaquirá",
        "createdAt": "2020-01-16T13:33:35.878Z",
        "updatedAt": "2020-01-16T13:33:35.878Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608267"
        },
        "nombre": "Soacha",
        "descripcion": "Soacha",
        "abreviacion": "Soacha",
        "createdAt": "2020-01-16T13:33:35.881Z",
        "updatedAt": "2020-01-16T13:33:35.881Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60826c"
        },
        "nombre": "Sutatausa",
        "descripcion": "Sutatausa",
        "abreviacion": "Sutatausa",
        "createdAt": "2020-01-16T13:33:35.884Z",
        "updatedAt": "2020-01-16T13:33:35.884Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608271"
        },
        "nombre": "Tenjo",
        "descripcion": "Tenjo",
        "abreviacion": "Tenjo",
        "createdAt": "2020-01-16T13:33:35.888Z",
        "updatedAt": "2020-01-16T13:33:35.888Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608276"
        },
        "nombre": "Topaipí",
        "descripcion": "Topaipí",
        "abreviacion": "Topaipí",
        "createdAt": "2020-01-16T13:33:35.891Z",
        "updatedAt": "2020-01-16T13:33:35.891Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60827b"
        },
        "nombre": "Útica",
        "descripcion": "Útica",
        "abreviacion": "Útica",
        "createdAt": "2020-01-16T13:33:35.894Z",
        "updatedAt": "2020-01-16T13:33:35.894Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608280"
        },
        "nombre": "Villeta",
        "descripcion": "Villeta",
        "abreviacion": "Villeta",
        "createdAt": "2020-01-16T13:33:35.900Z",
        "updatedAt": "2020-01-16T13:33:35.900Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608254"
        },
        "nombre": "Paratebueno",
        "descripcion": "Paratebueno",
        "abreviacion": "Paratebueno",
        "createdAt": "2020-01-16T13:33:35.870Z",
        "updatedAt": "2020-01-16T13:33:35.870Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608285"
        },
        "nombre": "Alto Baudo",
        "descripcion": "Alto Baudo",
        "abreviacion": "Alto Baudo",
        "createdAt": "2020-01-16T13:33:35.903Z",
        "updatedAt": "2020-01-16T13:33:35.903Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608259"
        },
        "nombre": "Quetame",
        "descripcion": "Quetame",
        "abreviacion": "Quetame",
        "createdAt": "2020-01-16T13:33:35.873Z",
        "updatedAt": "2020-01-16T13:33:35.873Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60828a"
        },
        "nombre": "Belén",
        "descripcion": "Belén",
        "abreviacion": "Belén",
        "createdAt": "2020-01-16T13:33:35.905Z",
        "updatedAt": "2020-01-16T13:33:35.905Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60825e"
        },
        "nombre": "San Bernardo",
        "descripcion": "San Bernardo",
        "abreviacion": "San Bernardo",
        "createdAt": "2020-01-16T13:33:35.875Z",
        "updatedAt": "2020-01-16T13:33:35.875Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60828f"
        },
        "nombre": "Condoto",
        "descripcion": "Condoto",
        "abreviacion": "Condoto",
        "createdAt": "2020-01-16T13:33:35.908Z",
        "updatedAt": "2020-01-16T13:33:35.908Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608263"
        },
        "nombre": "Sesquilé",
        "descripcion": "Sesquilé",
        "abreviacion": "Sesquilé",
        "createdAt": "2020-01-16T13:33:35.879Z",
        "updatedAt": "2020-01-16T13:33:35.879Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608294"
        },
        "nombre": "Medio Atrato",
        "descripcion": "Medio Atrato",
        "abreviacion": "Medio Atrato",
        "createdAt": "2020-01-16T13:33:35.911Z",
        "updatedAt": "2020-01-16T13:33:35.911Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608299"
        },
        "nombre": "Río Iro",
        "descripcion": "Río Iro",
        "abreviacion": "Río Iro",
        "createdAt": "2020-01-16T13:33:35.914Z",
        "updatedAt": "2020-01-16T13:33:35.914Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608268"
        },
        "nombre": "Subachoque",
        "descripcion": "Subachoque",
        "abreviacion": "Subachoque",
        "createdAt": "2020-01-16T13:33:35.882Z",
        "updatedAt": "2020-01-16T13:33:35.882Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60829e"
        },
        "nombre": "Unguía",
        "descripcion": "Unguía",
        "abreviacion": "Unguía",
        "createdAt": "2020-01-16T13:33:35.917Z",
        "updatedAt": "2020-01-16T13:33:35.917Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60826d"
        },
        "nombre": "Tabio",
        "descripcion": "Tabio",
        "abreviacion": "Tabio",
        "createdAt": "2020-01-16T13:33:35.885Z",
        "updatedAt": "2020-01-16T13:33:35.885Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082a3"
        },
        "nombre": "Algeciras",
        "descripcion": "Algeciras",
        "abreviacion": "Algeciras",
        "createdAt": "2020-01-16T13:33:35.920Z",
        "updatedAt": "2020-01-16T13:33:35.920Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608272"
        },
        "nombre": "Tibacuy",
        "descripcion": "Tibacuy",
        "abreviacion": "Tibacuy",
        "createdAt": "2020-01-16T13:33:35.889Z",
        "updatedAt": "2020-01-16T13:33:35.889Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082a8"
        },
        "nombre": "Mapiripana",
        "descripcion": "Mapiripana",
        "abreviacion": "Mapiripana",
        "createdAt": "2020-01-16T13:33:35.923Z",
        "updatedAt": "2020-01-16T13:33:35.923Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608277"
        },
        "nombre": "Ubalá",
        "descripcion": "Ubalá",
        "abreviacion": "Ubalá",
        "createdAt": "2020-01-16T13:33:35.892Z",
        "updatedAt": "2020-01-16T13:33:35.892Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082ad"
        },
        "nombre": "Pana Pana",
        "descripcion": "Pana Pana",
        "abreviacion": "Pana Pana",
        "createdAt": "2020-01-16T13:33:35.926Z",
        "updatedAt": "2020-01-16T13:33:35.926Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60827c"
        },
        "nombre": "Castilla la Nueva",
        "descripcion": "Castilla la Nueva",
        "abreviacion": "Castilla la Nueva",
        "createdAt": "2020-01-16T13:33:35.895Z",
        "updatedAt": "2020-01-16T13:33:35.895Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082b2"
        },
        "nombre": "Taraira",
        "descripcion": "Taraira",
        "abreviacion": "Taraira",
        "createdAt": "2020-01-16T13:33:35.929Z",
        "updatedAt": "2020-01-16T13:33:35.929Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608281"
        },
        "nombre": "Viotá",
        "descripcion": "Viotá",
        "abreviacion": "Viotá",
        "createdAt": "2020-01-16T13:33:35.900Z",
        "updatedAt": "2020-01-16T13:33:35.900Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082b7"
        },
        "nombre": "Cumaribo",
        "descripcion": "Cumaribo",
        "abreviacion": "Cumaribo",
        "createdAt": "2020-01-16T13:33:35.933Z",
        "updatedAt": "2020-01-16T13:33:35.933Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608286"
        },
        "nombre": "Atrato",
        "descripcion": "Atrato",
        "abreviacion": "Atrato",
        "createdAt": "2020-01-16T13:33:35.903Z",
        "updatedAt": "2020-01-16T13:33:35.903Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082bc"
        },
        "nombre": "Juan de Acosta",
        "descripcion": "Juan de Acosta",
        "abreviacion": "Juan de Acosta",
        "createdAt": "2020-01-16T13:33:35.937Z",
        "updatedAt": "2020-01-16T13:33:35.937Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608252"
        },
        "nombre": "Paime",
        "descripcion": "Paime",
        "abreviacion": "Paime",
        "createdAt": "2020-01-16T13:33:35.869Z",
        "updatedAt": "2020-01-16T13:33:35.869Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608257"
        },
        "nombre": "Pulí",
        "descripcion": "Pulí",
        "abreviacion": "Pulí",
        "createdAt": "2020-01-16T13:33:35.871Z",
        "updatedAt": "2020-01-16T13:33:35.871Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60825c"
        },
        "nombre": "Ricaurte",
        "descripcion": "Ricaurte",
        "abreviacion": "Ricaurte",
        "createdAt": "2020-01-16T13:33:35.874Z",
        "updatedAt": "2020-01-16T13:33:35.874Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608261"
        },
        "nombre": "La Uvita",
        "descripcion": "La Uvita",
        "abreviacion": "La Uvita",
        "createdAt": "2020-01-16T13:33:35.877Z",
        "updatedAt": "2020-01-16T13:33:35.877Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608266"
        },
        "nombre": "Simijaca",
        "descripcion": "Simijaca",
        "abreviacion": "Simijaca",
        "createdAt": "2020-01-16T13:33:35.881Z",
        "updatedAt": "2020-01-16T13:33:35.881Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60826b"
        },
        "nombre": "Susa",
        "descripcion": "Susa",
        "abreviacion": "Susa",
        "createdAt": "2020-01-16T13:33:35.884Z",
        "updatedAt": "2020-01-16T13:33:35.884Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608270"
        },
        "nombre": "Tena",
        "descripcion": "Tena",
        "abreviacion": "Tena",
        "createdAt": "2020-01-16T13:33:35.887Z",
        "updatedAt": "2020-01-16T13:33:35.887Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608275"
        },
        "nombre": "Tocancipá",
        "descripcion": "Tocancipá",
        "abreviacion": "Tocancipá",
        "createdAt": "2020-01-16T13:33:35.891Z",
        "updatedAt": "2020-01-16T13:33:35.891Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60827a"
        },
        "nombre": "Une",
        "descripcion": "Une",
        "abreviacion": "Une",
        "createdAt": "2020-01-16T13:33:35.894Z",
        "updatedAt": "2020-01-16T13:33:35.894Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60827f"
        },
        "nombre": "Villapinzón",
        "descripcion": "Villapinzón",
        "abreviacion": "Villapinzón",
        "createdAt": "2020-01-16T13:33:35.899Z",
        "updatedAt": "2020-01-16T13:33:35.899Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608284"
        },
        "nombre": "Acandí",
        "descripcion": "Acandí",
        "abreviacion": "Acandí",
        "createdAt": "2020-01-16T13:33:35.902Z",
        "updatedAt": "2020-01-16T13:33:35.902Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608289"
        },
        "nombre": "Bajo Baudó",
        "descripcion": "Bajo Baudó",
        "abreviacion": "Bajo Baudó",
        "createdAt": "2020-01-16T13:33:35.905Z",
        "updatedAt": "2020-01-16T13:33:35.905Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60828e"
        },
        "nombre": "Cértegui",
        "descripcion": "Cértegui",
        "abreviacion": "Cértegui",
        "createdAt": "2020-01-16T13:33:35.908Z",
        "updatedAt": "2020-01-16T13:33:35.908Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608293"
        },
        "nombre": "Lloró",
        "descripcion": "Lloró",
        "abreviacion": "Lloró",
        "createdAt": "2020-01-16T13:33:35.910Z",
        "updatedAt": "2020-01-16T13:33:35.910Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608298"
        },
        "nombre": "Nuquí",
        "descripcion": "Nuquí",
        "abreviacion": "Nuquí",
        "createdAt": "2020-01-16T13:33:35.913Z",
        "updatedAt": "2020-01-16T13:33:35.913Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082c1"
        },
        "nombre": "Barranco de Loba",
        "descripcion": "Barranco de Loba",
        "abreviacion": "Barranco de Loba",
        "createdAt": "2020-01-16T13:33:35.940Z",
        "updatedAt": "2020-01-16T13:33:35.940Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60829d"
        },
        "nombre": "Sipí",
        "descripcion": "Sipí",
        "abreviacion": "Sipí",
        "createdAt": "2020-01-16T13:33:35.916Z",
        "updatedAt": "2020-01-16T13:33:35.916Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082c6"
        },
        "nombre": "San Sebastián de Buenavista",
        "descripcion": "San Sebastián de Buenavista",
        "abreviacion": "San Sebastián de Buenavista",
        "createdAt": "2020-01-16T13:33:35.943Z",
        "updatedAt": "2020-01-16T13:33:35.943Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082a2"
        },
        "nombre": "Aipe",
        "descripcion": "Aipe",
        "abreviacion": "Aipe",
        "createdAt": "2020-01-16T13:33:35.919Z",
        "updatedAt": "2020-01-16T13:33:35.919Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082cb"
        },
        "nombre": "Río de Oro",
        "descripcion": "Río de Oro",
        "abreviacion": "Río de Oro",
        "createdAt": "2020-01-16T13:33:35.946Z",
        "updatedAt": "2020-01-16T13:33:35.946Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082a7"
        },
        "nombre": "Barranco Minas",
        "descripcion": "Barranco Minas",
        "abreviacion": "Barranco Minas",
        "createdAt": "2020-01-16T13:33:35.923Z",
        "updatedAt": "2020-01-16T13:33:35.923Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082d0"
        },
        "nombre": "Miraflores",
        "descripcion": "Miraflores",
        "abreviacion": "Miraflores",
        "createdAt": "2020-01-16T13:33:35.950Z",
        "updatedAt": "2020-01-16T13:33:35.950Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082ac"
        },
        "nombre": "Cacahual",
        "descripcion": "Cacahual",
        "abreviacion": "Cacahual",
        "createdAt": "2020-01-16T13:33:35.925Z",
        "updatedAt": "2020-01-16T13:33:35.925Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082d5"
        },
        "nombre": "San Andrés de Cuerquía",
        "descripcion": "San Andrés de Cuerquía",
        "abreviacion": "San Andrés de Cuerquía",
        "createdAt": "2020-01-16T13:33:35.955Z",
        "updatedAt": "2020-01-16T13:33:35.955Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082b1"
        },
        "nombre": "Pacoa",
        "descripcion": "Pacoa",
        "abreviacion": "Pacoa",
        "createdAt": "2020-01-16T13:33:35.928Z",
        "updatedAt": "2020-01-16T13:33:35.928Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082da"
        },
        "nombre": "Santa Rosa de Cabal",
        "descripcion": "Santa Rosa de Cabal",
        "abreviacion": "Santa Rosa de Cabal",
        "createdAt": "2020-01-16T13:33:35.961Z",
        "updatedAt": "2020-01-16T13:33:35.961Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082b6"
        },
        "nombre": "Santa Rosalía",
        "descripcion": "Santa Rosalía",
        "abreviacion": "Santa Rosalía",
        "createdAt": "2020-01-16T13:33:35.933Z",
        "updatedAt": "2020-01-16T13:33:35.933Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082df"
        },
        "nombre": "San Pablo de Borbur",
        "descripcion": "San Pablo de Borbur",
        "abreviacion": "San Pablo de Borbur",
        "createdAt": "2020-01-16T13:33:35.966Z",
        "updatedAt": "2020-01-16T13:33:35.966Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082bb"
        },
        "nombre": "San Juan de Río Seco",
        "descripcion": "San Juan de Río Seco",
        "abreviacion": "San Juan de Río Seco",
        "createdAt": "2020-01-16T13:33:35.936Z",
        "updatedAt": "2020-01-16T13:33:35.936Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082e4"
        },
        "nombre": "El Carmen de Bolívar",
        "descripcion": "El Carmen de Bolívar",
        "abreviacion": "El Carmen de Bolívar",
        "createdAt": "2020-01-16T13:33:35.971Z",
        "updatedAt": "2020-01-16T13:33:35.971Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082c0"
        },
        "nombre": "Villa de San Diego de Ubate",
        "descripcion": "Villa de San Diego de Ubate",
        "abreviacion": "Villa de San Diego de Ubate",
        "createdAt": "2020-01-16T13:33:35.939Z",
        "updatedAt": "2020-01-16T13:33:35.939Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082e9"
        },
        "nombre": "San Martín de Loba",
        "descripcion": "San Martín de Loba",
        "abreviacion": "San Martín de Loba",
        "createdAt": "2020-01-16T13:33:35.975Z",
        "updatedAt": "2020-01-16T13:33:35.975Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082c5"
        },
        "nombre": "Villa de Leyva",
        "descripcion": "Villa de Leyva",
        "abreviacion": "Villa de Leyva",
        "createdAt": "2020-01-16T13:33:35.942Z",
        "updatedAt": "2020-01-16T13:33:35.942Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60828b"
        },
        "nombre": "Bojaya",
        "descripcion": "Bojaya",
        "abreviacion": "Bojaya",
        "createdAt": "2020-01-16T13:33:35.906Z",
        "updatedAt": "2020-01-16T13:33:35.906Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082ca"
        },
        "nombre": "Calamar",
        "descripcion": "Calamar",
        "abreviacion": "Calamar",
        "createdAt": "2020-01-16T13:33:35.945Z",
        "updatedAt": "2020-01-16T13:33:35.945Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082ee"
        },
        "nombre": "El Retorno",
        "descripcion": "El Retorno",
        "abreviacion": "El Retorno",
        "createdAt": "2020-01-16T13:33:35.978Z",
        "updatedAt": "2020-01-16T13:33:35.978Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082cf"
        },
        "nombre": "Santander de Quilichao",
        "descripcion": "Santander de Quilichao",
        "abreviacion": "Santander de Quilichao",
        "createdAt": "2020-01-16T13:33:35.949Z",
        "updatedAt": "2020-01-16T13:33:35.949Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608290"
        },
        "nombre": "Villagarzón",
        "descripcion": "Villagarzón",
        "abreviacion": "Villagarzón",
        "createdAt": "2020-01-16T13:33:35.909Z",
        "updatedAt": "2020-01-16T13:33:35.909Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082d4"
        },
        "nombre": "Santa Rosa de Osos",
        "descripcion": "Santa Rosa de Osos",
        "abreviacion": "Santa Rosa de Osos",
        "createdAt": "2020-01-16T13:33:35.954Z",
        "updatedAt": "2020-01-16T13:33:35.954Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082f3"
        },
        "nombre": "San José de La Montaña",
        "descripcion": "San José de La Montaña",
        "abreviacion": "San José de La Montaña",
        "createdAt": "2020-01-16T13:33:35.981Z",
        "updatedAt": "2020-01-16T13:33:35.981Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082d9"
        },
        "nombre": "Providencia",
        "descripcion": "Providencia",
        "abreviacion": "Providencia",
        "createdAt": "2020-01-16T13:33:35.960Z",
        "updatedAt": "2020-01-16T13:33:35.960Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608295"
        },
        "nombre": "Medio Baudó",
        "descripcion": "Medio Baudó",
        "abreviacion": "Medio Baudó",
        "createdAt": "2020-01-16T13:33:35.911Z",
        "updatedAt": "2020-01-16T13:33:35.911Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082de"
        },
        "nombre": "Santa Helena del Opón",
        "descripcion": "Santa Helena del Opón",
        "abreviacion": "Santa Helena del Opón",
        "createdAt": "2020-01-16T13:33:35.965Z",
        "updatedAt": "2020-01-16T13:33:35.965Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608247"
        },
        "nombre": "Madrid",
        "descripcion": "Madrid",
        "abreviacion": "Madrid",
        "createdAt": "2020-01-16T13:33:35.863Z",
        "updatedAt": "2020-01-16T13:33:35.863Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60824c"
        },
        "nombre": "Nemocón",
        "descripcion": "Nemocón",
        "abreviacion": "Nemocón",
        "createdAt": "2020-01-16T13:33:35.865Z",
        "updatedAt": "2020-01-16T13:33:35.865Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608251"
        },
        "nombre": "Pacho",
        "descripcion": "Pacho",
        "abreviacion": "Pacho",
        "createdAt": "2020-01-16T13:33:35.868Z",
        "updatedAt": "2020-01-16T13:33:35.868Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608256"
        },
        "nombre": "Puerto Salgar",
        "descripcion": "Puerto Salgar",
        "abreviacion": "Puerto Salgar",
        "createdAt": "2020-01-16T13:33:35.871Z",
        "updatedAt": "2020-01-16T13:33:35.871Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60825b"
        },
        "nombre": "Apulo",
        "descripcion": "Apulo",
        "abreviacion": "Apulo",
        "createdAt": "2020-01-16T13:33:35.874Z",
        "updatedAt": "2020-01-16T13:33:35.874Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608260"
        },
        "nombre": "San Francisco",
        "descripcion": "San Francisco",
        "abreviacion": "San Francisco",
        "createdAt": "2020-01-16T13:33:35.876Z",
        "updatedAt": "2020-01-16T13:33:35.876Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608265"
        },
        "nombre": "Silvania",
        "descripcion": "Silvania",
        "abreviacion": "Silvania",
        "createdAt": "2020-01-16T13:33:35.880Z",
        "updatedAt": "2020-01-16T13:33:35.880Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60826a"
        },
        "nombre": "Supatá",
        "descripcion": "Supatá",
        "abreviacion": "Supatá",
        "createdAt": "2020-01-16T13:33:35.883Z",
        "updatedAt": "2020-01-16T13:33:35.883Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60826f"
        },
        "nombre": "Tausa",
        "descripcion": "Tausa",
        "abreviacion": "Tausa",
        "createdAt": "2020-01-16T13:33:35.887Z",
        "updatedAt": "2020-01-16T13:33:35.887Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608274"
        },
        "nombre": "Tocaima",
        "descripcion": "Tocaima",
        "abreviacion": "Tocaima",
        "createdAt": "2020-01-16T13:33:35.890Z",
        "updatedAt": "2020-01-16T13:33:35.890Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608279"
        },
        "nombre": "Suárez",
        "descripcion": "Suárez",
        "abreviacion": "Suárez",
        "createdAt": "2020-01-16T13:33:35.893Z",
        "updatedAt": "2020-01-16T13:33:35.893Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60827e"
        },
        "nombre": "Villagómez",
        "descripcion": "Villagómez",
        "abreviacion": "Villagómez",
        "createdAt": "2020-01-16T13:33:35.898Z",
        "updatedAt": "2020-01-16T13:33:35.898Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608283"
        },
        "nombre": "Quibdó",
        "descripcion": "Quibdó",
        "abreviacion": "Quibdó",
        "createdAt": "2020-01-16T13:33:35.901Z",
        "updatedAt": "2020-01-16T13:33:35.901Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608288"
        },
        "nombre": "Bahía Solano",
        "descripcion": "Bahía Solano",
        "abreviacion": "Bahía Solano",
        "createdAt": "2020-01-16T13:33:35.904Z",
        "updatedAt": "2020-01-16T13:33:35.904Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60828d"
        },
        "nombre": "Pueblo Viejo",
        "descripcion": "Pueblo Viejo",
        "abreviacion": "Pueblo Viejo",
        "createdAt": "2020-01-16T13:33:35.907Z",
        "updatedAt": "2020-01-16T13:33:35.907Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608292"
        },
        "nombre": "Juradó",
        "descripcion": "Juradó",
        "abreviacion": "Juradó",
        "createdAt": "2020-01-16T13:33:35.910Z",
        "updatedAt": "2020-01-16T13:33:35.910Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608297"
        },
        "nombre": "Nóvita",
        "descripcion": "Nóvita",
        "abreviacion": "Nóvita",
        "createdAt": "2020-01-16T13:33:35.913Z",
        "updatedAt": "2020-01-16T13:33:35.913Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60829c"
        },
        "nombre": "Puerto Libertador",
        "descripcion": "Puerto Libertador",
        "abreviacion": "Puerto Libertador",
        "createdAt": "2020-01-16T13:33:35.916Z",
        "updatedAt": "2020-01-16T13:33:35.916Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082a1"
        },
        "nombre": "Agrado",
        "descripcion": "Agrado",
        "abreviacion": "Agrado",
        "createdAt": "2020-01-16T13:33:35.918Z",
        "updatedAt": "2020-01-16T13:33:35.918Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082a6"
        },
        "nombre": "Inírida",
        "descripcion": "Inírida",
        "abreviacion": "Inírida",
        "createdAt": "2020-01-16T13:33:35.922Z",
        "updatedAt": "2020-01-16T13:33:35.922Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082ab"
        },
        "nombre": "La Guadalupe",
        "descripcion": "La Guadalupe",
        "abreviacion": "La Guadalupe",
        "createdAt": "2020-01-16T13:33:35.925Z",
        "updatedAt": "2020-01-16T13:33:35.925Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082e3"
        },
        "nombre": "San Luis de Gaceno",
        "descripcion": "San Luis de Gaceno",
        "abreviacion": "San Luis de Gaceno",
        "createdAt": "2020-01-16T13:33:35.970Z",
        "updatedAt": "2020-01-16T13:33:35.970Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082b0"
        },
        "nombre": "Caruru",
        "descripcion": "Caruru",
        "abreviacion": "Caruru",
        "createdAt": "2020-01-16T13:33:35.928Z",
        "updatedAt": "2020-01-16T13:33:35.928Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082e8"
        },
        "nombre": "Vigía del Fuerte",
        "descripcion": "Vigía del Fuerte",
        "abreviacion": "Vigía del Fuerte",
        "createdAt": "2020-01-16T13:33:35.974Z",
        "updatedAt": "2020-01-16T13:33:35.974Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082b5"
        },
        "nombre": "La Primavera",
        "descripcion": "La Primavera",
        "abreviacion": "La Primavera",
        "createdAt": "2020-01-16T13:33:35.932Z",
        "updatedAt": "2020-01-16T13:33:35.932Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082ed"
        },
        "nombre": "Sabana de Torres",
        "descripcion": "Sabana de Torres",
        "abreviacion": "Sabana de Torres",
        "createdAt": "2020-01-16T13:33:35.977Z",
        "updatedAt": "2020-01-16T13:33:35.977Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082ba"
        },
        "nombre": "Palmas del Socorro",
        "descripcion": "Palmas del Socorro",
        "abreviacion": "Palmas del Socorro",
        "createdAt": "2020-01-16T13:33:35.935Z",
        "updatedAt": "2020-01-16T13:33:35.935Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082f2"
        },
        "nombre": "San Juan de Arama",
        "descripcion": "San Juan de Arama",
        "abreviacion": "San Juan de Arama",
        "createdAt": "2020-01-16T13:33:35.980Z",
        "updatedAt": "2020-01-16T13:33:35.980Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082bf"
        },
        "nombre": "El Litoral del San Juan",
        "descripcion": "El Litoral del San Juan",
        "abreviacion": "El Litoral del San Juan",
        "createdAt": "2020-01-16T13:33:35.939Z",
        "updatedAt": "2020-01-16T13:33:35.939Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082f7"
        },
        "nombre": "San Jacinto del Cauca",
        "descripcion": "San Jacinto del Cauca",
        "abreviacion": "San Jacinto del Cauca",
        "createdAt": "2020-01-16T13:33:35.983Z",
        "updatedAt": "2020-01-16T13:33:35.983Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082c4"
        },
        "nombre": "El Cantón del San Pablo",
        "descripcion": "El Cantón del San Pablo",
        "abreviacion": "El Cantón del San Pablo",
        "createdAt": "2020-01-16T13:33:35.942Z",
        "updatedAt": "2020-01-16T13:33:35.942Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082fc"
        },
        "nombre": "Valle de Guamez",
        "descripcion": "Valle de Guamez",
        "abreviacion": "Valle de Guamez",
        "createdAt": "2020-01-16T13:33:35.987Z",
        "updatedAt": "2020-01-16T13:33:35.987Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082c9"
        },
        "nombre": "Sabanas de San Angel",
        "descripcion": "Sabanas de San Angel",
        "abreviacion": "Sabanas de San Angel",
        "createdAt": "2020-01-16T13:33:35.945Z",
        "updatedAt": "2020-01-16T13:33:35.945Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082ce"
        },
        "nombre": "Santa Rosa de Viterbo",
        "descripcion": "Santa Rosa de Viterbo",
        "abreviacion": "Santa Rosa de Viterbo",
        "createdAt": "2020-01-16T13:33:35.948Z",
        "updatedAt": "2020-01-16T13:33:35.948Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608301"
        },
        "nombre": "Ciénaga de Oro",
        "descripcion": "Ciénaga de Oro",
        "abreviacion": "Ciénaga de Oro",
        "createdAt": "2020-01-16T13:33:35.990Z",
        "updatedAt": "2020-01-16T13:33:35.990Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082d3"
        },
        "nombre": "Palmar de Varela",
        "descripcion": "Palmar de Varela",
        "abreviacion": "Palmar de Varela",
        "createdAt": "2020-01-16T13:33:35.952Z",
        "updatedAt": "2020-01-16T13:33:35.952Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608306"
        },
        "nombre": "Belén de Umbría",
        "descripcion": "Belén de Umbría",
        "abreviacion": "Belén de Umbría",
        "createdAt": "2020-01-16T13:33:35.992Z",
        "updatedAt": "2020-01-16T13:33:35.992Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082d8"
        },
        "nombre": "San José de Miranda",
        "descripcion": "San José de Miranda",
        "abreviacion": "San José de Miranda",
        "createdAt": "2020-01-16T13:33:35.959Z",
        "updatedAt": "2020-01-16T13:33:35.959Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60830b"
        },
        "nombre": "San Antonio",
        "descripcion": "San Antonio",
        "abreviacion": "San Antonio",
        "createdAt": "2020-01-16T13:33:35.995Z",
        "updatedAt": "2020-01-16T13:33:35.995Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082dd"
        },
        "nombre": "Paz de Ariporo",
        "descripcion": "Paz de Ariporo",
        "abreviacion": "Paz de Ariporo",
        "createdAt": "2020-01-16T13:33:35.964Z",
        "updatedAt": "2020-01-16T13:33:35.964Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082f8"
        },
        "nombre": "San Agustín",
        "descripcion": "San Agustín",
        "abreviacion": "San Agustín",
        "createdAt": "2020-01-16T13:33:35.984Z",
        "updatedAt": "2020-01-16T13:33:35.984Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082e2"
        },
        "nombre": "San Luis de Sincé",
        "descripcion": "San Luis de Sincé",
        "abreviacion": "San Luis de Sincé",
        "createdAt": "2020-01-16T13:33:35.969Z",
        "updatedAt": "2020-01-16T13:33:35.969Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60829a"
        },
        "nombre": "Río Quito",
        "descripcion": "Río Quito",
        "abreviacion": "Río Quito",
        "createdAt": "2020-01-16T13:33:35.914Z",
        "updatedAt": "2020-01-16T13:33:35.914Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082e7"
        },
        "nombre": "Pijiño del Carmen",
        "descripcion": "Pijiño del Carmen",
        "abreviacion": "Pijiño del Carmen",
        "createdAt": "2020-01-16T13:33:35.973Z",
        "updatedAt": "2020-01-16T13:33:35.973Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082fd"
        },
        "nombre": "San Pablo de Borbur",
        "descripcion": "San Pablo de Borbur",
        "abreviacion": "San Pablo de Borbur",
        "createdAt": "2020-01-16T13:33:35.987Z",
        "updatedAt": "2020-01-16T13:33:35.987Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082ec"
        },
        "nombre": "San Antonio del Tequendama",
        "descripcion": "San Antonio del Tequendama",
        "abreviacion": "San Antonio del Tequendama",
        "createdAt": "2020-01-16T13:33:35.976Z",
        "updatedAt": "2020-01-16T13:33:35.976Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60829f"
        },
        "nombre": "Neiva",
        "descripcion": "Neiva",
        "abreviacion": "Neiva",
        "createdAt": "2020-01-16T13:33:35.917Z",
        "updatedAt": "2020-01-16T13:33:35.917Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082f1"
        },
        "nombre": "Campo de La Cruz",
        "descripcion": "Campo de La Cruz",
        "abreviacion": "Campo de La Cruz",
        "createdAt": "2020-01-16T13:33:35.979Z",
        "updatedAt": "2020-01-16T13:33:35.979Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608310"
        },
        "nombre": "Hato",
        "descripcion": "Hato",
        "abreviacion": "Hato",
        "createdAt": "2020-01-16T13:33:35.999Z",
        "updatedAt": "2020-01-16T13:33:35.999Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082f6"
        },
        "nombre": "Agua de Dios",
        "descripcion": "Agua de Dios",
        "abreviacion": "Agua de Dios",
        "createdAt": "2020-01-16T13:33:35.982Z",
        "updatedAt": "2020-01-16T13:33:35.982Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082a4"
        },
        "nombre": "Altamira",
        "descripcion": "Altamira",
        "abreviacion": "Altamira",
        "createdAt": "2020-01-16T13:33:35.921Z",
        "updatedAt": "2020-01-16T13:33:35.921Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082fb"
        },
        "nombre": "San José de Pare",
        "descripcion": "San José de Pare",
        "abreviacion": "San José de Pare",
        "createdAt": "2020-01-16T13:33:35.986Z",
        "updatedAt": "2020-01-16T13:33:35.986Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608302"
        },
        "nombre": "San Juan de Urabá",
        "descripcion": "San Juan de Urabá",
        "abreviacion": "San Juan de Urabá",
        "createdAt": "2020-01-16T13:33:35.990Z",
        "updatedAt": "2020-01-16T13:33:35.990Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608300"
        },
        "nombre": "Carmen de Carupa",
        "descripcion": "Carmen de Carupa",
        "abreviacion": "Carmen de Carupa",
        "createdAt": "2020-01-16T13:33:35.989Z",
        "updatedAt": "2020-01-16T13:33:35.989Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608315"
        },
        "nombre": "Silos",
        "descripcion": "Silos",
        "abreviacion": "Silos",
        "createdAt": "2020-01-16T13:33:36.002Z",
        "updatedAt": "2020-01-16T13:33:36.002Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608305"
        },
        "nombre": "El Carmen de Viboral",
        "descripcion": "El Carmen de Viboral",
        "abreviacion": "El Carmen de Viboral",
        "createdAt": "2020-01-16T13:33:35.992Z",
        "updatedAt": "2020-01-16T13:33:35.992Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082a9"
        },
        "nombre": "San Felipe",
        "descripcion": "San Felipe",
        "abreviacion": "San Felipe",
        "createdAt": "2020-01-16T13:33:35.924Z",
        "updatedAt": "2020-01-16T13:33:35.924Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60830a"
        },
        "nombre": "San Miguel de Sema",
        "descripcion": "San Miguel de Sema",
        "abreviacion": "San Miguel de Sema",
        "createdAt": "2020-01-16T13:33:35.995Z",
        "updatedAt": "2020-01-16T13:33:35.995Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608307"
        },
        "nombre": "Belén de Bajira",
        "descripcion": "Belén de Bajira",
        "abreviacion": "Belén de Bajira",
        "createdAt": "2020-01-16T13:33:35.993Z",
        "updatedAt": "2020-01-16T13:33:35.993Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60830f"
        },
        "nombre": "Puerto Alegría",
        "descripcion": "Puerto Alegría",
        "abreviacion": "Puerto Alegría",
        "createdAt": "2020-01-16T13:33:35.998Z",
        "updatedAt": "2020-01-16T13:33:35.998Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60831a"
        },
        "nombre": "Mutiscua",
        "descripcion": "Mutiscua",
        "abreviacion": "Mutiscua",
        "createdAt": "2020-01-16T13:33:36.005Z",
        "updatedAt": "2020-01-16T13:33:36.005Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608314"
        },
        "nombre": "Tuta",
        "descripcion": "Tuta",
        "abreviacion": "Tuta",
        "createdAt": "2020-01-16T13:33:36.001Z",
        "updatedAt": "2020-01-16T13:33:36.001Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082ae"
        },
        "nombre": "Morichal",
        "descripcion": "Morichal",
        "abreviacion": "Morichal",
        "createdAt": "2020-01-16T13:33:35.927Z",
        "updatedAt": "2020-01-16T13:33:35.927Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608319"
        },
        "nombre": "Roldanillo",
        "descripcion": "Roldanillo",
        "abreviacion": "Roldanillo",
        "createdAt": "2020-01-16T13:33:36.004Z",
        "updatedAt": "2020-01-16T13:33:36.004Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60830c"
        },
        "nombre": "San Benito",
        "descripcion": "San Benito",
        "abreviacion": "San Benito",
        "createdAt": "2020-01-16T13:33:35.996Z",
        "updatedAt": "2020-01-16T13:33:35.996Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60831e"
        },
        "nombre": "Sevilla",
        "descripcion": "Sevilla",
        "abreviacion": "Sevilla",
        "createdAt": "2020-01-16T13:33:36.007Z",
        "updatedAt": "2020-01-16T13:33:36.007Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60831f"
        },
        "nombre": "Zarzal",
        "descripcion": "Zarzal",
        "abreviacion": "Zarzal",
        "createdAt": "2020-01-16T13:33:36.011Z",
        "updatedAt": "2020-01-16T13:33:36.011Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082b3"
        },
        "nombre": "Papunaua",
        "descripcion": "Papunaua",
        "abreviacion": "Papunaua",
        "createdAt": "2020-01-16T13:33:35.930Z",
        "updatedAt": "2020-01-16T13:33:35.930Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608255"
        },
        "nombre": "Pasca",
        "descripcion": "Pasca",
        "abreviacion": "Pasca",
        "createdAt": "2020-01-16T13:33:35.870Z",
        "updatedAt": "2020-01-16T13:33:35.870Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60825a"
        },
        "nombre": "Quipile",
        "descripcion": "Quipile",
        "abreviacion": "Quipile",
        "createdAt": "2020-01-16T13:33:35.873Z",
        "updatedAt": "2020-01-16T13:33:35.873Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60825f"
        },
        "nombre": "San Cayetano",
        "descripcion": "San Cayetano",
        "abreviacion": "San Cayetano",
        "createdAt": "2020-01-16T13:33:35.876Z",
        "updatedAt": "2020-01-16T13:33:35.876Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608264"
        },
        "nombre": "Sibaté",
        "descripcion": "Sibaté",
        "abreviacion": "Sibaté",
        "createdAt": "2020-01-16T13:33:35.879Z",
        "updatedAt": "2020-01-16T13:33:35.879Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608269"
        },
        "nombre": "Suesca",
        "descripcion": "Suesca",
        "abreviacion": "Suesca",
        "createdAt": "2020-01-16T13:33:35.883Z",
        "updatedAt": "2020-01-16T13:33:35.883Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60826e"
        },
        "nombre": "Génova",
        "descripcion": "Génova",
        "abreviacion": "Génova",
        "createdAt": "2020-01-16T13:33:35.886Z",
        "updatedAt": "2020-01-16T13:33:35.886Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608273"
        },
        "nombre": "Tibirita",
        "descripcion": "Tibirita",
        "abreviacion": "Tibirita",
        "createdAt": "2020-01-16T13:33:35.889Z",
        "updatedAt": "2020-01-16T13:33:35.889Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608278"
        },
        "nombre": "Ubaque",
        "descripcion": "Ubaque",
        "abreviacion": "Ubaque",
        "createdAt": "2020-01-16T13:33:35.892Z",
        "updatedAt": "2020-01-16T13:33:35.892Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60827d"
        },
        "nombre": "Vianí",
        "descripcion": "Vianí",
        "abreviacion": "Vianí",
        "createdAt": "2020-01-16T13:33:35.895Z",
        "updatedAt": "2020-01-16T13:33:35.895Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608282"
        },
        "nombre": "Zipacón",
        "descripcion": "Zipacón",
        "abreviacion": "Zipacón",
        "createdAt": "2020-01-16T13:33:35.901Z",
        "updatedAt": "2020-01-16T13:33:35.901Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608287"
        },
        "nombre": "Bagadó",
        "descripcion": "Bagadó",
        "abreviacion": "Bagadó",
        "createdAt": "2020-01-16T13:33:35.904Z",
        "updatedAt": "2020-01-16T13:33:35.904Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60828c"
        },
        "nombre": "Unión Panamericana",
        "descripcion": "Unión Panamericana",
        "abreviacion": "Unión Panamericana",
        "createdAt": "2020-01-16T13:33:35.906Z",
        "updatedAt": "2020-01-16T13:33:35.906Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608291"
        },
        "nombre": "Facatativá",
        "descripcion": "Facatativá",
        "abreviacion": "Facatativá",
        "createdAt": "2020-01-16T13:33:35.909Z",
        "updatedAt": "2020-01-16T13:33:35.909Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608296"
        },
        "nombre": "Medio San Juan",
        "descripcion": "Medio San Juan",
        "abreviacion": "Medio San Juan",
        "createdAt": "2020-01-16T13:33:35.912Z",
        "updatedAt": "2020-01-16T13:33:35.912Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60829b"
        },
        "nombre": "Riosucio",
        "descripcion": "Riosucio",
        "abreviacion": "Riosucio",
        "createdAt": "2020-01-16T13:33:35.915Z",
        "updatedAt": "2020-01-16T13:33:35.915Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082a0"
        },
        "nombre": "Acevedo",
        "descripcion": "Acevedo",
        "abreviacion": "Acevedo",
        "createdAt": "2020-01-16T13:33:35.918Z",
        "updatedAt": "2020-01-16T13:33:35.918Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082a5"
        },
        "nombre": "Tarapacá",
        "descripcion": "Tarapacá",
        "abreviacion": "Tarapacá",
        "createdAt": "2020-01-16T13:33:35.921Z",
        "updatedAt": "2020-01-16T13:33:35.921Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608323"
        },
        "nombre": "Caicedonia",
        "descripcion": "Caicedonia",
        "abreviacion": "Caicedonia",
        "createdAt": "2020-01-16T13:33:36.014Z",
        "updatedAt": "2020-01-16T13:33:36.014Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608311"
        },
        "nombre": "San Jacinto",
        "descripcion": "San Jacinto",
        "abreviacion": "San Jacinto",
        "createdAt": "2020-01-16T13:33:36.000Z",
        "updatedAt": "2020-01-16T13:33:36.000Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608324"
        },
        "nombre": "Puerto Santander",
        "descripcion": "Puerto Santander",
        "abreviacion": "Puerto Santander",
        "createdAt": "2020-01-16T13:33:36.015Z",
        "updatedAt": "2020-01-16T13:33:36.015Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608328"
        },
        "nombre": "La Unión",
        "descripcion": "La Unión",
        "abreviacion": "La Unión",
        "createdAt": "2020-01-16T13:33:36.018Z",
        "updatedAt": "2020-01-16T13:33:36.018Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082b8"
        },
        "nombre": "San José del Fragua",
        "descripcion": "San José del Fragua",
        "abreviacion": "San José del Fragua",
        "createdAt": "2020-01-16T13:33:35.934Z",
        "updatedAt": "2020-01-16T13:33:35.934Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608316"
        },
        "nombre": "Cácota",
        "descripcion": "Cácota",
        "abreviacion": "Cácota",
        "createdAt": "2020-01-16T13:33:36.003Z",
        "updatedAt": "2020-01-16T13:33:36.003Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608329"
        },
        "nombre": "Restrepo",
        "descripcion": "Restrepo",
        "abreviacion": "Restrepo",
        "createdAt": "2020-01-16T13:33:36.018Z",
        "updatedAt": "2020-01-16T13:33:36.018Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082aa"
        },
        "nombre": "Puerto Colombia",
        "descripcion": "Puerto Colombia",
        "abreviacion": "Puerto Colombia",
        "createdAt": "2020-01-16T13:33:35.924Z",
        "updatedAt": "2020-01-16T13:33:35.924Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082af"
        },
        "nombre": "Mitú",
        "descripcion": "Mitú",
        "abreviacion": "Mitú",
        "createdAt": "2020-01-16T13:33:35.927Z",
        "updatedAt": "2020-01-16T13:33:35.927Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082b4"
        },
        "nombre": "Puerto Carreño",
        "descripcion": "Puerto Carreño",
        "abreviacion": "Puerto Carreño",
        "createdAt": "2020-01-16T13:33:35.931Z",
        "updatedAt": "2020-01-16T13:33:35.931Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082b9"
        },
        "nombre": "Barranca de Upía",
        "descripcion": "Barranca de Upía",
        "abreviacion": "Barranca de Upía",
        "createdAt": "2020-01-16T13:33:35.935Z",
        "updatedAt": "2020-01-16T13:33:35.935Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082be"
        },
        "nombre": "San Luis de Gaceno",
        "descripcion": "San Luis de Gaceno",
        "abreviacion": "San Luis de Gaceno",
        "createdAt": "2020-01-16T13:33:35.938Z",
        "updatedAt": "2020-01-16T13:33:35.938Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082c3"
        },
        "nombre": "Santa Rosa del Sur",
        "descripcion": "Santa Rosa del Sur",
        "abreviacion": "Santa Rosa del Sur",
        "createdAt": "2020-01-16T13:33:35.941Z",
        "updatedAt": "2020-01-16T13:33:35.941Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082c8"
        },
        "nombre": "Hatillo de Loba",
        "descripcion": "Hatillo de Loba",
        "abreviacion": "Hatillo de Loba",
        "createdAt": "2020-01-16T13:33:35.944Z",
        "updatedAt": "2020-01-16T13:33:35.944Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082cd"
        },
        "nombre": "San José del Guaviare",
        "descripcion": "San José del Guaviare",
        "abreviacion": "San José del Guaviare",
        "createdAt": "2020-01-16T13:33:35.947Z",
        "updatedAt": "2020-01-16T13:33:35.947Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082d2"
        },
        "nombre": "San Carlos de Guaroa",
        "descripcion": "San Carlos de Guaroa",
        "abreviacion": "San Carlos de Guaroa",
        "createdAt": "2020-01-16T13:33:35.951Z",
        "updatedAt": "2020-01-16T13:33:35.951Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082d7"
        },
        "nombre": "San Vicente de Chucurí",
        "descripcion": "San Vicente de Chucurí",
        "abreviacion": "San Vicente de Chucurí",
        "createdAt": "2020-01-16T13:33:35.958Z",
        "updatedAt": "2020-01-16T13:33:35.958Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082bd"
        },
        "nombre": "Fuente de Oro",
        "descripcion": "Fuente de Oro",
        "abreviacion": "Fuente de Oro",
        "createdAt": "2020-01-16T13:33:35.938Z",
        "updatedAt": "2020-01-16T13:33:35.938Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082dc"
        },
        "nombre": "Belén de Los Andaquies",
        "descripcion": "Belén de Los Andaquies",
        "abreviacion": "Belén de Los Andaquies",
        "createdAt": "2020-01-16T13:33:35.963Z",
        "updatedAt": "2020-01-16T13:33:35.963Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60832d"
        },
        "nombre": "Guacarí",
        "descripcion": "Guacarí",
        "abreviacion": "Guacarí",
        "createdAt": "2020-01-16T13:33:36.022Z",
        "updatedAt": "2020-01-16T13:33:36.022Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082e1"
        },
        "nombre": "La Jagua de Ibirico",
        "descripcion": "La Jagua de Ibirico",
        "abreviacion": "La Jagua de Ibirico",
        "createdAt": "2020-01-16T13:33:35.968Z",
        "updatedAt": "2020-01-16T13:33:35.968Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60831b"
        },
        "nombre": "Argelia",
        "descripcion": "Argelia",
        "abreviacion": "Argelia",
        "createdAt": "2020-01-16T13:33:36.005Z",
        "updatedAt": "2020-01-16T13:33:36.005Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082e6"
        },
        "nombre": "San Juan de Betulia",
        "descripcion": "San Juan de Betulia",
        "abreviacion": "San Juan de Betulia",
        "createdAt": "2020-01-16T13:33:35.973Z",
        "updatedAt": "2020-01-16T13:33:35.973Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082eb"
        },
        "nombre": "Carmen de Apicala",
        "descripcion": "Carmen de Apicala",
        "abreviacion": "Carmen de Apicala",
        "createdAt": "2020-01-16T13:33:35.976Z",
        "updatedAt": "2020-01-16T13:33:35.976Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082c2"
        },
        "nombre": "Togüí",
        "descripcion": "Togüí",
        "abreviacion": "Togüí",
        "createdAt": "2020-01-16T13:33:35.941Z",
        "updatedAt": "2020-01-16T13:33:35.941Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082f0"
        },
        "nombre": "San Pedro de Cartago",
        "descripcion": "San Pedro de Cartago",
        "abreviacion": "San Pedro de Cartago",
        "createdAt": "2020-01-16T13:33:35.979Z",
        "updatedAt": "2020-01-16T13:33:35.979Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608332"
        },
        "nombre": "Convención",
        "descripcion": "Convención",
        "abreviacion": "Convención",
        "createdAt": "2020-01-16T13:33:36.027Z",
        "updatedAt": "2020-01-16T13:33:36.027Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082f5"
        },
        "nombre": "San José del Palmar",
        "descripcion": "San José del Palmar",
        "abreviacion": "San José del Palmar",
        "createdAt": "2020-01-16T13:33:35.982Z",
        "updatedAt": "2020-01-16T13:33:35.982Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60832e"
        },
        "nombre": "Lourdes",
        "descripcion": "Lourdes",
        "abreviacion": "Lourdes",
        "createdAt": "2020-01-16T13:33:36.023Z",
        "updatedAt": "2020-01-16T13:33:36.023Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082fa"
        },
        "nombre": "San Andrés",
        "descripcion": "San Andrés",
        "abreviacion": "San Andrés",
        "createdAt": "2020-01-16T13:33:35.985Z",
        "updatedAt": "2020-01-16T13:33:35.985Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608320"
        },
        "nombre": "Cucutilla",
        "descripcion": "Cucutilla",
        "abreviacion": "Cucutilla",
        "createdAt": "2020-01-16T13:33:36.012Z",
        "updatedAt": "2020-01-16T13:33:36.012Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082ff"
        },
        "nombre": "Bogotá D.C.",
        "descripcion": "Bogotá D.C.",
        "abreviacion": "Bogotá D.C.",
        "createdAt": "2020-01-16T13:33:35.989Z",
        "updatedAt": "2020-01-16T13:33:35.989Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082c7"
        },
        "nombre": "Paz de Río",
        "descripcion": "Paz de Río",
        "abreviacion": "Paz de Río",
        "createdAt": "2020-01-16T13:33:35.944Z",
        "updatedAt": "2020-01-16T13:33:35.944Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608304"
        },
        "nombre": "El Carmen de Chucurí",
        "descripcion": "El Carmen de Chucurí",
        "abreviacion": "El Carmen de Chucurí",
        "createdAt": "2020-01-16T13:33:35.991Z",
        "updatedAt": "2020-01-16T13:33:35.991Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608337"
        },
        "nombre": "Yumbo",
        "descripcion": "Yumbo",
        "abreviacion": "Yumbo",
        "createdAt": "2020-01-16T13:33:36.030Z",
        "updatedAt": "2020-01-16T13:33:36.030Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608309"
        },
        "nombre": "San Luis",
        "descripcion": "San Luis",
        "abreviacion": "San Luis",
        "createdAt": "2020-01-16T13:33:35.994Z",
        "updatedAt": "2020-01-16T13:33:35.994Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608333"
        },
        "nombre": "Hacarí",
        "descripcion": "Hacarí",
        "abreviacion": "Hacarí",
        "createdAt": "2020-01-16T13:33:36.027Z",
        "updatedAt": "2020-01-16T13:33:36.027Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60830e"
        },
        "nombre": "San Carlos",
        "descripcion": "San Carlos",
        "abreviacion": "San Carlos",
        "createdAt": "2020-01-16T13:33:35.998Z",
        "updatedAt": "2020-01-16T13:33:35.998Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608325"
        },
        "nombre": "Gramalote",
        "descripcion": "Gramalote",
        "abreviacion": "Gramalote",
        "createdAt": "2020-01-16T13:33:36.016Z",
        "updatedAt": "2020-01-16T13:33:36.016Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082cc"
        },
        "nombre": "San Pedro de Uraba",
        "descripcion": "San Pedro de Uraba",
        "abreviacion": "San Pedro de Uraba",
        "createdAt": "2020-01-16T13:33:35.947Z",
        "updatedAt": "2020-01-16T13:33:35.947Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60833c"
        },
        "nombre": "Bolívar",
        "descripcion": "Bolívar",
        "abreviacion": "Bolívar",
        "createdAt": "2020-01-16T13:33:36.033Z",
        "updatedAt": "2020-01-16T13:33:36.033Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608313"
        },
        "nombre": "San Carlos",
        "descripcion": "San Carlos",
        "abreviacion": "San Carlos",
        "createdAt": "2020-01-16T13:33:36.001Z",
        "updatedAt": "2020-01-16T13:33:36.001Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082d1"
        },
        "nombre": "Santafé de Antioquia",
        "descripcion": "Santafé de Antioquia",
        "abreviacion": "Santafé de Antioquia",
        "createdAt": "2020-01-16T13:33:35.951Z",
        "updatedAt": "2020-01-16T13:33:35.951Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60832a"
        },
        "nombre": "Teorama",
        "descripcion": "Teorama",
        "abreviacion": "Teorama",
        "createdAt": "2020-01-16T13:33:36.019Z",
        "updatedAt": "2020-01-16T13:33:36.019Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608338"
        },
        "nombre": "Obando",
        "descripcion": "Obando",
        "abreviacion": "Obando",
        "createdAt": "2020-01-16T13:33:36.031Z",
        "updatedAt": "2020-01-16T13:33:36.031Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608341"
        },
        "nombre": "Chinácota",
        "descripcion": "Chinácota",
        "abreviacion": "Chinácota",
        "createdAt": "2020-01-16T13:33:36.037Z",
        "updatedAt": "2020-01-16T13:33:36.037Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608318"
        },
        "nombre": "Toledo",
        "descripcion": "Toledo",
        "abreviacion": "Toledo",
        "createdAt": "2020-01-16T13:33:36.004Z",
        "updatedAt": "2020-01-16T13:33:36.004Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60832f"
        },
        "nombre": "Ansermanuevo",
        "descripcion": "Ansermanuevo",
        "abreviacion": "Ansermanuevo",
        "createdAt": "2020-01-16T13:33:36.024Z",
        "updatedAt": "2020-01-16T13:33:36.024Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60833d"
        },
        "nombre": "La Playa",
        "descripcion": "La Playa",
        "abreviacion": "La Playa",
        "createdAt": "2020-01-16T13:33:36.034Z",
        "updatedAt": "2020-01-16T13:33:36.034Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082d6"
        },
        "nombre": "Valle de San Juan",
        "descripcion": "Valle de San Juan",
        "abreviacion": "Valle de San Juan",
        "createdAt": "2020-01-16T13:33:35.956Z",
        "updatedAt": "2020-01-16T13:33:35.956Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608346"
        },
        "nombre": "Calima",
        "descripcion": "Calima",
        "abreviacion": "Calima",
        "createdAt": "2020-01-16T13:33:36.042Z",
        "updatedAt": "2020-01-16T13:33:36.042Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60831d"
        },
        "nombre": "Salazar",
        "descripcion": "Salazar",
        "abreviacion": "Salazar",
        "createdAt": "2020-01-16T13:33:36.007Z",
        "updatedAt": "2020-01-16T13:33:36.007Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082db"
        },
        "nombre": "Guayabal de Siquima",
        "descripcion": "Guayabal de Siquima",
        "abreviacion": "Guayabal de Siquima",
        "createdAt": "2020-01-16T13:33:35.962Z",
        "updatedAt": "2020-01-16T13:33:35.962Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608334"
        },
        "nombre": "La Victoria",
        "descripcion": "La Victoria",
        "abreviacion": "La Victoria",
        "createdAt": "2020-01-16T13:33:36.028Z",
        "updatedAt": "2020-01-16T13:33:36.028Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608342"
        },
        "nombre": "Ragonvalia",
        "descripcion": "Ragonvalia",
        "abreviacion": "Ragonvalia",
        "createdAt": "2020-01-16T13:33:36.039Z",
        "updatedAt": "2020-01-16T13:33:36.039Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60834b"
        },
        "nombre": "Los Patios",
        "descripcion": "Los Patios",
        "abreviacion": "Los Patios",
        "createdAt": "2020-01-16T13:33:36.046Z",
        "updatedAt": "2020-01-16T13:33:36.046Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608339"
        },
        "nombre": "Tibú",
        "descripcion": "Tibú",
        "abreviacion": "Tibú",
        "createdAt": "2020-01-16T13:33:36.031Z",
        "updatedAt": "2020-01-16T13:33:36.031Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082e0"
        },
        "nombre": "La Jagua del Pilar",
        "descripcion": "La Jagua del Pilar",
        "abreviacion": "La Jagua del Pilar",
        "createdAt": "2020-01-16T13:33:35.967Z",
        "updatedAt": "2020-01-16T13:33:35.967Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608322"
        },
        "nombre": "Cartago",
        "descripcion": "Cartago",
        "abreviacion": "Cartago",
        "createdAt": "2020-01-16T13:33:36.014Z",
        "updatedAt": "2020-01-16T13:33:36.014Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608347"
        },
        "nombre": "Sardinata",
        "descripcion": "Sardinata",
        "abreviacion": "Sardinata",
        "createdAt": "2020-01-16T13:33:36.043Z",
        "updatedAt": "2020-01-16T13:33:36.043Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608350"
        },
        "nombre": "Riofrío",
        "descripcion": "Riofrío",
        "abreviacion": "Riofrío",
        "createdAt": "2020-01-16T13:33:36.049Z",
        "updatedAt": "2020-01-16T13:33:36.049Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082e5"
        },
        "nombre": "El Carmen de Atrato",
        "descripcion": "El Carmen de Atrato",
        "abreviacion": "El Carmen de Atrato",
        "createdAt": "2020-01-16T13:33:35.972Z",
        "updatedAt": "2020-01-16T13:33:35.972Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60833e"
        },
        "nombre": "Cali",
        "descripcion": "Cali",
        "abreviacion": "Cali",
        "createdAt": "2020-01-16T13:33:36.034Z",
        "updatedAt": "2020-01-16T13:33:36.034Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60834c"
        },
        "nombre": "Ocaña",
        "descripcion": "Ocaña",
        "abreviacion": "Ocaña",
        "createdAt": "2020-01-16T13:33:36.046Z",
        "updatedAt": "2020-01-16T13:33:36.046Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608355"
        },
        "nombre": "Cachirá",
        "descripcion": "Cachirá",
        "abreviacion": "Cachirá",
        "createdAt": "2020-01-16T13:33:36.052Z",
        "updatedAt": "2020-01-16T13:33:36.052Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608343"
        },
        "nombre": "La Esperanza",
        "descripcion": "La Esperanza",
        "abreviacion": "La Esperanza",
        "createdAt": "2020-01-16T13:33:36.040Z",
        "updatedAt": "2020-01-16T13:33:36.040Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608327"
        },
        "nombre": "El Tarra",
        "descripcion": "El Tarra",
        "abreviacion": "El Tarra",
        "createdAt": "2020-01-16T13:33:36.017Z",
        "updatedAt": "2020-01-16T13:33:36.017Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082ea"
        },
        "nombre": "Altos del Rosario",
        "descripcion": "Altos del Rosario",
        "abreviacion": "Altos del Rosario",
        "createdAt": "2020-01-16T13:33:35.975Z",
        "updatedAt": "2020-01-16T13:33:35.975Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608351"
        },
        "nombre": "Santiago",
        "descripcion": "Santiago",
        "abreviacion": "Santiago",
        "createdAt": "2020-01-16T13:33:36.050Z",
        "updatedAt": "2020-01-16T13:33:36.050Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608348"
        },
        "nombre": "Andalucía",
        "descripcion": "Andalucía",
        "abreviacion": "Andalucía",
        "createdAt": "2020-01-16T13:33:36.044Z",
        "updatedAt": "2020-01-16T13:33:36.044Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60835a"
        },
        "nombre": "Candelaria",
        "descripcion": "Candelaria",
        "abreviacion": "Candelaria",
        "createdAt": "2020-01-16T13:33:36.055Z",
        "updatedAt": "2020-01-16T13:33:36.055Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60832c"
        },
        "nombre": "Arboledas",
        "descripcion": "Arboledas",
        "abreviacion": "Arboledas",
        "createdAt": "2020-01-16T13:33:36.021Z",
        "updatedAt": "2020-01-16T13:33:36.021Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082ef"
        },
        "nombre": "San José de Uré",
        "descripcion": "San José de Uré",
        "abreviacion": "San José de Uré",
        "createdAt": "2020-01-16T13:33:35.978Z",
        "updatedAt": "2020-01-16T13:33:35.978Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608356"
        },
        "nombre": "Villa Caro",
        "descripcion": "Villa Caro",
        "abreviacion": "Villa Caro",
        "createdAt": "2020-01-16T13:33:36.053Z",
        "updatedAt": "2020-01-16T13:33:36.053Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60835f"
        },
        "nombre": "Lorica",
        "descripcion": "Lorica",
        "abreviacion": "Lorica",
        "createdAt": "2020-01-16T13:33:36.389Z",
        "updatedAt": "2020-01-16T13:33:36.389Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60834d"
        },
        "nombre": "Bucarasica",
        "descripcion": "Bucarasica",
        "abreviacion": "Bucarasica",
        "createdAt": "2020-01-16T13:33:36.047Z",
        "updatedAt": "2020-01-16T13:33:36.047Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608331"
        },
        "nombre": "Bugalagrande",
        "descripcion": "Bugalagrande",
        "abreviacion": "Bugalagrande",
        "createdAt": "2020-01-16T13:33:36.026Z",
        "updatedAt": "2020-01-16T13:33:36.026Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60835b"
        },
        "nombre": "La Cumbre",
        "descripcion": "La Cumbre",
        "abreviacion": "La Cumbre",
        "createdAt": "2020-01-16T13:33:36.056Z",
        "updatedAt": "2020-01-16T13:33:36.056Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082f4"
        },
        "nombre": "Cartagena del Chairá",
        "descripcion": "Cartagena del Chairá",
        "abreviacion": "Cartagena del Chairá",
        "createdAt": "2020-01-16T13:33:35.981Z",
        "updatedAt": "2020-01-16T13:33:35.981Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608364"
        },
        "nombre": "Buenavista",
        "descripcion": "Buenavista",
        "abreviacion": "Buenavista",
        "createdAt": "2020-01-16T13:33:36.394Z",
        "updatedAt": "2020-01-16T13:33:36.394Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608352"
        },
        "nombre": "Alcalá",
        "descripcion": "Alcalá",
        "abreviacion": "Alcalá",
        "createdAt": "2020-01-16T13:33:36.050Z",
        "updatedAt": "2020-01-16T13:33:36.050Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082f9"
        },
        "nombre": "El Tablón de Gómez",
        "descripcion": "El Tablón de Gómez",
        "abreviacion": "El Tablón de Gómez",
        "createdAt": "2020-01-16T13:33:35.985Z",
        "updatedAt": "2020-01-16T13:33:35.985Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608360"
        },
        "nombre": "Líbano",
        "descripcion": "Líbano",
        "abreviacion": "Líbano",
        "createdAt": "2020-01-16T13:33:36.390Z",
        "updatedAt": "2020-01-16T13:33:36.390Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608336"
        },
        "nombre": "Ginebra",
        "descripcion": "Ginebra",
        "abreviacion": "Ginebra",
        "createdAt": "2020-01-16T13:33:36.029Z",
        "updatedAt": "2020-01-16T13:33:36.029Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608369"
        },
        "nombre": "Tadó",
        "descripcion": "Tadó",
        "abreviacion": "Tadó",
        "createdAt": "2020-01-16T13:33:36.398Z",
        "updatedAt": "2020-01-16T13:33:36.398Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608357"
        },
        "nombre": "Durania",
        "descripcion": "Durania",
        "abreviacion": "Durania",
        "createdAt": "2020-01-16T13:33:36.053Z",
        "updatedAt": "2020-01-16T13:33:36.053Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda6082fe"
        },
        "nombre": "Santiago de Tolú",
        "descripcion": "Santiago de Tolú",
        "abreviacion": "Santiago de Tolú",
        "createdAt": "2020-01-16T13:33:35.988Z",
        "updatedAt": "2020-01-16T13:33:35.988Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608365"
        },
        "nombre": "Montería",
        "descripcion": "Montería",
        "abreviacion": "Montería",
        "createdAt": "2020-01-16T13:33:36.395Z",
        "updatedAt": "2020-01-16T13:33:36.395Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60833b"
        },
        "nombre": "San Calixto",
        "descripcion": "San Calixto",
        "abreviacion": "San Calixto",
        "createdAt": "2020-01-16T13:33:36.032Z",
        "updatedAt": "2020-01-16T13:33:36.032Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60836e"
        },
        "nombre": "Gamarra",
        "descripcion": "Gamarra",
        "abreviacion": "Gamarra",
        "createdAt": "2020-01-16T13:33:36.403Z",
        "updatedAt": "2020-01-16T13:33:36.403Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60835c"
        },
        "nombre": "Ulloa",
        "descripcion": "Ulloa",
        "abreviacion": "Ulloa",
        "createdAt": "2020-01-16T13:33:36.056Z",
        "updatedAt": "2020-01-16T13:33:36.056Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608303"
        },
        "nombre": "San Juan del Cesar",
        "descripcion": "San Juan del Cesar",
        "abreviacion": "San Juan del Cesar",
        "createdAt": "2020-01-16T13:33:35.991Z",
        "updatedAt": "2020-01-16T13:33:35.991Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60836a"
        },
        "nombre": "Pelaya",
        "descripcion": "Pelaya",
        "abreviacion": "Pelaya",
        "createdAt": "2020-01-16T13:33:36.399Z",
        "updatedAt": "2020-01-16T13:33:36.399Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608340"
        },
        "nombre": "Guadalajara de Buga",
        "descripcion": "Guadalajara de Buga",
        "abreviacion": "Guadalajara de Buga",
        "createdAt": "2020-01-16T13:33:36.037Z",
        "updatedAt": "2020-01-16T13:33:36.037Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608373"
        },
        "nombre": "Astrea",
        "descripcion": "Astrea",
        "abreviacion": "Astrea",
        "createdAt": "2020-01-16T13:33:36.407Z",
        "updatedAt": "2020-01-16T13:33:36.407Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608361"
        },
        "nombre": "Orocué",
        "descripcion": "Orocué",
        "abreviacion": "Orocué",
        "createdAt": "2020-01-16T13:33:36.391Z",
        "updatedAt": "2020-01-16T13:33:36.391Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda608308"
        },
        "nombre": "Valle de San José",
        "descripcion": "Valle de San José",
        "abreviacion": "Valle de San José",
        "createdAt": "2020-01-16T13:33:35.994Z",
        "updatedAt": "2020-01-16T13:33:35.994Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60836f"
        },
        "nombre": "El Copey",
        "descripcion": "El Copey",
        "abreviacion": "El Copey",
        "createdAt": "2020-01-16T13:33:36.404Z",
        "updatedAt": "2020-01-16T13:33:36.404Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608378"
        },
        "nombre": "Timbío",
        "descripcion": "Timbío",
        "abreviacion": "Timbío",
        "createdAt": "2020-01-16T13:33:36.412Z",
        "updatedAt": "2020-01-16T13:33:36.412Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608345"
        },
        "nombre": "Chitagá",
        "descripcion": "Chitagá",
        "abreviacion": "Chitagá",
        "createdAt": "2020-01-16T13:33:36.042Z",
        "updatedAt": "2020-01-16T13:33:36.042Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608366"
        },
        "nombre": "Tamalameque",
        "descripcion": "Tamalameque",
        "abreviacion": "Tamalameque",
        "createdAt": "2020-01-16T13:33:36.396Z",
        "updatedAt": "2020-01-16T13:33:36.396Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e20662fc6788b7bda60830d"
        },
        "nombre": "Vergara",
        "descripcion": "Vergara",
        "abreviacion": "Vergara",
        "createdAt": "2020-01-16T13:33:35.996Z",
        "updatedAt": "2020-01-16T13:33:35.996Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608374"
        },
        "nombre": "Aguachica",
        "descripcion": "Aguachica",
        "abreviacion": "Aguachica",
        "createdAt": "2020-01-16T13:33:36.408Z",
        "updatedAt": "2020-01-16T13:33:36.408Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60837d"
        },
        "nombre": "Puracé",
        "descripcion": "Puracé",
        "abreviacion": "Puracé",
        "createdAt": "2020-01-16T13:33:36.416Z",
        "updatedAt": "2020-01-16T13:33:36.416Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60834a"
        },
        "nombre": "Abrego",
        "descripcion": "Abrego",
        "abreviacion": "Abrego",
        "createdAt": "2020-01-16T13:33:36.045Z",
        "updatedAt": "2020-01-16T13:33:36.045Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60836b"
        },
        "nombre": "Pailitas",
        "descripcion": "Pailitas",
        "abreviacion": "Pailitas",
        "createdAt": "2020-01-16T13:33:36.400Z",
        "updatedAt": "2020-01-16T13:33:36.400Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608312"
        },
        "nombre": "San Sebastián",
        "descripcion": "San Sebastián",
        "abreviacion": "San Sebastián",
        "createdAt": "2020-01-16T13:33:36.000Z",
        "updatedAt": "2020-01-16T13:33:36.000Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608379"
        },
        "nombre": "Suárez",
        "descripcion": "Suárez",
        "abreviacion": "Suárez",
        "createdAt": "2020-01-16T13:33:36.413Z",
        "updatedAt": "2020-01-16T13:33:36.413Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608382"
        },
        "nombre": "Miranda",
        "descripcion": "Miranda",
        "abreviacion": "Miranda",
        "createdAt": "2020-01-16T13:33:36.420Z",
        "updatedAt": "2020-01-16T13:33:36.420Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60834f"
        },
        "nombre": "Palmira",
        "descripcion": "Palmira",
        "abreviacion": "Palmira",
        "createdAt": "2020-01-16T13:33:36.048Z",
        "updatedAt": "2020-01-16T13:33:36.048Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608370"
        },
        "nombre": "Chiriguaná",
        "descripcion": "Chiriguaná",
        "abreviacion": "Chiriguaná",
        "createdAt": "2020-01-16T13:33:36.404Z",
        "updatedAt": "2020-01-16T13:33:36.404Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60837e"
        },
        "nombre": "Puerto Tejada",
        "descripcion": "Puerto Tejada",
        "abreviacion": "Puerto Tejada",
        "createdAt": "2020-01-16T13:33:36.417Z",
        "updatedAt": "2020-01-16T13:33:36.417Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608317"
        },
        "nombre": "El Dovio",
        "descripcion": "El Dovio",
        "abreviacion": "El Dovio",
        "createdAt": "2020-01-16T13:33:36.003Z",
        "updatedAt": "2020-01-16T13:33:36.003Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608387"
        },
        "nombre": "Inzá",
        "descripcion": "Inzá",
        "abreviacion": "Inzá",
        "createdAt": "2020-01-16T13:33:36.424Z",
        "updatedAt": "2020-01-16T13:33:36.424Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608354"
        },
        "nombre": "Labateca",
        "descripcion": "Labateca",
        "abreviacion": "Labateca",
        "createdAt": "2020-01-16T13:33:36.052Z",
        "updatedAt": "2020-01-16T13:33:36.052Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608375"
        },
        "nombre": "Villa Rica",
        "descripcion": "Villa Rica",
        "abreviacion": "Villa Rica",
        "createdAt": "2020-01-16T13:33:36.409Z",
        "updatedAt": "2020-01-16T13:33:36.409Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60831c"
        },
        "nombre": "El Zulia",
        "descripcion": "El Zulia",
        "abreviacion": "El Zulia",
        "createdAt": "2020-01-16T13:33:36.006Z",
        "updatedAt": "2020-01-16T13:33:36.006Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608359"
        },
        "nombre": "Toro",
        "descripcion": "Toro",
        "abreviacion": "Toro",
        "createdAt": "2020-01-16T13:33:36.054Z",
        "updatedAt": "2020-01-16T13:33:36.054Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608383"
        },
        "nombre": "Mercaderes",
        "descripcion": "Mercaderes",
        "abreviacion": "Mercaderes",
        "createdAt": "2020-01-16T13:33:36.421Z",
        "updatedAt": "2020-01-16T13:33:36.421Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60838c"
        },
        "nombre": "La Victoria",
        "descripcion": "La Victoria",
        "abreviacion": "La Victoria",
        "createdAt": "2020-01-16T13:33:36.506Z",
        "updatedAt": "2020-01-16T13:33:36.506Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60837a"
        },
        "nombre": "Silvia",
        "descripcion": "Silvia",
        "abreviacion": "Silvia",
        "createdAt": "2020-01-16T13:33:36.414Z",
        "updatedAt": "2020-01-16T13:33:36.414Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608321"
        },
        "nombre": "El Cerrito",
        "descripcion": "El Cerrito",
        "abreviacion": "El Cerrito",
        "createdAt": "2020-01-16T13:33:36.013Z",
        "updatedAt": "2020-01-16T13:33:36.013Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60835e"
        },
        "nombre": "Vijes",
        "descripcion": "Vijes",
        "abreviacion": "Vijes",
        "createdAt": "2020-01-16T13:33:36.057Z",
        "updatedAt": "2020-01-16T13:33:36.057Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608388"
        },
        "nombre": "Guapi",
        "descripcion": "Guapi",
        "abreviacion": "Guapi",
        "createdAt": "2020-01-16T13:33:36.425Z",
        "updatedAt": "2020-01-16T13:33:36.425Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60837f"
        },
        "nombre": "Piamonte",
        "descripcion": "Piamonte",
        "abreviacion": "Piamonte",
        "createdAt": "2020-01-16T13:33:36.418Z",
        "updatedAt": "2020-01-16T13:33:36.418Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608363"
        },
        "nombre": "Cereté",
        "descripcion": "Cereté",
        "abreviacion": "Cereté",
        "createdAt": "2020-01-16T13:33:36.393Z",
        "updatedAt": "2020-01-16T13:33:36.393Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608391"
        },
        "nombre": "Santiago",
        "descripcion": "Santiago",
        "abreviacion": "Santiago",
        "createdAt": "2020-01-16T13:33:36.511Z",
        "updatedAt": "2020-01-16T13:33:36.511Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608326"
        },
        "nombre": "El Cairo",
        "descripcion": "El Cairo",
        "abreviacion": "El Cairo",
        "createdAt": "2020-01-16T13:33:36.017Z",
        "updatedAt": "2020-01-16T13:33:36.017Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60838d"
        },
        "nombre": "La Pedrera",
        "descripcion": "La Pedrera",
        "abreviacion": "La Pedrera",
        "createdAt": "2020-01-16T13:33:36.507Z",
        "updatedAt": "2020-01-16T13:33:36.507Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608384"
        },
        "nombre": "López",
        "descripcion": "López",
        "abreviacion": "López",
        "createdAt": "2020-01-16T13:33:36.422Z",
        "updatedAt": "2020-01-16T13:33:36.422Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608396"
        },
        "nombre": "Puerto Guzmán",
        "descripcion": "Puerto Guzmán",
        "abreviacion": "Puerto Guzmán",
        "createdAt": "2020-01-16T13:33:36.516Z",
        "updatedAt": "2020-01-16T13:33:36.516Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60832b"
        },
        "nombre": "Dagua",
        "descripcion": "Dagua",
        "abreviacion": "Dagua",
        "createdAt": "2020-01-16T13:33:36.020Z",
        "updatedAt": "2020-01-16T13:33:36.020Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608392"
        },
        "nombre": "San Miguel",
        "descripcion": "San Miguel",
        "abreviacion": "San Miguel",
        "createdAt": "2020-01-16T13:33:36.512Z",
        "updatedAt": "2020-01-16T13:33:36.512Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608368"
        },
        "nombre": "La Paz",
        "descripcion": "La Paz",
        "abreviacion": "La Paz",
        "createdAt": "2020-01-16T13:33:36.397Z",
        "updatedAt": "2020-01-16T13:33:36.397Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608389"
        },
        "nombre": "Puerto Santander",
        "descripcion": "Puerto Santander",
        "abreviacion": "Puerto Santander",
        "createdAt": "2020-01-16T13:33:36.504Z",
        "updatedAt": "2020-01-16T13:33:36.504Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60839b"
        },
        "nombre": "Villanueva",
        "descripcion": "Villanueva",
        "abreviacion": "Villanueva",
        "createdAt": "2020-01-16T13:33:36.520Z",
        "updatedAt": "2020-01-16T13:33:36.520Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608330"
        },
        "nombre": "Bochalema",
        "descripcion": "Bochalema",
        "abreviacion": "Bochalema",
        "createdAt": "2020-01-16T13:33:36.025Z",
        "updatedAt": "2020-01-16T13:33:36.025Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608397"
        },
        "nombre": "Puerto Caicedo",
        "descripcion": "Puerto Caicedo",
        "abreviacion": "Puerto Caicedo",
        "createdAt": "2020-01-16T13:33:36.517Z",
        "updatedAt": "2020-01-16T13:33:36.517Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60836d"
        },
        "nombre": "González",
        "descripcion": "González",
        "abreviacion": "González",
        "createdAt": "2020-01-16T13:33:36.402Z",
        "updatedAt": "2020-01-16T13:33:36.402Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60838e"
        },
        "nombre": "La Chorrera",
        "descripcion": "La Chorrera",
        "abreviacion": "La Chorrera",
        "createdAt": "2020-01-16T13:33:36.508Z",
        "updatedAt": "2020-01-16T13:33:36.508Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083a0"
        },
        "nombre": "Recetor",
        "descripcion": "Recetor",
        "abreviacion": "Recetor",
        "createdAt": "2020-01-16T13:33:36.527Z",
        "updatedAt": "2020-01-16T13:33:36.527Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608335"
        },
        "nombre": "Herrán",
        "descripcion": "Herrán",
        "abreviacion": "Herrán",
        "createdAt": "2020-01-16T13:33:36.029Z",
        "updatedAt": "2020-01-16T13:33:36.029Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60839c"
        },
        "nombre": "Trinidad",
        "descripcion": "Trinidad",
        "abreviacion": "Trinidad",
        "createdAt": "2020-01-16T13:33:36.521Z",
        "updatedAt": "2020-01-16T13:33:36.521Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608372"
        },
        "nombre": "Becerril",
        "descripcion": "Becerril",
        "abreviacion": "Becerril",
        "createdAt": "2020-01-16T13:33:36.406Z",
        "updatedAt": "2020-01-16T13:33:36.406Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608393"
        },
        "nombre": "San Francisco",
        "descripcion": "San Francisco",
        "abreviacion": "San Francisco",
        "createdAt": "2020-01-16T13:33:36.513Z",
        "updatedAt": "2020-01-16T13:33:36.513Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083a5"
        },
        "nombre": "Chámeza",
        "descripcion": "Chámeza",
        "abreviacion": "Chámeza",
        "createdAt": "2020-01-16T13:33:36.530Z",
        "updatedAt": "2020-01-16T13:33:36.530Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60833a"
        },
        "nombre": "San Cayetano",
        "descripcion": "San Cayetano",
        "abreviacion": "San Cayetano",
        "createdAt": "2020-01-16T13:33:36.032Z",
        "updatedAt": "2020-01-16T13:33:36.032Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083a1"
        },
        "nombre": "Pore",
        "descripcion": "Pore",
        "abreviacion": "Pore",
        "createdAt": "2020-01-16T13:33:36.527Z",
        "updatedAt": "2020-01-16T13:33:36.527Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608377"
        },
        "nombre": "Timbiquí",
        "descripcion": "Timbiquí",
        "abreviacion": "Timbiquí",
        "createdAt": "2020-01-16T13:33:36.411Z",
        "updatedAt": "2020-01-16T13:33:36.411Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608398"
        },
        "nombre": "Orito",
        "descripcion": "Orito",
        "abreviacion": "Orito",
        "createdAt": "2020-01-16T13:33:36.518Z",
        "updatedAt": "2020-01-16T13:33:36.518Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083aa"
        },
        "nombre": "Saravena",
        "descripcion": "Saravena",
        "abreviacion": "Saravena",
        "createdAt": "2020-01-16T13:33:36.533Z",
        "updatedAt": "2020-01-16T13:33:36.533Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60833f"
        },
        "nombre": "San Pedro",
        "descripcion": "San Pedro",
        "abreviacion": "San Pedro",
        "createdAt": "2020-01-16T13:33:36.035Z",
        "updatedAt": "2020-01-16T13:33:36.035Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083a6"
        },
        "nombre": "Aguazul",
        "descripcion": "Aguazul",
        "abreviacion": "Aguazul",
        "createdAt": "2020-01-16T13:33:36.531Z",
        "updatedAt": "2020-01-16T13:33:36.531Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60837c"
        },
        "nombre": "El Peñón",
        "descripcion": "El Peñón",
        "abreviacion": "El Peñón",
        "createdAt": "2020-01-16T13:33:36.415Z",
        "updatedAt": "2020-01-16T13:33:36.415Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60839d"
        },
        "nombre": "Tauramena",
        "descripcion": "Tauramena",
        "abreviacion": "Tauramena",
        "createdAt": "2020-01-16T13:33:36.522Z",
        "updatedAt": "2020-01-16T13:33:36.522Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083af"
        },
        "nombre": "Villarrica",
        "descripcion": "Villarrica",
        "abreviacion": "Villarrica",
        "createdAt": "2020-01-16T13:33:36.537Z",
        "updatedAt": "2020-01-16T13:33:36.537Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608344"
        },
        "nombre": "Villa del Rosario",
        "descripcion": "Villa del Rosario",
        "abreviacion": "Villa del Rosario",
        "createdAt": "2020-01-16T13:33:36.041Z",
        "updatedAt": "2020-01-16T13:33:36.041Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083ab"
        },
        "nombre": "Puerto Rondón",
        "descripcion": "Puerto Rondón",
        "abreviacion": "Puerto Rondón",
        "createdAt": "2020-01-16T13:33:36.534Z",
        "updatedAt": "2020-01-16T13:33:36.534Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608381"
        },
        "nombre": "Padilla",
        "descripcion": "Padilla",
        "abreviacion": "Padilla",
        "createdAt": "2020-01-16T13:33:36.419Z",
        "updatedAt": "2020-01-16T13:33:36.419Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083a2"
        },
        "nombre": "Monterrey",
        "descripcion": "Monterrey",
        "abreviacion": "Monterrey",
        "createdAt": "2020-01-16T13:33:36.528Z",
        "updatedAt": "2020-01-16T13:33:36.528Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083b4"
        },
        "nombre": "Rovira",
        "descripcion": "Rovira",
        "abreviacion": "Rovira",
        "createdAt": "2020-01-16T13:33:36.541Z",
        "updatedAt": "2020-01-16T13:33:36.541Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608349"
        },
        "nombre": "Pradera",
        "descripcion": "Pradera",
        "abreviacion": "Pradera",
        "createdAt": "2020-01-16T13:33:36.044Z",
        "updatedAt": "2020-01-16T13:33:36.044Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083b0"
        },
        "nombre": "Villahermosa",
        "descripcion": "Villahermosa",
        "abreviacion": "Villahermosa",
        "createdAt": "2020-01-16T13:33:36.538Z",
        "updatedAt": "2020-01-16T13:33:36.538Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608386"
        },
        "nombre": "La Sierra",
        "descripcion": "La Sierra",
        "abreviacion": "La Sierra",
        "createdAt": "2020-01-16T13:33:36.423Z",
        "updatedAt": "2020-01-16T13:33:36.423Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083a7"
        },
        "nombre": "Yopal",
        "descripcion": "Yopal",
        "abreviacion": "Yopal",
        "createdAt": "2020-01-16T13:33:36.531Z",
        "updatedAt": "2020-01-16T13:33:36.531Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083b9"
        },
        "nombre": "Planadas",
        "descripcion": "Planadas",
        "abreviacion": "Planadas",
        "createdAt": "2020-01-16T13:33:36.544Z",
        "updatedAt": "2020-01-16T13:33:36.544Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60834e"
        },
        "nombre": "Yotoco",
        "descripcion": "Yotoco",
        "abreviacion": "Yotoco",
        "createdAt": "2020-01-16T13:33:36.047Z",
        "updatedAt": "2020-01-16T13:33:36.047Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083b5"
        },
        "nombre": "Roncesvalles",
        "descripcion": "Roncesvalles",
        "abreviacion": "Roncesvalles",
        "createdAt": "2020-01-16T13:33:36.542Z",
        "updatedAt": "2020-01-16T13:33:36.542Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60838b"
        },
        "nombre": "Puerto Arica",
        "descripcion": "Puerto Arica",
        "abreviacion": "Puerto Arica",
        "createdAt": "2020-01-16T13:33:36.506Z",
        "updatedAt": "2020-01-16T13:33:36.506Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083ac"
        },
        "nombre": "Fortul",
        "descripcion": "Fortul",
        "abreviacion": "Fortul",
        "createdAt": "2020-01-16T13:33:36.535Z",
        "updatedAt": "2020-01-16T13:33:36.535Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083be"
        },
        "nombre": "Murillo",
        "descripcion": "Murillo",
        "abreviacion": "Murillo",
        "createdAt": "2020-01-16T13:33:36.548Z",
        "updatedAt": "2020-01-16T13:33:36.548Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608353"
        },
        "nombre": "Versalles",
        "descripcion": "Versalles",
        "abreviacion": "Versalles",
        "createdAt": "2020-01-16T13:33:36.051Z",
        "updatedAt": "2020-01-16T13:33:36.051Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083ba"
        },
        "nombre": "Piedras",
        "descripcion": "Piedras",
        "abreviacion": "Piedras",
        "createdAt": "2020-01-16T13:33:36.545Z",
        "updatedAt": "2020-01-16T13:33:36.545Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608390"
        },
        "nombre": "Leticia",
        "descripcion": "Leticia",
        "abreviacion": "Leticia",
        "createdAt": "2020-01-16T13:33:36.510Z",
        "updatedAt": "2020-01-16T13:33:36.510Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083b1"
        },
        "nombre": "Venadillo",
        "descripcion": "Venadillo",
        "abreviacion": "Venadillo",
        "createdAt": "2020-01-16T13:33:36.539Z",
        "updatedAt": "2020-01-16T13:33:36.539Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083c3"
        },
        "nombre": "Herveo",
        "descripcion": "Herveo",
        "abreviacion": "Herveo",
        "createdAt": "2020-01-16T13:33:36.551Z",
        "updatedAt": "2020-01-16T13:33:36.551Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083bf"
        },
        "nombre": "Melgar",
        "descripcion": "Melgar",
        "abreviacion": "Melgar",
        "createdAt": "2020-01-16T13:33:36.549Z",
        "updatedAt": "2020-01-16T13:33:36.549Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608358"
        },
        "nombre": "El Águila",
        "descripcion": "El Águila",
        "abreviacion": "El Águila",
        "createdAt": "2020-01-16T13:33:36.054Z",
        "updatedAt": "2020-01-16T13:33:36.054Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608395"
        },
        "nombre": "Leguízamo",
        "descripcion": "Leguízamo",
        "abreviacion": "Leguízamo",
        "createdAt": "2020-01-16T13:33:36.515Z",
        "updatedAt": "2020-01-16T13:33:36.515Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083b6"
        },
        "nombre": "Rio Blanco",
        "descripcion": "Rio Blanco",
        "abreviacion": "Rio Blanco",
        "createdAt": "2020-01-16T13:33:36.542Z",
        "updatedAt": "2020-01-16T13:33:36.542Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60835d"
        },
        "nombre": "Trujillo",
        "descripcion": "Trujillo",
        "abreviacion": "Trujillo",
        "createdAt": "2020-01-16T13:33:36.057Z",
        "updatedAt": "2020-01-16T13:33:36.057Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60839a"
        },
        "nombre": "Mocoa",
        "descripcion": "Mocoa",
        "abreviacion": "Mocoa",
        "createdAt": "2020-01-16T13:33:36.519Z",
        "updatedAt": "2020-01-16T13:33:36.519Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083c8"
        },
        "nombre": "Espinal",
        "descripcion": "Espinal",
        "abreviacion": "Espinal",
        "createdAt": "2020-01-16T13:33:36.554Z",
        "updatedAt": "2020-01-16T13:33:36.554Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083bb"
        },
        "nombre": "Palocabildo",
        "descripcion": "Palocabildo",
        "abreviacion": "Palocabildo",
        "createdAt": "2020-01-16T13:33:36.546Z",
        "updatedAt": "2020-01-16T13:33:36.546Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083cd"
        },
        "nombre": "Chaparral",
        "descripcion": "Chaparral",
        "abreviacion": "Chaparral",
        "createdAt": "2020-01-16T13:33:36.557Z",
        "updatedAt": "2020-01-16T13:33:36.557Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083c4"
        },
        "nombre": "Guamo",
        "descripcion": "Guamo",
        "abreviacion": "Guamo",
        "createdAt": "2020-01-16T13:33:36.552Z",
        "updatedAt": "2020-01-16T13:33:36.552Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60839f"
        },
        "nombre": "Sabanalarga",
        "descripcion": "Sabanalarga",
        "abreviacion": "Sabanalarga",
        "createdAt": "2020-01-16T13:33:36.525Z",
        "updatedAt": "2020-01-16T13:33:36.525Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608362"
        },
        "nombre": "Chimá",
        "descripcion": "Chimá",
        "abreviacion": "Chimá",
        "createdAt": "2020-01-16T13:33:36.392Z",
        "updatedAt": "2020-01-16T13:33:36.392Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083c0"
        },
        "nombre": "Mariquita",
        "descripcion": "Mariquita",
        "abreviacion": "Mariquita",
        "createdAt": "2020-01-16T13:33:36.549Z",
        "updatedAt": "2020-01-16T13:33:36.549Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083c9"
        },
        "nombre": "Dolores",
        "descripcion": "Dolores",
        "abreviacion": "Dolores",
        "createdAt": "2020-01-16T13:33:36.554Z",
        "updatedAt": "2020-01-16T13:33:36.554Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083d2"
        },
        "nombre": "Alvarado",
        "descripcion": "Alvarado",
        "abreviacion": "Alvarado",
        "createdAt": "2020-01-16T13:33:36.560Z",
        "updatedAt": "2020-01-16T13:33:36.560Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608367"
        },
        "nombre": "San Diego",
        "descripcion": "San Diego",
        "abreviacion": "San Diego",
        "createdAt": "2020-01-16T13:33:36.397Z",
        "updatedAt": "2020-01-16T13:33:36.397Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083c5"
        },
        "nombre": "Fresno",
        "descripcion": "Fresno",
        "abreviacion": "Fresno",
        "createdAt": "2020-01-16T13:33:36.552Z",
        "updatedAt": "2020-01-16T13:33:36.552Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083a4"
        },
        "nombre": "Hato Corozal",
        "descripcion": "Hato Corozal",
        "abreviacion": "Hato Corozal",
        "createdAt": "2020-01-16T13:33:36.529Z",
        "updatedAt": "2020-01-16T13:33:36.529Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083ce"
        },
        "nombre": "Cajamarca",
        "descripcion": "Cajamarca",
        "abreviacion": "Cajamarca",
        "createdAt": "2020-01-16T13:33:36.557Z",
        "updatedAt": "2020-01-16T13:33:36.557Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083d7"
        },
        "nombre": "San Onofre",
        "descripcion": "San Onofre",
        "abreviacion": "San Onofre",
        "createdAt": "2020-01-16T13:33:36.562Z",
        "updatedAt": "2020-01-16T13:33:36.562Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60836c"
        },
        "nombre": "Jamundí",
        "descripcion": "Jamundí",
        "abreviacion": "Jamundí",
        "createdAt": "2020-01-16T13:33:36.401Z",
        "updatedAt": "2020-01-16T13:33:36.401Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083ca"
        },
        "nombre": "Cunday",
        "descripcion": "Cunday",
        "abreviacion": "Cunday",
        "createdAt": "2020-01-16T13:33:36.555Z",
        "updatedAt": "2020-01-16T13:33:36.555Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083a9"
        },
        "nombre": "Tame",
        "descripcion": "Tame",
        "abreviacion": "Tame",
        "createdAt": "2020-01-16T13:33:36.533Z",
        "updatedAt": "2020-01-16T13:33:36.533Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608371"
        },
        "nombre": "Chimichagua",
        "descripcion": "Chimichagua",
        "abreviacion": "Chimichagua",
        "createdAt": "2020-01-16T13:33:36.405Z",
        "updatedAt": "2020-01-16T13:33:36.405Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083d3"
        },
        "nombre": "Alpujarra",
        "descripcion": "Alpujarra",
        "abreviacion": "Alpujarra",
        "createdAt": "2020-01-16T13:33:36.560Z",
        "updatedAt": "2020-01-16T13:33:36.560Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083cf"
        },
        "nombre": "Ataco",
        "descripcion": "Ataco",
        "abreviacion": "Ataco",
        "createdAt": "2020-01-16T13:33:36.558Z",
        "updatedAt": "2020-01-16T13:33:36.558Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083dc"
        },
        "nombre": "Morroa",
        "descripcion": "Morroa",
        "abreviacion": "Morroa",
        "createdAt": "2020-01-16T13:33:36.566Z",
        "updatedAt": "2020-01-16T13:33:36.566Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083ae"
        },
        "nombre": "Arauquita",
        "descripcion": "Arauquita",
        "abreviacion": "Arauquita",
        "createdAt": "2020-01-16T13:33:36.537Z",
        "updatedAt": "2020-01-16T13:33:36.537Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083d8"
        },
        "nombre": "San Marcos",
        "descripcion": "San Marcos",
        "abreviacion": "San Marcos",
        "createdAt": "2020-01-16T13:33:36.563Z",
        "updatedAt": "2020-01-16T13:33:36.563Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608376"
        },
        "nombre": "Totoró",
        "descripcion": "Totoró",
        "abreviacion": "Totoró",
        "createdAt": "2020-01-16T13:33:36.410Z",
        "updatedAt": "2020-01-16T13:33:36.410Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083d4"
        },
        "nombre": "Tolú Viejo",
        "descripcion": "Tolú Viejo",
        "abreviacion": "Tolú Viejo",
        "createdAt": "2020-01-16T13:33:36.561Z",
        "updatedAt": "2020-01-16T13:33:36.561Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083e1"
        },
        "nombre": "Galeras",
        "descripcion": "Galeras",
        "abreviacion": "Galeras",
        "createdAt": "2020-01-16T13:33:36.570Z",
        "updatedAt": "2020-01-16T13:33:36.570Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083b3"
        },
        "nombre": "Saldaña",
        "descripcion": "Saldaña",
        "abreviacion": "Saldaña",
        "createdAt": "2020-01-16T13:33:36.540Z",
        "updatedAt": "2020-01-16T13:33:36.540Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083dd"
        },
        "nombre": "Majagual",
        "descripcion": "Majagual",
        "abreviacion": "Majagual",
        "createdAt": "2020-01-16T13:33:36.567Z",
        "updatedAt": "2020-01-16T13:33:36.567Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60837b"
        },
        "nombre": "Santa Rosa",
        "descripcion": "Santa Rosa",
        "abreviacion": "Santa Rosa",
        "createdAt": "2020-01-16T13:33:36.414Z",
        "updatedAt": "2020-01-16T13:33:36.414Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083d9"
        },
        "nombre": "San Benito Abad",
        "descripcion": "San Benito Abad",
        "abreviacion": "San Benito Abad",
        "createdAt": "2020-01-16T13:33:36.564Z",
        "updatedAt": "2020-01-16T13:33:36.564Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083e6"
        },
        "nombre": "Caimito",
        "descripcion": "Caimito",
        "abreviacion": "Caimito",
        "createdAt": "2020-01-16T13:33:36.573Z",
        "updatedAt": "2020-01-16T13:33:36.573Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083b8"
        },
        "nombre": "Prado",
        "descripcion": "Prado",
        "abreviacion": "Prado",
        "createdAt": "2020-01-16T13:33:36.544Z",
        "updatedAt": "2020-01-16T13:33:36.544Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083de"
        },
        "nombre": "Los Palmitos",
        "descripcion": "Los Palmitos",
        "abreviacion": "Los Palmitos",
        "createdAt": "2020-01-16T13:33:36.567Z",
        "updatedAt": "2020-01-16T13:33:36.567Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083e2"
        },
        "nombre": "El Roble",
        "descripcion": "El Roble",
        "abreviacion": "El Roble",
        "createdAt": "2020-01-16T13:33:36.570Z",
        "updatedAt": "2020-01-16T13:33:36.570Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083eb"
        },
        "nombre": "Vetas",
        "descripcion": "Vetas",
        "abreviacion": "Vetas",
        "createdAt": "2020-01-16T13:33:36.580Z",
        "updatedAt": "2020-01-16T13:33:36.580Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608380"
        },
        "nombre": "Patía",
        "descripcion": "Patía",
        "abreviacion": "Patía",
        "createdAt": "2020-01-16T13:33:36.418Z",
        "updatedAt": "2020-01-16T13:33:36.418Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083bd"
        },
        "nombre": "Natagaima",
        "descripcion": "Natagaima",
        "abreviacion": "Natagaima",
        "createdAt": "2020-01-16T13:33:36.547Z",
        "updatedAt": "2020-01-16T13:33:36.547Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083e7"
        },
        "nombre": "Buenavista",
        "descripcion": "Buenavista",
        "abreviacion": "Buenavista",
        "createdAt": "2020-01-16T13:33:36.574Z",
        "updatedAt": "2020-01-16T13:33:36.574Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083e3"
        },
        "nombre": "Chalán",
        "descripcion": "Chalán",
        "abreviacion": "Chalán",
        "createdAt": "2020-01-16T13:33:36.571Z",
        "updatedAt": "2020-01-16T13:33:36.571Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608385"
        },
        "nombre": "La Vega",
        "descripcion": "La Vega",
        "abreviacion": "La Vega",
        "createdAt": "2020-01-16T13:33:36.422Z",
        "updatedAt": "2020-01-16T13:33:36.422Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083f0"
        },
        "nombre": "Suaita",
        "descripcion": "Suaita",
        "abreviacion": "Suaita",
        "createdAt": "2020-01-16T13:33:36.584Z",
        "updatedAt": "2020-01-16T13:33:36.584Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083ec"
        },
        "nombre": "Vélez",
        "descripcion": "Vélez",
        "abreviacion": "Vélez",
        "createdAt": "2020-01-16T13:33:36.581Z",
        "updatedAt": "2020-01-16T13:33:36.581Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083c2"
        },
        "nombre": "Honda",
        "descripcion": "Honda",
        "abreviacion": "Honda",
        "createdAt": "2020-01-16T13:33:36.550Z",
        "updatedAt": "2020-01-16T13:33:36.550Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60838a"
        },
        "nombre": "Puerto Nariño",
        "descripcion": "Puerto Nariño",
        "abreviacion": "Puerto Nariño",
        "createdAt": "2020-01-16T13:33:36.505Z",
        "updatedAt": "2020-01-16T13:33:36.505Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083f5"
        },
        "nombre": "San Joaquín",
        "descripcion": "San Joaquín",
        "abreviacion": "San Joaquín",
        "createdAt": "2020-01-16T13:33:36.588Z",
        "updatedAt": "2020-01-16T13:33:36.588Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083e8"
        },
        "nombre": "Sincelejo",
        "descripcion": "Sincelejo",
        "abreviacion": "Sincelejo",
        "createdAt": "2020-01-16T13:33:36.575Z",
        "updatedAt": "2020-01-16T13:33:36.575Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083f1"
        },
        "nombre": "Socorro",
        "descripcion": "Socorro",
        "abreviacion": "Socorro",
        "createdAt": "2020-01-16T13:33:36.585Z",
        "updatedAt": "2020-01-16T13:33:36.585Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083c7"
        },
        "nombre": "Falan",
        "descripcion": "Falan",
        "abreviacion": "Falan",
        "createdAt": "2020-01-16T13:33:36.553Z",
        "updatedAt": "2020-01-16T13:33:36.553Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083fa"
        },
        "nombre": "Pinchote",
        "descripcion": "Pinchote",
        "abreviacion": "Pinchote",
        "createdAt": "2020-01-16T13:33:36.591Z",
        "updatedAt": "2020-01-16T13:33:36.591Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60838f"
        },
        "nombre": "El Encanto",
        "descripcion": "El Encanto",
        "abreviacion": "El Encanto",
        "createdAt": "2020-01-16T13:33:36.509Z",
        "updatedAt": "2020-01-16T13:33:36.509Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083ed"
        },
        "nombre": "Tona",
        "descripcion": "Tona",
        "abreviacion": "Tona",
        "createdAt": "2020-01-16T13:33:36.582Z",
        "updatedAt": "2020-01-16T13:33:36.582Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083cc"
        },
        "nombre": "Coello",
        "descripcion": "Coello",
        "abreviacion": "Coello",
        "createdAt": "2020-01-16T13:33:36.556Z",
        "updatedAt": "2020-01-16T13:33:36.556Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083f6"
        },
        "nombre": "San Gil",
        "descripcion": "San Gil",
        "abreviacion": "San Gil",
        "createdAt": "2020-01-16T13:33:36.588Z",
        "updatedAt": "2020-01-16T13:33:36.588Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083ff"
        },
        "nombre": "Oiba",
        "descripcion": "Oiba",
        "abreviacion": "Oiba",
        "createdAt": "2020-01-16T13:33:36.595Z",
        "updatedAt": "2020-01-16T13:33:36.595Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608394"
        },
        "nombre": "Sibundoy",
        "descripcion": "Sibundoy",
        "abreviacion": "Sibundoy",
        "createdAt": "2020-01-16T13:33:36.514Z",
        "updatedAt": "2020-01-16T13:33:36.514Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083f2"
        },
        "nombre": "Simacota",
        "descripcion": "Simacota",
        "abreviacion": "Simacota",
        "createdAt": "2020-01-16T13:33:36.586Z",
        "updatedAt": "2020-01-16T13:33:36.586Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083d1"
        },
        "nombre": "Ambalema",
        "descripcion": "Ambalema",
        "abreviacion": "Ambalema",
        "createdAt": "2020-01-16T13:33:36.559Z",
        "updatedAt": "2020-01-16T13:33:36.559Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083fb"
        },
        "nombre": "Piedecuesta",
        "descripcion": "Piedecuesta",
        "abreviacion": "Piedecuesta",
        "createdAt": "2020-01-16T13:33:36.592Z",
        "updatedAt": "2020-01-16T13:33:36.592Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608404"
        },
        "nombre": "Málaga",
        "descripcion": "Málaga",
        "abreviacion": "Málaga",
        "createdAt": "2020-01-16T13:33:36.600Z",
        "updatedAt": "2020-01-16T13:33:36.600Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608399"
        },
        "nombre": "Colón",
        "descripcion": "Colón",
        "abreviacion": "Colón",
        "createdAt": "2020-01-16T13:33:36.518Z",
        "updatedAt": "2020-01-16T13:33:36.518Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083f7"
        },
        "nombre": "San Andrés",
        "descripcion": "San Andrés",
        "abreviacion": "San Andrés",
        "createdAt": "2020-01-16T13:33:36.589Z",
        "updatedAt": "2020-01-16T13:33:36.589Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083d6"
        },
        "nombre": "San Pedro",
        "descripcion": "San Pedro",
        "abreviacion": "San Pedro",
        "createdAt": "2020-01-16T13:33:36.562Z",
        "updatedAt": "2020-01-16T13:33:36.562Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608400"
        },
        "nombre": "Ocamonte",
        "descripcion": "Ocamonte",
        "abreviacion": "Ocamonte",
        "createdAt": "2020-01-16T13:33:36.595Z",
        "updatedAt": "2020-01-16T13:33:36.595Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608409"
        },
        "nombre": "Landázuri",
        "descripcion": "Landázuri",
        "abreviacion": "Landázuri",
        "createdAt": "2020-01-16T13:33:36.605Z",
        "updatedAt": "2020-01-16T13:33:36.605Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60839e"
        },
        "nombre": "Sácama",
        "descripcion": "Sácama",
        "abreviacion": "Sácama",
        "createdAt": "2020-01-16T13:33:36.523Z",
        "updatedAt": "2020-01-16T13:33:36.523Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083fc"
        },
        "nombre": "Páramo",
        "descripcion": "Páramo",
        "abreviacion": "Páramo",
        "createdAt": "2020-01-16T13:33:36.593Z",
        "updatedAt": "2020-01-16T13:33:36.593Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083db"
        },
        "nombre": "Ovejas",
        "descripcion": "Ovejas",
        "abreviacion": "Ovejas",
        "createdAt": "2020-01-16T13:33:36.565Z",
        "updatedAt": "2020-01-16T13:33:36.565Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608405"
        },
        "nombre": "Macaravita",
        "descripcion": "Macaravita",
        "abreviacion": "Macaravita",
        "createdAt": "2020-01-16T13:33:36.601Z",
        "updatedAt": "2020-01-16T13:33:36.601Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60840e"
        },
        "nombre": "Guavatá",
        "descripcion": "Guavatá",
        "abreviacion": "Guavatá",
        "createdAt": "2020-01-16T13:33:36.608Z",
        "updatedAt": "2020-01-16T13:33:36.608Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083a3"
        },
        "nombre": "La Salina",
        "descripcion": "La Salina",
        "abreviacion": "La Salina",
        "createdAt": "2020-01-16T13:33:36.529Z",
        "updatedAt": "2020-01-16T13:33:36.529Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608401"
        },
        "nombre": "Molagavita",
        "descripcion": "Molagavita",
        "abreviacion": "Molagavita",
        "createdAt": "2020-01-16T13:33:36.596Z",
        "updatedAt": "2020-01-16T13:33:36.596Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083e0"
        },
        "nombre": "Guaranda",
        "descripcion": "Guaranda",
        "abreviacion": "Guaranda",
        "createdAt": "2020-01-16T13:33:36.569Z",
        "updatedAt": "2020-01-16T13:33:36.569Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60840a"
        },
        "nombre": "La Belleza",
        "descripcion": "La Belleza",
        "abreviacion": "La Belleza",
        "createdAt": "2020-01-16T13:33:36.606Z",
        "updatedAt": "2020-01-16T13:33:36.606Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608413"
        },
        "nombre": "Gambita",
        "descripcion": "Gambita",
        "abreviacion": "Gambita",
        "createdAt": "2020-01-16T13:33:36.611Z",
        "updatedAt": "2020-01-16T13:33:36.611Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083a8"
        },
        "nombre": "Arauca",
        "descripcion": "Arauca",
        "abreviacion": "Arauca",
        "createdAt": "2020-01-16T13:33:36.532Z",
        "updatedAt": "2020-01-16T13:33:36.532Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608406"
        },
        "nombre": "Los Santos",
        "descripcion": "Los Santos",
        "abreviacion": "Los Santos",
        "createdAt": "2020-01-16T13:33:36.603Z",
        "updatedAt": "2020-01-16T13:33:36.603Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083e5"
        },
        "nombre": "Coloso",
        "descripcion": "Coloso",
        "abreviacion": "Coloso",
        "createdAt": "2020-01-16T13:33:36.573Z",
        "updatedAt": "2020-01-16T13:33:36.573Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60840f"
        },
        "nombre": "Guapotá",
        "descripcion": "Guapotá",
        "abreviacion": "Guapotá",
        "createdAt": "2020-01-16T13:33:36.609Z",
        "updatedAt": "2020-01-16T13:33:36.609Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608418"
        },
        "nombre": "Encino",
        "descripcion": "Encino",
        "abreviacion": "Encino",
        "createdAt": "2020-01-16T13:33:36.614Z",
        "updatedAt": "2020-01-16T13:33:36.614Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083ad"
        },
        "nombre": "Cravo Norte",
        "descripcion": "Cravo Norte",
        "abreviacion": "Cravo Norte",
        "createdAt": "2020-01-16T13:33:36.536Z",
        "updatedAt": "2020-01-16T13:33:36.536Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60840b"
        },
        "nombre": "Jordán",
        "descripcion": "Jordán",
        "abreviacion": "Jordán",
        "createdAt": "2020-01-16T13:33:36.606Z",
        "updatedAt": "2020-01-16T13:33:36.606Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083ea"
        },
        "nombre": "Villanueva",
        "descripcion": "Villanueva",
        "abreviacion": "Villanueva",
        "createdAt": "2020-01-16T13:33:36.576Z",
        "updatedAt": "2020-01-16T13:33:36.576Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608414"
        },
        "nombre": "Galán",
        "descripcion": "Galán",
        "abreviacion": "Galán",
        "createdAt": "2020-01-16T13:33:36.612Z",
        "updatedAt": "2020-01-16T13:33:36.612Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60841d"
        },
        "nombre": "Imués",
        "descripcion": "Imués",
        "abreviacion": "Imués",
        "createdAt": "2020-01-16T13:33:36.617Z",
        "updatedAt": "2020-01-16T13:33:36.617Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608410"
        },
        "nombre": "Guadalupe",
        "descripcion": "Guadalupe",
        "abreviacion": "Guadalupe",
        "createdAt": "2020-01-16T13:33:36.609Z",
        "updatedAt": "2020-01-16T13:33:36.609Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083b2"
        },
        "nombre": "Santa Isabel",
        "descripcion": "Santa Isabel",
        "abreviacion": "Santa Isabel",
        "createdAt": "2020-01-16T13:33:36.540Z",
        "updatedAt": "2020-01-16T13:33:36.540Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083ef"
        },
        "nombre": "Sucre",
        "descripcion": "Sucre",
        "abreviacion": "Sucre",
        "createdAt": "2020-01-16T13:33:36.583Z",
        "updatedAt": "2020-01-16T13:33:36.583Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608419"
        },
        "nombre": "El Playón",
        "descripcion": "El Playón",
        "abreviacion": "El Playón",
        "createdAt": "2020-01-16T13:33:36.615Z",
        "updatedAt": "2020-01-16T13:33:36.615Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608422"
        },
        "nombre": "Curití",
        "descripcion": "Curití",
        "abreviacion": "Curití",
        "createdAt": "2020-01-16T13:33:36.622Z",
        "updatedAt": "2020-01-16T13:33:36.622Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608415"
        },
        "nombre": "Floridablanca",
        "descripcion": "Floridablanca",
        "abreviacion": "Floridablanca",
        "createdAt": "2020-01-16T13:33:36.612Z",
        "updatedAt": "2020-01-16T13:33:36.612Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083b7"
        },
        "nombre": "Purificación",
        "descripcion": "Purificación",
        "abreviacion": "Purificación",
        "createdAt": "2020-01-16T13:33:36.543Z",
        "updatedAt": "2020-01-16T13:33:36.543Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083f4"
        },
        "nombre": "San Miguel",
        "descripcion": "San Miguel",
        "abreviacion": "San Miguel",
        "createdAt": "2020-01-16T13:33:36.587Z",
        "updatedAt": "2020-01-16T13:33:36.587Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60841e"
        },
        "nombre": "Aldana",
        "descripcion": "Aldana",
        "abreviacion": "Aldana",
        "createdAt": "2020-01-16T13:33:36.618Z",
        "updatedAt": "2020-01-16T13:33:36.618Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608427"
        },
        "nombre": "Cimitarra",
        "descripcion": "Cimitarra",
        "abreviacion": "Cimitarra",
        "createdAt": "2020-01-16T13:33:36.627Z",
        "updatedAt": "2020-01-16T13:33:36.627Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60841a"
        },
        "nombre": "El Guacamayo",
        "descripcion": "El Guacamayo",
        "abreviacion": "El Guacamayo",
        "createdAt": "2020-01-16T13:33:36.615Z",
        "updatedAt": "2020-01-16T13:33:36.615Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083bc"
        },
        "nombre": "Ortega",
        "descripcion": "Ortega",
        "abreviacion": "Ortega",
        "createdAt": "2020-01-16T13:33:36.547Z",
        "updatedAt": "2020-01-16T13:33:36.547Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083f9"
        },
        "nombre": "Puente Nacional",
        "descripcion": "Puente Nacional",
        "abreviacion": "Puente Nacional",
        "createdAt": "2020-01-16T13:33:36.590Z",
        "updatedAt": "2020-01-16T13:33:36.590Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608423"
        },
        "nombre": "Coromoro",
        "descripcion": "Coromoro",
        "abreviacion": "Coromoro",
        "createdAt": "2020-01-16T13:33:36.623Z",
        "updatedAt": "2020-01-16T13:33:36.623Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60842c"
        },
        "nombre": "Cepitá",
        "descripcion": "Cepitá",
        "abreviacion": "Cepitá",
        "createdAt": "2020-01-16T13:33:36.633Z",
        "updatedAt": "2020-01-16T13:33:36.633Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083c1"
        },
        "nombre": "Icononzo",
        "descripcion": "Icononzo",
        "abreviacion": "Icononzo",
        "createdAt": "2020-01-16T13:33:36.550Z",
        "updatedAt": "2020-01-16T13:33:36.550Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60841f"
        },
        "nombre": "Acacias",
        "descripcion": "Acacias",
        "abreviacion": "Acacias",
        "createdAt": "2020-01-16T13:33:36.618Z",
        "updatedAt": "2020-01-16T13:33:36.618Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083fe"
        },
        "nombre": "Onzaga",
        "descripcion": "Onzaga",
        "abreviacion": "Onzaga",
        "createdAt": "2020-01-16T13:33:36.594Z",
        "updatedAt": "2020-01-16T13:33:36.594Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608428"
        },
        "nombre": "Chipatá",
        "descripcion": "Chipatá",
        "abreviacion": "Chipatá",
        "createdAt": "2020-01-16T13:33:36.628Z",
        "updatedAt": "2020-01-16T13:33:36.628Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608431"
        },
        "nombre": "Betulia",
        "descripcion": "Betulia",
        "abreviacion": "Betulia",
        "createdAt": "2020-01-16T13:33:36.638Z",
        "updatedAt": "2020-01-16T13:33:36.638Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608424"
        },
        "nombre": "Contratación",
        "descripcion": "Contratación",
        "abreviacion": "Contratación",
        "createdAt": "2020-01-16T13:33:36.623Z",
        "updatedAt": "2020-01-16T13:33:36.623Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083c6"
        },
        "nombre": "Flandes",
        "descripcion": "Flandes",
        "abreviacion": "Flandes",
        "createdAt": "2020-01-16T13:33:36.553Z",
        "updatedAt": "2020-01-16T13:33:36.553Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608403"
        },
        "nombre": "Matanza",
        "descripcion": "Matanza",
        "abreviacion": "Matanza",
        "createdAt": "2020-01-16T13:33:36.598Z",
        "updatedAt": "2020-01-16T13:33:36.598Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608436"
        },
        "nombre": "Albania",
        "descripcion": "Albania",
        "abreviacion": "Albania",
        "createdAt": "2020-01-16T13:33:36.641Z",
        "updatedAt": "2020-01-16T13:33:36.641Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60842d"
        },
        "nombre": "Carcasí",
        "descripcion": "Carcasí",
        "abreviacion": "Carcasí",
        "createdAt": "2020-01-16T13:33:36.634Z",
        "updatedAt": "2020-01-16T13:33:36.634Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083cb"
        },
        "nombre": "Coyaima",
        "descripcion": "Coyaima",
        "abreviacion": "Coyaima",
        "createdAt": "2020-01-16T13:33:36.556Z",
        "updatedAt": "2020-01-16T13:33:36.556Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608429"
        },
        "nombre": "Charta",
        "descripcion": "Charta",
        "abreviacion": "Charta",
        "createdAt": "2020-01-16T13:33:36.630Z",
        "updatedAt": "2020-01-16T13:33:36.630Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608408"
        },
        "nombre": "La Paz",
        "descripcion": "La Paz",
        "abreviacion": "La Paz",
        "createdAt": "2020-01-16T13:33:36.604Z",
        "updatedAt": "2020-01-16T13:33:36.604Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608432"
        },
        "nombre": "Barrancabermeja",
        "descripcion": "Barrancabermeja",
        "abreviacion": "Barrancabermeja",
        "createdAt": "2020-01-16T13:33:36.639Z",
        "updatedAt": "2020-01-16T13:33:36.639Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60843b"
        },
        "nombre": "Mistrató",
        "descripcion": "Mistrató",
        "abreviacion": "Mistrató",
        "createdAt": "2020-01-16T13:33:36.644Z",
        "updatedAt": "2020-01-16T13:33:36.644Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083d0"
        },
        "nombre": "Armero",
        "descripcion": "Armero",
        "abreviacion": "Armero",
        "createdAt": "2020-01-16T13:33:36.558Z",
        "updatedAt": "2020-01-16T13:33:36.558Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60842e"
        },
        "nombre": "California",
        "descripcion": "California",
        "abreviacion": "California",
        "createdAt": "2020-01-16T13:33:36.635Z",
        "updatedAt": "2020-01-16T13:33:36.635Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60840d"
        },
        "nombre": "Güepsa",
        "descripcion": "Güepsa",
        "abreviacion": "Güepsa",
        "createdAt": "2020-01-16T13:33:36.607Z",
        "updatedAt": "2020-01-16T13:33:36.607Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608437"
        },
        "nombre": "Aguada",
        "descripcion": "Aguada",
        "abreviacion": "Aguada",
        "createdAt": "2020-01-16T13:33:36.642Z",
        "updatedAt": "2020-01-16T13:33:36.642Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608440"
        },
        "nombre": "Dosquebradas",
        "descripcion": "Dosquebradas",
        "abreviacion": "Dosquebradas",
        "createdAt": "2020-01-16T13:33:36.647Z",
        "updatedAt": "2020-01-16T13:33:36.647Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083d5"
        },
        "nombre": "Sucre",
        "descripcion": "Sucre",
        "abreviacion": "Sucre",
        "createdAt": "2020-01-16T13:33:36.561Z",
        "updatedAt": "2020-01-16T13:33:36.561Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608433"
        },
        "nombre": "Barichara",
        "descripcion": "Barichara",
        "abreviacion": "Barichara",
        "createdAt": "2020-01-16T13:33:36.639Z",
        "updatedAt": "2020-01-16T13:33:36.639Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608412"
        },
        "nombre": "Girón",
        "descripcion": "Girón",
        "abreviacion": "Girón",
        "createdAt": "2020-01-16T13:33:36.610Z",
        "updatedAt": "2020-01-16T13:33:36.610Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60843c"
        },
        "nombre": "Marsella",
        "descripcion": "Marsella",
        "abreviacion": "Marsella",
        "createdAt": "2020-01-16T13:33:36.645Z",
        "updatedAt": "2020-01-16T13:33:36.645Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083da"
        },
        "nombre": "Palmito",
        "descripcion": "Palmito",
        "abreviacion": "Palmito",
        "createdAt": "2020-01-16T13:33:36.565Z",
        "updatedAt": "2020-01-16T13:33:36.565Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608445"
        },
        "nombre": "Quimbaya",
        "descripcion": "Quimbaya",
        "abreviacion": "Quimbaya",
        "createdAt": "2020-01-16T13:33:36.650Z",
        "updatedAt": "2020-01-16T13:33:36.650Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608438"
        },
        "nombre": "Bucaramanga",
        "descripcion": "Bucaramanga",
        "abreviacion": "Bucaramanga",
        "createdAt": "2020-01-16T13:33:36.642Z",
        "updatedAt": "2020-01-16T13:33:36.642Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608417"
        },
        "nombre": "Enciso",
        "descripcion": "Enciso",
        "abreviacion": "Enciso",
        "createdAt": "2020-01-16T13:33:36.613Z",
        "updatedAt": "2020-01-16T13:33:36.613Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608441"
        },
        "nombre": "Balboa",
        "descripcion": "Balboa",
        "abreviacion": "Balboa",
        "createdAt": "2020-01-16T13:33:36.647Z",
        "updatedAt": "2020-01-16T13:33:36.647Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083df"
        },
        "nombre": "La Unión",
        "descripcion": "La Unión",
        "abreviacion": "La Unión",
        "createdAt": "2020-01-16T13:33:36.568Z",
        "updatedAt": "2020-01-16T13:33:36.568Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60844a"
        },
        "nombre": "Córdoba",
        "descripcion": "Córdoba",
        "abreviacion": "Córdoba",
        "createdAt": "2020-01-16T13:33:36.653Z",
        "updatedAt": "2020-01-16T13:33:36.653Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60843d"
        },
        "nombre": "La Virginia",
        "descripcion": "La Virginia",
        "abreviacion": "La Virginia",
        "createdAt": "2020-01-16T13:33:36.645Z",
        "updatedAt": "2020-01-16T13:33:36.645Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608446"
        },
        "nombre": "Pijao",
        "descripcion": "Pijao",
        "abreviacion": "Pijao",
        "createdAt": "2020-01-16T13:33:36.650Z",
        "updatedAt": "2020-01-16T13:33:36.650Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60841c"
        },
        "nombre": "Belmira",
        "descripcion": "Belmira",
        "abreviacion": "Belmira",
        "createdAt": "2020-01-16T13:33:36.616Z",
        "updatedAt": "2020-01-16T13:33:36.616Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083e4"
        },
        "nombre": "Coveñas",
        "descripcion": "Coveñas",
        "abreviacion": "Coveñas",
        "createdAt": "2020-01-16T13:33:36.572Z",
        "updatedAt": "2020-01-16T13:33:36.572Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60844f"
        },
        "nombre": "Puerto Wilches",
        "descripcion": "Puerto Wilches",
        "abreviacion": "Puerto Wilches",
        "createdAt": "2020-01-16T13:33:36.656Z",
        "updatedAt": "2020-01-16T13:33:36.656Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608442"
        },
        "nombre": "Apía",
        "descripcion": "Apía",
        "abreviacion": "Apía",
        "createdAt": "2020-01-16T13:33:36.648Z",
        "updatedAt": "2020-01-16T13:33:36.648Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60844b"
        },
        "nombre": "Circasia",
        "descripcion": "Circasia",
        "abreviacion": "Circasia",
        "createdAt": "2020-01-16T13:33:36.654Z",
        "updatedAt": "2020-01-16T13:33:36.654Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608421"
        },
        "nombre": "Oporapa",
        "descripcion": "Oporapa",
        "abreviacion": "Oporapa",
        "createdAt": "2020-01-16T13:33:36.621Z",
        "updatedAt": "2020-01-16T13:33:36.621Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083e9"
        },
        "nombre": "Zapatoca",
        "descripcion": "Zapatoca",
        "abreviacion": "Zapatoca",
        "createdAt": "2020-01-16T13:33:36.575Z",
        "updatedAt": "2020-01-16T13:33:36.575Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608454"
        },
        "nombre": "Taminango",
        "descripcion": "Taminango",
        "abreviacion": "Taminango",
        "createdAt": "2020-01-16T13:33:36.659Z",
        "updatedAt": "2020-01-16T13:33:36.659Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608447"
        },
        "nombre": "Montenegro",
        "descripcion": "Montenegro",
        "abreviacion": "Montenegro",
        "createdAt": "2020-01-16T13:33:36.651Z",
        "updatedAt": "2020-01-16T13:33:36.651Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608426"
        },
        "nombre": "Concepción",
        "descripcion": "Concepción",
        "abreviacion": "Concepción",
        "createdAt": "2020-01-16T13:33:36.626Z",
        "updatedAt": "2020-01-16T13:33:36.626Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608450"
        },
        "nombre": "Yacuanquer",
        "descripcion": "Yacuanquer",
        "abreviacion": "Yacuanquer",
        "createdAt": "2020-01-16T13:33:36.657Z",
        "updatedAt": "2020-01-16T13:33:36.657Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083ee"
        },
        "nombre": "Suratá",
        "descripcion": "Suratá",
        "abreviacion": "Suratá",
        "createdAt": "2020-01-16T13:33:36.582Z",
        "updatedAt": "2020-01-16T13:33:36.582Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608459"
        },
        "nombre": "San Lorenzo",
        "descripcion": "San Lorenzo",
        "abreviacion": "San Lorenzo",
        "createdAt": "2020-01-16T13:33:36.662Z",
        "updatedAt": "2020-01-16T13:33:36.662Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60844c"
        },
        "nombre": "Buenavista",
        "descripcion": "Buenavista",
        "abreviacion": "Buenavista",
        "createdAt": "2020-01-16T13:33:36.654Z",
        "updatedAt": "2020-01-16T13:33:36.654Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608455"
        },
        "nombre": "Sapuyes",
        "descripcion": "Sapuyes",
        "abreviacion": "Sapuyes",
        "createdAt": "2020-01-16T13:33:36.660Z",
        "updatedAt": "2020-01-16T13:33:36.660Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083f3"
        },
        "nombre": "Santa Bárbara",
        "descripcion": "Santa Bárbara",
        "abreviacion": "Santa Bárbara",
        "createdAt": "2020-01-16T13:33:36.586Z",
        "updatedAt": "2020-01-16T13:33:36.586Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60842b"
        },
        "nombre": "Cerrito",
        "descripcion": "Cerrito",
        "abreviacion": "Cerrito",
        "createdAt": "2020-01-16T13:33:36.632Z",
        "updatedAt": "2020-01-16T13:33:36.632Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60845e"
        },
        "nombre": "Ricaurte",
        "descripcion": "Ricaurte",
        "abreviacion": "Ricaurte",
        "createdAt": "2020-01-16T13:33:36.665Z",
        "updatedAt": "2020-01-16T13:33:36.665Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608451"
        },
        "nombre": "Túquerres",
        "descripcion": "Túquerres",
        "abreviacion": "Túquerres",
        "createdAt": "2020-01-16T13:33:36.658Z",
        "updatedAt": "2020-01-16T13:33:36.658Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60845a"
        },
        "nombre": "San Bernardo",
        "descripcion": "San Bernardo",
        "abreviacion": "San Bernardo",
        "createdAt": "2020-01-16T13:33:36.663Z",
        "updatedAt": "2020-01-16T13:33:36.663Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083f8"
        },
        "nombre": "Rionegro",
        "descripcion": "Rionegro",
        "abreviacion": "Rionegro",
        "createdAt": "2020-01-16T13:33:36.590Z",
        "updatedAt": "2020-01-16T13:33:36.590Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608430"
        },
        "nombre": "Bolívar",
        "descripcion": "Bolívar",
        "abreviacion": "Bolívar",
        "createdAt": "2020-01-16T13:33:36.637Z",
        "updatedAt": "2020-01-16T13:33:36.637Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608463"
        },
        "nombre": "Policarpa",
        "descripcion": "Policarpa",
        "abreviacion": "Policarpa",
        "createdAt": "2020-01-16T13:33:36.670Z",
        "updatedAt": "2020-01-16T13:33:36.670Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608456"
        },
        "nombre": "Santa Bárbara",
        "descripcion": "Santa Bárbara",
        "abreviacion": "Santa Bárbara",
        "createdAt": "2020-01-16T13:33:36.660Z",
        "updatedAt": "2020-01-16T13:33:36.660Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60845f"
        },
        "nombre": "Pupiales",
        "descripcion": "Pupiales",
        "abreviacion": "Pupiales",
        "createdAt": "2020-01-16T13:33:36.666Z",
        "updatedAt": "2020-01-16T13:33:36.666Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6083fd"
        },
        "nombre": "Palmar",
        "descripcion": "Palmar",
        "abreviacion": "Palmar",
        "createdAt": "2020-01-16T13:33:36.593Z",
        "updatedAt": "2020-01-16T13:33:36.593Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608435"
        },
        "nombre": "Aratoca",
        "descripcion": "Aratoca",
        "abreviacion": "Aratoca",
        "createdAt": "2020-01-16T13:33:36.641Z",
        "updatedAt": "2020-01-16T13:33:36.641Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608468"
        },
        "nombre": "Mosquera",
        "descripcion": "Mosquera",
        "abreviacion": "Mosquera",
        "createdAt": "2020-01-16T13:33:36.676Z",
        "updatedAt": "2020-01-16T13:33:36.676Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60845b"
        },
        "nombre": "Sandoná",
        "descripcion": "Sandoná",
        "abreviacion": "Sandoná",
        "createdAt": "2020-01-16T13:33:36.664Z",
        "updatedAt": "2020-01-16T13:33:36.664Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608464"
        },
        "nombre": "Francisco Pizarro",
        "descripcion": "Francisco Pizarro",
        "abreviacion": "Francisco Pizarro",
        "createdAt": "2020-01-16T13:33:36.670Z",
        "updatedAt": "2020-01-16T13:33:36.670Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608402"
        },
        "nombre": "Mogotes",
        "descripcion": "Mogotes",
        "abreviacion": "Mogotes",
        "createdAt": "2020-01-16T13:33:36.597Z",
        "updatedAt": "2020-01-16T13:33:36.597Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60846d"
        },
        "nombre": "Leiva",
        "descripcion": "Leiva",
        "abreviacion": "Leiva",
        "createdAt": "2020-01-16T13:33:36.680Z",
        "updatedAt": "2020-01-16T13:33:36.680Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60843a"
        },
        "nombre": "Pueblo Rico",
        "descripcion": "Pueblo Rico",
        "abreviacion": "Pueblo Rico",
        "createdAt": "2020-01-16T13:33:36.643Z",
        "updatedAt": "2020-01-16T13:33:36.643Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608460"
        },
        "nombre": "Puerres",
        "descripcion": "Puerres",
        "abreviacion": "Puerres",
        "createdAt": "2020-01-16T13:33:36.666Z",
        "updatedAt": "2020-01-16T13:33:36.666Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608469"
        },
        "nombre": "Mallama",
        "descripcion": "Mallama",
        "abreviacion": "Mallama",
        "createdAt": "2020-01-16T13:33:36.677Z",
        "updatedAt": "2020-01-16T13:33:36.677Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608407"
        },
        "nombre": "Lebríja",
        "descripcion": "Lebríja",
        "abreviacion": "Lebríja",
        "createdAt": "2020-01-16T13:33:36.604Z",
        "updatedAt": "2020-01-16T13:33:36.604Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60843f"
        },
        "nombre": "Guática",
        "descripcion": "Guática",
        "abreviacion": "Guática",
        "createdAt": "2020-01-16T13:33:36.646Z",
        "updatedAt": "2020-01-16T13:33:36.646Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608472"
        },
        "nombre": "La Cruz",
        "descripcion": "La Cruz",
        "abreviacion": "La Cruz",
        "createdAt": "2020-01-16T13:33:36.688Z",
        "updatedAt": "2020-01-16T13:33:36.688Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608465"
        },
        "nombre": "Ospina",
        "descripcion": "Ospina",
        "abreviacion": "Ospina",
        "createdAt": "2020-01-16T13:33:36.671Z",
        "updatedAt": "2020-01-16T13:33:36.671Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60846e"
        },
        "nombre": "La Unión",
        "descripcion": "La Unión",
        "abreviacion": "La Unión",
        "createdAt": "2020-01-16T13:33:36.681Z",
        "updatedAt": "2020-01-16T13:33:36.681Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608444"
        },
        "nombre": "Salento",
        "descripcion": "Salento",
        "abreviacion": "Salento",
        "createdAt": "2020-01-16T13:33:36.649Z",
        "updatedAt": "2020-01-16T13:33:36.649Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60840c"
        },
        "nombre": "Jesús María",
        "descripcion": "Jesús María",
        "abreviacion": "Jesús María",
        "createdAt": "2020-01-16T13:33:36.607Z",
        "updatedAt": "2020-01-16T13:33:36.607Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608477"
        },
        "nombre": "Guachucal",
        "descripcion": "Guachucal",
        "abreviacion": "Guachucal",
        "createdAt": "2020-01-16T13:33:36.692Z",
        "updatedAt": "2020-01-16T13:33:36.692Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60846a"
        },
        "nombre": "Magüí",
        "descripcion": "Magüí",
        "abreviacion": "Magüí",
        "createdAt": "2020-01-16T13:33:36.678Z",
        "updatedAt": "2020-01-16T13:33:36.678Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608473"
        },
        "nombre": "Ipiales",
        "descripcion": "Ipiales",
        "abreviacion": "Ipiales",
        "createdAt": "2020-01-16T13:33:36.689Z",
        "updatedAt": "2020-01-16T13:33:36.689Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608449"
        },
        "nombre": "Filandia",
        "descripcion": "Filandia",
        "abreviacion": "Filandia",
        "createdAt": "2020-01-16T13:33:36.653Z",
        "updatedAt": "2020-01-16T13:33:36.653Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608411"
        },
        "nombre": "Guaca",
        "descripcion": "Guaca",
        "abreviacion": "Guaca",
        "createdAt": "2020-01-16T13:33:36.610Z",
        "updatedAt": "2020-01-16T13:33:36.610Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60847c"
        },
        "nombre": "El Peñol",
        "descripcion": "El Peñol",
        "abreviacion": "El Peñol",
        "createdAt": "2020-01-16T13:33:36.695Z",
        "updatedAt": "2020-01-16T13:33:36.695Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60846f"
        },
        "nombre": "La Tola",
        "descripcion": "La Tola",
        "abreviacion": "La Tola",
        "createdAt": "2020-01-16T13:33:36.681Z",
        "updatedAt": "2020-01-16T13:33:36.681Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608478"
        },
        "nombre": "Funes",
        "descripcion": "Funes",
        "abreviacion": "Funes",
        "createdAt": "2020-01-16T13:33:36.692Z",
        "updatedAt": "2020-01-16T13:33:36.692Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60844e"
        },
        "nombre": "Puerto Parra",
        "descripcion": "Puerto Parra",
        "abreviacion": "Puerto Parra",
        "createdAt": "2020-01-16T13:33:36.656Z",
        "updatedAt": "2020-01-16T13:33:36.656Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608481"
        },
        "nombre": "Córdoba",
        "descripcion": "Córdoba",
        "abreviacion": "Córdoba",
        "createdAt": "2020-01-16T13:33:36.700Z",
        "updatedAt": "2020-01-16T13:33:36.700Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608416"
        },
        "nombre": "Florián",
        "descripcion": "Florián",
        "abreviacion": "Florián",
        "createdAt": "2020-01-16T13:33:36.613Z",
        "updatedAt": "2020-01-16T13:33:36.613Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608474"
        },
        "nombre": "Iles",
        "descripcion": "Iles",
        "abreviacion": "Iles",
        "createdAt": "2020-01-16T13:33:36.690Z",
        "updatedAt": "2020-01-16T13:33:36.690Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60847d"
        },
        "nombre": "El Charco",
        "descripcion": "El Charco",
        "abreviacion": "El Charco",
        "createdAt": "2020-01-16T13:33:36.696Z",
        "updatedAt": "2020-01-16T13:33:36.696Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608453"
        },
        "nombre": "Tangua",
        "descripcion": "Tangua",
        "abreviacion": "Tangua",
        "createdAt": "2020-01-16T13:33:36.659Z",
        "updatedAt": "2020-01-16T13:33:36.659Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608486"
        },
        "nombre": "Motavita",
        "descripcion": "Motavita",
        "abreviacion": "Motavita",
        "createdAt": "2020-01-16T13:33:36.704Z",
        "updatedAt": "2020-01-16T13:33:36.704Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608479"
        },
        "nombre": "El Tambo",
        "descripcion": "El Tambo",
        "abreviacion": "El Tambo",
        "createdAt": "2020-01-16T13:33:36.693Z",
        "updatedAt": "2020-01-16T13:33:36.693Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60841b"
        },
        "nombre": "Santuario",
        "descripcion": "Santuario",
        "abreviacion": "Santuario",
        "createdAt": "2020-01-16T13:33:36.616Z",
        "updatedAt": "2020-01-16T13:33:36.616Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608482"
        },
        "nombre": "Contadero",
        "descripcion": "Contadero",
        "abreviacion": "Contadero",
        "createdAt": "2020-01-16T13:33:36.701Z",
        "updatedAt": "2020-01-16T13:33:36.701Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608458"
        },
        "nombre": "San Pablo",
        "descripcion": "San Pablo",
        "abreviacion": "San Pablo",
        "createdAt": "2020-01-16T13:33:36.662Z",
        "updatedAt": "2020-01-16T13:33:36.662Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60848b"
        },
        "nombre": "Pasto",
        "descripcion": "Pasto",
        "abreviacion": "Pasto",
        "createdAt": "2020-01-16T13:33:36.707Z",
        "updatedAt": "2020-01-16T13:33:36.707Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608420"
        },
        "nombre": "Uribia",
        "descripcion": "Uribia",
        "abreviacion": "Uribia",
        "createdAt": "2020-01-16T13:33:36.619Z",
        "updatedAt": "2020-01-16T13:33:36.619Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60847e"
        },
        "nombre": "Cumbitara",
        "descripcion": "Cumbitara",
        "abreviacion": "Cumbitara",
        "createdAt": "2020-01-16T13:33:36.696Z",
        "updatedAt": "2020-01-16T13:33:36.696Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608490"
        },
        "nombre": "Ciénaga",
        "descripcion": "Ciénaga",
        "abreviacion": "Ciénaga",
        "createdAt": "2020-01-16T13:33:36.710Z",
        "updatedAt": "2020-01-16T13:33:36.710Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60845d"
        },
        "nombre": "Roberto Payán",
        "descripcion": "Roberto Payán",
        "abreviacion": "Roberto Payán",
        "createdAt": "2020-01-16T13:33:36.665Z",
        "updatedAt": "2020-01-16T13:33:36.665Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608483"
        },
        "nombre": "Consaca",
        "descripcion": "Consaca",
        "abreviacion": "Consaca",
        "createdAt": "2020-01-16T13:33:36.702Z",
        "updatedAt": "2020-01-16T13:33:36.702Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608425"
        },
        "nombre": "Confines",
        "descripcion": "Confines",
        "abreviacion": "Confines",
        "createdAt": "2020-01-16T13:33:36.624Z",
        "updatedAt": "2020-01-16T13:33:36.624Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608487"
        },
        "nombre": "Barbacoas",
        "descripcion": "Barbacoas",
        "abreviacion": "Barbacoas",
        "createdAt": "2020-01-16T13:33:36.704Z",
        "updatedAt": "2020-01-16T13:33:36.704Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608462"
        },
        "nombre": "Potosí",
        "descripcion": "Potosí",
        "abreviacion": "Potosí",
        "createdAt": "2020-01-16T13:33:36.669Z",
        "updatedAt": "2020-01-16T13:33:36.669Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608495"
        },
        "nombre": "Puerto Gaitán",
        "descripcion": "Puerto Gaitán",
        "abreviacion": "Puerto Gaitán",
        "createdAt": "2020-01-16T13:33:36.713Z",
        "updatedAt": "2020-01-16T13:33:36.713Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608488"
        },
        "nombre": "Tununguá",
        "descripcion": "Tununguá",
        "abreviacion": "Tununguá",
        "createdAt": "2020-01-16T13:33:36.705Z",
        "updatedAt": "2020-01-16T13:33:36.705Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60842a"
        },
        "nombre": "Charalá",
        "descripcion": "Charalá",
        "abreviacion": "Charalá",
        "createdAt": "2020-01-16T13:33:36.631Z",
        "updatedAt": "2020-01-16T13:33:36.631Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60849a"
        },
        "nombre": "Mesetas",
        "descripcion": "Mesetas",
        "abreviacion": "Mesetas",
        "createdAt": "2020-01-16T13:33:36.715Z",
        "updatedAt": "2020-01-16T13:33:36.715Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608467"
        },
        "nombre": "Nariño",
        "descripcion": "Nariño",
        "abreviacion": "Nariño",
        "createdAt": "2020-01-16T13:33:36.675Z",
        "updatedAt": "2020-01-16T13:33:36.675Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60848c"
        },
        "nombre": "Vista Hermosa",
        "descripcion": "Vista Hermosa",
        "abreviacion": "Vista Hermosa",
        "createdAt": "2020-01-16T13:33:36.708Z",
        "updatedAt": "2020-01-16T13:33:36.708Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60848d"
        },
        "nombre": "San Martín",
        "descripcion": "San Martín",
        "abreviacion": "San Martín",
        "createdAt": "2020-01-16T13:33:36.708Z",
        "updatedAt": "2020-01-16T13:33:36.708Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60842f"
        },
        "nombre": "Cabrera",
        "descripcion": "Cabrera",
        "abreviacion": "Cabrera",
        "createdAt": "2020-01-16T13:33:36.636Z",
        "updatedAt": "2020-01-16T13:33:36.636Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60846c"
        },
        "nombre": "Linares",
        "descripcion": "Linares",
        "abreviacion": "Linares",
        "createdAt": "2020-01-16T13:33:36.679Z",
        "updatedAt": "2020-01-16T13:33:36.679Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608491"
        },
        "nombre": "Restrepo",
        "descripcion": "Restrepo",
        "abreviacion": "Restrepo",
        "createdAt": "2020-01-16T13:33:36.710Z",
        "updatedAt": "2020-01-16T13:33:36.710Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608492"
        },
        "nombre": "Puerto Rico",
        "descripcion": "Puerto Rico",
        "abreviacion": "Puerto Rico",
        "createdAt": "2020-01-16T13:33:36.711Z",
        "updatedAt": "2020-01-16T13:33:36.711Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60849f"
        },
        "nombre": "El Dorado",
        "descripcion": "El Dorado",
        "abreviacion": "El Dorado",
        "createdAt": "2020-01-16T13:33:36.720Z",
        "updatedAt": "2020-01-16T13:33:36.720Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608496"
        },
        "nombre": "Puerto Concordia",
        "descripcion": "Puerto Concordia",
        "abreviacion": "Puerto Concordia",
        "createdAt": "2020-01-16T13:33:36.713Z",
        "updatedAt": "2020-01-16T13:33:36.713Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608471"
        },
        "nombre": "La Florida",
        "descripcion": "La Florida",
        "abreviacion": "La Florida",
        "createdAt": "2020-01-16T13:33:36.687Z",
        "updatedAt": "2020-01-16T13:33:36.687Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084a4"
        },
        "nombre": "Cabuyaro",
        "descripcion": "Cabuyaro",
        "abreviacion": "Cabuyaro",
        "createdAt": "2020-01-16T13:33:36.724Z",
        "updatedAt": "2020-01-16T13:33:36.724Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608434"
        },
        "nombre": "Barbosa",
        "descripcion": "Barbosa",
        "abreviacion": "Barbosa",
        "createdAt": "2020-01-16T13:33:36.640Z",
        "updatedAt": "2020-01-16T13:33:36.640Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608497"
        },
        "nombre": "Lejanías",
        "descripcion": "Lejanías",
        "abreviacion": "Lejanías",
        "createdAt": "2020-01-16T13:33:36.714Z",
        "updatedAt": "2020-01-16T13:33:36.714Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60849b"
        },
        "nombre": "Mapiripán",
        "descripcion": "Mapiripán",
        "abreviacion": "Mapiripán",
        "createdAt": "2020-01-16T13:33:36.716Z",
        "updatedAt": "2020-01-16T13:33:36.716Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608476"
        },
        "nombre": "Guaitarilla",
        "descripcion": "Guaitarilla",
        "abreviacion": "Guaitarilla",
        "createdAt": "2020-01-16T13:33:36.691Z",
        "updatedAt": "2020-01-16T13:33:36.691Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084a9"
        },
        "nombre": "Zona Bananera",
        "descripcion": "Zona Bananera",
        "abreviacion": "Zona Bananera",
        "createdAt": "2020-01-16T13:33:36.731Z",
        "updatedAt": "2020-01-16T13:33:36.731Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608439"
        },
        "nombre": "Quinchía",
        "descripcion": "Quinchía",
        "abreviacion": "Quinchía",
        "createdAt": "2020-01-16T13:33:36.643Z",
        "updatedAt": "2020-01-16T13:33:36.643Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60849c"
        },
        "nombre": "Guamal",
        "descripcion": "Guamal",
        "abreviacion": "Guamal",
        "createdAt": "2020-01-16T13:33:36.717Z",
        "updatedAt": "2020-01-16T13:33:36.717Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60847b"
        },
        "nombre": "El Rosario",
        "descripcion": "El Rosario",
        "abreviacion": "El Rosario",
        "createdAt": "2020-01-16T13:33:36.694Z",
        "updatedAt": "2020-01-16T13:33:36.694Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084ae"
        },
        "nombre": "Elías",
        "descripcion": "Elías",
        "abreviacion": "Elías",
        "createdAt": "2020-01-16T13:33:36.733Z",
        "updatedAt": "2020-01-16T13:33:36.733Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084a0"
        },
        "nombre": "El Castillo",
        "descripcion": "El Castillo",
        "abreviacion": "El Castillo",
        "createdAt": "2020-01-16T13:33:36.721Z",
        "updatedAt": "2020-01-16T13:33:36.721Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084a1"
        },
        "nombre": "El Calvario",
        "descripcion": "El Calvario",
        "abreviacion": "El Calvario",
        "createdAt": "2020-01-16T13:33:36.722Z",
        "updatedAt": "2020-01-16T13:33:36.722Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60843e"
        },
        "nombre": "La Celia",
        "descripcion": "La Celia",
        "abreviacion": "La Celia",
        "createdAt": "2020-01-16T13:33:36.646Z",
        "updatedAt": "2020-01-16T13:33:36.646Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084b3"
        },
        "nombre": "Iquira",
        "descripcion": "Iquira",
        "abreviacion": "Iquira",
        "createdAt": "2020-01-16T13:33:36.736Z",
        "updatedAt": "2020-01-16T13:33:36.736Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608480"
        },
        "nombre": "Cuaspud",
        "descripcion": "Cuaspud",
        "abreviacion": "Cuaspud",
        "createdAt": "2020-01-16T13:33:36.699Z",
        "updatedAt": "2020-01-16T13:33:36.699Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084a5"
        },
        "nombre": "Santa Ana",
        "descripcion": "Santa Ana",
        "abreviacion": "Santa Ana",
        "createdAt": "2020-01-16T13:33:36.728Z",
        "updatedAt": "2020-01-16T13:33:36.728Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084a6"
        },
        "nombre": "Sitionuevo",
        "descripcion": "Sitionuevo",
        "abreviacion": "Sitionuevo",
        "createdAt": "2020-01-16T13:33:36.729Z",
        "updatedAt": "2020-01-16T13:33:36.729Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608443"
        },
        "nombre": "Pereira",
        "descripcion": "Pereira",
        "abreviacion": "Pereira",
        "createdAt": "2020-01-16T13:33:36.649Z",
        "updatedAt": "2020-01-16T13:33:36.649Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084b8"
        },
        "nombre": "Nátaga",
        "descripcion": "Nátaga",
        "abreviacion": "Nátaga",
        "createdAt": "2020-01-16T13:33:36.739Z",
        "updatedAt": "2020-01-16T13:33:36.739Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084aa"
        },
        "nombre": "Villavicencio",
        "descripcion": "Villavicencio",
        "abreviacion": "Villavicencio",
        "createdAt": "2020-01-16T13:33:36.731Z",
        "updatedAt": "2020-01-16T13:33:36.731Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608485"
        },
        "nombre": "San Bernardo del Viento",
        "descripcion": "San Bernardo del Viento",
        "abreviacion": "San Bernardo del Viento",
        "createdAt": "2020-01-16T13:33:36.703Z",
        "updatedAt": "2020-01-16T13:33:36.703Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084ab"
        },
        "nombre": "Baraya",
        "descripcion": "Baraya",
        "abreviacion": "Baraya",
        "createdAt": "2020-01-16T13:33:36.732Z",
        "updatedAt": "2020-01-16T13:33:36.732Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608448"
        },
        "nombre": "La Tebaida",
        "descripcion": "La Tebaida",
        "abreviacion": "La Tebaida",
        "createdAt": "2020-01-16T13:33:36.652Z",
        "updatedAt": "2020-01-16T13:33:36.652Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084af"
        },
        "nombre": "Garzón",
        "descripcion": "Garzón",
        "abreviacion": "Garzón",
        "createdAt": "2020-01-16T13:33:36.734Z",
        "updatedAt": "2020-01-16T13:33:36.734Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084bd"
        },
        "nombre": "Pitalito",
        "descripcion": "Pitalito",
        "abreviacion": "Pitalito",
        "createdAt": "2020-01-16T13:33:36.741Z",
        "updatedAt": "2020-01-16T13:33:36.741Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60848a"
        },
        "nombre": "Albán",
        "descripcion": "Albán",
        "abreviacion": "Albán",
        "createdAt": "2020-01-16T13:33:36.706Z",
        "updatedAt": "2020-01-16T13:33:36.706Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084b0"
        },
        "nombre": "Gigante",
        "descripcion": "Gigante",
        "abreviacion": "Gigante",
        "createdAt": "2020-01-16T13:33:36.735Z",
        "updatedAt": "2020-01-16T13:33:36.735Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084c2"
        },
        "nombre": "Suaza",
        "descripcion": "Suaza",
        "abreviacion": "Suaza",
        "createdAt": "2020-01-16T13:33:36.744Z",
        "updatedAt": "2020-01-16T13:33:36.744Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60844d"
        },
        "nombre": "Armenia",
        "descripcion": "Armenia",
        "abreviacion": "Armenia",
        "createdAt": "2020-01-16T13:33:36.655Z",
        "updatedAt": "2020-01-16T13:33:36.655Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60848f"
        },
        "nombre": "Ponedera",
        "descripcion": "Ponedera",
        "abreviacion": "Ponedera",
        "createdAt": "2020-01-16T13:33:36.709Z",
        "updatedAt": "2020-01-16T13:33:36.709Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084b4"
        },
        "nombre": "Isnos",
        "descripcion": "Isnos",
        "abreviacion": "Isnos",
        "createdAt": "2020-01-16T13:33:36.737Z",
        "updatedAt": "2020-01-16T13:33:36.737Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084b5"
        },
        "nombre": "La Argentina",
        "descripcion": "La Argentina",
        "abreviacion": "La Argentina",
        "createdAt": "2020-01-16T13:33:36.737Z",
        "updatedAt": "2020-01-16T13:33:36.737Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084c7"
        },
        "nombre": "Timaná",
        "descripcion": "Timaná",
        "abreviacion": "Timaná",
        "createdAt": "2020-01-16T13:33:36.747Z",
        "updatedAt": "2020-01-16T13:33:36.747Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084b9"
        },
        "nombre": "Paicol",
        "descripcion": "Paicol",
        "abreviacion": "Paicol",
        "createdAt": "2020-01-16T13:33:36.739Z",
        "updatedAt": "2020-01-16T13:33:36.739Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608494"
        },
        "nombre": "Puerto López",
        "descripcion": "Puerto López",
        "abreviacion": "Puerto López",
        "createdAt": "2020-01-16T13:33:36.712Z",
        "updatedAt": "2020-01-16T13:33:36.712Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608452"
        },
        "nombre": "Santacruz",
        "descripcion": "Santacruz",
        "abreviacion": "Santacruz",
        "createdAt": "2020-01-16T13:33:36.658Z",
        "updatedAt": "2020-01-16T13:33:36.658Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084ba"
        },
        "nombre": "Palermo",
        "descripcion": "Palermo",
        "abreviacion": "Palermo",
        "createdAt": "2020-01-16T13:33:36.740Z",
        "updatedAt": "2020-01-16T13:33:36.740Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084cc"
        },
        "nombre": "Barrancas",
        "descripcion": "Barrancas",
        "abreviacion": "Barrancas",
        "createdAt": "2020-01-16T13:33:36.749Z",
        "updatedAt": "2020-01-16T13:33:36.749Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608499"
        },
        "nombre": "La Macarena",
        "descripcion": "La Macarena",
        "abreviacion": "La Macarena",
        "createdAt": "2020-01-16T13:33:36.715Z",
        "updatedAt": "2020-01-16T13:33:36.715Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084be"
        },
        "nombre": "Rivera",
        "descripcion": "Rivera",
        "abreviacion": "Rivera",
        "createdAt": "2020-01-16T13:33:36.742Z",
        "updatedAt": "2020-01-16T13:33:36.742Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608457"
        },
        "nombre": "Ciénega",
        "descripcion": "Ciénega",
        "abreviacion": "Ciénega",
        "createdAt": "2020-01-16T13:33:36.661Z",
        "updatedAt": "2020-01-16T13:33:36.661Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084bf"
        },
        "nombre": "Saladoblanco",
        "descripcion": "Saladoblanco",
        "abreviacion": "Saladoblanco",
        "createdAt": "2020-01-16T13:33:36.742Z",
        "updatedAt": "2020-01-16T13:33:36.742Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084d1"
        },
        "nombre": "Hatonuevo",
        "descripcion": "Hatonuevo",
        "abreviacion": "Hatonuevo",
        "createdAt": "2020-01-16T13:33:36.752Z",
        "updatedAt": "2020-01-16T13:33:36.752Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60849e"
        },
        "nombre": "Buenaventura",
        "descripcion": "Buenaventura",
        "abreviacion": "Buenaventura",
        "createdAt": "2020-01-16T13:33:36.718Z",
        "updatedAt": "2020-01-16T13:33:36.718Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084c3"
        },
        "nombre": "Tarqui",
        "descripcion": "Tarqui",
        "abreviacion": "Tarqui",
        "createdAt": "2020-01-16T13:33:36.745Z",
        "updatedAt": "2020-01-16T13:33:36.745Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60845c"
        },
        "nombre": "Samaniego",
        "descripcion": "Samaniego",
        "abreviacion": "Samaniego",
        "createdAt": "2020-01-16T13:33:36.664Z",
        "updatedAt": "2020-01-16T13:33:36.664Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084d6"
        },
        "nombre": "Santa Marta",
        "descripcion": "Santa Marta",
        "abreviacion": "Santa Marta",
        "createdAt": "2020-01-16T13:33:36.754Z",
        "updatedAt": "2020-01-16T13:33:36.754Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084c4"
        },
        "nombre": "Tesalia",
        "descripcion": "Tesalia",
        "abreviacion": "Tesalia",
        "createdAt": "2020-01-16T13:33:36.745Z",
        "updatedAt": "2020-01-16T13:33:36.745Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084a3"
        },
        "nombre": "Cubarral",
        "descripcion": "Cubarral",
        "abreviacion": "Cubarral",
        "createdAt": "2020-01-16T13:33:36.724Z",
        "updatedAt": "2020-01-16T13:33:36.724Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084c8"
        },
        "nombre": "Villavieja",
        "descripcion": "Villavieja",
        "abreviacion": "Villavieja",
        "createdAt": "2020-01-16T13:33:36.747Z",
        "updatedAt": "2020-01-16T13:33:36.747Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084c9"
        },
        "nombre": "Yaguará",
        "descripcion": "Yaguará",
        "abreviacion": "Yaguará",
        "createdAt": "2020-01-16T13:33:36.748Z",
        "updatedAt": "2020-01-16T13:33:36.748Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084a8"
        },
        "nombre": "Zapayán",
        "descripcion": "Zapayán",
        "abreviacion": "Zapayán",
        "createdAt": "2020-01-16T13:33:36.730Z",
        "updatedAt": "2020-01-16T13:33:36.730Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084db"
        },
        "nombre": "Chivolo",
        "descripcion": "Chivolo",
        "abreviacion": "Chivolo",
        "createdAt": "2020-01-16T13:33:36.757Z",
        "updatedAt": "2020-01-16T13:33:36.757Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084cd"
        },
        "nombre": "Dibula",
        "descripcion": "Dibula",
        "abreviacion": "Dibula",
        "createdAt": "2020-01-16T13:33:36.750Z",
        "updatedAt": "2020-01-16T13:33:36.750Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608461"
        },
        "nombre": "Providencia",
        "descripcion": "Providencia",
        "abreviacion": "Providencia",
        "createdAt": "2020-01-16T13:33:36.668Z",
        "updatedAt": "2020-01-16T13:33:36.668Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084ce"
        },
        "nombre": "Distracción",
        "descripcion": "Distracción",
        "abreviacion": "Distracción",
        "createdAt": "2020-01-16T13:33:36.750Z",
        "updatedAt": "2020-01-16T13:33:36.750Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084ad"
        },
        "nombre": "Colombia",
        "descripcion": "Colombia",
        "abreviacion": "Colombia",
        "createdAt": "2020-01-16T13:33:36.733Z",
        "updatedAt": "2020-01-16T13:33:36.733Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084e0"
        },
        "nombre": "Fundación",
        "descripcion": "Fundación",
        "abreviacion": "Fundación",
        "createdAt": "2020-01-16T13:33:36.759Z",
        "updatedAt": "2020-01-16T13:33:36.759Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608466"
        },
        "nombre": "Olaya Herrera",
        "descripcion": "Olaya Herrera",
        "abreviacion": "Olaya Herrera",
        "createdAt": "2020-01-16T13:33:36.674Z",
        "updatedAt": "2020-01-16T13:33:36.674Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084d2"
        },
        "nombre": "Maicao",
        "descripcion": "Maicao",
        "abreviacion": "Maicao",
        "createdAt": "2020-01-16T13:33:36.752Z",
        "updatedAt": "2020-01-16T13:33:36.752Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084b2"
        },
        "nombre": "Hobo",
        "descripcion": "Hobo",
        "abreviacion": "Hobo",
        "createdAt": "2020-01-16T13:33:36.736Z",
        "updatedAt": "2020-01-16T13:33:36.736Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084e5"
        },
        "nombre": "Plato",
        "descripcion": "Plato",
        "abreviacion": "Plato",
        "createdAt": "2020-01-16T13:33:36.762Z",
        "updatedAt": "2020-01-16T13:33:36.762Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084d3"
        },
        "nombre": "Manaure",
        "descripcion": "Manaure",
        "abreviacion": "Manaure",
        "createdAt": "2020-01-16T13:33:36.753Z",
        "updatedAt": "2020-01-16T13:33:36.753Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60846b"
        },
        "nombre": "Los Andes",
        "descripcion": "Los Andes",
        "abreviacion": "Los Andes",
        "createdAt": "2020-01-16T13:33:36.678Z",
        "updatedAt": "2020-01-16T13:33:36.678Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084d7"
        },
        "nombre": "Algarrobo",
        "descripcion": "Algarrobo",
        "abreviacion": "Algarrobo",
        "createdAt": "2020-01-16T13:33:36.755Z",
        "updatedAt": "2020-01-16T13:33:36.755Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084b7"
        },
        "nombre": "Marquetalia",
        "descripcion": "Marquetalia",
        "abreviacion": "Marquetalia",
        "createdAt": "2020-01-16T13:33:36.738Z",
        "updatedAt": "2020-01-16T13:33:36.738Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084d8"
        },
        "nombre": "Aracataca",
        "descripcion": "Aracataca",
        "abreviacion": "Aracataca",
        "createdAt": "2020-01-16T13:33:36.755Z",
        "updatedAt": "2020-01-16T13:33:36.755Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084dc"
        },
        "nombre": "Concordia",
        "descripcion": "Concordia",
        "abreviacion": "Concordia",
        "createdAt": "2020-01-16T13:33:36.757Z",
        "updatedAt": "2020-01-16T13:33:36.757Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608470"
        },
        "nombre": "La Llanada",
        "descripcion": "La Llanada",
        "abreviacion": "La Llanada",
        "createdAt": "2020-01-16T13:33:36.687Z",
        "updatedAt": "2020-01-16T13:33:36.687Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084bc"
        },
        "nombre": "Pital",
        "descripcion": "Pital",
        "abreviacion": "Pital",
        "createdAt": "2020-01-16T13:33:36.741Z",
        "updatedAt": "2020-01-16T13:33:36.741Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084dd"
        },
        "nombre": "El Banco",
        "descripcion": "El Banco",
        "abreviacion": "El Banco",
        "createdAt": "2020-01-16T13:33:36.758Z",
        "updatedAt": "2020-01-16T13:33:36.758Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084e1"
        },
        "nombre": "Guamal",
        "descripcion": "Guamal",
        "abreviacion": "Guamal",
        "createdAt": "2020-01-16T13:33:36.760Z",
        "updatedAt": "2020-01-16T13:33:36.760Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608475"
        },
        "nombre": "Gualmatán",
        "descripcion": "Gualmatán",
        "abreviacion": "Gualmatán",
        "createdAt": "2020-01-16T13:33:36.691Z",
        "updatedAt": "2020-01-16T13:33:36.691Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084c1"
        },
        "nombre": "Santa María",
        "descripcion": "Santa María",
        "abreviacion": "Santa María",
        "createdAt": "2020-01-16T13:33:36.744Z",
        "updatedAt": "2020-01-16T13:33:36.744Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084e2"
        },
        "nombre": "Nueva Granada",
        "descripcion": "Nueva Granada",
        "abreviacion": "Nueva Granada",
        "createdAt": "2020-01-16T13:33:36.760Z",
        "updatedAt": "2020-01-16T13:33:36.760Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60847a"
        },
        "nombre": "Istmina",
        "descripcion": "Istmina",
        "abreviacion": "Istmina",
        "createdAt": "2020-01-16T13:33:36.694Z",
        "updatedAt": "2020-01-16T13:33:36.694Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084e6"
        },
        "nombre": "Remolino",
        "descripcion": "Remolino",
        "abreviacion": "Remolino",
        "createdAt": "2020-01-16T13:33:36.763Z",
        "updatedAt": "2020-01-16T13:33:36.763Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084e7"
        },
        "nombre": "Salamina",
        "descripcion": "Salamina",
        "abreviacion": "Salamina",
        "createdAt": "2020-01-16T13:33:36.764Z",
        "updatedAt": "2020-01-16T13:33:36.764Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084c6"
        },
        "nombre": "Teruel",
        "descripcion": "Teruel",
        "abreviacion": "Teruel",
        "createdAt": "2020-01-16T13:33:36.746Z",
        "updatedAt": "2020-01-16T13:33:36.746Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60847f"
        },
        "nombre": "Cumbal",
        "descripcion": "Cumbal",
        "abreviacion": "Cumbal",
        "createdAt": "2020-01-16T13:33:36.698Z",
        "updatedAt": "2020-01-16T13:33:36.698Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084cb"
        },
        "nombre": "Albania",
        "descripcion": "Albania",
        "abreviacion": "Albania",
        "createdAt": "2020-01-16T13:33:36.749Z",
        "updatedAt": "2020-01-16T13:33:36.749Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608484"
        },
        "nombre": "Colón",
        "descripcion": "Colón",
        "abreviacion": "Colón",
        "createdAt": "2020-01-16T13:33:36.702Z",
        "updatedAt": "2020-01-16T13:33:36.702Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084d0"
        },
        "nombre": "Fonseca",
        "descripcion": "Fonseca",
        "abreviacion": "Fonseca",
        "createdAt": "2020-01-16T13:33:36.751Z",
        "updatedAt": "2020-01-16T13:33:36.751Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608489"
        },
        "nombre": "Ancuyá",
        "descripcion": "Ancuyá",
        "abreviacion": "Ancuyá",
        "createdAt": "2020-01-16T13:33:36.705Z",
        "updatedAt": "2020-01-16T13:33:36.705Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084d5"
        },
        "nombre": "Villanueva",
        "descripcion": "Villanueva",
        "abreviacion": "Villanueva",
        "createdAt": "2020-01-16T13:33:36.754Z",
        "updatedAt": "2020-01-16T13:33:36.754Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60848e"
        },
        "nombre": "San Juanito",
        "descripcion": "San Juanito",
        "abreviacion": "San Juanito",
        "createdAt": "2020-01-16T13:33:36.709Z",
        "updatedAt": "2020-01-16T13:33:36.709Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084da"
        },
        "nombre": "Cerro San Antonio",
        "descripcion": "Cerro San Antonio",
        "abreviacion": "Cerro San Antonio",
        "createdAt": "2020-01-16T13:33:36.756Z",
        "updatedAt": "2020-01-16T13:33:36.756Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608493"
        },
        "nombre": "Puerto Lleras",
        "descripcion": "Puerto Lleras",
        "abreviacion": "Puerto Lleras",
        "createdAt": "2020-01-16T13:33:36.711Z",
        "updatedAt": "2020-01-16T13:33:36.711Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084df"
        },
        "nombre": "El Retén",
        "descripcion": "El Retén",
        "abreviacion": "El Retén",
        "createdAt": "2020-01-16T13:33:36.759Z",
        "updatedAt": "2020-01-16T13:33:36.759Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda608498"
        },
        "nombre": "Uribe",
        "descripcion": "Uribe",
        "abreviacion": "Uribe",
        "createdAt": "2020-01-16T13:33:36.714Z",
        "updatedAt": "2020-01-16T13:33:36.714Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084e4"
        },
        "nombre": "Pivijay",
        "descripcion": "Pivijay",
        "abreviacion": "Pivijay",
        "createdAt": "2020-01-16T13:33:36.761Z",
        "updatedAt": "2020-01-16T13:33:36.761Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda60849d"
        },
        "nombre": "Granada",
        "descripcion": "Granada",
        "abreviacion": "Granada",
        "createdAt": "2020-01-16T13:33:36.717Z",
        "updatedAt": "2020-01-16T13:33:36.717Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084a2"
        },
        "nombre": "Cumaral",
        "descripcion": "Cumaral",
        "abreviacion": "Cumaral",
        "createdAt": "2020-01-16T13:33:36.723Z",
        "updatedAt": "2020-01-16T13:33:36.723Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084a7"
        },
        "nombre": "Tenerife",
        "descripcion": "Tenerife",
        "abreviacion": "Tenerife",
        "createdAt": "2020-01-16T13:33:36.730Z",
        "updatedAt": "2020-01-16T13:33:36.730Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084ac"
        },
        "nombre": "Campoalegre",
        "descripcion": "Campoalegre",
        "abreviacion": "Campoalegre",
        "createdAt": "2020-01-16T13:33:36.732Z",
        "updatedAt": "2020-01-16T13:33:36.732Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084b1"
        },
        "nombre": "Guadalupe",
        "descripcion": "Guadalupe",
        "abreviacion": "Guadalupe",
        "createdAt": "2020-01-16T13:33:36.735Z",
        "updatedAt": "2020-01-16T13:33:36.735Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084b6"
        },
        "nombre": "La Plata",
        "descripcion": "La Plata",
        "abreviacion": "La Plata",
        "createdAt": "2020-01-16T13:33:36.738Z",
        "updatedAt": "2020-01-16T13:33:36.738Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084bb"
        },
        "nombre": "Palestina",
        "descripcion": "Palestina",
        "abreviacion": "Palestina",
        "createdAt": "2020-01-16T13:33:36.740Z",
        "updatedAt": "2020-01-16T13:33:36.740Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084c0"
        },
        "nombre": "Arboleda",
        "descripcion": "Arboleda",
        "abreviacion": "Arboleda",
        "createdAt": "2020-01-16T13:33:36.743Z",
        "updatedAt": "2020-01-16T13:33:36.743Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084c5"
        },
        "nombre": "Tello",
        "descripcion": "Tello",
        "abreviacion": "Tello",
        "createdAt": "2020-01-16T13:33:36.746Z",
        "updatedAt": "2020-01-16T13:33:36.746Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084ca"
        },
        "nombre": "Riohacha",
        "descripcion": "Riohacha",
        "abreviacion": "Riohacha",
        "createdAt": "2020-01-16T13:33:36.748Z",
        "updatedAt": "2020-01-16T13:33:36.748Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084cf"
        },
        "nombre": "El Molino",
        "descripcion": "El Molino",
        "abreviacion": "El Molino",
        "createdAt": "2020-01-16T13:33:36.751Z",
        "updatedAt": "2020-01-16T13:33:36.751Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084d4"
        },
        "nombre": "Urumita",
        "descripcion": "Urumita",
        "abreviacion": "Urumita",
        "createdAt": "2020-01-16T13:33:36.753Z",
        "updatedAt": "2020-01-16T13:33:36.753Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084d9"
        },
        "nombre": "Ariguaní",
        "descripcion": "Ariguaní",
        "abreviacion": "Ariguaní",
        "createdAt": "2020-01-16T13:33:36.756Z",
        "updatedAt": "2020-01-16T13:33:36.756Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084de"
        },
        "nombre": "El Piñon",
        "descripcion": "El Piñon",
        "abreviacion": "El Piñon",
        "createdAt": "2020-01-16T13:33:36.758Z",
        "updatedAt": "2020-01-16T13:33:36.758Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084e3"
        },
        "nombre": "Pedraza",
        "descripcion": "Pedraza",
        "abreviacion": "Pedraza",
        "createdAt": "2020-01-16T13:33:36.761Z",
        "updatedAt": "2020-01-16T13:33:36.761Z",
        "__v": 0
    },
    {
        "_id": {
            "$oid": "5e206630c6788b7bda6084e8"
        },
        "nombre": "San Zenón",
        "descripcion": "San Zenón",
        "abreviacion": "San Zenón",
        "createdAt": "2020-01-16T13:33:36.765Z",
        "updatedAt": "2020-01-16T13:33:36.765Z",
        "__v": 0
    }
]

function uploadcity(id){
   
   
    url = 'http://localhost:3000'

   axios.delete(`${url}/ciudad/${id}`)
    .then(( {data} ) =>{ 
        console.log('estado', data)
       return data.id
    })
    .catch(error => {
     console.log(`error en crear municipio,`,  error)
     return null
    })
    return id
}
for(i=0;i<ciudades.length;i++){
    uploadcity(ciudades[i]._id.$oid)
}
