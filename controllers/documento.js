//importar modelo
var Documento = require('../models/documento');

//funcion create
exports.documento_create = function (req, res, next) {
    var documento = new Documento(
        req.body
    );

    documento.save(function (err, documento) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: documento._id})
    })
};
//funcion read by id
exports.documento_details = function (req, res, next) {
    Documento.findById(req.params.id, function (err, documento) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(documento);
    })
};
//funcion read all
exports.documento_all = function (req, res, next) {
    Documento.find(req.params.id, function (err, documento) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(documento);
    })
};
//funcion update
exports.documento_update = function (req, res, next) {
    Documento.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, documento) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", documento: documento });
    });
};
//funcion add afiliado
exports.Documento_add_afiliado = function (req, res, next) {
Documento.findByIdAndUpdate(req.params.id, { $push: { afiliado: req.body.afiliado } }, { new: true }, function (err, Documento) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Documento: Documento });
});
};
//funcion add tipodocumentosoporte
exports.Documento_add_tipodocumentosoporte = function (req, res, next) {
Documento.findByIdAndUpdate(req.params.id, { $push: { tipodocumentosoporte: req.body.tipodocumentosoporte } }, { new: true }, function (err, Documento) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Documento: Documento });
});
};

//funcion delete
exports.documento_delete = function (req, res, next) {
    Documento.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};