//importar modelo
var Pais = require('../models/pais');

//funcion create
exports.pais_create = function (req, res, next) {
    var pais = new Pais(
        req.body
    );

    pais.save(function (err, pais) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: pais._id})
    })
};
//funcion read by id
exports.pais_details = function (req, res, next) {
    Pais.findById(req.params.id, function (err, pais) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(pais);
    })
};
//funcion read all
exports.pais_all = function (req, res, next) {
    Pais.find(req.params.id, function (err, pais) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(pais);
    })
};
//funcion update
exports.pais_update = function (req, res, next) {
    Pais.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, pais) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", pais: pais });
    });
};

//funcion delete
exports.pais_delete = function (req, res, next) {
    Pais.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};