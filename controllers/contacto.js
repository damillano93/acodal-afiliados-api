//importar modelo
var Contacto = require('../models/contacto');

//funcion create
exports.contacto_create = function (req, res, next) {
    var contacto = new Contacto(
        req.body
    );

    contacto.save(function (err, contacto) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: contacto._id})
    })
};
//funcion read by id
exports.contacto_details = function (req, res, next) {
    Contacto.findById(req.params.id, function (err, contacto) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(contacto);
    })
};
//funcion read all
exports.contacto_all = function (req, res, next) {
    Contacto.find(req.params.id, function (err, contacto) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(contacto);
    })
};
//funcion update
exports.contacto_update = function (req, res, next) {
    Contacto.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, contacto) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", contacto: contacto });
    });
};
//funcion add afiliado
exports.Contacto_add_afiliado = function (req, res, next) {
Contacto.findByIdAndUpdate(req.params.id, { $push: { afiliado: req.body.afiliado } }, { new: true }, function (err, Contacto) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Contacto: Contacto });
});
};
//funcion add tipocontacto
exports.Contacto_add_tipocontacto = function (req, res, next) {
Contacto.findByIdAndUpdate(req.params.id, { $push: { tipocontacto: req.body.tipocontacto } }, { new: true }, function (err, Contacto) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Contacto: Contacto });
});
};

//funcion delete
exports.contacto_delete = function (req, res, next) {
    Contacto.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};