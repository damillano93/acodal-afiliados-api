//importar modelo
var Factura = require('../models/factura');
const xlsx = require('node-xlsx').default;
const fs = require('fs');

//funcion create
exports.factura_create = function (req, res, next) {
    var factura = new Factura(
        req.body
    );

    factura.save(function (err, factura) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: factura._id})
    })
};
//funcion read by id
exports.factura_details = function (req, res, next) {
    Factura.findById(req.params.id, function (err, factura) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(factura);
    })
};
//funcion read all
exports.factura_all = function (req, res, next) {
    Factura.find(req.params.id, function (err, factura) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(factura);
    })
};
//funcion update
exports.factura_update = function (req, res, next) {
    Factura.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, factura) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", factura: factura });
    });
};

//funcion delete
exports.factura_delete = function (req, res, next) {
    Factura.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};

//funcion upload file
exports.factura_upload = function (req, res, next) {
    let acodalFile;
    let uploadPath;
    console.log(req.files)
    if (!req.files || Object.keys(req.files).length === 0) {
      res.status(400).send('No files were uploaded.');
      return;
    }
    acodalFile = req.files.acodalFile;
  
    uploadPath = __dirname +'/uploads/' + acodalFile.name;
  
    acodalFile.mv(uploadPath, function(err) {
      if (err) {
        res.send({status: "Err", error:err})
        return next(err);
      }
      const workSheetsFromFile = xlsx.parse(uploadPath);

      console.log(workSheetsFromFile[0].data)
      try {
        fs.unlinkSync(uploadPath)
        //file removed
      } catch(err) {
        console.error(err)
      }
      res.send(workSheetsFromFile[0].data);
    });
};


exports.factura_find_by_afiliado = function (req, res, next) {
    Factura.find({afiliado:req.params.id}, function (err, facturas) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
         
        res.send(facturas);
    })
};