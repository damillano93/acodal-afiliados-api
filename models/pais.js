//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Pais
var PaisSchema = new Schema({
    nombre: { type: String, required: true },
    ciudad: [{ type: Schema.Types.ObjectId, ref: 'Ciudad', autopopulate: true , required: false}],
},

    {
        timestamps: true
    }


);

PaisSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Pais', PaisSchema);