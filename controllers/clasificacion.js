//importar modelo
var Clasificacion = require('../models/clasificacion');

//funcion create
exports.clasificacion_create = function (req, res, next) {
    var clasificacion = new Clasificacion(
        req.body
    );

    clasificacion.save(function (err, clasificacion) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: clasificacion._id})
    })
};
//funcion read by id
exports.clasificacion_details = function (req, res, next) {
    Clasificacion.findById(req.params.id, function (err, clasificacion) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(clasificacion);
    })
};
//funcion read all
exports.clasificacion_all = function (req, res, next) {
    Clasificacion.find(req.params.id, function (err, clasificacion) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(clasificacion);
    })
};
//funcion update
exports.clasificacion_update = function (req, res, next) {
    Clasificacion.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, clasificacion) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", clasificacion: clasificacion });
    });
};

//funcion delete
exports.clasificacion_delete = function (req, res, next) {
    Clasificacion.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};