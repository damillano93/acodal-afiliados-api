// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de subafinidad
var subafinidad_controller = require('../controllers/subafinidad');

// GET /:id
router.get('/:id', subafinidad_controller.subafinidad_details);
// GET /
router.get('/', subafinidad_controller.subafinidad_all);
// POST /
router.post('/', subafinidad_controller.subafinidad_create);
// PUT /:id
router.put('/:id', subafinidad_controller.subafinidad_update);
// DELETE /:id
router.delete('/:id', subafinidad_controller.subafinidad_delete);

//export router
module.exports = router;