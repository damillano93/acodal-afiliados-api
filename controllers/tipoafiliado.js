//importar modelo
var Tipoafiliado = require('../models/tipoafiliado');

//funcion create
exports.tipoafiliado_create = function (req, res, next) {
    var tipoafiliado = new Tipoafiliado(
        req.body
    );

    tipoafiliado.save(function (err, tipoafiliado) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: tipoafiliado._id})
    })
};
//funcion read by id
exports.tipoafiliado_details = function (req, res, next) {
    Tipoafiliado.findById(req.params.id, function (err, tipoafiliado) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(tipoafiliado);
    })
};
//funcion read all
exports.tipoafiliado_all = function (req, res, next) {
    Tipoafiliado.find(req.params.id, function (err, tipoafiliado) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(tipoafiliado);
    })
};
//funcion update
exports.tipoafiliado_update = function (req, res, next) {
    Tipoafiliado.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, tipoafiliado) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", tipoafiliado: tipoafiliado });
    });
};

//funcion delete
exports.tipoafiliado_delete = function (req, res, next) {
    Tipoafiliado.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};