// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de tipoempresa
var tipoempresa_controller = require('../controllers/tipoempresa');

// GET /:id
router.get('/:id', tipoempresa_controller.tipoempresa_details);
// GET /
router.get('/', tipoempresa_controller.tipoempresa_all);
// POST /
router.post('/', tipoempresa_controller.tipoempresa_create);
// PUT /:id
router.put('/:id', tipoempresa_controller.tipoempresa_update);
// DELETE /:id
router.delete('/:id', tipoempresa_controller.tipoempresa_delete);

//export router
module.exports = router;