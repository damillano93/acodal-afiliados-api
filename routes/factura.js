// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de factura
var factura_controller = require('../controllers/factura');

// GET /:id
router.get('/:id', factura_controller.factura_details);
// GET /
router.get('/', factura_controller.factura_all);
// POST /
router.post('/', factura_controller.factura_create);
// PUT /:id
router.put('/:id', factura_controller.factura_update);

// GET facturas by afiliado/:id 
router.get('/afiliado/:id', factura_controller.factura_find_by_afiliado);

// DELETE /:id
router.delete('/:id', factura_controller.factura_delete);

router.post('/upload', factura_controller.factura_upload);
//export router
module.exports = router;