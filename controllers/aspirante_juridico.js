//importar modelo
var Aspirante_juridico = require('../models/aspirante_juridico');

//funcion create
exports.aspirante_juridico_create = function (req, res, next) {
    var aspirante_juridico = new Aspirante_juridico(
        req.body
    );

    aspirante_juridico.save(function (err, aspirante_juridico) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: aspirante_juridico._id})
    })
};
//funcion read by id
exports.aspirante_juridico_details = function (req, res, next) {
    Aspirante_juridico.findById(req.params.id, function (err, aspirante_juridico) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(aspirante_juridico);
    })
};
//funcion read all
exports.aspirante_juridico_all = function (req, res, next) {
    Aspirante_juridico.find(req.params.id, function (err, aspirante_juridico) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(aspirante_juridico);
    })
};
//funcion update
exports.aspirante_juridico_update = function (req, res, next) {
    Aspirante_juridico.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, aspirante_juridico) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", aspirante_juridico: aspirante_juridico });
    });
};
//funcion add ciudad
exports.Aspirante_juridico_add_ciudad = function (req, res, next) {
Aspirante_juridico.findByIdAndUpdate(req.params.id, { $push: { ciudad: req.body.ciudad } }, { new: true }, function (err, Aspirante_juridico) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Aspirante_juridico: Aspirante_juridico });
});
};
//funcion add seccional
exports.Aspirante_juridico_add_seccional = function (req, res, next) {
Aspirante_juridico.findByIdAndUpdate(req.params.id, { $push: { seccional: req.body.seccional } }, { new: true }, function (err, Aspirante_juridico) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Aspirante_juridico: Aspirante_juridico });
});
};
//funcion add tipo_empresa
exports.Aspirante_juridico_add_tipo_empresa = function (req, res, next) {
Aspirante_juridico.findByIdAndUpdate(req.params.id, { $push: { tipo_empresa: req.body.tipo_empresa } }, { new: true }, function (err, Aspirante_juridico) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Aspirante_juridico: Aspirante_juridico });
});
};

//funcion delete
exports.aspirante_juridico_delete = function (req, res, next) {
    Aspirante_juridico.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};