// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de tipoafiliado
var tipoafiliado_controller = require('../controllers/tipoafiliado');

// GET /:id
router.get('/:id', tipoafiliado_controller.tipoafiliado_details);
// GET /
router.get('/', tipoafiliado_controller.tipoafiliado_all);
// POST /
router.post('/', tipoafiliado_controller.tipoafiliado_create);
// PUT /:id
router.put('/:id', tipoafiliado_controller.tipoafiliado_update);
// DELETE /:id
router.delete('/:id', tipoafiliado_controller.tipoafiliado_delete);

//export router
module.exports = router;