//importar modelo
var Tipocontacto = require('../models/tipocontacto');

//funcion create
exports.tipocontacto_create = function (req, res, next) {
    var tipocontacto = new Tipocontacto(
        req.body
    );

    tipocontacto.save(function (err, tipocontacto) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: tipocontacto._id})
    })
};
//funcion read by id
exports.tipocontacto_details = function (req, res, next) {
    Tipocontacto.findById(req.params.id, function (err, tipocontacto) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(tipocontacto);
    })
};
//funcion read all
exports.tipocontacto_all = function (req, res, next) {
    Tipocontacto.find(req.params.id, function (err, tipocontacto) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(tipocontacto);
    })
};
//funcion update
exports.tipocontacto_update = function (req, res, next) {
    Tipocontacto.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, tipocontacto) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", tipocontacto: tipocontacto });
    });
};

//funcion delete
exports.tipocontacto_delete = function (req, res, next) {
    Tipocontacto.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};