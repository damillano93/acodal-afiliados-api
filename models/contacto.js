//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Contacto
var ContactoSchema = new Schema({
afiliado:[ {type:Schema.Types.ObjectId, ref: 'Afiliado' , autopopulate: true }], 
tipocontacto:[ {type:Schema.Types.ObjectId, ref: 'Tipocontacto' , autopopulate: true }], 
valor: {type:String, required:true}, 

});

ContactoSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Contacto', ContactoSchema);