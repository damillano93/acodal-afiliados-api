//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Tipodocumentosoporte
var TipodocumentosoporteSchema = new Schema({
nombre: {type:String, required:true}, 
tipo: {type:String, required:true}, 

});

TipodocumentosoporteSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Tipodocumentosoporte', TipodocumentosoporteSchema);