//importar modelo
var Subafinidad = require('../models/subafinidad');

//funcion create
exports.subafinidad_create = function (req, res, next) {
    var subafinidad = new Subafinidad(
        req.body
    );

    subafinidad.save(function (err, subafinidad) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: subafinidad._id})
    })
};
//funcion read by id
exports.subafinidad_details = function (req, res, next) {
    Subafinidad.findById(req.params.id, function (err, subafinidad) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(subafinidad);
    })
};
//funcion read all
exports.subafinidad_all = function (req, res, next) {
    Subafinidad.find(req.params.id, function (err, subafinidad) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(subafinidad);
    })
};
//funcion update
exports.subafinidad_update = function (req, res, next) {
    Subafinidad.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, subafinidad) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", subafinidad: subafinidad });
    });
};

//funcion delete
exports.subafinidad_delete = function (req, res, next) {
    Subafinidad.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};