//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Subafinidad
var SubafinidadSchema = new Schema({
nombre: {type:String, required:true}, 
descripcion: {type:String, required:true}, 
abreviacion: {type:String, required:true}, 

});

SubafinidadSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Subafinidad', SubafinidadSchema);