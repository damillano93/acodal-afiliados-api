// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de tipocontacto
var tipocontacto_controller = require('../controllers/tipocontacto');

// GET /:id
router.get('/:id', tipocontacto_controller.tipocontacto_details);
// GET /
router.get('/', tipocontacto_controller.tipocontacto_all);
// POST /
router.post('/', tipocontacto_controller.tipocontacto_create);
// PUT /:id
router.put('/:id', tipocontacto_controller.tipocontacto_update);
// DELETE /:id
router.delete('/:id', tipocontacto_controller.tipocontacto_delete);

//export router
module.exports = router;