//importar modelo
var Seccional = require('../models/seccional');

//funcion create
exports.seccional_create = function (req, res, next) {
    var seccional = new Seccional(
        req.body
    );

    seccional.save(function (err, seccional) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: seccional._id})
    })
};
//funcion read by id
exports.seccional_details = function (req, res, next) {
    Seccional.findById(req.params.id, function (err, seccional) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(seccional);
    })
};
//funcion read all
exports.seccional_all = function (req, res, next) {
    Seccional.find(req.params.id, function (err, seccional) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(seccional);
    })
};
//funcion update
exports.seccional_update = function (req, res, next) {
    Seccional.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, seccional) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", seccional: seccional });
    });
};

//funcion delete
exports.seccional_delete = function (req, res, next) {
    Seccional.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};