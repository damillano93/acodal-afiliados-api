var _ = require('underscore'),
	AWS = require('aws-sdk'),
	fs = require('fs'),
	path = require('path'),
	flow = require('flow');
// importar controlador de documento

AWS.config = new AWS.Config();
AWS.config.accessKeyId = process.env.ACCESS_KEY_ID;
AWS.config.secretAccessKey = process.env.SECRET_ACCESS_KEY;
AWS.config.region = "us-east-1";

let url = ''
exports.s3 = function (req, res, next) {
	var s3 = new AWS.S3(),
		file = req.file,
		result = {
			error: 0,
			uploaded: []
		};

	flow.exec(
		function () { // Read temp File
			fs.readFile(file.path, this);
		},
		function (err, data) { // Upload file to S3
			const fecha = Date.now();
			const name = file.originalname.split(' ').join('-');
			url = `${fecha}${name}`

			if (url.endsWith('pdf')) {

				s3.putObject({
					Bucket: 'acodal-files', //Bucket Name
					Key: url, //Upload File Name, Default the original name
					Body: data,
					ACL: 'public-read',
					ContentType : 'application/pdf',
					ContentDisposition: 'inline',
				}, this);
			} else {
				s3.putObject({
					Bucket: 'acodal-files', //Bucket Name
					Key: url, //Upload File Name, Default the original name
					Body: data,
					ACL: 'public-read'
				}, this);

			}


		},
		function (err, data) { //Upload Callback
			if (err) {
				console.error('Error : ' + err);
				result.error++;
			}
			result.uploaded.push(data.ETag);
			this();
		},
		function () {
			res.send({ status: 'ok', file: `https://acodal-files.s3.amazonaws.com/${url}` });
		});
};
