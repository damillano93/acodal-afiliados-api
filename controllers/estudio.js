//importar modelo
var Estudio = require('../models/estudio');

//funcion create
exports.estudio_create = function (req, res, next) {
    var estudio = new Estudio(
        req.body
    );

    estudio.save(function (err, estudio) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send(estudio)
    })
};
//funcion read by id
exports.estudio_details = function (req, res, next) {
    Estudio.findById(req.params.id, function (err, estudio) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(estudio);
    })
};
//funcion read all
exports.estudio_all = function (req, res, next) {
    Estudio.find(req.params.id, function (err, estudio) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(estudio);
    })
};
//funcion update
exports.estudio_update = function (req, res, next) {
    Estudio.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, estudio) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", estudio: estudio });
    });
};

//funcion delete
exports.estudio_delete = function (req, res, next) {
    Estudio.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};