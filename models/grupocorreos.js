//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Grupocorreos
var GrupocorreosSchema = new Schema({
nombre: {type:String, required:true}, 
descripcion: {type:String, required:true}, 
abreviacion: {type:String, required:true}, 

});

GrupocorreosSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Grupocorreos', GrupocorreosSchema);