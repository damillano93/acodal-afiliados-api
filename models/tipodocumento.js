//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Tipodocumento
var TipodocumentoSchema = new Schema({
nombre: {type:String, required:true}, 
descripcion: {type:String, required:true}, 
abreviacion: {type:String, required:true}, 

});

TipodocumentoSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Tipodocumento', TipodocumentoSchema);