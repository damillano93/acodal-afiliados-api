//importar modelo
var Tipo_documento = require('../models/tipo_documento');

//funcion create
exports.tipo_documento_create = function (req, res, next) {
    var tipo_documento = new Tipo_documento(
        req.body
    );

    tipo_documento.save(function (err, tipo_documento) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: tipo_documento._id})
    })
};
//funcion read by id
exports.tipo_documento_details = function (req, res, next) {
    Tipo_documento.findById(req.params.id, function (err, tipo_documento) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(tipo_documento);
    })
};
//funcion read all
exports.tipo_documento_all = function (req, res, next) {
    Tipo_documento.find(req.params.id, function (err, tipo_documento) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(tipo_documento);
    })
};
//funcion update
exports.tipo_documento_update = function (req, res, next) {
    Tipo_documento.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, tipo_documento) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", tipo_documento: tipo_documento });
    });
};

//funcion delete
exports.tipo_documento_delete = function (req, res, next) {
    Tipo_documento.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};