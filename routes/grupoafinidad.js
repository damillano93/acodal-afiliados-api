// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de grupoafinidad
var grupoafinidad_controller = require('../controllers/grupoafinidad');

// GET /:id
router.get('/:id', grupoafinidad_controller.grupoafinidad_details);
// GET /
router.get('/', grupoafinidad_controller.grupoafinidad_all);
// POST /
router.post('/', grupoafinidad_controller.grupoafinidad_create);
// PUT /:id
router.put('/:id', grupoafinidad_controller.grupoafinidad_update);
// DELETE /:id
router.delete('/:id', grupoafinidad_controller.grupoafinidad_delete);

//export router
module.exports = router;