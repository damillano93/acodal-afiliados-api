// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de seccional
var seccional_controller = require('../controllers/seccional');

// GET /:id
router.get('/:id', seccional_controller.seccional_details);
// GET /
router.get('/', seccional_controller.seccional_all);
// POST /
router.post('/', seccional_controller.seccional_create);
// PUT /:id
router.put('/:id', seccional_controller.seccional_update);
// DELETE /:id
router.delete('/:id', seccional_controller.seccional_delete);

//export router
module.exports = router;